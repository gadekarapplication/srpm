package com.salesmanagement.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.salesmanagement.Model.TrackingResponse;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SRPMDatabase extends SQLiteOpenHelper {

    // Database Name
    public final static String DB_NAME = "srpm";
    //Table Name Location Tracking
    public final static String LOCATION_TABLE_NAME = "location_tracking";
    // Database Version
    private static final int DATABASE_VERSION = 1;
    //All Create Table Query
    public static String TRACKING_QUERY = "CREATE TABLE IF NOT EXISTS " + LOCATION_TABLE_NAME + "(tracking_id INTEGER PRIMARY KEY AUTOINCREMENT, userId VARCHAR, latitude VARCHAR, longitude VARCHAR, tracking_date VARCHAR, tracking_status TINYINT)";
    //All Drop Table Query
    public static String TRACKING_DROP = "DROP TABLE IF EXISTS " + LOCATION_TABLE_NAME;

    //Constructor of class
    public SRPMDatabase(Context context) {
        super(context, DB_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TRACKING_QUERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(TRACKING_DROP);
        onCreate(db);
    }

    //To insert location into db
    public void insertData(double latitude, double longitude, String userId, int status) {

        String date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(new Date());

        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("latitude", "" + latitude);
            contentValues.put("longitude", "" + longitude);
            contentValues.put("userId", "" + userId);
            contentValues.put("tracking_date", "" + date);
            contentValues.put("tracking_status", status);

            db.insert(LOCATION_TABLE_NAME, null, contentValues);
            Log.e("Tracking", "Insert Successfully" + contentValues);

            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
    }


    public List<TrackingResponse> getAllLocationList() {

        List<TrackingResponse> trackingResponseList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + LOCATION_TABLE_NAME, null);
        TrackingResponse trackingResponse;
        while (cursor.moveToNext()) {

            trackingResponse = new TrackingResponse();
            trackingResponse.setTrackingId(cursor.getString(cursor.getColumnIndex("tracking_id")));
            trackingResponse.setLatitude(cursor.getString(cursor.getColumnIndex("latitude")));
            trackingResponse.setLongitude(cursor.getString(cursor.getColumnIndex("longitude")));
            trackingResponse.setEmployeeId(cursor.getString(cursor.getColumnIndex("userId")));
            trackingResponse.setTrackingDate(cursor.getString(cursor.getColumnIndex("tracking_date")));
            trackingResponse.setTrackingStatus(cursor.getString(cursor.getColumnIndex("tracking_status")));
            trackingResponseList.add(trackingResponse);

        }

        Log.e("trackingResponseList", "" + trackingResponseList.size());

        db.close();
        return trackingResponseList;

    }

    public Cursor getUnSyncedLocation(int status) {

        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * FROM " + LOCATION_TABLE_NAME + " WHERE tracking_status = '" + status + "' ";
        Cursor cursor = db.rawQuery(sql, null);

        return cursor;

    }

    public boolean updateLocationStatus(String trackingId, int status) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("tracking_status", status);
        db.update(LOCATION_TABLE_NAME, contentValues, "tracking_id=?", new String[]{"" + trackingId});
        Log.e("locationStatusUpdate", "Update Successfully" + contentValues);
        db.close();

        return true;
    }
}
