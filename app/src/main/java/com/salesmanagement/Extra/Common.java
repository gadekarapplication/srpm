package com.salesmanagement.Extra;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;

import com.loopj.android.http.AsyncHttpClient;
import com.salesmanagement.Model.AreaResponse;
import com.salesmanagement.Model.CityResponse;
import com.salesmanagement.Model.CountryResponse;
import com.salesmanagement.Model.DistrictResponse;
import com.salesmanagement.Model.PinCodeResponse;
import com.salesmanagement.Model.StateResponse;
import com.salesmanagement.Model.TalukaResponse;
import com.salesmanagement.Retrofit.Api;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Prasad
 */

public class Common {

    public static final String SHARED_PREF = "userData";
    public static AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
    public static ProgressDialog progressDialog;

    public static int emp_id;

    public static List<CountryResponse> countryResponseList = new ArrayList<>();
    public static List<StateResponse> stateResponseList = new ArrayList<>();
    public static List<DistrictResponse> districtResponseList = new ArrayList<>();
    public static List<TalukaResponse> talukaResponseList = new ArrayList<>();
    public static List<CityResponse> cityResponseList = new ArrayList<>();
    public static List<PinCodeResponse> pinCodeResponseList = new ArrayList<>();
    public static List<AreaResponse> areaResponseList = new ArrayList<>();

    public static void saveUserData(Context context, String key, String value) {

        try {

            SharedPreferences pref = context.getSharedPreferences(SHARED_PREF, 0);
            SharedPreferences.Editor editor = pref.edit();
            editor.putString(key, value);
            editor.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void saveIntData(Context context, String key, int value) {

        try {

            SharedPreferences pref = context.getSharedPreferences(SHARED_PREF, 0);
            SharedPreferences.Editor editor = pref.edit();
            editor.putInt(key, value);
            editor.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void saveEmployeeData(Context context, Integer employeeId, String employeeName, String employeeQualification, String employeeMobileNo,
                                        String employeeAlternateMobileNo, String employeePassword, String employeeEmail,
                                        String employeeWorkingArea, String employeeStatus, Integer roleId,
                                        String roleName) {

        try {

            SharedPreferences pref = context.getSharedPreferences(SHARED_PREF, 0);
            SharedPreferences.Editor editor = pref.edit();
            editor.putInt("employeeId", employeeId);
            editor.putString("employeeName", employeeName);
            editor.putString("employeeQualification", employeeQualification);
            editor.putString("employeeMobileNo", employeeMobileNo);
            editor.putString("employeeAlternateMobileNo", employeeAlternateMobileNo);
            editor.putString("employeePassword", employeePassword);
            editor.putString("employeeEmail", employeeEmail);
            editor.putString("employeeWorkingArea", employeeWorkingArea);
            editor.putString("employeeStatus", employeeStatus);
            editor.putInt("roleId", roleId);
            editor.putString("roleName", roleName);
            editor.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static String getSavedUserData(Context context, String key) {
        SharedPreferences pref = context.getSharedPreferences(SHARED_PREF, 0);
        return pref.getString(key, "");

    }

    public static int getSavedIntData(Context context, String key) {
        SharedPreferences pref = context.getSharedPreferences(SHARED_PREF, 0);
        return pref.getInt(key, 0);

    }

    public static void callToGetCountry() {

        Call<List<CountryResponse>> call = Api.getClient().getCountry();
        call.enqueue(new Callback<List<CountryResponse>>() {
            @Override
            public void onResponse(Call<List<CountryResponse>> call, Response<List<CountryResponse>> response) {
                countryResponseList.clear();
                countryResponseList = response.body();


            }

            @Override
            public void onFailure(Call<List<CountryResponse>> call, Throwable t) {


            }
        });


    }

    public static void callToGetState(int countryId) {

        Call<List<StateResponse>> call = Api.getClient().getStateByCountry(countryId);
        call.enqueue(new Callback<List<StateResponse>>() {
            @Override
            public void onResponse(Call<List<StateResponse>> call, Response<List<StateResponse>> response) {
                stateResponseList.clear();
                stateResponseList = response.body();


            }

            @Override
            public void onFailure(Call<List<StateResponse>> call, Throwable t) {


            }
        });


    }

    public static void callToGetDistrict(int stateId) {

        Call<List<DistrictResponse>> call = Api.getClient().getDistrictByState(stateId);
        call.enqueue(new Callback<List<DistrictResponse>>() {
            @Override
            public void onResponse(Call<List<DistrictResponse>> call, Response<List<DistrictResponse>> response) {
                districtResponseList.clear();
                districtResponseList = response.body();


            }

            @Override
            public void onFailure(Call<List<DistrictResponse>> call, Throwable t) {


            }
        });
    }

    public static void callToGetTaluka(int districtId) {
        Call<List<TalukaResponse>> call = Api.getClient().getTalukaByDistrict(districtId);
        call.enqueue(new Callback<List<TalukaResponse>>() {
            @Override
            public void onResponse(Call<List<TalukaResponse>> call, Response<List<TalukaResponse>> response) {
                talukaResponseList.clear();
                talukaResponseList = response.body();


            }

            @Override
            public void onFailure(Call<List<TalukaResponse>> call, Throwable t) {


            }
        });


    }

    public static void callToGetCity(int talukaId) {
        Call<List<CityResponse>> call = Api.getClient().getCityByTaluka(talukaId);
        call.enqueue(new Callback<List<CityResponse>>() {
            @Override
            public void onResponse(Call<List<CityResponse>> call, Response<List<CityResponse>> response) {
                cityResponseList.clear();
                cityResponseList = response.body();


            }

            @Override
            public void onFailure(Call<List<CityResponse>> call, Throwable t) {


            }
        });

    }

    public static void callToGetPinCode(int cityId) {
        Call<List<PinCodeResponse>> call = Api.getClient().getPinByCity(cityId);
        call.enqueue(new Callback<List<PinCodeResponse>>() {
            @Override
            public void onResponse(Call<List<PinCodeResponse>> call, Response<List<PinCodeResponse>> response) {

                pinCodeResponseList.clear();
                pinCodeResponseList = response.body();

            }

            @Override
            public void onFailure(Call<List<PinCodeResponse>> call, Throwable t) {


            }
        });

    }

    public static void callToGetArea(int pinCodeId) {
        Call<List<AreaResponse>> call = Api.getClient().getAreaByPinCode(pinCodeId);
        call.enqueue(new Callback<List<AreaResponse>>() {
            @Override
            public void onResponse(Call<List<AreaResponse>> call, Response<List<AreaResponse>> response) {
                areaResponseList.clear();
                areaResponseList = response.body();


            }

            @Override
            public void onFailure(Call<List<AreaResponse>> call, Throwable t) {


            }
        });

    }

}
