package com.salesmanagement.Extra;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;

import androidx.annotation.ColorRes;
import androidx.core.content.ContextCompat;

import com.applandeo.materialcalendarview.CalendarUtils;
import com.applandeo.materialcalendarview.utils.DayColorsUtils;
import com.salesmanagement.R;


/**
 * Created by Prasad Sawant.
 */

public final class DrawableUtils {

    private DrawableUtils() {
    }

    public static Drawable getCircleDrawableWithText(Context context, String string) {
        Drawable background = ContextCompat.getDrawable(context, R.drawable.sample_circle);
        Drawable text = CalendarUtils.getDrawableText(context, string, null, android.R.color.white, 12);

        Drawable[] layers = {background, text};
        return new LayerDrawable(layers);
    }

    public static Drawable getCircleDrawable(Context context, String string) {
        Drawable background = ContextCompat.getDrawable(context, R.drawable.sample_red_circle);
        Drawable text = CalendarUtils.getDrawableText(context, string, null, android.R.color.white, 8);

        Drawable[] layers = {background, text};
        return new LayerDrawable(layers);
    }


    public static Drawable getDayCircle(Context context,  int borderColor, int fillColor) {
        GradientDrawable drawable = (GradientDrawable) ContextCompat.getDrawable(context, R.drawable.sample_red_circle);
        drawable.setStroke(10,  borderColor);
        drawable.setColor(fillColor);
        return drawable;
    }
}
