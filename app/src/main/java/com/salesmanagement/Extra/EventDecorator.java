package com.salesmanagement.Extra;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;

import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.salesmanagement.R;

import java.util.Calendar;

public class EventDecorator implements DayViewDecorator {

    private  Drawable drawable;
    private final CalendarDay day;
    private final int color;
    private String title;

    public EventDecorator(MaterialCalendarView materialCalendarView, Calendar day, int color,String title) {
        System.out.println("Title:"+title);
        this.title=title;
        if(this.title.equals("Absent")){
            drawable = createAbsentDrawable(materialCalendarView.getContext());
        }else if(this.title.equals("Present")){
            drawable = createPresentDrawable(materialCalendarView.getContext());
        }

        this.day = CalendarDay.from(day) ;
        this.color = color;
    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        if (this.day.equals(day)) {
            return true;
        }
        return false;
    }

    @Override
    public void decorate(DayViewFacade view) {
        view.setSelectionDrawable(drawable);
    }

    private  Drawable createTintedDrawable(Context context, int color) {

        if(title.equals("Absent")){
            return applyTint(createAbsentDrawable(context), color);
        }else
        return applyTint(createPresentDrawable(context), color);
    }

    private static Drawable applyTint(Drawable drawable, int color) {
        Drawable wrappedDrawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(wrappedDrawable, color);
        return wrappedDrawable;
    }

    private static Drawable createAbsentDrawable(Context context) {
        return ContextCompat.getDrawable(context, R.drawable.circle_background_red);
    }
    private static Drawable createPresentDrawable(Context context) {
       Drawable drawable=   ContextCompat.getDrawable(context, R.drawable.circle_background_green);
       drawable.setBounds(0,0,24,24);
       return drawable;
    }
}
