package com.salesmanagement.Retrofit;

import com.salesmanagement.Fragment.ChequeEmployeCompanyReqModel;
import com.salesmanagement.Fragment.LeaveResponseModel;
import com.salesmanagement.Model.LeaveformRequest;
import com.salesmanagement.Model.AreaResponse;
import com.salesmanagement.Model.AttendanceReqModel;
import com.salesmanagement.Model.AttendanceResponse;
import com.salesmanagement.Model.BlankVisitRequest;
import com.salesmanagement.Model.BlankVisitResponse;
import com.salesmanagement.Model.CartRequest;
import com.salesmanagement.Model.CartResponse;
import com.salesmanagement.Model.CategoryResponse;
import com.salesmanagement.Model.ChequeRequestModel;
import com.salesmanagement.Model.ChequeResponseModel;
import com.salesmanagement.Model.CityResponse;
import com.salesmanagement.Model.CollectionAndSaleResponse;
import com.salesmanagement.Model.CollectionRequest;
import com.salesmanagement.Model.CollectionsResponse;
import com.salesmanagement.Model.CompanyResponse;
import com.salesmanagement.Model.CountryResponse;
import com.salesmanagement.Model.DealerAddressModel;
import com.salesmanagement.Model.DealerAddressResponseModel;
import com.salesmanagement.Model.DealerListResponse;
import com.salesmanagement.Model.DealerRegistrationRequest;
import com.salesmanagement.Model.DealerResponse;
import com.salesmanagement.Model.DistrictResponse;
import com.salesmanagement.Model.EmpAttendanceModel;
import com.salesmanagement.Model.EmployeeLoginRequest;
import com.salesmanagement.Model.EmployeeLoginResponse;
import com.salesmanagement.Model.EmployeeResponse;
import com.salesmanagement.Model.ExpenseRequest;
import com.salesmanagement.Model.ExpenseResponse;
import com.salesmanagement.Model.FarmerRequestModel;
import com.salesmanagement.Model.FarmerResponse;
import com.salesmanagement.Model.FarmerVisiterRequest;
import com.salesmanagement.Model.GetPunchInResponse;
import com.salesmanagement.Model.MonthlyAttendanceResponseModel;
import com.salesmanagement.Model.OrderResponse;
import com.salesmanagement.Model.PinCodeResponse;
import com.salesmanagement.Model.ProductResponse;
import com.salesmanagement.Model.SaleTargetRequest;
import com.salesmanagement.Model.SizeWiseProduct;
import com.salesmanagement.Model.StateResponse;
import com.salesmanagement.Model.SubCategoryResponse;
import com.salesmanagement.Model.TalukaResponse;
import com.salesmanagement.Model.TargetCountResponse;
import com.salesmanagement.Model.TrackingResponse;
import com.salesmanagement.Model.UploadImgResponse;
import com.salesmanagement.Model.UserOrderResponse;
import com.salesmanagement.Model.VisitorReportRequest;
import com.salesmanagement.Model.VisitorReportResponse;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;


public interface ApiInterface {

    @GET("categorymaster/getCategoryByCompanyId/{compId}")
    Call<List<CategoryResponse>> getCategoryList(@Path("compId") String compId);


    @GET("subcategorymaster/getSubCategoryListByCategoryId/{categoryId}")
    Call<List<SubCategoryResponse>> getSubCategoryList(@Path("categoryId") String categoryId);


    @POST("farmermaster")
    Call<Boolean> saveFarmer(@Body FarmerRequestModel farmerRequestModel);

    @PUT("farmermaster")
    Call<Boolean> updateFarmer(@Body FarmerRequestModel farmerRequestModel);


    @POST("employeemaster/employeeLogin")
    Call<EmployeeLoginResponse> employeeLogin(@Body EmployeeLoginRequest employeeLoginRequest);


    @GET("productmaster/getProductMasterByCategoryMasterId/{categoryId}")
    Call<List<ProductResponse>> getProductList(@Path("categoryId") String subCategoryId);


    @POST("farmervisitmaster")
    Call<Boolean> saveFarmerVisiter(@Body FarmerVisiterRequest farmerVisiterRequest);


    @Multipart
    @POST("/uploadFile")
    Call<UploadImgResponse> uploadImg(@Part MultipartBody.Part file);


    @GET("dealershipmaster/active")
    Call<List<DealerListResponse>> getActiveDealer();


    @GET("farmermaster/getFarmerMasterByEmployeeId/{employeeId}")
    Call<List<FarmerRequestModel>> getFarmerList(@Path("employeeId") int employeeId);


    @GET("productmaster/editProductMasterDetails/{productId}")
    Call<ProductResponse> getProductDetails(@Path("productId") String productId);


    @POST("cart")
    Call<Boolean> addToCart(@Body CartResponse cartResponse);


    @POST("dealershipmaster")
    Call<Boolean> addDealer(@Body DealerRegistrationRequest dealerRegistrationRequest);


    @GET("companymaster/getActiveList")
    Call<List<CompanyResponse>> getCompanyList();


    @POST("chequedetailmaster")
    Call<Boolean> saveChequeDetail(@Body ChequeRequestModel chequeRequestModel);

    @PUT("chequedetailmaster")
    Call<Boolean> updateChequeDetail(@Body ChequeRequestModel chequeRequestModel);


    @GET("area/getActiveAreaAndPincodeIdList/{pincodeId}")
    Call<List<AreaResponse>> getAreaByPinCode(@Path("pincodeId") Integer pincodeId);


    @GET("pincode/getActivePincodeAndCityIdList/{cityId}")
    Call<List<PinCodeResponse>> getPinByCity(@Path("cityId") Integer cityId);


    @GET("city/getByTalukaId/{talukaId}")
    Call<List<CityResponse>> getCityByTaluka(@Path("talukaId") Integer talukaId);


    @GET("talukaMaster/findByDistrictIdAndActiveTalukaStatus/{districtId}")
    Call<List<TalukaResponse>> getTalukaByDistrict(@Path("districtId") Integer districtId);


    @GET("districtmaster/findByStateIdAndActiveDistrictStatus/{stateId}")
    Call<List<DistrictResponse>> getDistrictByState(@Path("stateId") Integer stateId);


    @GET("statemaster/findStateByCCountryWise/{countryId}")
    Call<List<StateResponse>> getStateByCountry(@Path("countryId") Integer countryId);


    @GET("countrymaster/active")
    Call<List<CountryResponse>> getCountry();

    @GET("dealeraddressmaster/getAllDealerAddressByDealerId/{dealerId}")
    Call<List<DealerAddressResponseModel>> getDealerAddress(@Path("dealerId") int dealerId);


    @GET("dealershipmaster/active")
    Call<List<DealerResponse>> getDealerList();

    /*@GET("dealershipmaster/getEmployeewiseDealerList/{employeeId}")
    Call<List<DealerResponse>> getDealerByEmp(@Path("employeeId")Integer employeeId);*/


    @GET("cart/getAllCartDetailsByDealerIdAndCompanyI/{dealershipId}/{companyId}")
    Call<List<CartResponse>> getCartList(@Path("dealershipId") String dealerId,
                                         @Path("companyId") String companyId);


    @GET("productmaster/getSizeMasterByProductMasterId/{productId}")
    Call<List<SizeWiseProduct>> getSizeWiseProductList(@Path("productId") String productId);


    @POST("order")
    Call<Boolean> postOrder(@Body OrderResponse orderResponse);


    @GET("farmermaster/getAllFarmerMasterList")
    Call<List<FarmerResponse>> getAllFarmerList();

    @POST("dealeraddressmaster")
    Call<Boolean> addDealerAddress(@Body DealerAddressModel dealerAddressModel);
    @GET("dealeraddressmaster/editDealerAddressById/{dealerAddressId}")
    Call<DealerAddressResponseModel> getDealerAddressById(@Path("dealerAddressId") int dealerAddressId );

    @PUT("dealershipmaster")
    Call<Boolean> updateDealer(@Body DealerRegistrationRequest dealerRegistrationRequest);

    @GET("area/getByAreaId/{areaId}")
    Call<AreaResponse> getAddressByAreaId(@Path("areaId") Integer areaId);

    @PUT("dealeraddressmaster")
    Call<Boolean> updateDealerAddress(@Body DealerAddressModel dealerAddressModel);

    @GET("farmervisitmaster/getFarmerVisitMasterByEmployeeId/{employeeId}")
    Call<List<FarmerVisiterRequest>> getFarmerVisiterList(@Path("employeeId") int employeeId);

    @POST("chequedetailmaster/getAllChequeDetailsByEmployeeIdCompId")
    Call<List<ChequeResponseModel>> getChequeList(@Body ChequeEmployeCompanyReqModel chequeEmployeCompanyReqModel);

    @GET("chequedetailmaster/getByChequeDetailMasterId/{chequeDetailId}")
    Call<ChequeResponseModel> getChequeDetail(@Path("chequeDetailId") int chequeDetailId);

    @POST("totalCollectionTransaction")
    Call<Boolean> addCollection(@Body CollectionRequest collectionRequest);

    @PUT("employeeAttendance/updatePunchInEmployeeAttendanceMaster")
    Call<Boolean> punchIn(@Body AttendanceResponse attendanceResponse);

    @PUT("employeeAttendance/updatePunchOutEmployeeAttendanceMaster")
    Call<Boolean> punchOut(@Body AttendanceResponse attendanceResponse);

    @GET("employeeAttendance/employeePunchInCheck/{employeeId}")
    Call<GetPunchInResponse> getPunchInStatus(@Path("employeeId") String userId);

    @POST("tracking")
    Call<Boolean> saveLocation(@Body TrackingResponse trackingResponse);

    @POST("totalCollectionTransaction/CompanyIdAndEmpIdWiseSaleDetail")
    Call<List<CollectionsResponse>> getCollections(@Body CollectionRequest collectionRequest);

    @GET("farmervisitmaster/editFarmerVisitMasterDetails/{farmerVisitId}")
    Call<FarmerVisiterRequest> getFarmerVisitDetail(@Path("farmerVisitId") int farmerVisitId);

    @GET("order/getOrderListByEmployeeId/{employeeId}")
    Call<List<UserOrderResponse>> getOrderList(@Path("employeeId") String userId);

    @PUT("cart")
    Call<Boolean> updateCart(@Body CartRequest cartRequest );



    @POST("report/totalVisitedTargetCount")
    Call<VisitorReportResponse> getVisitorReport(@Body VisitorReportRequest visitorReportRequest);

    @POST("employeeAttendance/getMonthwiseEmpAttendance")
    Call<EmpAttendanceModel> getMonthlyAttendanceList(@Body AttendanceReqModel attendanceReqModel);


    @POST("report/getTotalTargetReport")
    Call<CollectionAndSaleResponse> getSaleReport(@Body SaleTargetRequest saleTargetRequest);


    @GET("report/totalTargetReportCount/{employeeId}")
    Call<TargetCountResponse> getTargetResponseCount(@Path("employeeId") int employeeId);

    @DELETE("/cart/deleteCart/{cartId}")
    Call<Boolean> deleteCartItem(@Path("cartId")int cartId);

    @GET("expenseMaster/getListByEmployeeId/{employeeId}")
    Call<List<ExpenseResponse>> getExpense(@Path("employeeId") Integer employeeId);

    @POST("expenseMaster")
    Call<Boolean> addExpense(@Body ExpenseRequest expenseRequest);

    @PUT("expenseMaster")
    Call<Boolean> updateExpense(@Body ExpenseRequest expenseRequest);

    @GET("employeemaster/getChildEmployeeListByEmployeeId/{employeeId}")
    Call<List<EmployeeResponse>> getEmployees(@Path("employeeId") Integer employeeId);

    @POST("blankVisit")
    Call<Boolean> addBlankVisit(@Body BlankVisitRequest blankVisitRequest);


    @GET("blankVisit/getEmployeeWise/{employeeId}")
    Call<List<BlankVisitResponse>> getAllBlankVisit(@Path("employeeId")Integer employeeId);

    @POST("leave")
    Call<Boolean> leaveform(@Body LeaveformRequest leaveformRequest);

    @GET("leave/getByEmpoyeeId/{employeeId}")
    Call<List<LeaveResponseModel>> getLeaveList(@Path("employeeId") String employeeId);
}
