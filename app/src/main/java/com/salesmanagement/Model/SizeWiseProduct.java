package com.salesmanagement.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SizeWiseProduct {

    @SerializedName("sizeWiseProductId")
    @Expose
    private String sizeWiseProductId;
    @SerializedName("qty")
    @Expose
    private String qty;
    @SerializedName("sizeWiseProductAmount")
    @Expose
    private String sizeWiseProductAmount;
    @SerializedName("sizeWiseProductGSTAmount")
    @Expose
    private String sizeWiseProductGSTAmount;
    @SerializedName("sizeWiseProductGSTPercentage")
    @Expose
    private String sizeWiseProductGSTPercentage;
    @SerializedName("sizeWiseProductFinalAmount")
    @Expose
    private String sizeWiseProductFinalAmount;
    @SerializedName("sizeWiseProductDiscountAmount")
    @Expose
    private String sizeWiseProductDiscountAmount;
    @SerializedName("sizeWiseProductStatus")
    @Expose
    private String sizeWiseProductStatus;
    @SerializedName("dealerProductAmount")
    @Expose
    private String dealerProductAmount;
    @SerializedName("productImage")
    @Expose
    private String productImage;
    @SerializedName("unitId")
    @Expose
    private String unitId;
    @SerializedName("unitName")
    @Expose
    private String unitName;
    @SerializedName("unit")
    @Expose
    private String unit;
    @SerializedName("sizeId")
    @Expose
    private String sizeId;
    @SerializedName("sizeName")
    @Expose
    private String sizeName;
    @SerializedName("size")
    @Expose
    private String size;
    @SerializedName("productId")
    @Expose
    private String productId;
    @SerializedName("productName")
    @Expose
    private String productName;
    @SerializedName("productDescription")
    @Expose
    private String productDescription;
    @SerializedName("hsnCode")
    @Expose
    private String hsnCode;


    public String getSizeWiseProductId() {
        return sizeWiseProductId;
    }

    public void setSizeWiseProductId(String sizeWiseProductId) {
        this.sizeWiseProductId = sizeWiseProductId;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getSizeWiseProductAmount() {
        return sizeWiseProductAmount;
    }

    public void setSizeWiseProductAmount(String sizeWiseProductAmount) {
        this.sizeWiseProductAmount = sizeWiseProductAmount;
    }

    public String getSizeWiseProductGSTAmount() {
        return sizeWiseProductGSTAmount;
    }

    public void setSizeWiseProductGSTAmount(String sizeWiseProductGSTAmount) {
        this.sizeWiseProductGSTAmount = sizeWiseProductGSTAmount;
    }

    public String getSizeWiseProductGSTPercentage() {
        return sizeWiseProductGSTPercentage;
    }

    public void setSizeWiseProductGSTPercentage(String sizeWiseProductGSTPercentage) {
        this.sizeWiseProductGSTPercentage = sizeWiseProductGSTPercentage;
    }

    public String getSizeWiseProductFinalAmount() {
        return sizeWiseProductFinalAmount;
    }

    public void setSizeWiseProductFinalAmount(String sizeWiseProductFinalAmount) {
        this.sizeWiseProductFinalAmount = sizeWiseProductFinalAmount;
    }

    public String getSizeWiseProductDiscountAmount() {
        return sizeWiseProductDiscountAmount;
    }

    public void setSizeWiseProductDiscountAmount(String sizeWiseProductDiscountAmount) {
        this.sizeWiseProductDiscountAmount = sizeWiseProductDiscountAmount;
    }

    public String getSizeWiseProductStatus() {
        return sizeWiseProductStatus;
    }

    public void setSizeWiseProductStatus(String sizeWiseProductStatus) {
        this.sizeWiseProductStatus = sizeWiseProductStatus;
    }

    public String getDealerProductAmount() {
        return dealerProductAmount;
    }

    public void setDealerProductAmount(String dealerProductAmount) {
        this.dealerProductAmount = dealerProductAmount;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getSizeId() {
        return sizeId;
    }

    public void setSizeId(String sizeId) {
        this.sizeId = sizeId;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getHsnCode() {
        return hsnCode;
    }

    public void setHsnCode(String hsnCode) {
        this.hsnCode = hsnCode;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }
}
