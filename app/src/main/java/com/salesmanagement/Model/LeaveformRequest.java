package com.salesmanagement.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LeaveformRequest {

    @Expose
    @SerializedName("employeeId")
    private Integer empolyeeId;
    @Expose
    @SerializedName("leaveDescription")
    private String leaveDescription;
    @Expose
    @SerializedName("leaveDate")
    private String leaveDate;
    @Expose
    @SerializedName("leaveType")
    private String leaveType;

    public Integer getEmpolyeeId() {
        return empolyeeId;
    }

    public void setEmpolyeeId(Integer empolyeeId) {
        this.empolyeeId = empolyeeId;
    }

    public String getLeaveDescription() {
        return leaveDescription;
    }

    public void setLeaveDescription(String leaveDescription) {
        this.leaveDescription = leaveDescription;
    }

    public String getLeaveDate() {
        return leaveDate;
    }

    public void setLeaveDate(String leaveDate) {
        this.leaveDate = leaveDate;
    }

    public String getLeaveType() {
        return leaveType;
    }

    public void setLeaveType(String leaveType) {
        this.leaveType = leaveType;
    }
}
