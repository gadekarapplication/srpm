package com.salesmanagement.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LeaveformResponse {

    @Expose
    @SerializedName("leaveId")
    private String leaveId;
    @Expose
    @SerializedName("leaveType")
    private String leaveType;
    @Expose
    @SerializedName("leaveStatus")
    private String leaveStatus;
    @Expose
    @SerializedName("leaveDescription")
    private String leaveDescription;
    @Expose
    @SerializedName("leaveDate")
    private String leaveDate;

    @Expose
    @SerializedName("empolyeeId")
    private String empolyeeId;
    @Expose
    @SerializedName("empolyeeName")
    private String empolyeeName;

    public String getLeaveId() {
        return leaveId;
    }

    public void setLeaveId(String leaveId) {
        this.leaveId = leaveId;
    }

    public String getLeaveType() {
        return leaveType;
    }

    public void setLeaveType(String leaveType) {
        this.leaveType = leaveType;
    }

    public String getLeaveStatus() {
        return leaveStatus;
    }

    public void setLeaveStatus(String leaveStatus) {
        this.leaveStatus = leaveStatus;
    }

    public String getLeaveDescription() {
        return leaveDescription;
    }

    public void setLeaveDescription(String leaveDescription) {
        this.leaveDescription = leaveDescription;
    }

    public String getLeaveDate() {
        return leaveDate;
    }

    public void setLeaveDate(String leaveDate) {
        this.leaveDate = leaveDate;
    }

    public String getEmpolyeeId() {
        return empolyeeId;
    }

    public void setEmpolyeeId(String empolyeeId) {
        this.empolyeeId = empolyeeId;
    }

    public String getEmpolyeeName() {
        return empolyeeName;
    }

    public void setEmpolyeeName(String empolyeeName) {
        this.empolyeeName = empolyeeName;
    }
}
