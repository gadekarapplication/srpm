package com.salesmanagement.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PinCodeResponse {


    @Expose
    @SerializedName("talukaName")
    private String talukaName;
    @Expose
    @SerializedName("talukaId")
    private int talukaId;
    @Expose
    @SerializedName("districtName")
    private String districtName;
    @Expose
    @SerializedName("districtId")
    private int districtId;
    @Expose
    @SerializedName("cityName")
    private String cityName;
    @Expose
    @SerializedName("cityId")
    private int cityId;
    @Expose
    @SerializedName("stateName")
    private String stateName;
    @Expose
    @SerializedName("stateId")
    private int stateId;
    @Expose
    @SerializedName("countryName")
    private String countryName;
    @Expose
    @SerializedName("countryId")
    private int countryId;
    @Expose
    @SerializedName("pincodeStatus")
    private String pincodeStatus;
    @Expose
    @SerializedName("pincodeName")
    private String pincodeName;
    @Expose
    @SerializedName("pincodeId")
    private int pincodeId;

    public String getTalukaName() {
        return talukaName;
    }

    public void setTalukaName(String talukaName) {
        this.talukaName = talukaName;
    }

    public int getTalukaId() {
        return talukaId;
    }

    public void setTalukaId(int talukaId) {
        this.talukaId = talukaId;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public int getDistrictId() {
        return districtId;
    }

    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public int getStateId() {
        return stateId;
    }

    public void setStateId(int stateId) {
        this.stateId = stateId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public String getPincodeStatus() {
        return pincodeStatus;
    }

    public void setPincodeStatus(String pincodeStatus) {
        this.pincodeStatus = pincodeStatus;
    }

    public String getPincodeName() {
        return pincodeName;
    }

    public void setPincodeName(String pincodeName) {
        this.pincodeName = pincodeName;
    }

    public int getPincodeId() {
        return pincodeId;
    }

    public void setPincodeId(int pincodeId) {
        this.pincodeId = pincodeId;
    }

    @Override
    public String toString() {
        return pincodeName;
    }
}
