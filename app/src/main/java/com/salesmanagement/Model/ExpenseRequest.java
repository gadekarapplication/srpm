package com.salesmanagement.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class ExpenseRequest {

    @Expose
    @SerializedName("expenseType")
    private String expenseType;
    @Expose
    @SerializedName("expenseReceipt")
    private String expenseReceipt;
    @Expose
    @SerializedName("expenseMasterId")
    private int expenseMasterId;
    @Expose
    @SerializedName("expenseAmt")
    private Double expenseAmt;
    @Expose
    @SerializedName("employeeName")
    private String employeeName;
    @Expose
    @SerializedName("employeeId")
    private int employeeId;
    @Expose
    @SerializedName("description")
    private String description;
    @Expose
    @SerializedName("datetime")
    private String datetime;

    public String getExpenseType() {
        return expenseType;
    }

    public void setExpenseType(String expenseType) {
        this.expenseType = expenseType;
    }

    public String getExpenseReceipt() {
        return expenseReceipt;
    }

    public void setExpenseReceipt(String expenseReceipt) {
        this.expenseReceipt = expenseReceipt;
    }

    public int getExpenseMasterId() {
        return expenseMasterId;
    }

    public void setExpenseMasterId(int expenseMasterId) {
        this.expenseMasterId = expenseMasterId;
    }

    public Double getExpenseAmt() {
        return expenseAmt;
    }

    public void setExpenseAmt(Double expenseAmt) {
        this.expenseAmt = expenseAmt;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }
}
