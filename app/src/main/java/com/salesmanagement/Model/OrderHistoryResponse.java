package com.salesmanagement.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderHistoryResponse {

    @SerializedName("orderHistoryMasterId")
    @Expose
    private String orderHistoryMasterId;
    @SerializedName("productName")
    @Expose
    private String productName;
    @SerializedName("compName")
    @Expose
    private String compName;
    @SerializedName("categoryName")
    @Expose
    private String categoryName;
    @SerializedName("size")
    @Expose
    private String size;
    @SerializedName("unit")
    @Expose
    private String unit;
    @SerializedName("noOfPacket")
    @Expose
    private String noOfPacket;
    @SerializedName("productDescription")
    @Expose
    private String productDescription;
    @SerializedName("productImage")
    @Expose
    private String productImage;
    @SerializedName("qty")
    @Expose
    private String qty;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("gstAmount")
    @Expose
    private String gstAmount;
    @SerializedName("gstPercentage")
    @Expose
    private String gstPercentage;
    @SerializedName("finalAmount")
    @Expose
    private String finalAmount;
    @SerializedName("discountAmount")
    @Expose
    private String discountAmount;
    @SerializedName("discountPercentage")
    @Expose
    private String discountPercentage;
    @SerializedName("orderId")
    @Expose
    private String orderId;
    @SerializedName("productId")
    @Expose
    private String productId;


    public String getOrderHistoryMasterId() {
        return orderHistoryMasterId;
    }

    public void setOrderHistoryMasterId(String orderHistoryMasterId) {
        this.orderHistoryMasterId = orderHistoryMasterId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getCompName() {
        return compName;
    }

    public void setCompName(String compName) {
        this.compName = compName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getNoOfPacket() {
        return noOfPacket;
    }

    public void setNoOfPacket(String noOfPacket) {
        this.noOfPacket = noOfPacket;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getGstAmount() {
        return gstAmount;
    }

    public void setGstAmount(String gstAmount) {
        this.gstAmount = gstAmount;
    }

    public String getGstPercentage() {
        return gstPercentage;
    }

    public void setGstPercentage(String gstPercentage) {
        this.gstPercentage = gstPercentage;
    }

    public String getFinalAmount() {
        return finalAmount;
    }

    public void setFinalAmount(String finalAmount) {
        this.finalAmount = finalAmount;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(String discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
