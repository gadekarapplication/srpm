package com.salesmanagement.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MonthlyAttendanceResponseModel {
    @SerializedName("start")
    @Expose
    private String start;
    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("day")
    @Expose
    private Integer  day;

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }
}
