package com.salesmanagement.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class CollectionsReportResponse {


    @Expose
    @SerializedName("remainingTarget")
    private String remainingTarget;
    @Expose
    @SerializedName("remainingDays")
    private String remainingDays;
    @Expose
    @SerializedName("startDate")
    private String startDate;
    @Expose
    @SerializedName("timePeriod")
    private int timePeriod;
    @Expose
    @SerializedName("transactionType")
    private String transactionType;
    @Expose
    @SerializedName("collectionTargetMonthWiseAmount")
    private String collectionTargetMonthWiseAmount;
    @Expose
    @SerializedName("collectionAchivedTargetMonthWiseAmount")
    private String collectionAchivedTargetMonthWiseAmount;

    public String getRemainingTarget() {
        return remainingTarget;
    }

    public void setRemainingTarget(String remainingTarget) {
        this.remainingTarget = remainingTarget;
    }

    public String getRemainingDays() {
        return remainingDays;
    }

    public void setRemainingDays(String remainingDays) {
        this.remainingDays = remainingDays;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public int getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(int timePeriod) {
        this.timePeriod = timePeriod;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getCollectionTargetMonthWiseAmount() {
        return collectionTargetMonthWiseAmount;
    }

    public void setCollectionTargetMonthWiseAmount(String collectionTargetMonthWiseAmount) {
        this.collectionTargetMonthWiseAmount = collectionTargetMonthWiseAmount;
    }

    public String getCollectionAchivedTargetMonthWiseAmount() {
        return collectionAchivedTargetMonthWiseAmount;
    }

    public void setCollectionAchivedTargetMonthWiseAmount(String collectionAchivedTargetMonthWiseAmount) {
        this.collectionAchivedTargetMonthWiseAmount = collectionAchivedTargetMonthWiseAmount;
    }
}
