package com.salesmanagement.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderResponse {

    @SerializedName("cartlist")
    @Expose
    private List<CartResponse> cartResponseList = null;
    @SerializedName("compId")
    @Expose
    private String compId;
    @SerializedName("compName")
    @Expose
    private String compName;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("dealershipId")
    @Expose
    private String dealershipId;
    @SerializedName("finalTotalAmount")
    @Expose
    private String finalTotalAmount;
    @SerializedName("finalTotalDiscountAmount")
    @Expose
    private String finalTotalDiscountAmount;
    @SerializedName("finalTotalDiscountPer")
    @Expose
    private String finalTotalDiscountPer;
    @SerializedName("finalTotalGstAmount")
    @Expose
    private String finalTotalGstAmount;
    @SerializedName("finalTotalQty")
    @Expose
    private String finalTotalQty;
    @SerializedName("orderBillingAddress")
    @Expose
    private String orderBillingAddress;
    @SerializedName("orderCouponAmount")
    @Expose
    private String orderCouponAmount;
    @SerializedName("orderCouponCode")
    @Expose
    private String orderCouponCode;
    @SerializedName("orderDeliveryAddress")
    @Expose
    private String orderDeliveryAddress;
    @SerializedName("orderId")
    @Expose
    private String orderId;
    @SerializedName("orderNo")
    @Expose
    private String orderNo;
    @SerializedName("orderPaymentMode")
    @Expose
    private String orderPaymentMode;
    @SerializedName("orderStatus")
    @Expose
    private String orderStatus;
    @SerializedName("reason")
    @Expose
    private String reason;
    @SerializedName("dealerAddressId")
    private String dealerAddressId;

    public String getDealerAddressId() {
        return dealerAddressId;
    }

    public void setDealerAddressId(String dealerAddressId) {
        this.dealerAddressId = dealerAddressId;
    }

    public List<CartResponse> getCartResponseList() {
        return cartResponseList;
    }

    public void setCartResponseList(List<CartResponse> cartResponseList) {
        this.cartResponseList = cartResponseList;
    }

    public String getCompId() {
        return compId;
    }

    public void setCompId(String compId) {
        this.compId = compId;
    }

    public String getCompName() {
        return compName;
    }

    public void setCompName(String compName) {
        this.compName = compName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDealershipId() {
        return dealershipId;
    }

    public void setDealershipId(String dealershipId) {
        this.dealershipId = dealershipId;
    }

    public String getFinalTotalAmount() {
        return finalTotalAmount;
    }

    public void setFinalTotalAmount(String finalTotalAmount) {
        this.finalTotalAmount = finalTotalAmount;
    }

    public String getFinalTotalDiscountAmount() {
        return finalTotalDiscountAmount;
    }

    public void setFinalTotalDiscountAmount(String finalTotalDiscountAmount) {
        this.finalTotalDiscountAmount = finalTotalDiscountAmount;
    }

    public String getFinalTotalDiscountPer() {
        return finalTotalDiscountPer;
    }

    public void setFinalTotalDiscountPer(String finalTotalDiscountPer) {
        this.finalTotalDiscountPer = finalTotalDiscountPer;
    }

    public String getFinalTotalGstAmount() {
        return finalTotalGstAmount;
    }

    public void setFinalTotalGstAmount(String finalTotalGstAmount) {
        this.finalTotalGstAmount = finalTotalGstAmount;
    }

    public String getFinalTotalQty() {
        return finalTotalQty;
    }

    public void setFinalTotalQty(String finalTotalQty) {
        this.finalTotalQty = finalTotalQty;
    }

    public String getOrderBillingAddress() {
        return orderBillingAddress;
    }

    public void setOrderBillingAddress(String orderBillingAddress) {
        this.orderBillingAddress = orderBillingAddress;
    }

    public String getOrderCouponAmount() {
        return orderCouponAmount;
    }

    public void setOrderCouponAmount(String orderCouponAmount) {
        this.orderCouponAmount = orderCouponAmount;
    }

    public String getOrderCouponCode() {
        return orderCouponCode;
    }

    public void setOrderCouponCode(String orderCouponCode) {
        this.orderCouponCode = orderCouponCode;
    }

    public String getOrderDeliveryAddress() {
        return orderDeliveryAddress;
    }

    public void setOrderDeliveryAddress(String orderDeliveryAddress) {
        this.orderDeliveryAddress = orderDeliveryAddress;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOrderPaymentMode() {
        return orderPaymentMode;
    }

    public void setOrderPaymentMode(String orderPaymentMode) {
        this.orderPaymentMode = orderPaymentMode;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
