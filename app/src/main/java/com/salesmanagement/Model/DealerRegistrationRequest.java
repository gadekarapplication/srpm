package com.salesmanagement.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DealerRegistrationRequest {


    @Expose
    @SerializedName("status")
    private String status;
    @Expose
    @SerializedName("remainingAmount")
    private int remainingAmount;
    @Expose
    @SerializedName("mobileNo")
    private String mobileNo;
    @Expose
    @SerializedName("firmName")
    private String firmName;
    @Expose
    @SerializedName("firmAddress")
    private String firmAddress;
    @Expose
    @SerializedName("documentName")
    private String documentName;
    @Expose
    @SerializedName("dealershipId")
    private int dealershipId;
    @Expose
    @SerializedName("dealerPhoto")
    private String dealerPhoto;
    @Expose
    @SerializedName("dealerPanNo")
    private String dealerPanNo;
    @Expose
    @SerializedName("dealerIFSC")
    private String dealerIFSC;
    @Expose
    @SerializedName("dealerIDProof")
    private String dealerIDProof;
    @Expose
    @SerializedName("dealerGSTPhoto")
    private String dealerGSTPhoto;
    @Expose
    @SerializedName("dealerGSTNo")
    private String dealerGSTNo;
    @Expose
    @SerializedName("dealerFullName")
    private String dealerFullName;
    @Expose
    @SerializedName("dealerEmail")
    private String dealerEmail;
    @Expose
    @SerializedName("dealerDOB")
    private String dealerDOB;
    @Expose
    @SerializedName("dealerChequePhoto")
    private String dealerChequePhoto;
    @Expose
    @SerializedName("dealerBranch")
    private String dealerBranch;
    @Expose
    @SerializedName("dealerBankName")
    private String dealerBankName;
    @Expose
    @SerializedName("dealerAccountNo")
    private String dealerAccountNo;
    @Expose
    @SerializedName("dealerAadharNo")
    private String dealerAadharNo;
    @Expose
    @SerializedName("creditLimit")
    private int creditLimit;

    @SerializedName("employeeId")
    private Integer employeeId;

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getRemainingAmount() {
        return remainingAmount;
    }

    public void setRemainingAmount(int remainingAmount) {
        this.remainingAmount = remainingAmount;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getFirmName() {
        return firmName;
    }

    public void setFirmName(String firmName) {
        this.firmName = firmName;
    }

    public String getFirmAddress() {
        return firmAddress;
    }

    public void setFirmAddress(String firmAddress) {
        this.firmAddress = firmAddress;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public int getDealershipId() {
        return dealershipId;
    }

    public void setDealershipId(int dealershipId) {
        this.dealershipId = dealershipId;
    }

    public String getDealerPhoto() {
        return dealerPhoto;
    }

    public void setDealerPhoto(String dealerPhoto) {
        this.dealerPhoto = dealerPhoto;
    }

    public String getDealerPanNo() {
        return dealerPanNo;
    }

    public void setDealerPanNo(String dealerPanNo) {
        this.dealerPanNo = dealerPanNo;
    }

    public String getDealerIFSC() {
        return dealerIFSC;
    }

    public void setDealerIFSC(String dealerIFSC) {
        this.dealerIFSC = dealerIFSC;
    }

    public String getDealerIDProof() {
        return dealerIDProof;
    }

    public void setDealerIDProof(String dealerIDProof) {
        this.dealerIDProof = dealerIDProof;
    }

    public String getDealerGSTPhoto() {
        return dealerGSTPhoto;
    }

    public void setDealerGSTPhoto(String dealerGSTPhoto) {
        this.dealerGSTPhoto = dealerGSTPhoto;
    }

    public String getDealerGSTNo() {
        return dealerGSTNo;
    }

    public void setDealerGSTNo(String dealerGSTNo) {
        this.dealerGSTNo = dealerGSTNo;
    }

    public String getDealerFullName() {
        return dealerFullName;
    }

    public void setDealerFullName(String dealerFullName) {
        this.dealerFullName = dealerFullName;
    }

    public String getDealerEmail() {
        return dealerEmail;
    }

    public void setDealerEmail(String dealerEmail) {
        this.dealerEmail = dealerEmail;
    }

    public String getDealerDOB() {
        return dealerDOB;
    }

    public void setDealerDOB(String dealerDOB) {
        this.dealerDOB = dealerDOB;
    }

    public String getDealerChequePhoto() {
        return dealerChequePhoto;
    }

    public void setDealerChequePhoto(String dealerChequePhoto) {
        this.dealerChequePhoto = dealerChequePhoto;
    }

    public String getDealerBranch() {
        return dealerBranch;
    }

    public void setDealerBranch(String dealerBranch) {
        this.dealerBranch = dealerBranch;
    }

    public String getDealerBankName() {
        return dealerBankName;
    }

    public void setDealerBankName(String dealerBankName) {
        this.dealerBankName = dealerBankName;
    }

    public String getDealerAccountNo() {
        return dealerAccountNo;
    }

    public void setDealerAccountNo(String dealerAccountNo) {
        this.dealerAccountNo = dealerAccountNo;
    }

    public String getDealerAadharNo() {
        return dealerAadharNo;
    }

    public void setDealerAadharNo(String dealerAadharNo) {
        this.dealerAadharNo = dealerAadharNo;
    }

    public int getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(int creditLimit) {
        this.creditLimit = creditLimit;
    }
}
