package com.salesmanagement.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EmployeeLoginResponse {


    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("responseCode")
    private int responseCode;
    @Expose
    @SerializedName("roleName")
    private String roleName;
    @Expose
    @SerializedName("roleId")
    private int roleId;
    @Expose
    @SerializedName("employeeStatus")
    private String employeeStatus;
    @Expose
    @SerializedName("employeeWorkingArea")
    private String employeeWorkingArea;
    @Expose
    @SerializedName("employeeEmail")
    private String employeeEmail;
    @Expose
    @SerializedName("employeePassword")
    private String employeePassword;
    @Expose
    @SerializedName("employeeAlternateMobileNo")
    private String employeeAlternateMobileNo;
    @Expose
    @SerializedName("employeeMobileNo")
    private String employeeMobileNo;
    @Expose
    @SerializedName("employeeQualification")
    private String employeeQualification;
    @Expose
    @SerializedName("employeeName")
    private String employeeName;
    @Expose
    @SerializedName("employeeId")
    private int employeeId;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getEmployeeStatus() {
        return employeeStatus;
    }

    public void setEmployeeStatus(String employeeStatus) {
        this.employeeStatus = employeeStatus;
    }

    public String getEmployeeWorkingArea() {
        return employeeWorkingArea;
    }

    public void setEmployeeWorkingArea(String employeeWorkingArea) {
        this.employeeWorkingArea = employeeWorkingArea;
    }

    public String getEmployeeEmail() {
        return employeeEmail;
    }

    public void setEmployeeEmail(String employeeEmail) {
        this.employeeEmail = employeeEmail;
    }

    public String getEmployeePassword() {
        return employeePassword;
    }

    public void setEmployeePassword(String employeePassword) {
        this.employeePassword = employeePassword;
    }

    public String getEmployeeAlternateMobileNo() {
        return employeeAlternateMobileNo;
    }

    public void setEmployeeAlternateMobileNo(String employeeAlternateMobileNo) {
        this.employeeAlternateMobileNo = employeeAlternateMobileNo;
    }

    public String getEmployeeMobileNo() {
        return employeeMobileNo;
    }

    public void setEmployeeMobileNo(String employeeMobileNo) {
        this.employeeMobileNo = employeeMobileNo;
    }

    public String getEmployeeQualification() {
        return employeeQualification;
    }

    public void setEmployeeQualification(String employeeQualification) {
        this.employeeQualification = employeeQualification;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }
}
