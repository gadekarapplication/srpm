package com.salesmanagement.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class VisitorReportResponse {


    @Expose
    @SerializedName("totalTargetAchievedCnt")
    private int totalTargetAchievedCnt;
    @Expose
    @SerializedName("totalTargetCnt")
    private int totalTargetCnt;

    public int getTotalTargetAchievedCnt() {
        return totalTargetAchievedCnt;
    }

    public void setTotalTargetAchievedCnt(int totalTargetAchievedCnt) {
        this.totalTargetAchievedCnt = totalTargetAchievedCnt;
    }

    public int getTotalTargetCnt() {
        return totalTargetCnt;
    }

    public void setTotalTargetCnt(int totalTargetCnt) {
        this.totalTargetCnt = totalTargetCnt;
    }
}
