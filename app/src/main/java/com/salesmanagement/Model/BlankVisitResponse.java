package com.salesmanagement.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class BlankVisitResponse {


    @Expose
    @SerializedName("employeeName")
    private String employeeName;
    @Expose
    @SerializedName("employeeId")
    private int employeeId;
    @Expose
    @SerializedName("dealerFullName")
    private String dealerFullName;
    @Expose
    @SerializedName("dealershipId")
    private int dealershipId;
    @Expose
    @SerializedName("blankVisitLatitude")
    private String blankVisitLatitude;
    @Expose
    @SerializedName("blankVisitLongitude")
    private String blankVisitLongitude;
    @Expose
    @SerializedName("blankVisitDescription")
    private String blankVisitDescription;
    @Expose
    @SerializedName("blankVisitSubject")
    private String blankVisitSubject;
    @Expose
    @SerializedName("blankVisitId")
    private int blankVisitId;
    @Expose
    @SerializedName("blankVisitDate")
    private String blankVisitDate;

    public String getBlankVisitDate() {
        return blankVisitDate;
    }

    public void setBlankVisitDate(String blankVisitDate) {
        this.blankVisitDate = blankVisitDate;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getDealerFullName() {
        return dealerFullName;
    }

    public void setDealerFullName(String dealerFullName) {
        this.dealerFullName = dealerFullName;
    }

    public int getDealershipId() {
        return dealershipId;
    }

    public void setDealershipId(int dealershipId) {
        this.dealershipId = dealershipId;
    }

    public String getBlankVisitLatitude() {
        return blankVisitLatitude;
    }

    public void setBlankVisitLatitude(String blankVisitLatitude) {
        this.blankVisitLatitude = blankVisitLatitude;
    }

    public String getBlankVisitLongitude() {
        return blankVisitLongitude;
    }

    public void setBlankVisitLongitude(String blankVisitLongitude) {
        this.blankVisitLongitude = blankVisitLongitude;
    }

    public String getBlankVisitDescription() {
        return blankVisitDescription;
    }

    public void setBlankVisitDescription(String blankVisitDescription) {
        this.blankVisitDescription = blankVisitDescription;
    }

    public String getBlankVisitSubject() {
        return blankVisitSubject;
    }

    public void setBlankVisitSubject(String blankVisitSubject) {
        this.blankVisitSubject = blankVisitSubject;
    }

    public int getBlankVisitId() {
        return blankVisitId;
    }

    public void setBlankVisitId(int blankVisitId) {
        this.blankVisitId = blankVisitId;
    }
}
