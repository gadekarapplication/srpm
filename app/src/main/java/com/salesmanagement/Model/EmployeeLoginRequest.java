package com.salesmanagement.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EmployeeLoginRequest {


    @Expose
    @SerializedName("employeePassword")
    private String employeePassword;
    @Expose
    @SerializedName("employeeMobileNo")
    private String employeeMobileNo;
    @Expose
    @SerializedName("employeeAlternateMobileNo")
    private String employeeAlternateMobileNo;

    public String getEmployeePassword() {
        return employeePassword;
    }

    public void setEmployeePassword(String employeePassword) {
        this.employeePassword = employeePassword;
    }

    public String getEmployeeMobileNo() {
        return employeeMobileNo;
    }

    public void setEmployeeMobileNo(String employeeMobileNo) {
        this.employeeMobileNo = employeeMobileNo;
    }

    public String getEmployeeAlternateMobileNo() {
        return employeeAlternateMobileNo;
    }

    public void setEmployeeAlternateMobileNo(String employeeAlternateMobileNo) {
        this.employeeAlternateMobileNo = employeeAlternateMobileNo;
    }
}
