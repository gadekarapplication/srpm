package com.salesmanagement.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CollectionsResponse {


    @Expose
    @SerializedName("compName")
    private String compName;
    @Expose
    @SerializedName("compId")
    private int compId;
    @Expose
    @SerializedName("dealerFullName")
    private String dealerFullName;
    @Expose
    @SerializedName("dealershipId")
    private int dealershipId;
    @Expose
    @SerializedName("employeeName")
    private String employeeName;
    @Expose
    @SerializedName("employeeId")
    private int employeeId;
    @Expose
    @SerializedName("transactionType")
    private String transactionType;
    @Expose
    @SerializedName("totalTransactionSaleDate")
    private String totalTransactionSaleDate;
    @Expose
    @SerializedName("totalAmount")
    private Float totalAmount;
    @Expose
    @SerializedName("totalTransactionSaleAndCollectionId")
    private int totalTransactionSaleAndCollectionId;

    public String getCompName() {
        return compName;
    }

    public void setCompName(String compName) {
        this.compName = compName;
    }

    public int getCompId() {
        return compId;
    }

    public void setCompId(int compId) {
        this.compId = compId;
    }

    public String getDealerFullName() {
        return dealerFullName;
    }

    public void setDealerFullName(String dealerFullName) {
        this.dealerFullName = dealerFullName;
    }

    public int getDealershipId() {
        return dealershipId;
    }

    public void setDealershipId(int dealershipId) {
        this.dealershipId = dealershipId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getTotalTransactionSaleDate() {
        return totalTransactionSaleDate;
    }

    public void setTotalTransactionSaleDate(String totalTransactionSaleDate) {
        this.totalTransactionSaleDate = totalTransactionSaleDate;
    }

    public Float getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Float totalAmount) {
        this.totalAmount = totalAmount;
    }

    public int getTotalTransactionSaleAndCollectionId() {
        return totalTransactionSaleAndCollectionId;
    }

    public void setTotalTransactionSaleAndCollectionId(int totalTransactionSaleAndCollectionId) {
        this.totalTransactionSaleAndCollectionId = totalTransactionSaleAndCollectionId;
    }
}
