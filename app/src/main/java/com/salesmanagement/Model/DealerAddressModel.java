package com.salesmanagement.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DealerAddressModel {


    @Expose
    @SerializedName("status")
    private String status;
    @Expose
    @SerializedName("mobileNo")
    private String mobileNo;
    @Expose
    @SerializedName("landmark")
    private String landmark;
    @Expose
    @SerializedName("fullAddress")
    private String fullAddress;
    @Expose
    @SerializedName("flatNo")
    private String flatNo;
    @Expose
    @SerializedName("dealershipId")
    private int dealershipId;
    @Expose
    @SerializedName("dealerAddressId")
    private int dealerAddressId;
    @Expose
    @SerializedName("areaId")
    private int areaId;
    @Expose
    @SerializedName("area")
    private String area;
    @Expose
    @SerializedName("addressType")
    private String addressType;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getFlatNo() {
        return flatNo;
    }

    public void setFlatNo(String flatNo) {
        this.flatNo = flatNo;
    }

    public int getDealershipId() {
        return dealershipId;
    }

    public void setDealershipId(int dealershipId) {
        this.dealershipId = dealershipId;
    }

    public int getDealerAddressId() {
        return dealerAddressId;
    }

    public void setDealerAddressId(int dealerAddressId) {
        this.dealerAddressId = dealerAddressId;
    }

    public int getAreaId() {
        return areaId;
    }

    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }


}
