package com.salesmanagement.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserOrderResponse {

    @SerializedName("orderId")
    @Expose
    private String orderId;
    @SerializedName("orderNo")
    @Expose
    private String orderNo;
    @SerializedName("orderBillingAddress")
    @Expose
    private String orderBillingAddress;
    @SerializedName("orderDeliveryAddress")
    @Expose
    private String orderDeliveryAddress;
    @SerializedName("orderCouponCode")
    @Expose
    private String orderCouponCode;
    @SerializedName("orderCouponAmount")
    @Expose
    private String orderCouponAmount;
    @SerializedName("orderDiscountAmount")
    @Expose
    private String orderDiscountAmount;
    @SerializedName("orderTotalGstAmount")
    @Expose
    private String orderTotalGstAmount;
    @SerializedName("orderTotalQty")
    @Expose
    private String orderTotalQty;
    @SerializedName("orderTotalDiscountAmount")
    @Expose
    private String orderTotalDiscountAmount;
    @SerializedName("orderTotalDiscountPer")
    @Expose
    private String orderTotalDiscountPer;
    @SerializedName("orderFinalAmount")
    @Expose
    private String orderFinalAmount;
    @SerializedName("orderPaymentMode")
    @Expose
    private String orderPaymentMode;
    @SerializedName("orderStatus")
    @Expose
    private String orderStatus;
    @SerializedName("reason")
    @Expose
    private String reason;
    @SerializedName("orderDate")
    @Expose
    private String orderDate;
    @SerializedName("compId")
    @Expose
    private String compId;
    @SerializedName("compName")
    @Expose
    private String compName;
    @SerializedName("dealershipId")
    @Expose
    private String dealershipId;
    @SerializedName("dealerFullName")
    @Expose
    private String dealerFullName;
    @SerializedName("employeeId")
    @Expose
    private String employeeId;
    @SerializedName("orderHistoryList")
    @Expose
    private List<OrderHistoryResponse> orderHistoryList = null;
    @SerializedName("employeeName")
    @Expose
    private String employeeName;


    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOrderBillingAddress() {
        return orderBillingAddress;
    }

    public void setOrderBillingAddress(String orderBillingAddress) {
        this.orderBillingAddress = orderBillingAddress;
    }

    public String getOrderDeliveryAddress() {
        return orderDeliveryAddress;
    }

    public void setOrderDeliveryAddress(String orderDeliveryAddress) {
        this.orderDeliveryAddress = orderDeliveryAddress;
    }

    public String getOrderCouponCode() {
        return orderCouponCode;
    }

    public void setOrderCouponCode(String orderCouponCode) {
        this.orderCouponCode = orderCouponCode;
    }

    public String getOrderCouponAmount() {
        return orderCouponAmount;
    }

    public void setOrderCouponAmount(String orderCouponAmount) {
        this.orderCouponAmount = orderCouponAmount;
    }

    public String getOrderDiscountAmount() {
        return orderDiscountAmount;
    }

    public void setOrderDiscountAmount(String orderDiscountAmount) {
        this.orderDiscountAmount = orderDiscountAmount;
    }

    public String getOrderTotalGstAmount() {
        return orderTotalGstAmount;
    }

    public void setOrderTotalGstAmount(String orderTotalGstAmount) {
        this.orderTotalGstAmount = orderTotalGstAmount;
    }

    public String getOrderTotalQty() {
        return orderTotalQty;
    }

    public void setOrderTotalQty(String orderTotalQty) {
        this.orderTotalQty = orderTotalQty;
    }

    public String getOrderTotalDiscountAmount() {
        return orderTotalDiscountAmount;
    }

    public void setOrderTotalDiscountAmount(String orderTotalDiscountAmount) {
        this.orderTotalDiscountAmount = orderTotalDiscountAmount;
    }

    public String getOrderTotalDiscountPer() {
        return orderTotalDiscountPer;
    }

    public void setOrderTotalDiscountPer(String orderTotalDiscountPer) {
        this.orderTotalDiscountPer = orderTotalDiscountPer;
    }

    public String getOrderFinalAmount() {
        return orderFinalAmount;
    }

    public void setOrderFinalAmount(String orderFinalAmount) {
        this.orderFinalAmount = orderFinalAmount;
    }

    public String getOrderPaymentMode() {
        return orderPaymentMode;
    }

    public void setOrderPaymentMode(String orderPaymentMode) {
        this.orderPaymentMode = orderPaymentMode;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getCompId() {
        return compId;
    }

    public void setCompId(String compId) {
        this.compId = compId;
    }

    public String getCompName() {
        return compName;
    }

    public void setCompName(String compName) {
        this.compName = compName;
    }

    public String getDealershipId() {
        return dealershipId;
    }

    public void setDealershipId(String dealershipId) {
        this.dealershipId = dealershipId;
    }

    public String getDealerFullName() {
        return dealerFullName;
    }

    public void setDealerFullName(String dealerFullName) {
        this.dealerFullName = dealerFullName;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public List<OrderHistoryResponse> getOrderHistoryList() {
        return orderHistoryList;
    }

    public void setOrderHistoryList(List<OrderHistoryResponse> orderHistoryList) {
        this.orderHistoryList = orderHistoryList;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }
}
