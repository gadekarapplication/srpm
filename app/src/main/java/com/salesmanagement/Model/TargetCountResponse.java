package com.salesmanagement.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TargetCountResponse {
    @SerializedName("saleAchievedTargetMonthWiseAmount")
    @Expose
    private Integer saleAchievedTargetMonthWiseAmount;
    @SerializedName("saleTargetMonthWiseAmount")
    @Expose
    private Integer saleTargetMonthWiseAmount;
    @SerializedName("remainingDaysSale")
    @Expose
    private Integer remainingDaysSale;
    @SerializedName("remainingTargetSale")
    @Expose
    private Integer remainingTargetSale;
    @SerializedName("collectionAchievedTargetMonthWiseAmount")
    @Expose
    private Integer collectionAchievedTargetMonthWiseAmount;
    @SerializedName("collectionTargetMonthWiseAmount")
    @Expose
    private Integer collectionTargetMonthWiseAmount;
    @SerializedName("remainingDaysCollection")
    @Expose
    private Integer remainingDaysCollection;
    @SerializedName("remainingTargetCollection")
    @Expose
    private Integer remainingTargetCollection;
    @SerializedName("visitTargetCntMonthly")
    @Expose
    private Integer visitTargetCntMonthly;
    @SerializedName("visitTargetAchievedCntMonthly")
    @Expose
    private Integer visitTargetAchievedCntMonthly;
    @SerializedName("remainingTargetVisit")
    @Expose
    private Integer remainingTargetVisit;
    @SerializedName("remainingDaysVisit")
    @Expose
    private Integer remainingDaysVisit;

    public Integer getSaleAchievedTargetMonthWiseAmount() {
        return saleAchievedTargetMonthWiseAmount;
    }

    public void setSaleAchievedTargetMonthWiseAmount(Integer saleAchievedTargetMonthWiseAmount) {
        this.saleAchievedTargetMonthWiseAmount = saleAchievedTargetMonthWiseAmount;
    }

    public Integer getSaleTargetMonthWiseAmount() {
        return saleTargetMonthWiseAmount;
    }

    public void setSaleTargetMonthWiseAmount(Integer saleTargetMonthWiseAmount) {
        this.saleTargetMonthWiseAmount = saleTargetMonthWiseAmount;
    }

    public Integer getRemainingDaysSale() {
        return remainingDaysSale;
    }

    public void setRemainingDaysSale(Integer remainingDaysSale) {
        this.remainingDaysSale = remainingDaysSale;
    }

    public Integer getRemainingTargetSale() {
        return remainingTargetSale;
    }

    public void setRemainingTargetSale(Integer remainingTargetSale) {
        this.remainingTargetSale = remainingTargetSale;
    }

    public Integer getCollectionAchievedTargetMonthWiseAmount() {
        return collectionAchievedTargetMonthWiseAmount;
    }

    public void setCollectionAchievedTargetMonthWiseAmount(Integer collectionAchievedTargetMonthWiseAmount) {
        this.collectionAchievedTargetMonthWiseAmount = collectionAchievedTargetMonthWiseAmount;
    }

    public Integer getCollectionTargetMonthWiseAmount() {
        return collectionTargetMonthWiseAmount;
    }

    public void setCollectionTargetMonthWiseAmount(Integer collectionTargetMonthWiseAmount) {
        this.collectionTargetMonthWiseAmount = collectionTargetMonthWiseAmount;
    }

    public Integer getRemainingDaysCollection() {
        return remainingDaysCollection;
    }

    public void setRemainingDaysCollection(Integer remainingDaysCollection) {
        this.remainingDaysCollection = remainingDaysCollection;
    }

    public Integer getRemainingTargetCollection() {
        return remainingTargetCollection;
    }

    public void setRemainingTargetCollection(Integer remainingTargetCollection) {
        this.remainingTargetCollection = remainingTargetCollection;
    }

    public Integer getVisitTargetCntMonthly() {
        return visitTargetCntMonthly;
    }

    public void setVisitTargetCntMonthly(Integer visitTargetCntMonthly) {
        this.visitTargetCntMonthly = visitTargetCntMonthly;
    }

    public Integer getVisitTargetAchievedCntMonthly() {
        return visitTargetAchievedCntMonthly;
    }

    public void setVisitTargetAchievedCntMonthly(Integer visitTargetAchievedCntMonthly) {
        this.visitTargetAchievedCntMonthly = visitTargetAchievedCntMonthly;
    }

    public Integer getRemainingTargetVisit() {
        return remainingTargetVisit;
    }

    public void setRemainingTargetVisit(Integer remainingTargetVisit) {
        this.remainingTargetVisit = remainingTargetVisit;
    }

    public Integer getRemainingDaysVisit() {
        return remainingDaysVisit;
    }

    public void setRemainingDaysVisit(Integer remainingDaysVisit) {
        this.remainingDaysVisit = remainingDaysVisit;
    }
}
