package com.salesmanagement.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CompanyResponse {

    @SerializedName("compId")
    @Expose
    private String compId;
    @SerializedName("compName")
    @Expose
    private String compName;
    @SerializedName("compAddress")
    @Expose
    private String compAddress;
    @SerializedName("compContactNo")
    @Expose
    private String compContactNo;
    @SerializedName("compEmail")
    @Expose
    private String compEmail;
    @SerializedName("compContactPersonName")
    @Expose
    private String compContactPersonName;
    @SerializedName("compContactPersonEmail")
    @Expose
    private String compContactPersonEmail;
    @SerializedName("compContactPersonNo")
    @Expose
    private String compContactPersonNo;
    @SerializedName("compStatus")
    @Expose
    private String compStatus;
    @SerializedName("compCountryId")
    @Expose
    private Integer compCountryId;
    @SerializedName("compStateId")
    @Expose
    private Integer compStateId;
    @SerializedName("compDistrictId")
    @Expose
    private Integer compDistrictId;
    @SerializedName("compTalukaId")
    @Expose
    private Integer compTalukaId;
    @SerializedName("compCityId")
    @Expose
    private Integer compCityId;
    @SerializedName("compAreaId")
    @Expose
    private Integer compAreaId;
    @SerializedName("compPincodeId")
    @Expose
    private Integer compPincodeId;


    public String getCompId() {
        return compId;
    }

    public void setCompId(String compId) {
        this.compId = compId;
    }

    public String getCompName() {
        return compName;
    }

    public void setCompName(String compName) {
        this.compName = compName;
    }

    public String getCompAddress() {
        return compAddress;
    }

    public void setCompAddress(String compAddress) {
        this.compAddress = compAddress;
    }

    public String getCompContactNo() {
        return compContactNo;
    }

    public void setCompContactNo(String compContactNo) {
        this.compContactNo = compContactNo;
    }

    public String getCompEmail() {
        return compEmail;
    }

    public void setCompEmail(String compEmail) {
        this.compEmail = compEmail;
    }

    public String getCompContactPersonName() {
        return compContactPersonName;
    }

    public void setCompContactPersonName(String compContactPersonName) {
        this.compContactPersonName = compContactPersonName;
    }

    public String getCompContactPersonEmail() {
        return compContactPersonEmail;
    }

    public void setCompContactPersonEmail(String compContactPersonEmail) {
        this.compContactPersonEmail = compContactPersonEmail;
    }

    public String getCompContactPersonNo() {
        return compContactPersonNo;
    }

    public void setCompContactPersonNo(String compContactPersonNo) {
        this.compContactPersonNo = compContactPersonNo;
    }

    public String getCompStatus() {
        return compStatus;
    }

    public void setCompStatus(String compStatus) {
        this.compStatus = compStatus;
    }

    public Integer getCompCountryId() {
        return compCountryId;
    }

    public void setCompCountryId(Integer compCountryId) {
        this.compCountryId = compCountryId;
    }

    public Integer getCompStateId() {
        return compStateId;
    }

    public void setCompStateId(Integer compStateId) {
        this.compStateId = compStateId;
    }

    public Integer getCompDistrictId() {
        return compDistrictId;
    }

    public void setCompDistrictId(Integer compDistrictId) {
        this.compDistrictId = compDistrictId;
    }

    public Integer getCompTalukaId() {
        return compTalukaId;
    }

    public void setCompTalukaId(Integer compTalukaId) {
        this.compTalukaId = compTalukaId;
    }

    public Integer getCompCityId() {
        return compCityId;
    }

    public void setCompCityId(Integer compCityId) {
        this.compCityId = compCityId;
    }

    public Integer getCompAreaId() {
        return compAreaId;
    }

    public void setCompAreaId(Integer compAreaId) {
        this.compAreaId = compAreaId;
    }

    public Integer getCompPincodeId() {
        return compPincodeId;
    }

    public void setCompPincodeId(Integer compPincodeId) {
        this.compPincodeId = compPincodeId;
    }
}
