package com.salesmanagement.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class SaleTargetResponse {


    @Expose
    @SerializedName("totalAchievedcollectionAndSaleMonthWiseAmount")
    private int totalAchievedcollectionAndSaleMonthWiseAmount;
    @Expose
    @SerializedName("totalTargetcollectionAndSaleMonthWiseAmount")
    private int totalTargetcollectionAndSaleMonthWiseAmount;
    @Expose
    @SerializedName("collectionTargetMonthWiseAmount")
    private int collectionTargetMonthWiseAmount;
    @Expose
    @SerializedName("collectionAchivedTargetMonthWiseAmount")
    private int collectionAchivedTargetMonthWiseAmount;
    @Expose
    @SerializedName("saleTargetMonthWiseAmount")
    private int saleTargetMonthWiseAmount;
    @Expose
    @SerializedName("saleAchivedTargetMonthWiseAmount")
    private int saleAchivedTargetMonthWiseAmount;

    public int getTotalAchievedcollectionAndSaleMonthWiseAmount() {
        return totalAchievedcollectionAndSaleMonthWiseAmount;
    }

    public void setTotalAchievedcollectionAndSaleMonthWiseAmount(int totalAchievedcollectionAndSaleMonthWiseAmount) {
        this.totalAchievedcollectionAndSaleMonthWiseAmount = totalAchievedcollectionAndSaleMonthWiseAmount;
    }

    public int getTotalTargetcollectionAndSaleMonthWiseAmount() {
        return totalTargetcollectionAndSaleMonthWiseAmount;
    }

    public void setTotalTargetcollectionAndSaleMonthWiseAmount(int totalTargetcollectionAndSaleMonthWiseAmount) {
        this.totalTargetcollectionAndSaleMonthWiseAmount = totalTargetcollectionAndSaleMonthWiseAmount;
    }

    public int getCollectionTargetMonthWiseAmount() {
        return collectionTargetMonthWiseAmount;
    }

    public void setCollectionTargetMonthWiseAmount(int collectionTargetMonthWiseAmount) {
        this.collectionTargetMonthWiseAmount = collectionTargetMonthWiseAmount;
    }

    public int getCollectionAchivedTargetMonthWiseAmount() {
        return collectionAchivedTargetMonthWiseAmount;
    }

    public void setCollectionAchivedTargetMonthWiseAmount(int collectionAchivedTargetMonthWiseAmount) {
        this.collectionAchivedTargetMonthWiseAmount = collectionAchivedTargetMonthWiseAmount;
    }

    public int getSaleTargetMonthWiseAmount() {
        return saleTargetMonthWiseAmount;
    }

    public void setSaleTargetMonthWiseAmount(int saleTargetMonthWiseAmount) {
        this.saleTargetMonthWiseAmount = saleTargetMonthWiseAmount;
    }

    public int getSaleAchivedTargetMonthWiseAmount() {
        return saleAchivedTargetMonthWiseAmount;
    }

    public void setSaleAchivedTargetMonthWiseAmount(int saleAchivedTargetMonthWiseAmount) {
        this.saleAchivedTargetMonthWiseAmount = saleAchivedTargetMonthWiseAmount;
    }
}
