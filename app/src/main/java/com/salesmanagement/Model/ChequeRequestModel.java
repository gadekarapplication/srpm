package com.salesmanagement.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChequeRequestModel {
    @SerializedName("accountNo")
    @Expose
    private String accountNo;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("bankName")
    @Expose
    private String bankName;
    @SerializedName("checkDetailImage")
    @Expose
    private String checkDetailImage;
    @SerializedName("chequeDetailId")
    @Expose
    private Integer chequeDetailId;
    @SerializedName("chequeNo")
    @Expose
    private String chequeNo;
    @SerializedName("compId")
    @Expose
    private Integer compId;
    @SerializedName("compName")
    @Expose
    private String compName;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("dealerFullName")
    @Expose
    private String dealerFullName;
    @SerializedName("dealershipId")
    @Expose
    private Integer dealershipId;
    @SerializedName("employeeId")
    @Expose
    private Integer employeeId;
    @SerializedName("employeeName")
    @Expose
    private String employeeName;
    @SerializedName("expiaryDate")
    @Expose
    private String expiaryDate;
    @SerializedName("fineAmt")
    @Expose
    private Integer fineAmt;
    @SerializedName("ifsccode")
    @Expose
    private String ifsccode;
    @SerializedName("status")
    @Expose
    private String status;

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getCheckDetailImage() {
        return checkDetailImage;
    }

    public void setCheckDetailImage(String checkDetailImage) {
        this.checkDetailImage = checkDetailImage;
    }

    public Integer getChequeDetailId() {
        return chequeDetailId;
    }

    public void setChequeDetailId(Integer chequeDetailId) {
        this.chequeDetailId = chequeDetailId;
    }

    public String getChequeNo() {
        return chequeNo;
    }

    public void setChequeNo(String chequeNo) {
        this.chequeNo = chequeNo;
    }

    public Integer getCompId() {
        return compId;
    }

    public void setCompId(Integer compId) {
        this.compId = compId;
    }

    public String getCompName() {
        return compName;
    }

    public void setCompName(String compName) {
        this.compName = compName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDealerFullName() {
        return dealerFullName;
    }

    public void setDealerFullName(String dealerFullName) {
        this.dealerFullName = dealerFullName;
    }

    public Integer getDealershipId() {
        return dealershipId;
    }

    public void setDealershipId(Integer dealershipId) {
        this.dealershipId = dealershipId;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getExpiaryDate() {
        return expiaryDate;
    }

    public void setExpiaryDate(String expiaryDate) {
        this.expiaryDate = expiaryDate;
    }

    public Integer getFineAmt() {
        return fineAmt;
    }

    public void setFineAmt(Integer fineAmt) {
        this.fineAmt = fineAmt;
    }

    public String getIfsccode() {
        return ifsccode;
    }

    public void setIfsccode(String ifsccode) {
        this.ifsccode = ifsccode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
