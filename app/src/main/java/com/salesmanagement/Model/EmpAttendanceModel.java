package com.salesmanagement.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import butterknife.BindView;

public class EmpAttendanceModel {

    @SerializedName("resList")
    List<MonthlyAttendanceResponseModel> attendanceResponseModelList;

    @SerializedName("presentcnt")
    Integer presentcnt;

    @SerializedName("absetcnt")
    Integer absetcnt;

    @SerializedName("weekoffcnt")
    Integer weekoffcnt;

    public List<MonthlyAttendanceResponseModel> getAttendanceResponseModelList() {
        return attendanceResponseModelList;
    }

    public void setAttendanceResponseModelList(List<MonthlyAttendanceResponseModel> attendanceResponseModelList) {
        this.attendanceResponseModelList = attendanceResponseModelList;
    }

    public Integer getPresentcnt() {
        return presentcnt;
    }

    public void setPresentcnt(Integer presentcnt) {
        this.presentcnt = presentcnt;
    }

    public Integer getAbsetcnt() {
        return absetcnt;
    }

    public void setAbsetcnt(Integer absetcnt) {
        this.absetcnt = absetcnt;
    }

    public Integer getWeekoffcnt() {
        return weekoffcnt;
    }

    public void setWeekoffcnt(Integer weekoffcnt) {
        this.weekoffcnt = weekoffcnt;
    }
}
