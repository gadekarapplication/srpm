package com.salesmanagement.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class CartRequest {

    @Expose
    @SerializedName("sizeWiseProductId")
    private String sizeWiseProductId;
    @Expose
    @SerializedName("qty")
    private String qty;
    @Expose
    @SerializedName("productId")
    private String productId;
    @Expose
    @SerializedName("gstPercentage")
    private String gstPercentage;
    @Expose
    @SerializedName("gstAmount")
    private String gstAmount;
    @Expose
    @SerializedName("finalAmount")
    private String finalAmount;
    @Expose
    @SerializedName("employeeId")
    private String  employeeId;
    @Expose
    @SerializedName("discountPercentage")
    private String  discountPercentage;
    @Expose
    @SerializedName("discountAmount")
    private String  discountAmount;
    @Expose
    @SerializedName("dealershipId")
    private String  dealershipId;
    @Expose
    @SerializedName("compName")
    private String compName;
    @Expose
    @SerializedName("compId")
    private String  compId;
    @Expose
    @SerializedName("cartId")
    private String cartId;
    @Expose
    @SerializedName("cartDate")
    private String cartDate;

    public String getSizeWiseProductId() {
        return sizeWiseProductId;
    }

    public void setSizeWiseProductId(String sizeWiseProductId) {
        this.sizeWiseProductId = sizeWiseProductId;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getGstPercentage() {
        return gstPercentage;
    }

    public void setGstPercentage(String gstPercentage) {
        this.gstPercentage = gstPercentage;
    }

    public String getGstAmount() {
        return gstAmount;
    }

    public void setGstAmount(String gstAmount) {
        this.gstAmount = gstAmount;
    }

    public String getFinalAmount() {
        return finalAmount;
    }

    public void setFinalAmount(String finalAmount) {
        this.finalAmount = finalAmount;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(String discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getDealershipId() {
        return dealershipId;
    }

    public void setDealershipId(String dealershipId) {
        this.dealershipId = dealershipId;
    }

    public String getCompName() {
        return compName;
    }

    public void setCompName(String compName) {
        this.compName = compName;
    }

    public String getCompId() {
        return compId;
    }

    public void setCompId(String compId) {
        this.compId = compId;
    }

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public String getCartDate() {
        return cartDate;
    }

    public void setCartDate(String cartDate) {
        this.cartDate = cartDate;
    }
}
