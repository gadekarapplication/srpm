package com.salesmanagement.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DealerAddressResponseModel implements Parcelable, Serializable {

    public static final Creator<DealerAddressResponseModel> CREATOR = new Creator<DealerAddressResponseModel>() {
        @Override
        public DealerAddressResponseModel createFromParcel(Parcel in) {
            return new DealerAddressResponseModel(in);
        }

        @Override
        public DealerAddressResponseModel[] newArray(int size) {
            return new DealerAddressResponseModel[size];
        }
    };
    @Expose
    @SerializedName("dealershipId")
    private int dealershipId;
    @Expose
    @SerializedName("areaId")
    private int areaId;
    @Expose
    @SerializedName("landmark")
    private String landmark;
    @Expose
    @SerializedName("area")
    private String area;
    @Expose
    @SerializedName("flatNo")
    private String flatNo;
    @Expose
    @SerializedName("mobileNo")
    private String mobileNo;
    @Expose
    @SerializedName("status")
    private String status;
    @Expose
    @SerializedName("fullAddress")
    private String fullAddress;
    @Expose
    @SerializedName("addressType")
    private String addressType;
    @Expose
    @SerializedName("dealerAddressId")
    private int dealerAddressId;
    private boolean isSelected;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
    public DealerAddressResponseModel(){

    }

    protected DealerAddressResponseModel(Parcel in) {
        dealershipId = in.readInt();
        areaId = in.readInt();
        landmark = in.readString();
        area = in.readString();
        flatNo = in.readString();
        mobileNo = in.readString();
        status = in.readString();
        fullAddress = in.readString();
        addressType = in.readString();
        dealerAddressId = in.readInt();
    }

    public int getDealershipId() {
        return dealershipId;
    }

    public void setDealershipId(int dealershipId) {
        this.dealershipId = dealershipId;
    }

    public int getAreaId() {
        return areaId;
    }

    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getFlatNo() {
        return flatNo;
    }

    public void setFlatNo(String flatNo) {
        this.flatNo = flatNo;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    public int getDealerAddressId() {
        return dealerAddressId;
    }

    public void setDealerAddressId(int dealerAddressId) {
        this.dealerAddressId = dealerAddressId;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(dealershipId);
        dest.writeInt(areaId);
        dest.writeString(landmark);
        dest.writeString(area);
        dest.writeString(flatNo);
        dest.writeString(mobileNo);
        dest.writeString(status);
        dest.writeString(fullAddress);
        dest.writeString(addressType);
        dest.writeInt(dealerAddressId);
    }
}
