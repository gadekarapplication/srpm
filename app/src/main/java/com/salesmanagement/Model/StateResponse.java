package com.salesmanagement.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StateResponse {


    @Expose
    @SerializedName("countryName")
    private String countryName;
    @Expose
    @SerializedName("countryId")
    private int countryId;
    @Expose
    @SerializedName("stateStatus")
    private String stateStatus;
    @Expose
    @SerializedName("stateName")
    private String stateName;
    @Expose
    @SerializedName("stateId")
    private int stateId;

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public String getStateStatus() {
        return stateStatus;
    }

    public void setStateStatus(String stateStatus) {
        this.stateStatus = stateStatus;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public int getStateId() {
        return stateId;
    }

    public void setStateId(int stateId) {
        this.stateId = stateId;
    }

    @Override
    public String toString() {
        return stateName;
    }
}
