package com.salesmanagement.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class ExpenseResponse implements Parcelable {


    @Expose
    @SerializedName("employeeName")
    private String employeeName;
    @Expose
    @SerializedName("employeeId")
    private int employeeId;
    @Expose
    @SerializedName("datetime")
    private String datetime;
    @Expose
    @SerializedName("expenseAmt")
    private String expenseAmt;
    @Expose
    @SerializedName("description")
    private String description;
    @Expose
    @SerializedName("expenseReceipt")
    private String expenseReceipt;
    @Expose
    @SerializedName("expenseType")
    private String expenseType;
    @Expose
    @SerializedName("expenseMasterId")
    private int expenseMasterId;

    protected ExpenseResponse(Parcel in) {
        employeeName = in.readString();
        employeeId = in.readInt();
        datetime = in.readString();
        expenseAmt = in.readString();
        description = in.readString();
        expenseReceipt = in.readString();
        expenseType = in.readString();
        expenseMasterId = in.readInt();
    }

    public static final Creator<ExpenseResponse> CREATOR = new Creator<ExpenseResponse>() {
        @Override
        public ExpenseResponse createFromParcel(Parcel in) {
            return new ExpenseResponse(in);
        }

        @Override
        public ExpenseResponse[] newArray(int size) {
            return new ExpenseResponse[size];
        }
    };

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getExpenseAmt() {
        return expenseAmt;
    }

    public void setExpenseAmt(String expenseAmt) {
        this.expenseAmt = expenseAmt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExpenseReceipt() {
        return expenseReceipt;
    }

    public void setExpenseReceipt(String expenseReceipt) {
        this.expenseReceipt = expenseReceipt;
    }

    public String getExpenseType() {
        return expenseType;
    }

    public void setExpenseType(String expenseType) {
        this.expenseType = expenseType;
    }

    public int getExpenseMasterId() {
        return expenseMasterId;
    }

    public void setExpenseMasterId(int expenseMasterId) {
        this.expenseMasterId = expenseMasterId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(employeeName);
        dest.writeInt(employeeId);
        dest.writeString(datetime);
        dest.writeString(expenseAmt);
        dest.writeString(description);
        dest.writeString(expenseReceipt);
        dest.writeString(expenseType);
        dest.writeInt(expenseMasterId);
    }
}
