package com.salesmanagement.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DealerResponse implements Parcelable {

    @SerializedName("dealershipId")
    @Expose
    private String dealershipId;
    @SerializedName("firmName")
    @Expose
    private String firmName;
    @SerializedName("dealerFullName")
    @Expose
    private String dealerFullName;
    @SerializedName("firmAddress")
    @Expose
    private String firmAddress;
    @SerializedName("mobileNo")
    @Expose
    private String mobileNo;
    @SerializedName("dealerEmail")
    @Expose
    private String dealerEmail;
    @SerializedName("dealerDOB")
    @Expose
    private String dealerDOB;
    @SerializedName("dealerAadharNo")
    @Expose
    private String dealerAadharNo;
    @SerializedName("dealerPanNo")
    @Expose
    private String dealerPanNo;
    @SerializedName("dealerGSTNo")
    @Expose
    private String dealerGSTNo;
    @SerializedName("dealerBankName")
    @Expose
    private String dealerBankName;
    @SerializedName("dealerBranch")
    @Expose
    private String dealerBranch;
    @SerializedName("dealerAccountNo")
    @Expose
    private String dealerAccountNo;
    @SerializedName("creditLimit")
    @Expose
    private String creditLimit;
    @SerializedName("remainingAmount")
    @Expose
    private String remainingAmount;
    @SerializedName("dealerIFSC")
    @Expose
    private String dealerIFSC;
    @SerializedName("dealerPhoto")
    @Expose
    private String dealerPhoto;
    @SerializedName("dealerIDProof")
    @Expose
    private String dealerIDProof;
    @SerializedName("dealerGSTPhoto")
    @Expose
    private String dealerGSTPhoto;
    @SerializedName("dealerChequePhoto")
    @Expose
    private String dealerChequePhoto;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("documentName")
    @Expose
    private String documentName;
    @SerializedName("advanceAmount")
    @Expose
    private String advanceAmount;

    @SerializedName("employeeId")
    private Integer employeeId;

    @SerializedName("employeeName")
    private String employeeName;

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    protected DealerResponse(Parcel in) {
        dealershipId = in.readString();
        firmName = in.readString();
        dealerFullName = in.readString();
        firmAddress = in.readString();
        mobileNo = in.readString();
        dealerEmail = in.readString();
        dealerDOB = in.readString();
        dealerAadharNo = in.readString();
        dealerPanNo = in.readString();
        dealerGSTNo = in.readString();
        dealerBankName = in.readString();
        dealerBranch = in.readString();
        dealerAccountNo = in.readString();
        creditLimit = in.readString();
        remainingAmount = in.readString();
        dealerIFSC = in.readString();
        dealerPhoto = in.readString();
        dealerIDProof = in.readString();
        dealerGSTPhoto = in.readString();
        dealerChequePhoto = in.readString();
        status = in.readString();
        documentName = in.readString();
        advanceAmount = in.readString();
    }

    public static final Creator<DealerResponse> CREATOR = new Creator<DealerResponse>() {
        @Override
        public DealerResponse createFromParcel(Parcel in) {
            return new DealerResponse(in);
        }

        @Override
        public DealerResponse[] newArray(int size) {
            return new DealerResponse[size];
        }
    };

    public String getDealershipId() {
        return dealershipId;
    }

    public void setDealershipId(String dealershipId) {
        this.dealershipId = dealershipId;
    }

    public String getFirmName() {
        return firmName;
    }

    public void setFirmName(String firmName) {
        this.firmName = firmName;
    }

    public String getDealerFullName() {
        return dealerFullName;
    }

    public void setDealerFullName(String dealerFullName) {
        this.dealerFullName = dealerFullName;
    }

    public String getFirmAddress() {
        return firmAddress;
    }

    public void setFirmAddress(String firmAddress) {
        this.firmAddress = firmAddress;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getDealerEmail() {
        return dealerEmail;
    }

    public void setDealerEmail(String dealerEmail) {
        this.dealerEmail = dealerEmail;
    }

    public String getDealerDOB() {
        return dealerDOB;
    }

    public void setDealerDOB(String dealerDOB) {
        this.dealerDOB = dealerDOB;
    }

    public String getDealerAadharNo() {
        return dealerAadharNo;
    }

    public void setDealerAadharNo(String dealerAadharNo) {
        this.dealerAadharNo = dealerAadharNo;
    }

    public String getDealerPanNo() {
        return dealerPanNo;
    }

    public void setDealerPanNo(String dealerPanNo) {
        this.dealerPanNo = dealerPanNo;
    }

    public String getDealerGSTNo() {
        return dealerGSTNo;
    }

    public void setDealerGSTNo(String dealerGSTNo) {
        this.dealerGSTNo = dealerGSTNo;
    }

    public String getDealerBankName() {
        return dealerBankName;
    }

    public void setDealerBankName(String dealerBankName) {
        this.dealerBankName = dealerBankName;
    }

    public String getDealerBranch() {
        return dealerBranch;
    }

    public void setDealerBranch(String dealerBranch) {
        this.dealerBranch = dealerBranch;
    }

    public String getDealerAccountNo() {
        return dealerAccountNo;
    }

    public void setDealerAccountNo(String dealerAccountNo) {
        this.dealerAccountNo = dealerAccountNo;
    }

    public String getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(String creditLimit) {
        this.creditLimit = creditLimit;
    }

    public String getRemainingAmount() {
        return remainingAmount;
    }

    public void setRemainingAmount(String remainingAmount) {
        this.remainingAmount = remainingAmount;
    }

    public String getDealerIFSC() {
        return dealerIFSC;
    }

    public void setDealerIFSC(String dealerIFSC) {
        this.dealerIFSC = dealerIFSC;
    }

    public String getDealerPhoto() {
        return dealerPhoto;
    }

    public void setDealerPhoto(String dealerPhoto) {
        this.dealerPhoto = dealerPhoto;
    }

    public String getDealerIDProof() {
        return dealerIDProof;
    }

    public void setDealerIDProof(String dealerIDProof) {
        this.dealerIDProof = dealerIDProof;
    }

    public String getDealerGSTPhoto() {
        return dealerGSTPhoto;
    }

    public void setDealerGSTPhoto(String dealerGSTPhoto) {
        this.dealerGSTPhoto = dealerGSTPhoto;
    }

    public String getDealerChequePhoto() {
        return dealerChequePhoto;
    }

    public void setDealerChequePhoto(String dealerChequePhoto) {
        this.dealerChequePhoto = dealerChequePhoto;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public String getAdvanceAmount() {
        return advanceAmount;
    }

    public void setAdvanceAmount(String advanceAmount) {
        this.advanceAmount = advanceAmount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(dealershipId);
        dest.writeString(firmName);
        dest.writeString(dealerFullName);
        dest.writeString(firmAddress);
        dest.writeString(mobileNo);
        dest.writeString(dealerEmail);
        dest.writeString(dealerDOB);
        dest.writeString(dealerAadharNo);
        dest.writeString(dealerPanNo);
        dest.writeString(dealerGSTNo);
        dest.writeString(dealerBankName);
        dest.writeString(dealerBranch);
        dest.writeString(dealerAccountNo);
        dest.writeString(creditLimit);
        dest.writeString(remainingAmount);
        dest.writeString(dealerIFSC);
        dest.writeString(dealerPhoto);
        dest.writeString(dealerIDProof);
        dest.writeString(dealerGSTPhoto);
        dest.writeString(dealerChequePhoto);
        dest.writeString(status);
        dest.writeString(documentName);
        dest.writeString(advanceAmount);
    }
}
