package com.salesmanagement.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TalukaResponse {


    @Expose
    @SerializedName("districtName")
    private String districtName;
    @Expose
    @SerializedName("districtId")
    private int districtId;
    @Expose
    @SerializedName("stateName")
    private String stateName;
    @Expose
    @SerializedName("stateId")
    private int stateId;
    @Expose
    @SerializedName("countryName")
    private String countryName;
    @Expose
    @SerializedName("countryId")
    private int countryId;
    @Expose
    @SerializedName("talukaStatus")
    private String talukaStatus;
    @Expose
    @SerializedName("talukaName")
    private String talukaName;
    @Expose
    @SerializedName("talukaId")
    private int talukaId;

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public int getDistrictId() {
        return districtId;
    }

    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public int getStateId() {
        return stateId;
    }

    public void setStateId(int stateId) {
        this.stateId = stateId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public String getTalukaStatus() {
        return talukaStatus;
    }

    public void setTalukaStatus(String talukaStatus) {
        this.talukaStatus = talukaStatus;
    }

    public String getTalukaName() {
        return talukaName;
    }

    public void setTalukaName(String talukaName) {
        this.talukaName = talukaName;
    }

    public int getTalukaId() {
        return talukaId;
    }

    public void setTalukaId(int talukaId) {
        this.talukaId = talukaId;
    }

    @Override
    public String toString() {
        return talukaName;
    }
}
