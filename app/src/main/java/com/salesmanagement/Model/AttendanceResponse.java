package com.salesmanagement.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AttendanceResponse {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("empAttendanceId")
    @Expose
    private String empAttendanceId;
    @SerializedName("employeeId")
    @Expose
    private String employeeId;
    @SerializedName("employeeName")
    @Expose
    private String employeeName;
    @SerializedName("punchInDate")
    @Expose
    private String punchInDate;
    @SerializedName("punchInLatitude")
    @Expose
    private String punchInLatitude;
    @SerializedName("punchInLongitude")
    @Expose
    private String punchInLongitude;
    @SerializedName("punchOutDate")
    @Expose
    private String punchOutDate;
    @SerializedName("punchOutLatitude")
    @Expose
    private String punchOutLatitude;
    @SerializedName("punchOutLongitude")
    @Expose
    private String punchOutLongitude;
    @SerializedName("status")
    @Expose
    private String status;


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getEmpAttendanceId() {
        return empAttendanceId;
    }

    public void setEmpAttendanceId(String empAttendanceId) {
        this.empAttendanceId = empAttendanceId;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getPunchInDate() {
        return punchInDate;
    }

    public void setPunchInDate(String punchInDate) {
        this.punchInDate = punchInDate;
    }

    public String getPunchInLatitude() {
        return punchInLatitude;
    }

    public void setPunchInLatitude(String punchInLatitude) {
        this.punchInLatitude = punchInLatitude;
    }

    public String getPunchInLongitude() {
        return punchInLongitude;
    }

    public void setPunchInLongitude(String punchInLongitude) {
        this.punchInLongitude = punchInLongitude;
    }

    public String getPunchOutDate() {
        return punchOutDate;
    }

    public void setPunchOutDate(String punchOutDate) {
        this.punchOutDate = punchOutDate;
    }

    public String getPunchOutLatitude() {
        return punchOutLatitude;
    }

    public void setPunchOutLatitude(String punchOutLatitude) {
        this.punchOutLatitude = punchOutLatitude;
    }

    public String getPunchOutLongitude() {
        return punchOutLongitude;
    }

    public void setPunchOutLongitude(String punchOutLongitude) {
        this.punchOutLongitude = punchOutLongitude;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
