package com.salesmanagement.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CartResponse {

    @SerializedName("compId")
    @Expose
    private String compId;
    @SerializedName("cartDate")
    @Expose
    private String cartDate;
    @SerializedName("cartId")
    @Expose
    private String cartId;
    @SerializedName("compName")
    @Expose
    private String compName;
    @SerializedName("employeeId")
    @Expose
    private String employeeId;
    @SerializedName("productId")
    @Expose
    private String productId;
    @SerializedName("qty")
    @Expose
    private String qty;
    @SerializedName("sizeWiseProductId")
    @Expose
    private String sizeWiseProductId;
    @SerializedName("categoryId")
    @Expose
    private String categoryId;
    @SerializedName("productName")
    @Expose
    private String productName;
    @SerializedName("categoryName")
    @Expose
    private String categoryName;
    @SerializedName("productImage")
    @Expose
    private String productImage;
    @SerializedName("size")
    @Expose
    private String size;
    @SerializedName("unit")
    @Expose
    private String unit;
    @SerializedName("noOfPacket")
    @Expose
    private String noOfPacket;
    @SerializedName("productDescription")
    @Expose
    private String productDescription;
    @SerializedName("employeeName")
    @Expose
    private String employeeName;
    @SerializedName("employeeMobileNo")
    @Expose
    private String employeeMobileNo;
    @SerializedName("sizeId")
    @Expose
    private Object sizeId;
    @SerializedName("productAmount")
    @Expose
    private String productAmount;
    @SerializedName("gstAmount")
    @Expose
    private String gstAmount;
    @SerializedName("gstPercentage")
    @Expose
    private String gstPercentage;
    @SerializedName("finalAmount")
    @Expose
    private String finalAmount;
    @SerializedName("discountAmount")
    @Expose
    private String discountAmount;
    @SerializedName("discountPercentage")
    @Expose
    private String discountPercentage;
    @SerializedName("dealershipId")
    @Expose
    private String dealershipId;
    @SerializedName("dealerFullName")
    @Expose
    private String dealerFullName;

    @SerializedName("dealerMobileNo")
    private String dealerMobileNo;

    @SerializedName("dealerCreditLimit")
    private String dealerCreditLimit;

    @SerializedName("dealerRemaimingAmt")
    private String dealerRemaimingAmt;

    public String getDealerCreditLimit() {
        return dealerCreditLimit;
    }

    public void setDealerCreditLimit(String dealerCreditLimit) {
        this.dealerCreditLimit = dealerCreditLimit;
    }

    public String getDealerRemaimingAmt() {
        return dealerRemaimingAmt;
    }

    public void setDealerRemaimingAmt(String dealerRemaimingAmt) {
        this.dealerRemaimingAmt = dealerRemaimingAmt;
    }

    public String getDealerMobileNo() {
        return dealerMobileNo;
    }

    public void setDealerMobileNo(String dealerMobileNo) {
        this.dealerMobileNo = dealerMobileNo;
    }

    public String getCartDate() {
        return cartDate;
    }

    public void setCartDate(String cartDate) {
        this.cartDate = cartDate;
    }

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String  qty) {
        this.qty = qty;
    }

    public String getSizeWiseProductId() {
        return sizeWiseProductId;
    }

    public void setSizeWiseProductId(String sizeWiseProductId) {
        this.sizeWiseProductId = sizeWiseProductId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getNoOfPacket() {
        return noOfPacket;
    }

    public void setNoOfPacket(String noOfPacket) {
        this.noOfPacket = noOfPacket;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeMobileNo() {
        return employeeMobileNo;
    }

    public void setEmployeeMobileNo(String employeeMobileNo) {
        this.employeeMobileNo = employeeMobileNo;
    }

    public Object getSizeId() {
        return sizeId;
    }

    public void setSizeId(Object sizeId) {
        this.sizeId = sizeId;
    }

    public String getProductAmount() {
        return productAmount;
    }

    public void setProductAmount(String productAmount) {
        this.productAmount = productAmount;
    }

    public String getGstAmount() {
        return gstAmount;
    }

    public void setGstAmount(String gstAmount) {
        this.gstAmount = gstAmount;
    }

    public String getGstPercentage() {
        return gstPercentage;
    }

    public void setGstPercentage(String gstPercentage) {
        this.gstPercentage = gstPercentage;
    }

    public String getFinalAmount() {
        return finalAmount;
    }

    public void setFinalAmount(String finalAmount) {
        this.finalAmount = finalAmount;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getDealershipId() {
        return dealershipId;
    }

    public void setDealershipId(String dealershipId) {
        this.dealershipId = dealershipId;
    }

    public String getCompId() {
        return compId;
    }

    public void setCompId(String compId) {
        this.compId = compId;
    }

    public String getCompName() {
        return compName;
    }

    public void setCompName(String compName) {
        this.compName = compName;
    }

    public String getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(String discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public String getDealerFullName() {
        return dealerFullName;
    }

    public void setDealerFullName(String dealerFullName) {
        this.dealerFullName = dealerFullName;
    }
}
