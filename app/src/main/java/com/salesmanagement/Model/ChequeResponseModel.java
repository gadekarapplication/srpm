package com.salesmanagement.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChequeResponseModel {
    @SerializedName("chequeDetailId")
    @Expose
    private Integer chequeDetailId;
    @SerializedName("chequeNo")
    @Expose
    private String chequeNo;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("bankName")
    @Expose
    private String bankName;
    @SerializedName("accountNo")
    @Expose
    private String accountNo;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("checkDetailImage")
    @Expose
    private String checkDetailImage;
    @SerializedName("expiaryDate")
    @Expose
    private String expiaryDate;
    @SerializedName("checkDepositeDate")
    @Expose
    private String checkDepositeDate;
    @SerializedName("employeeId")
    @Expose
    private Integer employeeId;
    @SerializedName("employeeName")
    @Expose
    private String employeeName;
    @SerializedName("dealershipId")
    @Expose
    private Integer dealershipId;
    @SerializedName("dealerFullName")
    @Expose
    private String dealerFullName;
    @SerializedName("fineAmt")
    @Expose
    private Integer fineAmt;
    @SerializedName("compId")
    @Expose
    private Integer compId;
    @SerializedName("compName")
    @Expose
    private String compName;
    @SerializedName("ifsccode")
    @Expose
    private String ifsccode;

    public Integer getChequeDetailId() {
        return chequeDetailId;
    }

    public void setChequeDetailId(Integer chequeDetailId) {
        this.chequeDetailId = chequeDetailId;
    }

    public String getChequeNo() {
        return chequeNo;
    }

    public void setChequeNo(String chequeNo) {
        this.chequeNo = chequeNo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCheckDetailImage() {
        return checkDetailImage;
    }

    public void setCheckDetailImage(String checkDetailImage) {
        this.checkDetailImage = checkDetailImage;
    }

    public String getExpiaryDate() {
        return expiaryDate;
    }

    public void setExpiaryDate(String expiaryDate) {
        this.expiaryDate = expiaryDate;
    }

    public String getCheckDepositeDate() {
        return checkDepositeDate;
    }

    public void setCheckDepositeDate(String checkDepositeDate) {
        this.checkDepositeDate = checkDepositeDate;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public Integer getDealershipId() {
        return dealershipId;
    }

    public void setDealershipId(Integer dealershipId) {
        this.dealershipId = dealershipId;
    }

    public String getDealerFullName() {
        return dealerFullName;
    }

    public void setDealerFullName(String dealerFullName) {
        this.dealerFullName = dealerFullName;
    }

    public Integer getFineAmt() {
        return fineAmt;
    }

    public void setFineAmt(Integer fineAmt) {
        this.fineAmt = fineAmt;
    }

    public Integer getCompId() {
        return compId;
    }

    public void setCompId(Integer compId) {
        this.compId = compId;
    }

    public String getCompName() {
        return compName;
    }

    public void setCompName(String compName) {
        this.compName = compName;
    }

    public String getIfsccode() {
        return ifsccode;
    }

    public void setIfsccode(String ifsccode) {
        this.ifsccode = ifsccode;
    }
}
