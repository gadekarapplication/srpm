package com.salesmanagement.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DealerListResponse implements Parcelable, Serializable {


    public static final Creator<DealerListResponse> CREATOR = new Creator<DealerListResponse>() {
        @Override
        public DealerListResponse createFromParcel(Parcel in) {
            return new DealerListResponse(in);
        }

        @Override
        public DealerListResponse[] newArray(int size) {
            return new DealerListResponse[size];
        }
    };
    @Expose
    @SerializedName("documentName")
    private String documentName;
    @Expose
    @SerializedName("status")
    private String status;
    @Expose
    @SerializedName("dealerChequePhoto")
    private String dealerChequePhoto;
    @Expose
    @SerializedName("dealerGSTPhoto")
    private String dealerGSTPhoto;
    @Expose
    @SerializedName("dealerIDProof")
    private String dealerIDProof;
    @Expose
    @SerializedName("dealerPhoto")
    private String dealerPhoto;
    @Expose
    @SerializedName("dealerIFSC")
    private String dealerIFSC;
    @Expose
    @SerializedName("remainingAmount")
    private Float remainingAmount;
    @Expose
    @SerializedName("creditLimit")
    private int creditLimit;
    @Expose
    @SerializedName("dealerAccountNo")
    private String dealerAccountNo;
    @Expose
    @SerializedName("dealerBranch")
    private String dealerBranch;
    @Expose
    @SerializedName("dealerBankName")
    private String dealerBankName;
    @Expose
    @SerializedName("dealerGSTNo")
    private String dealerGSTNo;
    @Expose
    @SerializedName("dealerPanNo")
    private String dealerPanNo;
    @Expose
    @SerializedName("dealerAadharNo")
    private String dealerAadharNo;
    @Expose
    @SerializedName("dealerDOB")
    private String dealerDOB;
    @Expose
    @SerializedName("dealerEmail")
    private String dealerEmail;
    @Expose
    @SerializedName("mobileNo")
    private String mobileNo;
    @Expose
    @SerializedName("firmAddress")
    private String firmAddress;
    @Expose
    @SerializedName("dealerFullName")
    private String dealerFullName;
    @Expose
    @SerializedName("firmName")
    private String firmName;
    @Expose
    @SerializedName("dealershipId")
    private int dealershipId;

    protected DealerListResponse(Parcel in) {
        documentName = in.readString();
        status = in.readString();
        dealerChequePhoto = in.readString();
        dealerGSTPhoto = in.readString();
        dealerIDProof = in.readString();
        dealerPhoto = in.readString();
        dealerIFSC = in.readString();
        remainingAmount = in.readFloat();
        creditLimit = in.readInt();
        dealerAccountNo = in.readString();
        dealerBranch = in.readString();
        dealerBankName = in.readString();
        dealerGSTNo = in.readString();
        dealerPanNo = in.readString();
        dealerAadharNo = in.readString();
        dealerDOB = in.readString();
        dealerEmail = in.readString();
        mobileNo = in.readString();
        firmAddress = in.readString();
        dealerFullName = in.readString();
        firmName = in.readString();
        dealershipId = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(documentName);
        dest.writeString(status);
        dest.writeString(dealerChequePhoto);
        dest.writeString(dealerGSTPhoto);
        dest.writeString(dealerIDProof);
        dest.writeString(dealerPhoto);
        dest.writeString(dealerIFSC);
        dest.writeFloat(remainingAmount);
        dest.writeInt(creditLimit);
        dest.writeString(dealerAccountNo);
        dest.writeString(dealerBranch);
        dest.writeString(dealerBankName);
        dest.writeString(dealerGSTNo);
        dest.writeString(dealerPanNo);
        dest.writeString(dealerAadharNo);
        dest.writeString(dealerDOB);
        dest.writeString(dealerEmail);
        dest.writeString(mobileNo);
        dest.writeString(firmAddress);
        dest.writeString(dealerFullName);
        dest.writeString(firmName);
        dest.writeInt(dealershipId);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDealerChequePhoto() {
        return dealerChequePhoto;
    }

    public void setDealerChequePhoto(String dealerChequePhoto) {
        this.dealerChequePhoto = dealerChequePhoto;
    }

    public String getDealerGSTPhoto() {
        return dealerGSTPhoto;
    }

    public void setDealerGSTPhoto(String dealerGSTPhoto) {
        this.dealerGSTPhoto = dealerGSTPhoto;
    }

    public String getDealerIDProof() {
        return dealerIDProof;
    }

    public void setDealerIDProof(String dealerIDProof) {
        this.dealerIDProof = dealerIDProof;
    }

    public String getDealerPhoto() {
        return dealerPhoto;
    }

    public void setDealerPhoto(String dealerPhoto) {
        this.dealerPhoto = dealerPhoto;
    }

    public String getDealerIFSC() {
        return dealerIFSC;
    }

    public void setDealerIFSC(String dealerIFSC) {
        this.dealerIFSC = dealerIFSC;
    }

    public Float getRemainingAmount() {
        return remainingAmount;
    }

    public void setRemainingAmount(Float remainingAmount) {
        this.remainingAmount = remainingAmount;
    }

    public int getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(int creditLimit) {
        this.creditLimit = creditLimit;
    }

    public String getDealerAccountNo() {
        return dealerAccountNo;
    }

    public void setDealerAccountNo(String dealerAccountNo) {
        this.dealerAccountNo = dealerAccountNo;
    }

    public String getDealerBranch() {
        return dealerBranch;
    }

    public void setDealerBranch(String dealerBranch) {
        this.dealerBranch = dealerBranch;
    }

    public String getDealerBankName() {
        return dealerBankName;
    }

    public void setDealerBankName(String dealerBankName) {
        this.dealerBankName = dealerBankName;
    }

    public String getDealerGSTNo() {
        return dealerGSTNo;
    }

    public void setDealerGSTNo(String dealerGSTNo) {
        this.dealerGSTNo = dealerGSTNo;
    }

    public String getDealerPanNo() {
        return dealerPanNo;
    }

    public void setDealerPanNo(String dealerPanNo) {
        this.dealerPanNo = dealerPanNo;
    }

    public String getDealerAadharNo() {
        return dealerAadharNo;
    }

    public void setDealerAadharNo(String dealerAadharNo) {
        this.dealerAadharNo = dealerAadharNo;
    }

    public String getDealerDOB() {
        return dealerDOB;
    }

    public void setDealerDOB(String dealerDOB) {
        this.dealerDOB = dealerDOB;
    }

    public String getDealerEmail() {
        return dealerEmail;
    }

    public void setDealerEmail(String dealerEmail) {
        this.dealerEmail = dealerEmail;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getFirmAddress() {
        return firmAddress;
    }

    public void setFirmAddress(String firmAddress) {
        this.firmAddress = firmAddress;
    }

    public String getDealerFullName() {
        return dealerFullName;
    }

    public void setDealerFullName(String dealerFullName) {
        this.dealerFullName = dealerFullName;
    }

    public String getFirmName() {
        return firmName;
    }

    public void setFirmName(String firmName) {
        this.firmName = firmName;
    }

    public int getDealershipId() {
        return dealershipId;
    }

    public void setDealershipId(int dealershipId) {
        this.dealershipId = dealershipId;
    }


}
