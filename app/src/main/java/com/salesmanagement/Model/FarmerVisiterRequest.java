package com.salesmanagement.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FarmerVisiterRequest {
    @SerializedName("agreeToBeMember")
    @Expose
    private Boolean agreeToBeMember;
    @SerializedName("area")
    @Expose
    private String area;
    @SerializedName("crop")
    @Expose
    private String crop;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("demoPlace")
    @Expose
    private String demoPlace;
    @SerializedName("district")
    @Expose
    private String district;
    @SerializedName("dripIrrigation")
    @Expose
    private Boolean dripIrrigation;
    @SerializedName("employeeId")
    @Expose
    private Integer employeeId;
    @SerializedName("farmerId")
    @Expose
    private Integer farmerId;
    @SerializedName("farmerVisitId")
    @Expose
    private Integer farmerVisitId;
    @SerializedName("irrigation")
    @Expose
    private String irrigation;
    @SerializedName("landConservationMember")
    @Expose
    private Boolean landConservationMember;
    @SerializedName("medicines")
    @Expose
    private String medicines;
    @SerializedName("otherCrops")
    @Expose
    private String otherCrops;
    @SerializedName("remark")
    @Expose
    private String remark;
    @SerializedName("visitPhoto")
    @Expose
    private String visitPhoto;
    @SerializedName("waterSource")
    @Expose
    private String waterSource;
    @SerializedName("workPlace")
    @Expose
    private String workPlace;

    public Boolean getAgreeToBeMember() {
        return agreeToBeMember;
    }

    public void setAgreeToBeMember(Boolean agreeToBeMember) {
        this.agreeToBeMember = agreeToBeMember;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCrop() {
        return crop;
    }

    public void setCrop(String crop) {
        this.crop = crop;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDemoPlace() {
        return demoPlace;
    }

    public void setDemoPlace(String demoPlace) {
        this.demoPlace = demoPlace;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public Boolean getDripIrrigation() {
        return dripIrrigation;
    }

    public void setDripIrrigation(Boolean dripIrrigation) {
        this.dripIrrigation = dripIrrigation;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public Integer getFarmerId() {
        return farmerId;
    }

    public void setFarmerId(Integer farmerId) {
        this.farmerId = farmerId;
    }

    public Integer getFarmerVisitId() {
        return farmerVisitId;
    }

    public void setFarmerVisitId(Integer farmerVisitId) {
        this.farmerVisitId = farmerVisitId;
    }

    public String getIrrigation() {
        return irrigation;
    }

    public void setIrrigation(String irrigation) {
        this.irrigation = irrigation;
    }

    public Boolean getLandConservationMember() {
        return landConservationMember;
    }

    public void setLandConservationMember(Boolean landConservationMember) {
        this.landConservationMember = landConservationMember;
    }

    public String getMedicines() {
        return medicines;
    }

    public void setMedicines(String medicines) {
        this.medicines = medicines;
    }

    public String getOtherCrops() {
        return otherCrops;
    }

    public void setOtherCrops(String otherCrops) {
        this.otherCrops = otherCrops;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getVisitPhoto() {
        return visitPhoto;
    }

    public void setVisitPhoto(String visitPhoto) {
        this.visitPhoto = visitPhoto;
    }

    public String getWaterSource() {
        return waterSource;
    }

    public void setWaterSource(String waterSource) {
        this.waterSource = waterSource;
    }

    public String getWorkPlace() {
        return workPlace;
    }

    public void setWorkPlace(String workPlace) {
        this.workPlace = workPlace;
    }

}
