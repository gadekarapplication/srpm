package com.salesmanagement.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class EmployeeResponse implements Parcelable {

    @SerializedName("employeeId")
    private Integer employeeId;

    @SerializedName("employeeName")
    private String employeeName;

    public EmployeeResponse(){

    }
    protected EmployeeResponse(Parcel in) {
        if (in.readByte() == 0) {
            employeeId = null;
        } else {
            employeeId = in.readInt();
        }
        employeeName = in.readString();
    }

    public static final Creator<EmployeeResponse> CREATOR = new Creator<EmployeeResponse>() {
        @Override
        public EmployeeResponse createFromParcel(Parcel in) {
            return new EmployeeResponse(in);
        }

        @Override
        public EmployeeResponse[] newArray(int size) {
            return new EmployeeResponse[size];
        }
    };

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    @Override
    public String toString() {
        return   employeeName ;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (employeeId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(employeeId);
        }
        dest.writeString(employeeName);
    }
}
