package com.salesmanagement.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CollectionAndSaleResponse {

    @Expose
    @SerializedName("mainCollectionList")
    List<CollectionsReportResponse> collectionsReportResponseList;

    @Expose
    @SerializedName("mainSaleList")
    List<SalesReportResponse> salesReportResponseList;

    public List<CollectionsReportResponse> getCollectionsReportResponseList() {
        return collectionsReportResponseList;
    }

    public void setCollectionsReportResponseList(List<CollectionsReportResponse> collectionsReportResponseList) {
        this.collectionsReportResponseList = collectionsReportResponseList;
    }

    public List<SalesReportResponse> getSalesReportResponseList() {
        return salesReportResponseList;
    }

    public void setSalesReportResponseList(List<SalesReportResponse> salesReportResponseList) {
        this.salesReportResponseList = salesReportResponseList;
    }
}
