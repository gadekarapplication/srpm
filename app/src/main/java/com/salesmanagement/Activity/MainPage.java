package com.salesmanagement.Activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.navigation.NavigationView;
import com.salesmanagement.Extra.Common;
import com.salesmanagement.Fragment.AttendanceViewFragment;
import com.salesmanagement.Fragment.BlankVisitListFragment;
import com.salesmanagement.Fragment.ChequeListFragment;
import com.salesmanagement.Fragment.CollectionsListFragment;
import com.salesmanagement.Fragment.DealerListFragment;
import com.salesmanagement.Fragment.EmployeesReportFragment;
import com.salesmanagement.Fragment.ExpenseFragment;
import com.salesmanagement.Fragment.FarmerListFragment;
import com.salesmanagement.Fragment.Home;
import com.salesmanagement.Fragment.LeaveFragment;
import com.salesmanagement.Fragment.LeaveListFragment;
import com.salesmanagement.Fragment.OrderHistory;
import com.salesmanagement.Fragment.PurchaseOrders;
import com.salesmanagement.Fragment.ReportsFragment;
import com.salesmanagement.Fragment.ViewAttendance;
import com.salesmanagement.R;

import java.io.File;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainPage extends AppCompatActivity {

    public static ImageView menu, back, iv_logout;
    public static DrawerLayout drawerLayout;
    public static TextView title, userNameTxt, userDesignation,tvCount;
    public static LinearLayout toolbarContainer, llProductListCount;
    public static String userId, userName, struserDesignation, currency = "₹",roleName="";
    public static NavigationView navigationView;
    boolean doubleBackToExitPressedOnce = false;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        toolbarContainer = findViewById(R.id.toolbar_container);
        llProductListCount=findViewById(R.id.llProductListCount);
        tvCount=findViewById(R.id.tvCount);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        navigationView = findViewById(R.id.navigationView);
        View header = navigationView.getHeaderView(0);
        userNameTxt = header.findViewById(R.id.userName);
        userDesignation = header.findViewById(R.id.userDesignation);
        initViews();

        try {

            userId = Common.getSavedUserData(MainPage.this, "userId");
            userName = Common.getSavedUserData(MainPage.this, "userName");
            struserDesignation = Common.getSavedUserData(MainPage.this, "userNumber");
            roleName=Common.getSavedUserData(MainPage.this,"roleName");


        } catch (Exception e) {
            e.printStackTrace();
        }

        if(roleName.equals("FO")||roleName.equals("DO")) {
            System.out.println("Hide Item");
            hideItem();
        }
        userNameTxt.setText(userName);
        userDesignation.setText(roleName);
        loadFragment(new Home(), false);
        drawerLayout.closeDrawers();


        //Common.saveUserData(MainPage.this,"roleName","FO");


        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.home:
                        loadFragment(new Home(), true);
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.dealerRegistration:
                        loadFragment(new DealerListFragment(), true);
                        break;
                    case R.id.farmer:
                        loadFragment(new FarmerListFragment(), true);
                        break;
                    case R.id.collections:
                        loadFragment(new CollectionsListFragment(),true);
                        break;
                    case R.id.expense:
                           loadFragment(new ExpenseFragment(),true);
                           break;
                    case R.id.order:
                        loadFragment(new OrderHistory(), true);
                        break;
                    case R.id.cheque:
                        loadFragment(new ChequeListFragment(), true);
                        break;
                    case R.id.attendance:
                        loadFragment(new AttendanceViewFragment(), true);
                        break;
                    case R.id.reports:
                        loadFragment(new ReportsFragment(),true);
                        break;
                    case R.id.emp_reports:
                        loadFragment(new EmployeesReportFragment(),true);
                        break;
                    case R.id.blank_visit:
                        loadFragment(new BlankVisitListFragment(),true);
                        break;
                    case R.id.leaveApplication:
                        loadFragment(new LeaveListFragment(),true);
                        break;
                    case R.id.logout:
                        showLogoutDialog();
                        //logoutEmployee();
                        break;

                }

                return false;
            }
        });


    }
    private void hideItem()
    {

        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.dealerRegistration).setVisible(false);
        nav_Menu.findItem(R.id.collections).setVisible(false);

        nav_Menu.findItem(R.id.cheque).setVisible(false);
        nav_Menu.findItem(R.id.blank_visit).setVisible(false);
        nav_Menu.findItem(R.id.order).setVisible(false);
        nav_Menu.findItem(R.id.leaveApplication).setVisible(false);
    }
    private void logoutEmployee() {
        Common.saveUserData(MainPage.this, "userId", "");
        Common.saveUserData(MainPage.this, "userName", "");
        Common.saveUserData(MainPage.this, "roleName", "");
        pref = getSharedPreferences("user", Context.MODE_PRIVATE);
        editor = pref.edit();
        editor.clear();
        editor.commit();

        File file1 = new File("data/data/com.salesmanagement/shared_prefs/user.xml");
        if (file1.exists()) {
            System.out.println("File is deleted");
            file1.delete();
        }
        else {
            System.out.println("File not exist");
        }

        Intent intent = new Intent(MainPage.this, Login.class);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);


    }

    @SuppressLint("CutPasteId")
    private void initViews() {

        drawerLayout = findViewById(R.id.drawer_layout);
        title = findViewById(R.id.title);
        menu = findViewById(R.id.menu);
        back = findViewById(R.id.back);
        iv_logout = findViewById(R.id.iv_logout);

    }

    @SuppressLint("RtlHardcoded")
    @OnClick({R.id.menu, R.id.back, R.id.iv_logout})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.menu:
                if (!MainPage.drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                    MainPage.drawerLayout.openDrawer(Gravity.LEFT);
                }
                break;

            case R.id.back:
                removeCurrentFragmentAndMoveBack();
                break;

            case R.id.iv_logout:
                showLogoutDialog();
                break;

        }
    }

    private void call() {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + getResources().getString(R.string.contactNo)));
        startActivity(intent);
    }


    @Override
    public void onBackPressed() {
        // double press to exit
       /* if (menu.getVisibility() == View.VISIBLE) {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }
        } else {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toasty.normal(MainPage.this, "Press back once more to exit", Toasty.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);*/
      /*  super.onBackPressed();
        getFragmentManager().popBackStackImmediate();*/

        FragmentManager fragmentManager = getSupportFragmentManager();
        //fragmentManager.popBackStack();
        if (fragmentManager.getBackStackEntryCount() > 0) {
            Log.i("MainActivity", "popping backstack");
            fragmentManager.popBackStack();
        } else {
            Log.i("MainActivity", "nothing on backstack, calling super");
            super.onBackPressed();
        }

    }

    public void lockUnlockDrawer(int lockMode) {
        drawerLayout.setDrawerLockMode(lockMode);
        if (lockMode == DrawerLayout.LOCK_MODE_LOCKED_CLOSED) {
            menu.setVisibility(View.GONE);
            back.setVisibility(View.VISIBLE);
            title.setVisibility(View.VISIBLE);
        } else {
            menu.setVisibility(View.VISIBLE);
            back.setVisibility(View.GONE);
            title.setVisibility(View.GONE);
        }

    }

    public void removeCurrentFragmentAndMoveBack() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStack();
    }

    public void loadFragment(Fragment fragment, Boolean bool) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frameLayout, fragment);
        if (bool) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    private void showLogoutDialog() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainPage.this);
        // Setting Dialog Title
        alertDialog.setTitle("");
        // Setting Dialog Message
        alertDialog.setMessage(R.string.str_app_close_warn);

        // On pressing Settings button
        alertDialog.setPositiveButton(R.string.str_yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                logoutEmployee();                //finish();
            }
        });


        // on pressing cancel button
        alertDialog.setNegativeButton(R.string.str_no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();

    }

}