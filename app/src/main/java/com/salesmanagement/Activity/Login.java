package com.salesmanagement.Activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.salesmanagement.Extra.Common;
import com.salesmanagement.Extra.DetectConnection;
import com.salesmanagement.Model.EmployeeLoginRequest;
import com.salesmanagement.Model.EmployeeLoginResponse;
import com.salesmanagement.R;
import com.salesmanagement.Retrofit.Api;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity {

    public String modelName;
    @BindViews({R.id.email, R.id.password})
    List<TextInputEditText> textInputEditTexts;
    @BindViews({R.id.iv_passShow})
    List<ImageView> imageViews;
    SharedPreferences pref;
    @BindView(R.id.appVersion)
    TextView appVersion;
    SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

         File file = new File("data/data/com.salesmanagement/shared_prefs/user.xml");
                            if (file.exists()) {
                                System.out.println("File exists");
                                Intent intent = new Intent(Login.this, MainPage.class);
                                startActivity(intent);
                                finish();
                            }
                            else {
                                System.out.println("File not exists");
                            }

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy =
                    new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            appVersion.setText("v" + version + " Live");
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        textInputEditTexts.get(1).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                try {

                    if (textInputEditTexts.get(1).getText().toString().length() > 0) {
                        imageViews.get(0).setVisibility(View.VISIBLE);
                    } else {
                        imageViews.get(0).setVisibility(View.GONE);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        modelName = getDeviceName();

    }

    @OnClick({R.id.login_button_card_view, R.id.iv_passShow})
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.login_button_card_view:
                if (DetectConnection.checkInternetConnection(Login.this)) {
                    validateLogin();
                } else {
                    Toasty.warning(Login.this, "No Internet Connection", Toasty.LENGTH_SHORT, true).show();
                }

                break;

            case R.id.iv_passShow:

                if (textInputEditTexts.get(1).getTransformationMethod().equals(PasswordTransformationMethod.getInstance())) {
                    imageViews.get(0).setImageResource(R.drawable.ic_hide);
                    textInputEditTexts.get(1).setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    imageViews.get(0).setImageResource(R.drawable.ic_look);
                    textInputEditTexts.get(1).setTransformationMethod(PasswordTransformationMethod.getInstance());
                }

                textInputEditTexts.get(1).setSelection(textInputEditTexts.get(1).getText().toString().length());

                break;

        }

    }

    private void validateLogin() {
        String mobileNumber = textInputEditTexts.get(0).getText().toString();
        String password = textInputEditTexts.get(1).getText().toString();
        if (mobileNumber.equals("")) {
            textInputEditTexts.get(0).setError("Please Enter mobile number");

        } else if (password.equals("")) {
            textInputEditTexts.get(1).setError("Please enter password");
        } else {
            EmployeeLoginRequest employeeLoginRequest = new EmployeeLoginRequest();
            employeeLoginRequest.setEmployeeMobileNo(mobileNumber);
            employeeLoginRequest.setEmployeePassword(password);
            callToLoginEmployee(employeeLoginRequest);
        }


    }

    private void callToLoginEmployee(EmployeeLoginRequest employeeLoginRequest) {

        ProgressDialog progressDialog = new ProgressDialog(Login.this);
        progressDialog.setMessage("Loading...");
        progressDialog.setTitle("Account is in verification");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        progressDialog.setCancelable(false);

        Call<EmployeeLoginResponse> call = Api.getClient().employeeLogin(employeeLoginRequest);
        call.enqueue(new Callback<EmployeeLoginResponse>() {
            @Override
            public void onResponse(Call<EmployeeLoginResponse> call, Response<EmployeeLoginResponse> response) {

                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        EmployeeLoginResponse employeeLoginResponse = response.body();
                        if (response.body().getMessage().equals("Login Success")) {

                            progressDialog.dismiss();

                            pref = getSharedPreferences("user", Context.MODE_PRIVATE);
                            editor = pref.edit();
                            editor.putString("UserLogin", "UserLoginSuccessful");
                            editor.commit();

                            Common.saveUserData(Login.this,"roleName",response.body().getRoleName());
                            Common.saveUserData(Login.this, "userId", "" + response.body().getEmployeeId());
                            Common.saveUserData(Login.this,"userName",""+response.body().getEmployeeName());
                            Intent intent = new Intent(Login.this, MainPage.class);
                            startActivity(intent);

                        } else {
                            progressDialog.dismiss();
                            Toasty.error(Login.this, "" + employeeLoginResponse.getMessage(), Toasty.LENGTH_SHORT, true).show();
                        }

                    } else {
                        progressDialog.dismiss();
                        Log.e("TAG", "" + response.body());
                        Toast.makeText(Login.this, "Login Failed...", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    progressDialog.dismiss();
                    Log.e("TAG", "" + response.body());
                    Toast.makeText(Login.this, "Login Failed...", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<EmployeeLoginResponse> call, Throwable t) {

                progressDialog.dismiss();
                Log.e("TAG", "Server Error");
            }
        });


    }

    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }


    private String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    private boolean validatePassword(TextInputEditText textInputEditText) {
        if (textInputEditText.getText().toString().trim().length() > 5) {
            return true;
        } else if (textInputEditText.getText().toString().trim().length() > 0) {
            textInputEditText.setError("Password must be of 6 characters");
            textInputEditText.requestFocus();
            return false;
        }
        textInputEditText.setError("Please Fill This");
        textInputEditText.requestFocus();
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }

    @Override
    protected void onStart() {
        super.onStart();
        requestPermission();
    }

    private void requestPermission() {

        Dexter.withActivity(Login.this)
                .withPermissions(
                        android.Manifest.permission.READ_EXTERNAL_STORAGE,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        android.Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            createFile();
                        }
                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(Login.this, "Error occurred! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();

    }

    private void createFile() {

        try {

            String year = new SimpleDateFormat("yyyy").format(new Date());
            String month = new SimpleDateFormat("MMMM").format(new Date());
            Log.e("dateFormat", "" + year + "/" + month);

            File dir = new File(Environment.getExternalStorageDirectory(), "/KenSystems/");
            try {
                if (!dir.exists()) {
                    dir.mkdir();
                    Log.e("files", "Created" + dir);
                } else {

                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void showSettingsDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }

        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

}