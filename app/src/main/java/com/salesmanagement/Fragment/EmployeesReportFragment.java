package com.salesmanagement.Fragment;

import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Extra.DetectConnection;
import com.salesmanagement.Model.EmployeeResponse;
import com.salesmanagement.Model.VisitorReportResponse;
import com.salesmanagement.R;
import com.salesmanagement.Retrofit.Api;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.salesmanagement.Extra.Common.getSavedUserData;


public class EmployeesReportFragment extends Fragment {


    List<EmployeeResponse> employeeResponseList=new ArrayList<>();
    Integer empId;
    Integer selectedEmpId;
    @BindView(R.id.spinnerEmployee)
    SearchableSpinner spinnerEmployee;
    @BindViews({R.id.cv_report_attendance,R.id.cv_order_report,
            R.id.cv_visit_report,R.id.cv_sale_report,R.id.cv_Collection_report})
    List<CardView> cardViewList;
    public EmployeesReportFragment() {
        // Required empty public constructor
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_employees_report, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        MainPage.title.setText("Employee Reports");
        ((MainPage) getActivity()).lockUnlockDrawer(1);
        if (DetectConnection.checkInternetConnection(getActivity())) {
            init();


        } else {
            DetectConnection.noInternetConnection(getActivity());
        }

    }

    private void init() {
        //setDataToAdapter(employeeResponseList);
        try {
            empId = Integer.parseInt(getSavedUserData(getActivity(), "userId"));
        }catch (Exception e){
            e.printStackTrace();
        }
            callToGetEmployeeList(empId);


            cardViewList.get(0).setOnClickListener(view->{
                if(selectedEmpId!=null) {
                    AttendanceViewFragment attendanceViewFragment = new AttendanceViewFragment();

                    Bundle bundle = new Bundle();

                    bundle.putString("EMPLOYEE_ID", String.valueOf(selectedEmpId));
                    attendanceViewFragment.setArguments(bundle);
                    ((MainPage) getActivity()).loadFragment(attendanceViewFragment, true);
                }
                else {
                    Toast.makeText(getActivity(), "Select Employee ", Toast.LENGTH_SHORT).show();
                }

            });

            cardViewList.get(1).setOnClickListener(view->{
                System.out.println("On Click Selected Emp:"+selectedEmpId);
                if(selectedEmpId!=null) {
                    OrderHistory orderHistory = new OrderHistory();

                    Bundle bundle = new Bundle();

                    bundle.putString("EMPLOYEE_ID", String.valueOf(selectedEmpId));
                    orderHistory.setArguments(bundle);
                    ((MainPage) getActivity()).loadFragment(orderHistory, true);
                }
                else {
                    Toast.makeText(getActivity(), "Select Employee ", Toast.LENGTH_SHORT).show();
                }

            });
            cardViewList.get(2).setOnClickListener(view->{
                System.out.println("On Click Selected Emp:"+selectedEmpId);
                if(selectedEmpId!=null) {
                    VisitorFragment visitorFragment = new VisitorFragment("Visit");

                    Bundle bundle = new Bundle();

                    bundle.putString("EMPLOYEE_ID", String.valueOf(selectedEmpId));
                    visitorFragment.setArguments(bundle);
                    ((MainPage) getActivity()).loadFragment(visitorFragment, true);
                }
                else {
                    Toast.makeText(getActivity(), "Select Employee ", Toast.LENGTH_SHORT).show();
                }
            });
            cardViewList.get(3).setOnClickListener(view->{
                System.out.println("On Click Selected Emp:"+selectedEmpId);
                if(selectedEmpId!=null) {
                    VisitorFragment visitorFragment = new VisitorFragment("Sales");

                    Bundle bundle = new Bundle();

                    bundle.putString("EMPLOYEE_ID", String.valueOf(selectedEmpId));
                    visitorFragment.setArguments(bundle);
                    ((MainPage) getActivity()).loadFragment(visitorFragment, true);
                }
                else {
                    Toast.makeText(getActivity(), "Select Employee ", Toast.LENGTH_SHORT).show();
                }

            });
            cardViewList.get(4).setOnClickListener(view->{
                System.out.println("On Click Selected Emp:"+selectedEmpId);
                if(selectedEmpId!=null) {
                    VisitorFragment visitorFragment = new VisitorFragment("Collections");

                    Bundle bundle = new Bundle();

                    bundle.putString("EMPLOYEE_ID", String.valueOf(selectedEmpId));
                    visitorFragment.setArguments(bundle);
                    ((MainPage) getActivity()).loadFragment(visitorFragment, true);
                }
                else {
                    Toast.makeText(getActivity(), "Select Employee ", Toast.LENGTH_SHORT).show();
                }

            });

        }


    private void callToGetEmployeeList(Integer empId) {

        Call<List<EmployeeResponse>> call= Api.getClient().getEmployees(empId);
        call.enqueue(new Callback<List<EmployeeResponse>>() {
            @Override
            public void onResponse(Call<List<EmployeeResponse>> call, Response<List<EmployeeResponse>> response) {
                try {
                    employeeResponseList.clear();
                    if(response.body()!=null){
                          employeeResponseList=response.body();

                    }
                    else {

                    }
                    setDataToAdapter(employeeResponseList);
                }catch (Exception e){
                    e.printStackTrace();
                }
                finally {


                }
            }

            @Override
            public void onFailure(Call<List<EmployeeResponse>> call, Throwable t) {
                try {

                }catch (Exception e){
                    e.printStackTrace();
                }
                finally {

                }
            }
        });



    }

    private void setDataToAdapter(List<EmployeeResponse>  arrayList)
    {
        final EmployeeResponse[] employeeResponse = {new EmployeeResponse()};
        employeeResponse[0].setEmployeeName("Select Employee");



        arrayList.add(0, employeeResponse[0]);


        // adding list values to set


        // removing all values from list
        //arrayList.clear();

        // add all values from set to list.


        // Creating ArrayAdapter using the string array and default spinner layout
        ArrayAdapter<EmployeeResponse> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, arrayList);
        //ArrayAdapter<HospitalModel> arrayAdapter =  ArrayAdapter.createFromResource(this,arrayList,R.layout.spinner_text_view_unit);
        // Specify layout to be used when list of choices appears
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Applying the adapter to our spinner
        /*spinnerEmployee.setPrompt("Select Employee");*/

        spinnerEmployee.setTitle("Select Employee");


        spinnerEmployee.setAdapter(arrayAdapter);

        spinnerEmployee.setSelection(0,false);

        spinnerEmployee.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long l) {
                ((TextView)parent.getChildAt(0)).setTextSize(TypedValue.COMPLEX_UNIT_DIP,13);
                ((TextView)parent.getChildAt(0)).setHint("Select Employee");
                 EmployeeResponse employeeResponse1 =(EmployeeResponse)parent.getSelectedItem();
                 selectedEmpId= employeeResponse1.getEmployeeId();

                System.out.println("Selected Emp:"+selectedEmpId);


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });





    }
}