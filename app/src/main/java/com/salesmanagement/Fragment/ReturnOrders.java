package com.salesmanagement.Fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Extra.DetectConnection;
import com.salesmanagement.R;

import butterknife.ButterKnife;


public class ReturnOrders extends Fragment {

    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_return_orders, container, false);
        ButterKnife.bind(this, view);
        MainPage.title.setText("");


        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e("onStart", "called");
        ((MainPage) getActivity()).lockUnlockDrawer(1);
        if (DetectConnection.checkInternetConnection(getActivity())) {

        } else {
            DetectConnection.noInternetConnection(getActivity());
        }
    }
}