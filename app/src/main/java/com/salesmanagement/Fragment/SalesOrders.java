package com.salesmanagement.Fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputEditText;
import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Adapter.CartAdapter;
import com.salesmanagement.Adapter.SearchableProductAdapter;
import com.salesmanagement.Extra.DetectConnection;
import com.salesmanagement.Model.CartResponse;
import com.salesmanagement.Model.CategoryResponse;
import com.salesmanagement.Model.CompanyResponse;
import com.salesmanagement.Model.DealerResponse;
import com.salesmanagement.Model.FarmerResponse;
import com.salesmanagement.Model.OrderResponse;
import com.salesmanagement.Model.ProductResponse;
import com.salesmanagement.Model.SizeWiseProduct;
import com.salesmanagement.R;
import com.salesmanagement.Retrofit.Api;
import com.salesmanagement.popup.PopupProductList;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import fr.ganfra.materialspinner.MaterialSpinner;
import gr.escsoft.michaelprimez.searchablespinner.interfaces.OnItemSelectedListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.salesmanagement.Activity.MainPage.llProductListCount;
import static com.salesmanagement.Activity.MainPage.tvCount;

public class SalesOrders extends Fragment {


    public String[] dealerIdList, dealerNameList, dealerAddressList, companyIdList, companyNameList, categoryIdList, categoryNameList, productIdList, productNameList, sizeIdList, sizeNameList, amountList, productGstList;
    public String dealerType = "Dealer", dealerId, dealerName, dealerAddress, companyId, companyName, categoryId, categoryName, productId, productName, sizeId, sizeName, productAmount;
    public View view;
    public RadioButton radioButton;
    public List<CompanyResponse> companyResponseList = new ArrayList<>();
    public List<CategoryResponse> categoryResponseList = new ArrayList<>();
    public List<ProductResponse> productResponseList = new ArrayList<>();
    public List<SizeWiseProduct> sizeWiseProductList = new ArrayList<>();
    public List<DealerResponse> dealerResponseList = new ArrayList<>();
    public List<FarmerResponse> farmerResponseList = new ArrayList<>();
    public List<CartResponse> cartResponseList = new ArrayList<>();
    public double orderAmount = 0f;
    public String[] discountList = {"% (Percentage)", "₹ (Amount)"};
    public String[] gstList = {"0 %", "3 %", "5 %", "9 %", "12 %", "15 %", "18 %", "21 %", "24 %", "27 %"};
    public String discountType;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.linearLayout)
    LinearLayout linearLayout;
    @BindViews({R.id.companySpinner, R.id.categorySpinner,
            R.id.productSizeSpinner, R.id.dealerSpinner,
            R.id.gstTypeSpinner})
    List<MaterialSpinner> materialSpinners;
    /*@BindView(R.id.productSpinner)
    gr.escsoft.michaelprimez.searchablespinner.SearchableSpinner productSpinner;*/
    @BindViews({R.id.productAmount, R.id.productQuantity, R.id.discountAmount,
            R.id.productDiscountAmount, R.id.productFinalAmount, R.id.productGstAmount,
            R.id.productDiscountInPercent,R.id.tieProductName,R.id.tieFirmName})
    List<TextInputEditText> textInputEditTexts;
    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;
    @BindView(R.id.discountTypeSpinner)
    Spinner discountTypeSpinner;
    @BindViews({R.id.subTotal, R.id.totalAmount, R.id.goToCart})
    List<TextView> textViews;
    @BindView(R.id.pbAddToCart)
    ProgressBar pbAddToCart;
    @BindView(R.id.tv_button_label)
    AppCompatTextView tv_button_label;
   /* @BindView(R.id.llProductListCount)
    LinearLayout llProductListCount;
    @BindView(R.id.tvCount)
    TextView tvCount;*/
    SearchableProductAdapter searchableProductAdapter;
    @BindView(R.id.tvSelectProduct)
    TextView tvSelectProduct;
    Dialog dialogFirm;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_sales_orders, container, false);
        ButterKnife.bind(this, view);
        MainPage.title.setText("Order");

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy =
                    new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }


       /* MainPage.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //getCloseDialog();
                ((MainPage)getActivity()).removeCurrentFragmentAndMoveBack();
            }
        });*/

       /* view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    Log.i("tag", "onKey Back listener is working!!!");
                    getCloseDialog();
                    return true;
                }
                return false;
            }
        });*/


        initalProductAdapter();
        pbAddToCart.setVisibility(View.GONE);
        textInputEditTexts.get(0).setSelection(textInputEditTexts.get(0).getText().toString().length());
        textInputEditTexts.get(1).setSelection(textInputEditTexts.get(1).getText().toString().length());
        textInputEditTexts.get(2).setSelection(textInputEditTexts.get(2).getText().toString().length());
        textInputEditTexts.get(3).setSelection(textInputEditTexts.get(3).getText().toString().length());
        textInputEditTexts.get(4).setSelection(textInputEditTexts.get(4).getText().toString().length());
        textInputEditTexts.get(5).setSelection(textInputEditTexts.get(5).getText().toString().length());

        try {
            final ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, discountList);
            adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
            discountTypeSpinner.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        discountTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                discountType = discountList[position];
                try {
                    textInputEditTexts.get(3).setSelection(textInputEditTexts.get(3).getText().toString().length());
                    if (textInputEditTexts.get(3).getText().toString().length() > 0) {
                        if (discountType.equalsIgnoreCase("% (Percentage)")) {
                            double subAmount = Double.parseDouble(textInputEditTexts.get(0).getText().toString()) * Double.parseDouble(textInputEditTexts.get(1).getText().toString());
                            double discountAmount = Double.parseDouble(textInputEditTexts.get(3).getText().toString().trim());
                            double gstAmount = Double.parseDouble(textInputEditTexts.get(5).getText().toString().trim());
                            double discount = subAmount * (discountAmount / 100);
                            discount = subAmount - discount;
                            discount = discount + (discount * (gstAmount / 100f));
                            String discountTotal = String.format("%.2f", (discount));
                            textInputEditTexts.get(4).setText("" + discountTotal);
                        } else if (discountType.equalsIgnoreCase("₹ (Amount)")) {
                            double subAmount = Double.parseDouble(textInputEditTexts.get(0).getText().toString()) * Double.parseDouble(textInputEditTexts.get(1).getText().toString());
                            double discountAmount = Double.parseDouble(textInputEditTexts.get(3).getText().toString().trim());
                            double gstAmount = Double.parseDouble(textInputEditTexts.get(5).getText().toString().trim());
                            double discount = subAmount - discountAmount;
                            discount = discount + (discount * (gstAmount / 100f));
                            String discountTotal = String.format("%.2f", (discount));
                            textInputEditTexts.get(4).setText("" + discountTotal);
                        }
                    } else {
                        try {
                            double subAmount = Double.parseDouble(textInputEditTexts.get(0).getText().toString()) * Double.parseDouble(textInputEditTexts.get(1).getText().toString());
                            String discountTotal = String.format("%.2f", (subAmount));
                            textInputEditTexts.get(4).setText("" + discountTotal);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        textInputEditTexts.get(1).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
               try {

                    if (textInputEditTexts.get(6).getText().toString().length() > 0) {
                       // if (discountType.equalsIgnoreCase("% (Percentage)")) {
                            double subAmount = Double.parseDouble(textInputEditTexts.get(0).getText().toString()) * Double.parseDouble(textInputEditTexts.get(1).getText().toString());
                            double discountAmount = Double.parseDouble(textInputEditTexts.get(6).getText().toString().trim());
                            double gstAmount = Double.parseDouble(textInputEditTexts.get(5).getText().toString().trim());
                            double discount = subAmount * (discountAmount / 100f);

                            textInputEditTexts.get(3).setText(" "+discount);
                            discount = subAmount - discount;
                            discount = discount + (discount * (gstAmount / 100f));
                            String discountTotal = String.format("%.2f", (discount));
                            textInputEditTexts.get(4).setText("" + discountTotal);

                          } /*else {
                        try {
                            double subAmount = Double.parseDouble(textInputEditTexts.get(0).getText().toString()) * Double.parseDouble(textInputEditTexts.get(1).getText().toString());
                            String discountTotal = String.format("%.2f", (subAmount));
                            textInputEditTexts.get(4).setText("" + discountTotal);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }*/

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        textInputEditTexts.get(6).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    textInputEditTexts.get(6).setSelection(textInputEditTexts.get(6).getText().toString().length());
                    if (textInputEditTexts.get(6).getText().toString().length() > 0) {
                       // if (discountType.equalsIgnoreCase("% (Percentage)")) {
                            double subAmount = Double.parseDouble(textInputEditTexts.get(0).getText().toString()) * Double.parseDouble(textInputEditTexts.get(1).getText().toString());
                            double discountAmount = Double.parseDouble(textInputEditTexts.get(6).getText().toString().trim());
                            double gstAmount = Double.parseDouble(textInputEditTexts.get(5).getText().toString().trim());
                            double discount = subAmount * (discountAmount / 100f);

                            textInputEditTexts.get(3).setText(" "+ String.format("%.2f",discount));
                            discount = subAmount - discount;
                            discount = discount + (discount * (gstAmount / 100f));
                            String discountTotal = String.format("%.2f", (discount));
                            textInputEditTexts.get(4).setText("" + discountTotal);




                        /*} else if (discountType.equalsIgnoreCase("₹ (Amount)")) {
                            double subAmount = Double.parseDouble(textInputEditTexts.get(0).getText().toString()) * Double.parseDouble(textInputEditTexts.get(1).getText().toString());
                            double discountAmount = Double.parseDouble(textInputEditTexts.get(6).getText().toString().trim());
                            double gstAmount = Double.parseDouble(textInputEditTexts.get(5).getText().toString().trim());
                            double discount = subAmount - discountAmount;
                            discount = discount + (discount * (gstAmount / 100f));
                            String discountTotal = String.format("%.2f", (discount));
                            textInputEditTexts.get(4).setText("" + discountTotal);
                        }*/
                    } else {
                        try {
                            double subAmount = Double.parseDouble(textInputEditTexts.get(0).getText().toString()) * Double.parseDouble(textInputEditTexts.get(1).getText().toString());
                            String discountTotal = String.format("%.2f", (subAmount));
                            textInputEditTexts.get(4).setText("" + discountTotal);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        textInputEditTexts.get(8).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dealerNameList.length>0) {
                    dialogFirm = new Dialog(getActivity());

                    dialogFirm.setContentView(R.layout.drawable_searchable_spinner);

                    // dialogFirm.getWindow().setLayout(650,800);
                    Window window = dialogFirm.getWindow();
                    window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                    dialogFirm.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialogFirm.show();

                    EditText editText = dialogFirm.findViewById(R.id.etFirm);
                    ListView lview = dialogFirm.findViewById(R.id.listViewFirm);

                    ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, dealerNameList);
                    lview.setAdapter(adapter);

                    editText.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                            adapter.getFilter().filter(charSequence);
                        }

                        @Override
                        public void afterTextChanged(Editable editable) {

                        }
                    });
                    lview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            textInputEditTexts.get(8).setText(adapter.getItem(i));
                            dialogFirm.dismiss();

                            dealerId = dealerIdList[i];
                            dealerName = dealerNameList[i];
                            dealerAddress = dealerAddressList[i];
                            getCompanyList();

                        }
                    });
                }
            }
        });
        materialSpinners.get(0).setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                try {

                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                    if (dealerId != null) {
                        companyId = companyIdList[position];
                        companyName = companyNameList[position];
                        getCartList(dealerId, companyId);
                        getCategoryList(companyId);
                    } else {
                        Toast.makeText(getActivity(), "Please select dealer", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        materialSpinners.get(1).setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                try {

                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                    if (companyId != null) {
                        categoryId = categoryIdList[position];
                        categoryName = categoryNameList[position];
                        getProductList(categoryId);
                    } else {
                        Toasty.error(getActivity(), "Please select company", Toasty.LENGTH_SHORT, true).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /*materialSpinners.get(2).setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                try {

                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                    if (categoryId != null) {
                        productId = productIdList[position];
                        productName = productNameList[position];
                        textInputEditTexts.get(0).setText("");
                        textInputEditTexts.get(1).setText("");
                        getSizeList(productId);
                    } else {
                        Toasty.error(getActivity(), "Please select category", Toasty.LENGTH_SHORT, true).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

        materialSpinners.get(2).setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                try {

                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                    if (productId != null) {
                        sizeId = sizeIdList[position];
                        sizeName = sizeNameList[position];
                        productAmount = amountList[position];
                        productAmount = String.format("%.2f", Double.parseDouble(productAmount));
                        String productGstAmount = productGstList[position];
                        productGstAmount = String.format("%.2f", Double.parseDouble(productGstAmount));
                        textInputEditTexts.get(0).setText("" + productAmount);
                        textInputEditTexts.get(5).setText("" + productGstAmount);
                    } else {
                        Toasty.error(getActivity(), "Please select product", Toasty.LENGTH_SHORT, true).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        materialSpinners.get(3).setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                try {

                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                    if (dealerType != null) {
                        dealerId = dealerIdList[position];
                        dealerName = dealerNameList[position];
                        dealerAddress = dealerAddressList[position];
                        getCompanyList();
                    } else {
                        Toasty.error(getActivity(), "Please select purchase type", Toasty.LENGTH_SHORT, true).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        try {
            final ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, gstList);
            adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
            materialSpinners.get(4).setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }


        llProductListCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyCart myCart = new MyCart();
                Bundle bundle = new Bundle();
                bundle.putString("dealerId", "" + dealerId);
                bundle.putString("companyId", "" + companyId);
                myCart.setArguments(bundle);
                ((MainPage) getActivity()).loadFragment(myCart, true);
            }
        });
        textInputEditTexts.get(7).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("OnClickProduct");
                if(productResponseList.size()==0){
                    Toast.makeText(getActivity(), "Product list not found", Toast.LENGTH_SHORT).show();
                }
                else {
                    PopupProductList popupProductList=new PopupProductList(productResponseList);
                    popupProductList.showPopup(getActivity());

                    popupProductList.setCallBack(new PopupProductList.CallBack() {
                        @Override
                        public void callback(String value, String Id) {
                            //Toast.makeText(getActivity(), "ProdctName:"+value+"ProductId:"+Id, Toast.LENGTH_SHORT).show();
                            textInputEditTexts.get(7).setText(value);
                            productId=Id;
                            getSizeList(productId);
                        }
                    });

                }
            }
        });
        return view;

    }

    private void initalProductAdapter() {

        /*productSpinner=new SearchableSpinner(getActivity());
        productSpinner.setTitle("Select product");
        productSpinner.setPrompt("Select product");*/
        tvSelectProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });
    }

    private void getCloseDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false);
        builder.setMessage("Do you want to exit this page ?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    ((MainPage) getActivity()).removeCurrentFragmentAndMoveBack();
                    ((MainPage) getActivity()).loadFragment(new Home(), false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Create the alert dialog and change Buttons colour
        AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                dialog.getButton(AlertDialog.BUTTON_NEUTRAL).setTextColor(getResources().getColor(R.color.colorPrimary));
            }
        });

        dialog.show();

    }

    @OnClick({R.id.addToCart,  R.id.proceedToOrder})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.addToCart:

                if (dealerId != null) {
                    if (companyId != null) {
                        if (categoryId != null) {
                            if (productId != null) {
                                if (sizeId != null) {
                                    if (textInputEditTexts.get(0).getText().toString().length() > 0) {
                                        if (textInputEditTexts.get(1).getText().toString().length() > 0) {
                                            if (DetectConnection.checkInternetConnection(getActivity())) {
                                                addToCart(dealerId, companyId, categoryId, productId, sizeId, textInputEditTexts.get(0).getText().toString(), textInputEditTexts.get(1).getText().toString());
                                            } else {
                                                DetectConnection.noInternetConnection(getActivity());
                                            }
                                        } else {
                                            textInputEditTexts.get(1).setError("Please fill this");
                                            textInputEditTexts.get(1).requestFocus();
                                        }
                                    } else {
                                        textInputEditTexts.get(0).setError("Please fill this");
                                        textInputEditTexts.get(0).requestFocus();
                                    }
                                } else {
                                    Toast.makeText(getActivity(), "Select Product Size", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getActivity(), "Select Product", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getActivity(), "Select Category", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), "Select Company", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Select Dealer", Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.llProductListCount:

                MyCart myCart = new MyCart();
                Bundle bundle = new Bundle();
                bundle.putString("dealerId", "" + dealerId);
                bundle.putString("companyId", "" + companyId);
                myCart.setArguments(bundle);
                ((MainPage) getActivity()).loadFragment(myCart, true);

                break;

            case R.id.proceedToOrder:

                if (DetectConnection.checkInternetConnection(getActivity())) {
                    placeOrder(cartResponseList);
                } else {
                    DetectConnection.noInternetConnection(getActivity());
                }

                break;
        }
    }

    private void addToCart(String dealerId, String companyId, String catId, String proId, String sizeID, String productAmount, String productQuantity) {
        pbAddToCart.setVisibility(View.VISIBLE);
        tv_button_label.setVisibility(View.GONE);
        CartResponse cartResponse = new CartResponse();
        cartResponse.setDealershipId("" + dealerId);
        cartResponse.setCompId("" + companyId);
        cartResponse.setEmployeeId("" + MainPage.userId);
        cartResponse.setProductId("" + proId);
        cartResponse.setSizeWiseProductId("" + sizeID);
        cartResponse.setGstPercentage("" + textInputEditTexts.get(5).getText().toString());
        double subAmount = Double.parseDouble(textInputEditTexts.get(0).getText().toString());
        double discountAmount = Double.parseDouble(textInputEditTexts.get(6).getText().toString().trim());
        double gstAmountPercentage = Double.parseDouble(textInputEditTexts.get(5).getText().toString().trim());
        double discount = subAmount * (discountAmount / 100f);
        //System.out.println("Discount:"+discount);
        double totalDisAmount=(discount)*(Integer.parseInt(productQuantity));
        //System.out.println("Total Dis Amount:"+totalDisAmount);
        discount = subAmount - discount;
        //discount = discount + (discount * (gstAmountPercentage / 100f));
        //String discountTotal = String.format("%.2f", (discount));
        double gstAmount = (discount) * (gstAmountPercentage / 100f);
        double totalGSTAmount=(gstAmount)*(Integer.parseInt(productQuantity));
        //System.out.println("Total GST Amount: "+totalGSTAmount);
        cartResponse.setGstAmount(String.valueOf(totalGSTAmount));
        cartResponse.setDiscountAmount( String.valueOf(totalDisAmount));
        cartResponse.setDiscountPercentage(String.format("%.2f",discountAmount));
        /*if (discountType.equalsIgnoreCase("% (Percentage)")) {
            double subAmount = Double.parseDouble(textInputEditTexts.get(0).getText().toString());
            double discountAmount = Double.parseDouble(textInputEditTexts.get(6).getText().toString().trim());
            double gstAmountPercentage = Double.parseDouble(textInputEditTexts.get(5).getText().toString().trim());
            double discount = subAmount * (discountAmount / 100f);
            //System.out.println("Discount:"+discount);
            double totalDisAmount=(discount)*(Integer.parseInt(productQuantity));
            //System.out.println("Total Dis Amount:"+totalDisAmount);
            discount = subAmount - discount;
            //discount = discount + (discount * (gstAmountPercentage / 100f));
            //String discountTotal = String.format("%.2f", (discount));
            double gstAmount = (discount) * (gstAmountPercentage / 100f);
            double totalGSTAmount=(gstAmount)*(Integer.parseInt(productQuantity));
            //System.out.println("Total GST Amount: "+totalGSTAmount);
            cartResponse.setGstAmount(String.valueOf(totalGSTAmount));
            cartResponse.setDiscountAmount( String.valueOf(totalDisAmount));
            cartResponse.setDiscountPercentage(String.format("%.2f",discountAmount));


        } else {
            double subAmount = Double.parseDouble(textInputEditTexts.get(0).getText().toString());
            double discountAmount = Double.parseDouble(textInputEditTexts.get(6).getText().toString().trim());
            double gstAmountPercentage = Double.parseDouble(textInputEditTexts.get(5).getText().toString().trim());

            double disInPercent=(discountAmount*100)/subAmount;

            double totalDisAmount=(discountAmount)*(Integer.parseInt(productQuantity));
          //  System.out.println("Total Dis Amount:"+totalDisAmount);
            double discount = subAmount - discountAmount;

            double gstAmount = (discount) * (gstAmountPercentage / 100f);
            double totalGSTAmount=(gstAmount)*(Integer.parseInt(productQuantity));
          //  System.out.println("Total GST Amount: "+totalGSTAmount);
            cartResponse.setGstAmount(String.valueOf(totalGSTAmount));
            cartResponse.setDiscountAmount( String.valueOf(totalDisAmount));
            //System.out.println("Discount in percentage: "+String.format("%.2f",disInPercent));
            cartResponse.setDiscountPercentage(String.format("%.2f",disInPercent));
        }*/
        cartResponse.setQty("" + textInputEditTexts.get(1).getText().toString());
        cartResponse.setFinalAmount("" + textInputEditTexts.get(4).getText().toString());

        Call<Boolean> call = Api.getClient().addToCart(cartResponse);
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                pbAddToCart.setVisibility(View.GONE);
                tv_button_label.setVisibility(View.VISIBLE);
                if (response.isSuccessful()) {
                    if (response.body().booleanValue() == true) {
                        Toasty.success(getActivity(), "Product added into cart", Toasty.LENGTH_SHORT, true).show();
                        materialSpinners.get(1).setSelection(0);
                       // materialSpinners.get(2).setSelection(0);
                        materialSpinners.get(2).setSelection(0);
                        textInputEditTexts.get(5).setText("");
                        textInputEditTexts.get(0).setText("");
                        textInputEditTexts.get(1).setText("");
                        textInputEditTexts.get(6).setText("");
                        textInputEditTexts.get(4).setText("");
                        textInputEditTexts.get(3).setText("");
                        textInputEditTexts.get(7).setText("");
                        productId="";
                        sizeId="";
                        categoryId="";
                        getCartList(dealerId, companyId);
                    } else if (response.body().booleanValue() == false) {
                        Toasty.error(getActivity(), "Product fail to add into cart", Toasty.LENGTH_SHORT, true).show();
                    }
                } else {
                    Toasty.error(getActivity(), "fail to add into cart", Toasty.LENGTH_SHORT, true).show();
                }

            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                pbAddToCart.setVisibility(View.GONE);
                tv_button_label.setVisibility(View.VISIBLE);
                Log.e("cartError", "" + t.getMessage());
                Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void placeOrder(List<CartResponse> cartResponseList) {

        final SweetAlertDialog pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.colorPrimary));
        pDialog.setTitleText("Please wait");
        pDialog.setContentText("Order is placing");
        pDialog.setCancelable(false);
        pDialog.show();

        OrderResponse orderResponse = new OrderResponse();
        orderResponse.setDealershipId("" + dealerId);
        orderResponse.setOrderPaymentMode("Cash");
        orderResponse.setFinalTotalAmount("" + textViews.get(0).getText().toString().trim());
        if (discountType.equalsIgnoreCase("% (Percentage)")) {
            double subAmount = Double.parseDouble(textViews.get(0).getText().toString());
            double discountAmount = Double.parseDouble(textInputEditTexts.get(3).getText().toString().trim());
            double discount = subAmount * (discountAmount / 100);
            discount = subAmount - discount;
            orderResponse.setFinalTotalDiscountAmount("" + discount);
            orderResponse.setFinalTotalDiscountPer("" + textInputEditTexts.get(3).getText().toString().trim());
        } else {
            orderResponse.setFinalTotalDiscountAmount("" + textInputEditTexts.get(3).getText().toString().trim());
            double discountAmount = Double.parseDouble(textInputEditTexts.get(3).getText().toString().trim());
            double discount = discountAmount / 100;
            orderResponse.setFinalTotalDiscountPer("" + discount);

        }
        orderResponse.setOrderBillingAddress("" + dealerAddress);
        orderResponse.setOrderDeliveryAddress("" + dealerAddress);
        orderResponse.setCartResponseList(cartResponseList);

        Call<Boolean> call = Api.getClient().postOrder(orderResponse);
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {

                if (response.isSuccessful()) {

                    if (response.body().booleanValue() == true) {
                        pDialog.dismiss();

                        final Dialog dialog = new Dialog(getActivity());
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
                        dialog.setContentView(R.layout.confirmation_dialog);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        dialog.setCancelable(false);
                        TextView txtYes = dialog.findViewById(R.id.yes);

                        txtYes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                try {
                                    ((MainPage) getActivity()).removeCurrentFragmentAndMoveBack();
                                    ((MainPage) getActivity()).loadFragment(new Home(), true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        dialog.show();

                    } else if (response.body().booleanValue() == false) {
                        pDialog.dismiss();

                        final Dialog dialog = new Dialog(getActivity());
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
                        dialog.setContentView(R.layout.error_dialog);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        dialog.setCancelable(false);
                        TextView txtYes = dialog.findViewById(R.id.yes);

                        txtYes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                try {
                                    ((MainPage) getActivity()).removeCurrentFragmentAndMoveBack();
                                    ((MainPage) getActivity()).loadFragment(new Home(), false);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        dialog.show();

                    }

                } else {
                    pDialog.dismiss();

                    final Dialog dialog = new Dialog(getActivity());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
                    dialog.setContentView(R.layout.error_dialog);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    dialog.setCancelable(false);
                    TextView txtYes = dialog.findViewById(R.id.yes);

                    txtYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            try {
                                ((MainPage) getActivity()).removeCurrentFragmentAndMoveBack();
                                ((MainPage) getActivity()).loadFragment(new Home(), false);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

                    dialog.show();

                }

            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                pDialog.dismiss();
                Log.e("orderError", "" + t.getMessage());
                Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
            }
        });


    }


    @Override
    public void onStart() {
        super.onStart();
        Log.e("onStart", "called");
        ((MainPage) getActivity()).lockUnlockDrawer(1);
        if (DetectConnection.checkInternetConnection(getActivity())) {
            getDealerList();
        } else {
            DetectConnection.noInternetConnection(getActivity());
        }
    }

    private void getDealerList() {

        dealerResponseList.clear();

        //Call<List<DealerResponse>> call = Api.getClient().getDealerByEmp(Integer.valueOf(MainPage.userId));
        Call<List<DealerResponse>> call = Api.getClient().getDealerList();
        call.enqueue(new Callback<List<DealerResponse>>() {
            @Override
            public void onResponse(Call<List<DealerResponse>> call, Response<List<DealerResponse>> response) {
                if (response.isSuccessful()) {
                    try {
                        dealerResponseList = response.body();
                        if (dealerResponseList != null) {

                            dealerIdList = new String[dealerResponseList.size()];
                            dealerNameList = new String[dealerResponseList.size()];
                            dealerAddressList = new String[dealerResponseList.size()];
                            for (int i = 0; i < dealerResponseList.size(); i++) {
                                dealerIdList[i] = dealerResponseList.get(i).getDealershipId();
                                dealerNameList[i] = dealerResponseList.get(i).getFirmName();
                                dealerAddressList[i] = dealerResponseList.get(i).getFirmAddress();
                            }

                            try {
                                final ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, dealerNameList);
                                adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
                                materialSpinners.get(3).setAdapter(adapter);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else {
                            Toast.makeText(getActivity(), "No Dealer Found", Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getActivity(), "No Dealer Found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<DealerResponse>> call, Throwable t) {
                Log.e("categoryError", "" + t.getMessage());
                Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void getCartList(String dealerId, String companyId) {

        cartResponseList.clear();
        recyclerView.clearOnScrollListeners();
        linearLayout.setVisibility(View.GONE);

        Call<List<CartResponse>> call = Api.getClient().getCartList(dealerId, companyId);
        call.enqueue(new Callback<List<CartResponse>>() {
            @Override
            public void onResponse(Call<List<CartResponse>> call, Response<List<CartResponse>> response) {

                if (response.isSuccessful()) {
                    cartResponseList = response.body();
                    Log.e("cartResponseList", "" + cartResponseList.size());
                    double subAmount = 0f;
                    if (cartResponseList.size() > 0) {
                        try {
                            for (int i = 0; i < cartResponseList.size(); i++) {
                                subAmount = subAmount + (Double.parseDouble(cartResponseList.get(i).getProductAmount()) * Double.parseDouble(String.valueOf(cartResponseList.get(i).getQty())));
                            }
                            String subTotal = String.format("%.2f", (subAmount));
                            textViews.get(0).setText("" + subTotal);
                            textViews.get(1).setText("" + subTotal);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        CartAdapter adapter = new CartAdapter(getActivity(), cartResponseList);
                        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                        recyclerView.setAdapter(adapter);
                        recyclerView.setHasFixedSize(true);
                        adapter.notifyDataSetChanged();
                        llProductListCount.setVisibility(View.VISIBLE);
                        tvCount.setText(""+String.valueOf(cartResponseList.size()));
                        //textViews.get(2).setVisibility(View.VISIBLE);
                        linearLayout.setVisibility(View.GONE);

                    } else {
                        llProductListCount.setVisibility(View.GONE);
                        //textViews.get(2).setVisibility(View.GONE);
                        linearLayout.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "No Cart Found", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    //textViews.get(2).setVisibility(View.GONE);
                    linearLayout.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), "No Cart Found", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<List<CartResponse>> call, Throwable t) {
                Log.e("cartError", "" + t.getMessage());
                //textViews.get(2).setVisibility(View.GONE);
                linearLayout.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getCompanyList() {

        companyResponseList.clear();

        Call<List<CompanyResponse>> call = Api.getClient().getCompanyList();
        call.enqueue(new Callback<List<CompanyResponse>>() {
            @Override
            public void onResponse(Call<List<CompanyResponse>> call, Response<List<CompanyResponse>> response) {

                if (response.isSuccessful()) {
                    try {
                        companyResponseList = response.body();
                        if (companyResponseList != null) {

                            companyIdList = new String[companyResponseList.size()];
                            companyNameList = new String[companyResponseList.size()];

                            for (int i = 0; i < companyResponseList.size(); i++) {
                                companyIdList[i] = companyResponseList.get(i).getCompId();
                                companyNameList[i] = companyResponseList.get(i).getCompName();
                            }

                            try {
                                final ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, companyNameList);
                                adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
                                materialSpinners.get(0).setAdapter(adapter);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else {
                            Toast.makeText(getActivity(), "No Company Found", Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getActivity(), "No Company Found", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<List<CompanyResponse>> call, Throwable t) {
                Log.e("categoryError", "" + t.getMessage());
                Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getCategoryList(String companyId) {

        categoryResponseList.clear();

        Call<List<CategoryResponse>> call = Api.getClient().getCategoryList(companyId);
        call.enqueue(new Callback<List<CategoryResponse>>() {
            @Override
            public void onResponse(Call<List<CategoryResponse>> call, Response<List<CategoryResponse>> response) {

                if (response.isSuccessful()) {
                    try {
                        categoryResponseList = response.body();
                        if (categoryResponseList != null) {

                            categoryIdList = new String[categoryResponseList.size()];
                            categoryNameList = new String[categoryResponseList.size()];

                            for (int i = 0; i < categoryResponseList.size(); i++) {
                                categoryIdList[i] = categoryResponseList.get(i).getCategoryId();
                                categoryNameList[i] = categoryResponseList.get(i).getCategoryName();
                            }

                            try {
                                final ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, categoryNameList);
                                adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
                                materialSpinners.get(1).setAdapter(adapter);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else {
                            Toast.makeText(getActivity(), "No Category Found", Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getActivity(), "No Category Found", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<List<CategoryResponse>> call, Throwable t) {
                Log.e("categoryError", "" + t.getMessage());
                Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getProductList(String categoryId) {

        productResponseList.clear();

        Call<List<ProductResponse>> call = Api.getClient().getProductList(categoryId);
        call.enqueue(new Callback<List<ProductResponse>>() {
            @Override
            public void onResponse(Call<List<ProductResponse>> call, Response<List<ProductResponse>> response) {

                if (response.isSuccessful()) {

                    try {
                        productResponseList = response.body();
                        if (productResponseList != null) {

                            productIdList = new String[productResponseList.size()];
                            productNameList = new String[productResponseList.size()];

                            for (int i = 0; i < productResponseList.size(); i++) {
                                productIdList[i] = productResponseList.get(i).getProductId();
                                productNameList[i] = productResponseList.get(i).getProductName();
                            }

                            try {
                                System.out.println("ProductListBefore");
                               // setDataToProductAdapter(productResponseList);
                                /*final ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, productNameList);
                                adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);*/
                                //materialSpinners.get(2).setAdapter(adapter);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else {
                            Toast.makeText(getActivity(), "No Product Found", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getActivity(), "No Product Found", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<List<ProductResponse>> call, Throwable t) {
                Log.e("productError", "" + t.getMessage());
                Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
            }
        });

    }



    private void getSizeList(String productId) {

        sizeWiseProductList.clear();

        Call<List<SizeWiseProduct>> call = Api.getClient().getSizeWiseProductList(productId);
        call.enqueue(new Callback<List<SizeWiseProduct>>() {
            @Override
            public void onResponse(Call<List<SizeWiseProduct>> call, Response<List<SizeWiseProduct>> response) {

                if (response.isSuccessful()) {

                    try {

                        sizeWiseProductList = response.body();
                        if (sizeWiseProductList != null) {

                            sizeIdList = new String[sizeWiseProductList.size()];
                            sizeNameList = new String[sizeWiseProductList.size()];
                            amountList = new String[sizeWiseProductList.size()];
                            productGstList = new String[sizeWiseProductList.size()];

                            for (int i = 0; i < sizeWiseProductList.size(); i++) {
                                sizeIdList[i] = sizeWiseProductList.get(i).getSizeWiseProductId();
                                sizeNameList[i] = sizeWiseProductList.get(i).getSize() + " " + sizeWiseProductList.get(i).getUnit();
                                try {
                                    amountList[i] = sizeWiseProductList.get(i).getSizeWiseProductAmount();
                                    productGstList[i] = sizeWiseProductList.get(i).getSizeWiseProductGSTPercentage();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            try {
                                final ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, sizeNameList);
                                adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
                                materialSpinners.get(2).setAdapter(adapter);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else {
                            Toast.makeText(getActivity(), "No Size Found", Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getActivity(), "No Size Found", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<List<SizeWiseProduct>> call, Throwable t) {
                Log.e("sizeError", "" + t.getMessage());
                Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void setDataToProductAdapter(List<ProductResponse> productResponseList) {
       /* System.out.println("ProductListAfter");
        ProductResponse productResponse=new ProductResponse();
        productResponse.setProductName("Select Product");

        productResponseList.add(0, productResponse);

        // Creating ArrayAdapter using the string array and default spinner layout
        ArrayAdapter<ProductResponse> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, productResponseList);
        //ArrayAdapter<HospitalModel> arrayAdapter =  ArrayAdapter.createFromResource(this,arrayList,R.layout.spinner_text_view_unit);
        // Specify layout to be used when list of choices appears
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Applying the adapter to our spinner
        *//*spinnerEmployee.setPrompt("Select Employee");*//*

       // productSpinner.setTitle("Select Product");


        productSpinner.setAdapter(arrayAdapter);

       *//* productSpinner.setSelection(0,false);

        productSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long l) {
                ((TextView)parent.getChildAt(0)).setTextSize(TypedValue.COMPLEX_UNIT_DIP,15);
                ((TextView)parent.getChildAt(0)).setHint("Select Employee");
                ProductResponse productResponse1 =(ProductResponse)parent.getSelectedItem();
                 productResponse1.getProductName();
                productId= productResponse1.getProductId();
                getSizeList(productId);
                System.out.println("Selected Product:"+productResponse1.getProductName());


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/



    }
    private OnItemSelectedListener mOnItemSelectedListener = new OnItemSelectedListener() {
        @Override
        public void onItemSelected(View view, int position, long id) {
            Toast.makeText(getActivity(), "Item on position " + position + " : " + searchableProductAdapter.getItem(position) + " Selected", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onNothingSelected() {
            Toast.makeText(getActivity(), "Nothing Selected", Toast.LENGTH_SHORT).show();
        }
    };
}