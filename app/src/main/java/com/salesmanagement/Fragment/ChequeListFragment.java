package com.salesmanagement.Fragment;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.andreabaccega.widget.FormEditText;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Adapter.ChequeListAdapter;
import com.salesmanagement.Extra.DetectConnection;
import com.salesmanagement.Model.ChequeResponseModel;
import com.salesmanagement.Model.CompanyResponse;
import com.salesmanagement.R;
import com.salesmanagement.Retrofit.Api;

import java.util.ArrayList;
import java.util.List;

import fr.ganfra.materialspinner.MaterialSpinner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ChequeListFragment extends Fragment {
    public List<CompanyResponse> companyResponseList = new ArrayList<>();
    public String[] companyIdList, companyNameList;
    private View view;
    private RecyclerView rv_cheque_list;
    private FormEditText et_search;
    private ChequeListAdapter chequeListAdapter;
    private List<ChequeResponseModel> chequeList;
    private List<ChequeResponseModel> searchChequeList;
    private Spinner spinner_company;

    private FloatingActionButton fab_add;

    private String companyId, companyName;

    private Integer compId = 1;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_cheque_list, container, false);
       // init();



        return view;
    }

    private void listener() {
        spinner_company.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                try {

                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));

                    companyId = companyIdList[position];
                    companyName = companyNameList[position];


                    compId = Integer.valueOf(companyId);


                    //Toast.makeText(getActivity(), "company" + compId, Toast.LENGTH_SHORT).show();


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void init() {
        MainPage.title.setText("Cheque List");
        rv_cheque_list = view.findViewById(R.id.rv_cheque_list);
        et_search = view.findViewById(R.id.et_search);
        spinner_company = view.findViewById(R.id.spinner_company);
        fab_add = view.findViewById(R.id.fab_add);
        getCompanyList();

        listener();

        setAdapter();


        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                try {
                    searchList(s.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        fab_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainPage) getActivity()).loadFragment(new ChequeMasterFragment(), true);
            }
        });
    }

    private void setAdapter() {

        ChequeEmployeCompanyReqModel chequeEmployeCompanyReqModel = new ChequeEmployeCompanyReqModel();

        chequeEmployeCompanyReqModel.setCompId(compId);
        chequeEmployeCompanyReqModel.setEmployeeId(Integer.valueOf(MainPage.userId));

        Call<List<ChequeResponseModel>> call = Api.getClient().getChequeList(chequeEmployeCompanyReqModel);

        call.enqueue(new Callback<List<ChequeResponseModel>>() {
            @Override
            public void onResponse(Call<List<ChequeResponseModel>> call, Response<List<ChequeResponseModel>> response) {


                chequeList = response.body();


                chequeListAdapter = new ChequeListAdapter(getActivity(), response.body());
                rv_cheque_list.setLayoutManager(new LinearLayoutManager(getContext()));
                rv_cheque_list.setAdapter(chequeListAdapter);
                chequeListAdapter.notifyDataSetChanged();
                rv_cheque_list.setHasFixedSize(true);
            }

            @Override
            public void onFailure(Call<List<ChequeResponseModel>> call, Throwable t) {

            }
        });

    }

    private void getCompanyList() {

        companyResponseList.clear();

        Call<List<CompanyResponse>> call = Api.getClient().getCompanyList();
        call.enqueue(new Callback<List<CompanyResponse>>() {
            @Override
            public void onResponse(Call<List<CompanyResponse>> call, Response<List<CompanyResponse>> response) {

                if (response.isSuccessful()) {
                    try {
                        companyResponseList = response.body();
                        if (companyResponseList != null) {

                            companyIdList = new String[companyResponseList.size()];
                            companyNameList = new String[companyResponseList.size()];

                            for (int i = 0; i < companyResponseList.size(); i++) {
                                companyIdList[i] = companyResponseList.get(i).getCompId();
                                companyNameList[i] = companyResponseList.get(i).getCompName();
                            }

                            try {
                                final ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, companyNameList);
                                adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
                                spinner_company.setAdapter(adapter);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else {
                            Toast.makeText(getActivity(), "No Company Found", Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getActivity(), "No Company Found", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<List<CompanyResponse>> call, Throwable t) {
                Log.e("categoryError", "" + t.getMessage());
                Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void searchList(String s) {

        searchChequeList = new ArrayList<>();
        if (s.length() > 0) {
            for (int i = 0; i < chequeList.size(); i++)
                if ( chequeList.get(i).getChequeNo().toLowerCase().contains(s.toLowerCase().trim())||chequeList.get(i).getDealerFullName().toLowerCase().contains(s.toLowerCase().trim())) {
                    searchChequeList.add(chequeList.get(i));
                }

            if (searchChequeList.size() < 1) {

            } else {

            }

        } else {
            searchChequeList = new ArrayList<>();
            for (int i = 0; i < chequeList.size(); i++) {
                searchChequeList.add(chequeList.get(i));
            }
        }

        try {

            chequeListAdapter = new ChequeListAdapter(getActivity(), searchChequeList);
            rv_cheque_list.setLayoutManager(new LinearLayoutManager(getContext()));
            rv_cheque_list.setAdapter(chequeListAdapter);
            chequeListAdapter.notifyDataSetChanged();
            rv_cheque_list.setHasFixedSize(true);


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onStart() {
        super.onStart();
        ((MainPage) getActivity()).lockUnlockDrawer(1);
        if (DetectConnection.checkInternetConnection(getActivity())) {
            init();
        } else {
            DetectConnection.noInternetConnection(getActivity());
        }

    }


}