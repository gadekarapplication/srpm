package com.salesmanagement.Fragment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChequeEmployeCompanyReqModel {
    @SerializedName("compId")
    @Expose
    private Integer compId;
    @SerializedName("employeeId")
    @Expose
    private Integer employeeId;

    public Integer getCompId() {
        return compId;
    }

    public void setCompId(Integer compId) {
        this.compId = compId;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }
}
