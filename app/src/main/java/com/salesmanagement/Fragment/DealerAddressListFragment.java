package com.salesmanagement.Fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Adapter.DealerAddressListAdapter;
import com.salesmanagement.Extra.DetectConnection;
import com.salesmanagement.Model.DealerAddressResponseModel;
import com.salesmanagement.R;
import com.salesmanagement.Retrofit.Api;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DealerAddressListFragment extends Fragment {

    @BindView(R.id.rvDealerAddress)
    RecyclerView rvDealerAddress;
    DealerAddressListAdapter dealerAddressListAdapter;
    @BindView(R.id.fabAddAddressDealer)
    FloatingActionButton fabAddAddressDealer;
    private Integer dealerId;
    private List<DealerAddressResponseModel> addressResponseModelList = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dealer_address_list, container, false);
        ButterKnife.bind(this, view);
        init();

        return view;

    }

    private void init() {
        MainPage.title.setText("Address List");
        rvDealerAddress.setLayoutManager(new LinearLayoutManager(getActivity()));
        Bundle bundle = getArguments();
        try {
            dealerId = bundle.getInt("DEALER_ID");
            System.out.println("DealerFragment: " + dealerId);
            getDealerAddress(dealerId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        fabAddAddressDealer.setOnClickListener(v -> {

            Bundle bundle1 = new Bundle();
            bundle1.putInt("DEALER_ID", dealerId);
            DealerAddressFragment dealerAddressFragment = new DealerAddressFragment();
            dealerAddressFragment.setArguments(bundle1);
            ((MainPage) getActivity()).loadFragment(dealerAddressFragment, true);
        });

    }

    @Override
    public void onStart() {
        super.onStart();
        ((MainPage) getActivity()).lockUnlockDrawer(1);
        if (DetectConnection.checkInternetConnection(getActivity())) {
            init();
        } else {
            DetectConnection.noInternetConnection(getActivity());
        }

    }

    private void getDealerAddress(Integer dealerId) {

        ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.setTitle("Please wait ");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        progressDialog.setCancelable(false);
        Call<List<DealerAddressResponseModel>> call = Api.getClient().getDealerAddress(dealerId);
        call.enqueue(new Callback<List<DealerAddressResponseModel>>() {
            @Override
            public void onResponse(Call<List<DealerAddressResponseModel>> call, Response<List<DealerAddressResponseModel>> response) {

                if (response.isSuccessful()) {
                    try {
                        addressResponseModelList.clear();
                        addressResponseModelList = response.body();
                        if (addressResponseModelList.size() > 0) {

                            dealerAddressListAdapter = new DealerAddressListAdapter(addressResponseModelList, getActivity());
                            rvDealerAddress.setAdapter(dealerAddressListAdapter);

                            rvDealerAddress.setHasFixedSize(true);

                            progressDialog.dismiss();
                            rvDealerAddress.setVisibility(View.VISIBLE);

                        } else {
                            progressDialog.dismiss();
                            rvDealerAddress.setVisibility(View.GONE);
                            Toast.makeText(getContext(), "No Address Found", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    progressDialog.dismiss();
                    rvDealerAddress.setVisibility(View.GONE);
                    Toast.makeText(getContext(), "No Address Found", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<List<DealerAddressResponseModel>> call, Throwable t) {
                Log.e("Dealer List:", "" + t.getMessage());
                Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                rvDealerAddress.setVisibility(View.GONE);
            }
        });
    }

}