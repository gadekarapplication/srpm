package com.salesmanagement.Fragment;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Adapter.DealerAddressListAdapter;
import com.salesmanagement.Adapter.DeliveryAddressAdapter;
import com.salesmanagement.Extra.DetectConnection;
import com.salesmanagement.Model.DealerAddressResponseModel;
import com.salesmanagement.R;
import com.salesmanagement.Retrofit.Api;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DeliveryAddressFragment extends Fragment {
    @BindView(R.id.rvDeliveryAddress)
    RecyclerView rvDeliveryAddress;
    @BindView(R.id.pbDeliveryAddress)
    ProgressBar pbDeliveryAddress;
    private String dealerId,companyId;
   /* @BindView(R.id.fabAddAddressDealer)
    FloatingActionButton fabAddAddressDealer;*/

    private List<DealerAddressResponseModel> addressResponseModelList = new ArrayList<>();
    DeliveryAddressAdapter deliveryAddressAdapter;
    @BindView(R.id.fabAddAddressDelivery)
    FloatingActionButton fabAddAddressDelivery;
    @BindView(R.id.tvNoDeliveryAddressFound)
    TextView tvNoDeliveryAddressFound;
    public DeliveryAddressFragment() {
        // Required empty public constructor
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_delivery_address, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        ((MainPage) getActivity()).lockUnlockDrawer(1);
        if (DetectConnection.checkInternetConnection(getActivity())) {
            init();
        } else {
            DetectConnection.noInternetConnection(getActivity());
        }

    }

    private void init() {
        MainPage.title.setText("Delivery Address");
        rvDeliveryAddress.setLayoutManager(new LinearLayoutManager(getActivity()));
        Bundle bundle = getArguments();
        try {
            dealerId = bundle.getString("dealerId");
            companyId=bundle.getString("companyId");
            System.out.println("DealerFragment: " + dealerId);
            getDealerAddress(dealerId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        fabAddAddressDelivery.setOnClickListener(v -> {

            Bundle bundle1 = new Bundle();
            bundle1.putInt("DEALER_ID", Integer.parseInt(dealerId));
            DealerAddressFragment dealerAddressFragment = new DealerAddressFragment();
            dealerAddressFragment.setArguments(bundle1);
            ((MainPage) getActivity()).loadFragment(dealerAddressFragment, true);
        });
    }

    private void getDealerAddress(String dealerId) {

        pbDeliveryAddress.setVisibility(View.VISIBLE);
        Call<List<DealerAddressResponseModel>> call = Api.getClient().getDealerAddress(Integer.parseInt(dealerId));
        call.enqueue(new Callback<List<DealerAddressResponseModel>>() {
            @Override
            public void onResponse(Call<List<DealerAddressResponseModel>> call, Response<List<DealerAddressResponseModel>> response) {

                if (response.isSuccessful()) {
                    pbDeliveryAddress.setVisibility(View.GONE);
                    try {
                        addressResponseModelList.clear();
                        addressResponseModelList = response.body();
                        if (addressResponseModelList.size() > 0) {

                            deliveryAddressAdapter = new DeliveryAddressAdapter( getActivity(),addressResponseModelList,companyId,dealerId);
                            rvDeliveryAddress.setAdapter(deliveryAddressAdapter);

                            rvDeliveryAddress.setHasFixedSize(true);


                            rvDeliveryAddress.setVisibility(View.VISIBLE);
                            tvNoDeliveryAddressFound.setVisibility(View.GONE);
                        } else {
                            tvNoDeliveryAddressFound.setVisibility(View.VISIBLE);
                            rvDeliveryAddress.setVisibility(View.GONE);
                            //Toast.makeText(getContext(), "No Address Found", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    pbDeliveryAddress.setVisibility(View.GONE);
                    tvNoDeliveryAddressFound.setVisibility(View.VISIBLE);
                    rvDeliveryAddress.setVisibility(View.GONE);
                    //Toast.makeText(getContext(), "No Address Found", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<List<DealerAddressResponseModel>> call, Throwable t) {
                Log.e("Dealer List:", "" + t.getMessage());
                Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
                tvNoDeliveryAddressFound.setVisibility(View.GONE);
                pbDeliveryAddress.setVisibility(View.GONE);
                rvDeliveryAddress.setVisibility(View.GONE);
            }
        });
    }

}