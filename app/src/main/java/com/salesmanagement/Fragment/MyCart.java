package com.salesmanagement.Fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Adapter.CartAdapter;
import com.salesmanagement.Extra.DetectConnection;
import com.salesmanagement.Model.CartResponse;
import com.salesmanagement.Model.OrderResponse;
import com.salesmanagement.R;
import com.salesmanagement.Retrofit.Api;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.salesmanagement.Activity.MainPage.llProductListCount;


public class MyCart extends Fragment implements CartAdapter.IDeleteCartItem {

    public List<CartResponse> cartResponseList = new ArrayList<>();
    public String dealerId, companyId;
    View view;
    @BindViews({R.id.subTotal, R.id.totalAmount})
    List<TextView> textViews;
    @BindView(R.id.continueOrder)
    TextView continueOrder;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.linearLayout)
    LinearLayout linearLayout;
    @BindView(R.id.noDataFound)
    TextView noDataFound;
    CartAdapter cartadapter;
    @BindView(R.id.txtOrderAmount)
    TextView txtOrderAmount;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_my_cart, container, false);
        ButterKnife.bind(this, view);
        MainPage.title.setText("Cart List");
        llProductListCount.setVisibility(View.GONE);
        try {
            Bundle bundle = getArguments();
            dealerId = bundle.getString("dealerId");
            companyId = bundle.getString("companyId");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    @OnClick({R.id.continueOrder})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.continueOrder:

                if (DetectConnection.checkInternetConnection(getActivity())) {

                    /*PlaceOrderDetails placeOrderDetails = new PlaceOrderDetails();
                    Bundle bundle = new Bundle();
                    bundle.putString("dealerId", ""+dealerId);
                    bundle.putString("companyId", ""+companyId);
                    placeOrderDetails.setArguments(bundle);
                    ((MainPage) getActivity()).loadFragment(placeOrderDetails, true);*/
                    DeliveryAddressFragment deliveryAddressFragment = new DeliveryAddressFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("dealerId", ""+dealerId);
                    bundle.putString("companyId", ""+companyId);
                    deliveryAddressFragment.setArguments(bundle);
                    ((MainPage) getActivity()).loadFragment(deliveryAddressFragment, true);

                   // placeOrder(cartResponseList);

                } else {
                    DetectConnection.noInternetConnection(getActivity());
                }

                break;
        }
    }

    private void placeOrder(List<CartResponse> cartResponseList) {

        final SweetAlertDialog pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.colorPrimary));
        pDialog.setTitleText("Please wait");
        pDialog.setContentText("Order is placing");
        pDialog.setCancelable(false);
        pDialog.show();

        OrderResponse orderResponse = new OrderResponse();
        orderResponse.setDealershipId("" + dealerId);
        orderResponse.setOrderPaymentMode("Cash");
        orderResponse.setFinalTotalAmount("" + textViews.get(0).getText().toString().trim());
        orderResponse.setCartResponseList(cartResponseList);


        Call<Boolean> call = Api.getClient().postOrder(orderResponse);
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {

                if (response.isSuccessful()) {

                    if (response.body().booleanValue() == true) {
                        pDialog.dismiss();

                        final Dialog dialog = new Dialog(getActivity());
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
                        dialog.setContentView(R.layout.confirmation_dialog);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        dialog.setCancelable(false);
                        TextView txtYes = dialog.findViewById(R.id.yes);

                        txtYes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                try {
                                    ((MainPage) getActivity()).removeCurrentFragmentAndMoveBack();
                                    ((MainPage) getActivity()).loadFragment(new Home(), true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        dialog.show();

                    } else if (response.body().booleanValue() == false) {
                        pDialog.dismiss();

                        final Dialog dialog = new Dialog(getActivity());
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
                        dialog.setContentView(R.layout.error_dialog);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        dialog.setCancelable(false);
                        TextView txtYes = dialog.findViewById(R.id.yes);

                        txtYes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                try {
                                    ((MainPage) getActivity()).removeCurrentFragmentAndMoveBack();
                                    ((MainPage) getActivity()).loadFragment(new Home(), false);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        dialog.show();

                    }

                } else {
                    pDialog.dismiss();

                    final Dialog dialog = new Dialog(getActivity());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
                    dialog.setContentView(R.layout.error_dialog);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    dialog.setCancelable(false);
                    TextView txtYes = dialog.findViewById(R.id.yes);

                    txtYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            try {
                                ((MainPage) getActivity()).removeCurrentFragmentAndMoveBack();
                                ((MainPage) getActivity()).loadFragment(new Home(), false);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

                    dialog.show();

                }

            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                pDialog.dismiss();
                Log.e("orderError", "" + t.getMessage());
                Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e("onStart", "called");
        ((MainPage) getActivity()).lockUnlockDrawer(1);
        if (DetectConnection.checkInternetConnection(getActivity())) {
            getCartList(dealerId, companyId);
        } else {
            DetectConnection.noInternetConnection(getActivity());
        }
    }

    public void getCartList(String dealerId, String companyId) {

        ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        progressDialog.setCancelable(false);

        cartResponseList.clear();
        recyclerView.clearOnScrollListeners();
        linearLayout.setVisibility(View.GONE);
        noDataFound.setVisibility(View.GONE);
        continueOrder.setVisibility(View.GONE);

        Call<List<CartResponse>> call = Api.getClient().getCartList(dealerId, companyId);
        call.enqueue(new Callback<List<CartResponse>>() {
            @Override
            public void onResponse(Call<List<CartResponse>> call, Response<List<CartResponse>> response) {

                if (response.isSuccessful()) {
                    cartResponseList = response.body();
                    Log.e("cartResponseList", "" + cartResponseList.size());
                   // double subAmount = 0.0;
                    if (cartResponseList.size() > 0) {

                        loadCartList();
                        /*try {
                            for (int i = 0; i < cartResponseList.size(); i++) {
                                //subAmount = subAmount + (Double.parseDouble(cartResponseList.get(i).getProductAmount()) * Double.parseDouble(cartResponseList.get(i).getQty()));
                                subAmount = subAmount + (Double.parseDouble(cartResponseList.get(i).getFinalAmount()) );
                            }
                            String subTotal = String.format("%.2f", (subAmount));
                            textViews.get(0).setText("" + subTotal);
                            textViews.get(1).setText("" + subTotal);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                         cartadapter = new CartAdapter(getActivity(), cartResponseList,MyCart.this::deleteCartItem,"MyCart");
                        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                        recyclerView.setAdapter(cartadapter);
                        recyclerView.setHasFixedSize(true);
                        cartadapter.notifyDataSetChanged();

                        linearLayout.setVisibility(View.VISIBLE);
                        continueOrder.setVisibility(View.VISIBLE);
                        noDataFound.setVisibility(View.GONE);*/

                        progressDialog.dismiss();

                    } else {
                        linearLayout.setVisibility(View.GONE);
                        continueOrder.setVisibility(View.GONE);
                        noDataFound.setVisibility(View.VISIBLE);
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), "No Cart Found", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    linearLayout.setVisibility(View.GONE);
                    continueOrder.setVisibility(View.GONE);
                    noDataFound.setVisibility(View.VISIBLE);
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), "No Cart Found", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<List<CartResponse>> call, Throwable t) {
                Log.e("cartError", "" + t.getMessage());
                linearLayout.setVisibility(View.GONE);
                continueOrder.setVisibility(View.GONE);
                noDataFound.setVisibility(View.VISIBLE);
                progressDialog.dismiss();
                Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void loadCartList() {
        double subAmount = 0.0;
        try {
            for (int i = 0; i < cartResponseList.size(); i++) {
                //subAmount = subAmount + (Double.parseDouble(cartResponseList.get(i).getProductAmount()) * Double.parseDouble(cartResponseList.get(i).getQty()));
                subAmount = subAmount + (Double.parseDouble(cartResponseList.get(i).getFinalAmount()) );
            }
            String subTotal = String.format("%.2f", (subAmount));
            txtOrderAmount.setText("Total amount: ");
            textViews.get(0).setText("" + subTotal);
            textViews.get(1).setText("" + subTotal);
        } catch (Exception e) {
            e.printStackTrace();
        }

        cartadapter = new CartAdapter(getActivity(), cartResponseList,MyCart.this::deleteCartItem,"MyCart");
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(cartadapter);
        recyclerView.setHasFixedSize(true);
        cartadapter.notifyDataSetChanged();

        linearLayout.setVisibility(View.VISIBLE);
        continueOrder.setVisibility(View.VISIBLE);
        noDataFound.setVisibility(View.GONE);
    }

    @Override
    public void deleteCartItem(int position, int cartId) {
       // cartResponseList.remove(position);



        Call<Boolean> call = Api.getClient().deleteCartItem(cartId);
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                    try {
                        if(response.body()){

                            if(cartResponseList.size()==0){
                                System.out.println("Size after Delete from cart: "+cartResponseList.size());
                                noDataFound.setVisibility(View.VISIBLE);
                                continueOrder.setVisibility(View.GONE);
                                textViews.get(1).setVisibility(View.GONE);
                                txtOrderAmount.setVisibility(View.GONE);
                            }
                            else {
                            loadCartList();
                            }
                            cartadapter.notifyDataSetChanged();

                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }

            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {

              try {

              }catch (Exception e){
                  e.printStackTrace();
              }

            }
        });
    }
}