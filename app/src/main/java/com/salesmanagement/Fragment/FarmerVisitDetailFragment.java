package com.salesmanagement.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Model.FarmerVisiterRequest;
import com.salesmanagement.R;
import com.salesmanagement.Retrofit.Api;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FarmerVisitDetailFragment extends Fragment {

    TextView tv_area_name, tv_crop_name, tv_date, tv_place, tv_district, tv_remark, tv_irrigation, tv_medicine, tv_other_crop, tv_water_source;
    private View view;
    private int farmer_visit_id;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_farmer_visit_detail, container, false);
        init();

        Bundle bundle = getArguments();

        if (bundle != null) {
            farmer_visit_id = bundle.getInt("farmer_visit_id");
            Toast.makeText(getActivity(), "" + farmer_visit_id, Toast.LENGTH_SHORT).show();
        }

        setData();
        return view;
    }


    private void init() {

        tv_area_name = view.findViewById(R.id.tv_area_name);
        tv_crop_name = view.findViewById(R.id.tv_crop_name);
        tv_date = view.findViewById(R.id.tv_date);
        tv_place = view.findViewById(R.id.tv_place);
        tv_district = view.findViewById(R.id.tv_district);
        tv_remark = view.findViewById(R.id.tv_remark);
        tv_irrigation = view.findViewById(R.id.tv_irrigation);
        tv_medicine = view.findViewById(R.id.tv_medicine);
        tv_other_crop = view.findViewById(R.id.tv_other_crop);
        tv_water_source = view.findViewById(R.id.tv_water_source);
    }

    private void setData() {
        Call<FarmerVisiterRequest> call = Api.getClient().getFarmerVisitDetail(farmer_visit_id);
        call.enqueue(new Callback<FarmerVisiterRequest>() {
            @Override
            public void onResponse(Call<FarmerVisiterRequest> call, Response<FarmerVisiterRequest> response) {
                // Toast.makeText(getActivity(), "hiii", Toast.LENGTH_SHORT).show();
                tv_area_name.setText(response.body().getArea());
                tv_crop_name.setText(response.body().getCrop());
                tv_date.setText(response.body().getDate());
                tv_place.setText(response.body().getDemoPlace());
                tv_district.setText(response.body().getDistrict());
                tv_remark.setText(response.body().getRemark());
                tv_irrigation.setText(response.body().getIrrigation());
                tv_medicine.setText(response.body().getMedicines());
                tv_other_crop.setText(response.body().getOtherCrops());
                tv_water_source.setText(response.body().getWaterSource());
            }

            @Override
            public void onFailure(Call<FarmerVisiterRequest> call, Throwable t) {

            }
        });
    }


    @Override
    public void onStart() {
        super.onStart();

        MainPage.title.setText("Farmer Visit Detail");

    }
}
