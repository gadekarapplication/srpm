package com.salesmanagement.Fragment;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Adapter.AddressListAdapter;
import com.salesmanagement.Adapter.DealerAddressListAdapter;
import com.salesmanagement.Extra.DetectConnection;
import com.salesmanagement.Model.CartResponse;
import com.salesmanagement.Model.DealerAddressResponseModel;
import com.salesmanagement.R;
import com.salesmanagement.Retrofit.Api;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AllDealerAddress extends Fragment {

    public static String dealerId, companyId;
    View view;
    @BindView(R.id.rvDealerAddress)
    RecyclerView rvDealerAddress;
    private List<DealerAddressResponseModel> addressResponseModelList = new ArrayList<>();
    AddressListAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_all_dealer_address, container, false);
        ButterKnife.bind(this, view);
        MainPage.title.setText("Address");

        try {
            Bundle bundle = getArguments();
            dealerId = bundle.getString("dealerId");
            companyId = bundle.getString("companyId");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e("onStart", "called");
        ((MainPage) getActivity()).lockUnlockDrawer(1);
        if (DetectConnection.checkInternetConnection(getActivity())) {
            getDealerAddress(Integer.valueOf(dealerId));
        } else {
            DetectConnection.noInternetConnection(getActivity());
        }
    }

    private void getDealerAddress(Integer dealerId) {

        ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.setTitle("Please wait ");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        progressDialog.setCancelable(false);
        Call<List<DealerAddressResponseModel>> call = Api.getClient().getDealerAddress(dealerId);
        call.enqueue(new Callback<List<DealerAddressResponseModel>>() {
            @Override
            public void onResponse(Call<List<DealerAddressResponseModel>> call, Response<List<DealerAddressResponseModel>> response) {

                if (response.isSuccessful()) {
                    try {
                        addressResponseModelList.clear();
                        addressResponseModelList = response.body();
                        if (addressResponseModelList.size() > 0) {

                            adapter = new AddressListAdapter(addressResponseModelList, getActivity());
                            rvDealerAddress.setAdapter(adapter);
                            rvDealerAddress.setHasFixedSize(true);
                            progressDialog.dismiss();
                            rvDealerAddress.setVisibility(View.VISIBLE);

                        } else {
                            progressDialog.dismiss();
                            rvDealerAddress.setVisibility(View.GONE);
                            Toast.makeText(getContext(), "No Address Found", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    progressDialog.dismiss();
                    rvDealerAddress.setVisibility(View.GONE);
                    Toast.makeText(getContext(), "No Address Found", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<List<DealerAddressResponseModel>> call, Throwable t) {
                Log.e("Dealer List:", "" + t.getMessage());
                Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                rvDealerAddress.setVisibility(View.GONE);
            }
        });
    }


}