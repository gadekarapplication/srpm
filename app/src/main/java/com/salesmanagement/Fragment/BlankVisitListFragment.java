package com.salesmanagement.Fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Adapter.BlankVisitAdapter;
import com.salesmanagement.Adapter.DealerListAdapter;
import com.salesmanagement.Extra.DetectConnection;
import com.salesmanagement.Model.BlankVisitResponse;
import com.salesmanagement.Model.DealerListResponse;
import com.salesmanagement.R;
import com.salesmanagement.Retrofit.Api;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class BlankVisitListFragment extends Fragment {


    @BindView(R.id.rvBlankVisit)
    RecyclerView rvBlankVisit;
    @BindView(R.id.pbBlankVisit)
    ProgressBar pbBlankVisit;
    List<BlankVisitResponse> blankVisitResponseList=new ArrayList<>();
    BlankVisitAdapter blankVisitAdapter;
    @BindView(R.id.fabAddBlankVisit)
    FloatingActionButton fabAddBlankVisit;
    public BlankVisitListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_blank_visit_list, container, false);
        ButterKnife.bind(this,view);
        MainPage.title.setText("Blank Visit");
        ((MainPage) getActivity()).lockUnlockDrawer(1);
        pbBlankVisit.setVisibility(View.GONE);
        if (DetectConnection.checkInternetConnection(getActivity())) {
            init();
        } else {
            DetectConnection.noInternetConnection(getActivity());
        }
        fabAddBlankVisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainPage)getActivity()).loadFragment(new DealerListFragment(),true);
            }
        });

        return view;
    }

    private void init() {
        rvBlankVisit.setLayoutManager(new LinearLayoutManager(getActivity()));
        callToGetBlankVisit();
    }

    private void callToGetBlankVisit() {
        pbBlankVisit.setVisibility(View.VISIBLE);
        Call<List<BlankVisitResponse>> call = Api.getClient().getAllBlankVisit(Integer.valueOf(MainPage.userId));
        call.enqueue(new Callback<List<BlankVisitResponse>>() {
            @Override
            public void onResponse(Call<List<BlankVisitResponse>> call, Response<List<BlankVisitResponse>> response) {

                if (response.isSuccessful()) {
                    pbBlankVisit.setVisibility(View.GONE);
                    try {
                        // progressDialog.dismiss();
                        blankVisitResponseList.clear();
                        blankVisitResponseList = response.body();
                        if (blankVisitResponseList.size() > 0) {


                            blankVisitAdapter=new BlankVisitAdapter(getActivity(),blankVisitResponseList);


                            rvBlankVisit.setAdapter(blankVisitAdapter);
                            blankVisitAdapter.notifyDataSetChanged();
                            rvBlankVisit.setHasFixedSize(true);


                            rvBlankVisit.setVisibility(View.VISIBLE);

                        } else {
                            //progressDialog.dismiss();
                            rvBlankVisit.setVisibility(View.GONE);
                            Toast.makeText(getContext(), "No record found", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        pbBlankVisit.setVisibility(View.GONE);
                    }
                } else {
                    pbBlankVisit.setVisibility(View.GONE);
                    // progressDialog.dismiss();
                    rvBlankVisit.setVisibility(View.GONE);
                    Toast.makeText(getContext(), "No Dealer Found", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<List<BlankVisitResponse>> call, Throwable t) {
                Log.e("Blank List:", "" + t.getMessage());
                Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
                // progressDialog.dismiss();
                pbBlankVisit.setVisibility(View.GONE);
                rvBlankVisit.setVisibility(View.GONE);
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();

    }




}