package com.salesmanagement.Fragment;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.andreabaccega.widget.FormEditText;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Adapter.FarmerListAdapter;
import com.salesmanagement.Extra.DetectConnection;
import com.salesmanagement.Model.FarmerRequestModel;
import com.salesmanagement.R;
import com.salesmanagement.Retrofit.Api;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FarmerListFragment extends Fragment {

    List<FarmerRequestModel> farmerList;
    List<FarmerRequestModel> searchFarmerList;
    private View view;
    private FormEditText et_search;

    private RecyclerView rv_farmer_list;

    private FloatingActionButton fab_add;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_farmer_list, container, false);


        init();

        setAdapter();
        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                try {
                    searchList(s.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        return view;
    }

    private void init() {
        et_search = view.findViewById(R.id.et_search);
        rv_farmer_list = view.findViewById(R.id.rv_farmer_list);
        fab_add = view.findViewById(R.id.fab_add);

        fab_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainPage) getActivity()).loadFragment(new FarmerRegistarationFragment(), true);
            }
        });


    }

    private void searchList(String s) {

        searchFarmerList = new ArrayList<>();
        if (s.length() > 0) {
            for (int i = 0; i < farmerList.size(); i++)
                if ((farmerList.get(i).getName() + "" + farmerList.get(i).getMobileNo()).toLowerCase().contains(s.toLowerCase().trim())) {
                    searchFarmerList.add(farmerList.get(i));
                }

            if (searchFarmerList.size() < 1) {

            } else {

            }

        } else {
            searchFarmerList = new ArrayList<>();
            for (int i = 0; i < farmerList.size(); i++) {
                searchFarmerList.add(farmerList.get(i));
            }
        }

        try {


            FarmerListAdapter adapter = new FarmerListAdapter(getActivity(), searchFarmerList);
            rv_farmer_list.setLayoutManager(new LinearLayoutManager(getContext()));
            rv_farmer_list.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            rv_farmer_list.setHasFixedSize(true);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void setAdapter() {

        Call<List<FarmerRequestModel>> call = Api.getClient().getFarmerList(Integer.parseInt(MainPage.userId));
        call.enqueue(new Callback<List<FarmerRequestModel>>() {
            @Override
            public void onResponse(Call<List<FarmerRequestModel>> call, Response<List<FarmerRequestModel>> response) {
                farmerList = response.body();
                if(farmerList.size()>0) {
                    FarmerListAdapter adapter = new FarmerListAdapter(getActivity(), response.body());
                    rv_farmer_list.setLayoutManager(new LinearLayoutManager(getContext()));
                    rv_farmer_list.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                    rv_farmer_list.setHasFixedSize(true);
                }
            }

            @Override
            public void onFailure(Call<List<FarmerRequestModel>> call, Throwable t) {

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e("onStart", "called");
        MainPage.title.setText("Farmer List");
        ((MainPage) getActivity()).lockUnlockDrawer(1);
        if (DetectConnection.checkInternetConnection(getActivity())) {

        } else {
            DetectConnection.noInternetConnection(getActivity());
        }
    }


}