package com.salesmanagement.Fragment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.salesmanagement.R;


import java.util.ArrayList;
import java.util.List;

import butterknife.BindViews;
import butterknife.ButterKnife;

public class LeaveListAdapter extends RecyclerView.Adapter <LeaveListAdapter.ViewHolder>{
    Context context;
    private List<LeaveResponseModel> LeaveResponseModelList= new ArrayList<>();

    public LeaveListAdapter(Context context, List<LeaveResponseModel> leaveResponseModelList) {
        this.context = context;
        LeaveResponseModelList = leaveResponseModelList;
    }


    @Override
    public ViewHolder onCreateViewHolder(@NonNull  ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recyclerview_leave_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull  LeaveListAdapter.ViewHolder holder, int position) {
        try {
            holder.textViewList.get(0).setText(LeaveResponseModelList.get(position).getLeaveType());
            holder.textViewList.get(1).setText(LeaveResponseModelList.get(position).getLeaveDescription());
            holder.textViewList.get(2).setText(LeaveResponseModelList.get(position).getLeaveDate());
            holder.textViewList.get(3).setText(LeaveResponseModelList.get(position).getLeaveStatus());
        }catch (Exception e){
            e.printStackTrace();
        }



    }

    @Override
    public int getItemCount() {
        return LeaveResponseModelList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindViews({R.id.tvLeaveTypeValue,R.id.LeaveDescriptionValue,R.id.tvLeavedateValue,R.id.tvLeaveStatusValue})
        List<TextView> textViewList;

        public ViewHolder(@NonNull  View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
