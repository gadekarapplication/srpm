package com.salesmanagement.Fragment;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.canhub.cropper.CropImage;
import com.google.android.material.textfield.TextInputEditText;
import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Extra.DetectConnection;
import com.salesmanagement.Model.ExpenseRequest;
import com.salesmanagement.Model.ExpenseResponse;
import com.salesmanagement.Model.UploadImgResponse;
import com.salesmanagement.R;
import com.salesmanagement.Retrofit.Api;



import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.salesmanagement.Extra.Common.getSavedUserData;

public class AddExpenseFragment extends Fragment {


    @BindViews({R.id.tvSubmitExpense,R.id.tvExpensePhoto,R.id.tvChooseExpensePhoto})
    List<TextView> textViewList;
    @BindViews({R.id.dateExpense,R.id.expenseType,R.id.expenseDescription,R.id.expenseAmount})
    List<TextInputEditText> inputEditTexts;
    String filePath;
    Calendar date=Calendar.getInstance();
    String expenseDate;
    Integer empId;
    @BindView(R.id.ivExpense)
    ImageView ivExpense;
    ExpenseResponse expenseResponse;
    public AddExpenseFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_add_expense, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        ((MainPage) getActivity()).lockUnlockDrawer(1);
        if (DetectConnection.checkInternetConnection(getActivity())) {
            init();
        } else {
            DetectConnection.noInternetConnection(getActivity());
        }


    }

    private void init() {
        MainPage.title.setText("Add Expense");
        Bundle bundle=getArguments();
        try {
                if(getArguments().get("EXPENSE")!=null){
                    MainPage.title.setText("Update Expense");
                    expenseResponse = bundle.getParcelable("EXPENSE");
                    System.out.println(expenseResponse.getExpenseType());
                    if(expenseResponse!=null){
                        updateUI();
                    }
                    textViewList.get(0).setText("UPDATE");
                }


        }catch (Exception e){
            e.printStackTrace();
        }


        inputEditTexts.get(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Date", "Click on Date");
                showDatePicker();
            }
        });
        try {
            empId = Integer.parseInt(getSavedUserData(getActivity(), "userId"));
        } catch (Exception e) {
            e.printStackTrace();
        }



    }



    private void showDatePicker() {
        Log.d("Date", "Show Date Picker");
        DatePickerFragment date = new DatePickerFragment();
        /**
         * Set Up Current Date Into dialog
         */
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);
        /**
         * Set Call back to capture selected date
         */
        date.setCallBack(ondate);
        date.show(getFragmentManager(), "Date Picker");
    }
    @OnClick({R.id.tvSubmitExpense,R.id.tvChooseExpensePhoto})
    public void onClick(View view){

        switch (view.getId()){
            case R.id.tvSubmitExpense:

                    validateExpense();
                    break;
            case R.id.tvChooseExpensePhoto:


                 callToPickupImage();
                 break;


        }

    }

    final Calendar currentDate = Calendar.getInstance();

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog
            .OnDateSetListener() {


        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {


            System.out.println("Month Of Year: "+String.format("%02d",monthOfYear+1));
            System.out.println("Date Of Month: "+String.format("%02d",dayOfMonth));
            String month= String.format("%02d",monthOfYear+1);
            String dateOfMonth=String.format("%02d",dayOfMonth);

            // boatBookedDate=String.valueOf(year)+"-"+String.valueOf(monthOfYear+1)+"-"+String.valueOf(dayOfMonth);
            
            new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    date.set(Calendar.HOUR_OF_DAY, hourOfDay);
                    date.set(Calendar.MINUTE, minute);
                    String am_pm = null;
                    if (date.get(Calendar.AM_PM) == Calendar.AM)
                        am_pm = "AM";
                    else if (date.get(Calendar.AM_PM) == Calendar.PM)
                        am_pm = "PM";

                    String strHrsToShow = (date.get(Calendar.HOUR) == 0) ?"12":date.get(Calendar.HOUR)+"";

                    inputEditTexts.get(0).setText(String.valueOf(year) + "-" +month
                            + "-" +dateOfMonth+", "+strHrsToShow+":"+minute+":"+am_pm);
                    Log.v("AddExpense", "The choosen one " + date.get(Calendar.SECOND));

                    try {
                        String strHour=  String.format("%02d", hourOfDay);
                        String strMinute=String.format("%02d",minute);
                        String strSecond=String.format("%02d",date.get(Calendar.SECOND));
                        expenseDate=String.valueOf(year) + "-" + month
                                + "-" +dateOfMonth+"T"+strHour+":"+strMinute+":"+strSecond+".000Z";
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    //2021-07-14T06:51:15.432Z

                    System.out.println("FormatDate:"+expenseDate);
                }
            }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), false).show();

        }
    };
    private void validateExpense() {

        String dateExpense=inputEditTexts.get(0).getText().toString();
        String strTypeExpense=inputEditTexts.get(1).getText().toString();
        String strExpenseDescription=inputEditTexts.get(2).getText().toString();
        String strAmount=inputEditTexts.get(3).getText().toString();
        String strReceipt=textViewList.get(1).getText().toString();
        if(dateExpense.equals("")){
            Toasty.error(getActivity(),"Please Select Date ").show();

        }
        else if(strTypeExpense.equals("")){
            inputEditTexts.get(1).setError("Please enter type of expense");
        }
        else if(strExpenseDescription.equals("")){
            inputEditTexts.get(2).setError("Please enter description");
        }
        else if(strAmount.equals("") ){
            inputEditTexts.get(3).setError("Please enter amount");
        }else if(strReceipt.equals("")){
            Toasty.error(getActivity(),"Please select photo of expense").show();
        }
        else {

            ExpenseRequest expenseRequest=new ExpenseRequest();
            expenseRequest.setDatetime(String.valueOf(expenseDate));
            expenseRequest.setExpenseType(strTypeExpense);
            expenseRequest.setDescription(strExpenseDescription);
            expenseRequest.setExpenseReceipt(filePath);
            expenseRequest.setExpenseAmt(Double.valueOf(strAmount));
            expenseRequest.setEmployeeId(empId);
            if(textViewList.get(0).getText().toString().equals("UPDATE")) {
                expenseRequest.setExpenseMasterId(expenseResponse.getExpenseMasterId());
                callToUpdateExpense(expenseRequest);
            }
            else {
                callToAddExpense(expenseRequest);
            }


        }




    }




    private void callToPickupImage() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkCameraPermission()) {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, 111);
            } else if (checkStoragePermission()) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 112);
            } else {
                CropImage.activity().start(getActivity(), AddExpenseFragment.this);
            }
        } else {
            CropImage.activity().start(getActivity(), AddExpenseFragment.this);

        }
    }

    private boolean checkCameraPermission() {
        return ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkStoragePermission() {
        return ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == 203) {
                CropImage.ActivityResult result1 = CropImage.getActivityResult(data);
                if (resultCode == getActivity().RESULT_OK) {
                    Bitmap imageBitmap = null;
                    try {
                        imageBitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), result1.getOriginalUri());
                        ivExpense.setImageBitmap(imageBitmap);
                        Uri tempUri = getImageUri(getActivity(), imageBitmap);
                        // CALL THIS METHOD TO GET THE ACTUAL PATH
                        //File finalFile = new File(getRealPathFromURI(tempUri));
                        filePath = getRealPathFromURI(tempUri);

                        filePath = textViewList.get(1).getText().toString();

                        if (!filePath.equals(""))
                            sendImageToServer(filePath);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }





            } else {
                Toast.makeText(getActivity(), "You haven't picked Image", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    private void sendImageToServer(String path) {

        try {


            final File file = new File(path);

            RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("uploadfile", file.getName(), requestBody);


            Call<UploadImgResponse> call = Api.getClient().uploadImg(fileToUpload);
            call.enqueue(new Callback<UploadImgResponse>() {
                @Override
                public void onResponse(Call<UploadImgResponse> call, Response<UploadImgResponse> response) {
                    String resImage = "";

                    try {
                        if (response.body().getFlag()) {


                            filePath = response.body().getPath();
                            textViewList.get(1).setText(filePath);
                            if(filePath!=null){
                                Glide
                                        .with(getActivity())
                                        .load(filePath)
                                        .centerCrop()
                                        .into(ivExpense);
                            }
                            else {
                                ivExpense.setImageResource(R.drawable.no_image_found);
                            }

                        } else {

                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }



                }

                @Override
                public void onFailure(Call<UploadImgResponse> call, Throwable t) {
                    Toast.makeText(getContext(), "fail" + t.getMessage(), Toast.LENGTH_SHORT).show();


                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {

        if (getActivity().getContentResolver() != null) {
            Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                filePath = cursor.getString(idx);


                textViewList.get(1).setText(filePath);

                cursor.close();

                //

                Log.d("AddExpenseFilePath:", "" + filePath);
            }
        }
        return filePath;
    }

    private void callToAddExpense(ExpenseRequest expenseRequest) {

        ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.setTitle("Please wait ");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        progressDialog.setCancelable(false);

        Call<Boolean> call = Api.getClient().addExpense(expenseRequest);
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {


                try {
                    progressDialog.dismiss();
                    if (response.body()) {
                        Toasty.success(getActivity(), "Expense added successfully").show();
                        ((MainPage) getActivity()).removeCurrentFragmentAndMoveBack();
                        progressDialog.dismiss();


                    } else {
                        progressDialog.dismiss();

                        Toast.makeText(getContext(), "Record Not Add", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Log.e("Dealer List:", "" + t.getMessage());
                Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();

            }
        });

    }
    private void updateUI() {
        String strExpenseTime= String.valueOf(expenseResponse.getDatetime());

        long strExpenseTimeMillSecond=Long.parseLong(strExpenseTime);
        Date d = new Date((long)strExpenseTimeMillSecond);
        DateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.mmm'Z'");
        //System.out.println("Format Date: "+f.format(d));
        expenseDate = f.format(d);
        String strEndTime = android.text.format.DateFormat.format("yyyy-MM-dd , hh:mm a", new Date(strExpenseTimeMillSecond)).toString();
        //System.out.println("Time To Update"+strEndTime);
        inputEditTexts.get(0).setText(""+strEndTime);


        String strTypeExpense=expenseResponse.getExpenseType();
                inputEditTexts.get(1).setText(strTypeExpense);
        String strExpenseDescription=expenseResponse.getDescription();
                inputEditTexts.get(2).setText(strExpenseDescription);
        String strAmount=expenseResponse.getExpenseAmt();
                inputEditTexts.get(3).setText(strAmount);
        String strReceipt=expenseResponse.getExpenseReceipt();
                textViewList.get(1).setText(strReceipt);
                filePath=textViewList.get(1).getText().toString();

        if(strReceipt!=null){
            Glide
                    .with(getActivity())
                    .load(strReceipt)
                    .centerCrop()
                    .into(ivExpense);
        }
        else {
            ivExpense.setImageResource(R.drawable.no_image_found);
        }


    }

    private void callToUpdateExpense(ExpenseRequest expenseRequest) {

        ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.setTitle("Please wait ");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        progressDialog.setCancelable(false);

        Call<Boolean> call = Api.getClient().updateExpense(expenseRequest);
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {


                try {
                    progressDialog.dismiss();
                    if (response.body()) {
                        Toasty.success(getActivity(), "Expense Update successfully").show();
                        ((MainPage) getActivity()).removeCurrentFragmentAndMoveBack();
                        progressDialog.dismiss();


                    } else {
                        progressDialog.dismiss();

                        Toast.makeText(getContext(), "Record Not Update", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Log.e("Dealer List:", "" + t.getMessage());
                Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();

            }
        });
    }

}