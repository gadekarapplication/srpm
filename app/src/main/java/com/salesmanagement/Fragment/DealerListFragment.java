package com.salesmanagement.Fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Adapter.DealerListAdapter;
import com.salesmanagement.Extra.DetectConnection;
import com.salesmanagement.Model.DealerListResponse;
import com.salesmanagement.Model.DealerResponse;
import com.salesmanagement.R;
import com.salesmanagement.Retrofit.Api;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DealerListFragment extends Fragment {

    @BindView(R.id.rvDealerList)
    RecyclerView rvDealerList;
    DealerListAdapter dealerListAdapter;
    List<DealerResponse> dealerListResponseList = new ArrayList<>();
    @BindView(R.id.fabAddDealer)
    FloatingActionButton fabAddDealer;
    @BindView(R.id.searchDealerName)
    SearchView searchDealerName;


    public DealerListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dealer_list, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init() {
        MainPage.title.setText("Dealer List");
        rvDealerList.setLayoutManager(new LinearLayoutManager(getActivity()));
        fabAddDealer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainPage) getActivity()).loadFragment(new DealerRegistration(), true);
            }
        });
        // searchDealerName.setVisibility(View.GONE);
        searchDealerName.onActionViewExpanded();
        //searchViewHospital.setIconified(true);
        searchDealerName.setIconifiedByDefault(true);
        searchDealerName.setQueryHint("Search by firm name and dealer name");
        searchDealerName.setGravity(Gravity.CENTER_HORIZONTAL);
        searchDealerName.clearFocus();
        EditText searchEditText = (EditText) searchDealerName.findViewById(R.id.search_src_text);
        searchEditText.getText();
        searchEditText.setTextColor(getResources().getColor(R.color.black));
        searchEditText.setHintTextColor(getResources().getColor(R.color.black));
        ImageView searchMagIcon = (ImageView) searchDealerName.findViewById(R.id.search_close_btn);
        searchMagIcon.setImageResource(R.drawable.ic_close);
        ImageView searchPlate = (ImageView) searchDealerName.findViewById(R.id.search_mag_icon);
        searchPlate.setImageResource(R.drawable.ic_search);

        searchDealerName.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                processQuery(query);
                return true;
            }
        });
        getActiveListDealer();
    }

    private void processQuery(String query) {
        ArrayList<DealerResponse> result = new ArrayList<>();
        try {
        for (DealerResponse dealerListResponse : dealerListResponseList) {
            if (dealerListResponse.getFirmName().toLowerCase().contains(query.toLowerCase()) || dealerListResponse.getDealerFullName().toLowerCase().contains(query.toLowerCase())) {
                result.add(dealerListResponse);
            }
        }

            dealerListAdapter.setCriminalName(result);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onStart() {
        super.onStart();

        Log.e("onStart", "called");
        ((MainPage) getActivity()).lockUnlockDrawer(1);
        if (DetectConnection.checkInternetConnection(getActivity())) {
            init();
        } else {
            DetectConnection.noInternetConnection(getActivity());
        }
    }

    private void getActiveListDealer() {
      /*  ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.setTitle("Please wait ");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        progressDialog.setCancelable(false);*/
        //Call<List<DealerResponse>> call = Api.getClient().getDealerByEmp(Integer.valueOf(MainPage.userId));
        Call<List<DealerResponse>> call = Api.getClient().getDealerList();
        call.enqueue(new Callback<List<DealerResponse>>() {
            @Override
            public void onResponse(Call<List<DealerResponse>> call, Response<List<DealerResponse>> response) {

                if (response.isSuccessful()) {
                    try {
                       // progressDialog.dismiss();
                        dealerListResponseList.clear();
                        dealerListResponseList = response.body();
                        if (dealerListResponseList.size() > 0) {
                            //SubCategoryAdapter adapter = new SubCategoryAdapter(getActivity(), subCategoryResponseList);
                            dealerListAdapter = new DealerListAdapter(dealerListResponseList, getActivity());
                            //rvDealerList.setLayoutManager(new GridLayoutManager(getActivity(), 3));
                            rvDealerList.setAdapter(dealerListAdapter);
                            dealerListAdapter.notifyDataSetChanged();
                            rvDealerList.setHasFixedSize(true);


                            rvDealerList.setVisibility(View.VISIBLE);

                        } else {
                            //progressDialog.dismiss();
                            rvDealerList.setVisibility(View.GONE);
                            Toast.makeText(getContext(), "No Dealer Found", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                   // progressDialog.dismiss();
                    rvDealerList.setVisibility(View.GONE);
                    Toast.makeText(getContext(), "No Dealer Found", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<List<DealerResponse>> call, Throwable t) {
                Log.e("Dealer List:", "" + t.getMessage());
                Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
               // progressDialog.dismiss();
                rvDealerList.setVisibility(View.GONE);
            }
        });

    }
}