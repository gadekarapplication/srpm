package com.salesmanagement.Fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

public class ForwardDatePicker  extends DialogFragment {
    DatePickerDialog.OnDateSetListener ondateSet;
    private int year, month, day;
    public ForwardDatePicker(){

    }
    public void setCallBack(DatePickerDialog.OnDateSetListener ondate) {
        ondateSet = ondate;
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        year = args.getInt("year");
        month = args.getInt("month");
        day = args.getInt("day");
    }
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        //return super.onCreateDialog(savedInstanceState);

        DatePickerDialog picker = new DatePickerDialog(getActivity(),
                ondateSet, year, month, day);
        // picker.getDatePicker().setMinDate((System.currentTimeMillis() - 10000));
        //picker.getDatePicker().setMaxDate(System.currentTimeMillis());
       // picker.setMinDate(System.currentTimeMillis() - 1000);
        picker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        return picker;
    }
}
