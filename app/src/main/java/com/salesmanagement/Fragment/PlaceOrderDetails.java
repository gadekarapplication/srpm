package com.salesmanagement.Fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Adapter.AddressListAdapter;
import com.salesmanagement.Adapter.CartAdapter;
import com.salesmanagement.Extra.DetectConnection;
import com.salesmanagement.Model.CartResponse;
import com.salesmanagement.Model.DealerAddressResponseModel;
import com.salesmanagement.Model.OrderResponse;
import com.salesmanagement.R;
import com.salesmanagement.Retrofit.Api;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PlaceOrderDetails extends Fragment implements CartAdapter.IDeleteCartItem {

    public String dealerId, companyId, addressId;
    View view;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindViews({

            R.id.customerNameValue,R.id.customerNumberValue,
            R.id.CreditLimitValue,R.id.customerRemainingAmtValue,
            R.id.tvTotalAmtProd,R.id.tvTotalDiscountAmount,R.id.tvTotalGSTAmt,
            R.id.finalAmountValue,R.id.customerAddressValue})
    List<TextView> textViews;
    public List<CartResponse> cartResponseList = new ArrayList<>();
    Double totalQuantity=0.0;
            Double totalDiscountPercent=0.0;
    CartAdapter adapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_place_order_details, container, false);
        ButterKnife.bind(this, view);
        MainPage.title.setText("Order Review");

        try {
            Bundle bundle = getArguments();
            dealerId = bundle.getString("dealerId");
            companyId = bundle.getString("companyId");
            addressId = bundle.getString("addressId");
            System.out.println("AddressId"+addressId);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    @OnClick({R.id.placeOrder})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.placeOrder:
                if (DetectConnection.checkInternetConnection(getActivity())) {
                    placeOrder(cartResponseList);
                } else {
                    DetectConnection.noInternetConnection(getActivity());
                }
                break;

        }

    }


    private void placeOrder(List<CartResponse> cartResponseList) {

        final SweetAlertDialog pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.colorPrimary));
        pDialog.setTitleText("Please wait");
        pDialog.setContentText("Order is placing");
        pDialog.setCancelable(false);
        pDialog.show();

        OrderResponse orderResponse = new OrderResponse();
        orderResponse.setDealershipId("" + dealerId);
        orderResponse.setOrderPaymentMode("Cash");
        //orderResponse.setFinalTotalAmount("");
        orderResponse.setCartResponseList(cartResponseList);

        orderResponse.setFinalTotalAmount(""+textViews.get(7).getText().toString());
        orderResponse.setFinalTotalGstAmount(textViews.get(6).getText().toString());
        orderResponse.setFinalTotalDiscountAmount(textViews.get(5).getText().toString());
        orderResponse.setDealerAddressId(addressId);
        orderResponse.setOrderDeliveryAddress(textViews.get(8).getText().toString());
        //orderResponse.setOrderBillingAddress(addressId);
        orderResponse.setFinalTotalQty( String.format("%.2f", (totalQuantity)));
        orderResponse.setFinalTotalDiscountPer( String.format("%.2f", (totalDiscountPercent)));
        Call<Boolean> call = Api.getClient().postOrder(orderResponse);
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {

                if (response.isSuccessful()) {

                    if (response.body().booleanValue() == true) {
                        pDialog.dismiss();

                        final Dialog dialog = new Dialog(getActivity());
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
                        dialog.setContentView(R.layout.confirmation_dialog);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        dialog.setCancelable(false);
                        TextView txtYes = dialog.findViewById(R.id.yes);

                        txtYes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                try {
                                    ((MainPage) getActivity()).removeCurrentFragmentAndMoveBack();
                                    ((MainPage) getActivity()).loadFragment(new Home(), true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        dialog.show();

                    } else if (response.body().booleanValue() == false) {
                        pDialog.dismiss();

                        final Dialog dialog = new Dialog(getActivity());
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
                        dialog.setContentView(R.layout.error_dialog);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        dialog.setCancelable(false);
                        TextView txtYes = dialog.findViewById(R.id.yes);

                        txtYes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                try {
                                    ((MainPage) getActivity()).removeCurrentFragmentAndMoveBack();
                                    ((MainPage) getActivity()).loadFragment(new Home(), false);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        dialog.show();

                    }

                } else {
                    pDialog.dismiss();

                    final Dialog dialog = new Dialog(getActivity());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
                    dialog.setContentView(R.layout.error_dialog);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    dialog.setCancelable(false);
                    TextView txtYes = dialog.findViewById(R.id.yes);

                    txtYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            try {
                                ((MainPage) getActivity()).removeCurrentFragmentAndMoveBack();
                                ((MainPage) getActivity()).loadFragment(new Home(), false);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

                    dialog.show();

                }

            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                pDialog.dismiss();
                Log.e("orderError", "" + t.getMessage());
                Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
            }
        });


    }


    @Override
    public void onStart() {
        super.onStart();
        Log.e("onStart", "called");
        ((MainPage) getActivity()).lockUnlockDrawer(1);
        if (DetectConnection.checkInternetConnection(getActivity())) {
            getDealerAddressById(addressId);
            getCartList(dealerId, companyId);
        } else {
            DetectConnection.noInternetConnection(getActivity());
        }
    }

    private void getDealerAddressById(String dealerId) {
        Call<DealerAddressResponseModel> call = Api.getClient().getDealerAddressById(Integer.parseInt(dealerId));
        call.enqueue(new Callback<DealerAddressResponseModel>() {
            @Override
            public void onResponse(Call<DealerAddressResponseModel> call, Response<DealerAddressResponseModel> response) {

                if (response.body()!=null) {
                     DealerAddressResponseModel dealerAddressResponseModel=     response.body();
                    dealerAddressResponseModel.getFullAddress();
                    dealerAddressResponseModel.getFlatNo();
                    dealerAddressResponseModel.getArea();
                    dealerAddressResponseModel.getLandmark();
                    textViews.get(8).setText(""+dealerAddressResponseModel.getFullAddress()+
                            ", "+dealerAddressResponseModel.getFlatNo()+","+dealerAddressResponseModel.getArea()+
                            ","+dealerAddressResponseModel.getLandmark());
                }

            }

            @Override
            public void onFailure(Call<DealerAddressResponseModel> call, Throwable t) {
                Log.e("address", "" + t.getMessage());

                Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void getCartList(String dealerId, String companyId) {

        ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        progressDialog.setCancelable(false);

        cartResponseList.clear();
        recyclerView.clearOnScrollListeners();

        Call<List<CartResponse>> call = Api.getClient().getCartList(dealerId, companyId);
        call.enqueue(new Callback<List<CartResponse>>() {
            @Override
            public void onResponse(Call<List<CartResponse>> call, Response<List<CartResponse>> response) {

                if (response.isSuccessful()) {
                    cartResponseList = response.body();
                    Log.e("cartResponseList", "" + cartResponseList.size());
                    double totalProductAmount = 0f;
                    double discountAmount = 0f,totalGstAmt=0f,finalAmount=0f;

                    if (cartResponseList.size() > 0) {

                        try {
                            for (int i = 0; i < cartResponseList.size(); i++) {
                                totalProductAmount = totalProductAmount + (Double.parseDouble(cartResponseList.get(i).getProductAmount()) * Double.parseDouble(cartResponseList.get(i).getQty()));
                                discountAmount = discountAmount + (Double.parseDouble(cartResponseList.get(i).getDiscountAmount()));
                                totalGstAmt=totalGstAmt+(Double.parseDouble(cartResponseList.get(i).getGstAmount()));
                                finalAmount=finalAmount+(Double.parseDouble(cartResponseList.get(i).getFinalAmount()));
                                totalQuantity = totalQuantity + Double.parseDouble(cartResponseList.get(i).getQty());
                                totalDiscountPercent= totalDiscountPercent + Double.parseDouble(cartResponseList.get(i).getDiscountPercentage());
                            }
                            String totalProductAmt = String.format("%.2f", (totalProductAmount));
                            String discountTotal = String.format("%.2f", (discountAmount));
                            String strTotalGstAmt=String.format("%.2f",totalGstAmt);
                            //double finalAmount = totalProductAmount - discountAmount;
                            String finalTotal = String.format("%.2f", (finalAmount));

                            textViews.get(4).setText("" + totalProductAmt);
                            textViews.get(5).setText("" + discountTotal);
                            textViews.get(6).setText("" + strTotalGstAmt);
                            textViews.get(7).setText("" + finalTotal);

                            textViews.get(0).setText(""+cartResponseList.get(0).getDealerFullName());
                            textViews.get(1).setText(""+cartResponseList.get(0).getDealerMobileNo());
                            textViews.get(2).setText(""+cartResponseList.get(0).getDealerCreditLimit());
                            textViews.get(3).setText(""+cartResponseList.get(0).getDealerRemaimingAmt());
                            //textViews.get(6).setText("Customer Address: "+ companyAddress);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        adapter = new CartAdapter(getActivity(), cartResponseList,PlaceOrderDetails.this::deleteCartItem,"PlaceOrder");
                        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                        recyclerView.setAdapter(adapter);
                        recyclerView.setHasFixedSize(true);
                        adapter.notifyDataSetChanged();

                        progressDialog.dismiss();

                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), "No Cart Found", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), "No Cart Found", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<List<CartResponse>> call, Throwable t) {
                Log.e("cartError", "" + t.getMessage());
                progressDialog.dismiss();
                Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void deleteCartItem(int position, int cartId) {

        Call<Boolean> call = Api.getClient().deleteCartItem(cartId);
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                try {
                    if(response.body()){
                        adapter.notifyDataSetChanged();


                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {

                try {

                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });

    }
}