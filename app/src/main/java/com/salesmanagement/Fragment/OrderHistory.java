package com.salesmanagement.Fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Adapter.OrderAdapter;
import com.salesmanagement.Extra.DetectConnection;
import com.salesmanagement.Model.UserOrderResponse;
import com.salesmanagement.R;
import com.salesmanagement.Retrofit.Api;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.salesmanagement.Extra.Common.getSavedUserData;


public class OrderHistory extends Fragment {

    View view;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    List<UserOrderResponse> userOrderResponseList = new ArrayList<>();
    String empId;
    String selectedEmp;
    @BindView(R.id.fabAddOrder)
    FloatingActionButton fabAddOrder;
    @BindView(R.id.tvNoOrderFound)
    TextView tvNoOrderFound;
    @BindView(R.id.pbOrderHis)
    ProgressBar pbOrderHis;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_order_history, container, false);
        ButterKnife.bind(this, view);



        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e("onStart", "called");
        ((MainPage) getActivity()).lockUnlockDrawer(1);
        if (DetectConnection.checkInternetConnection(getActivity())) {
           init();

        } else {
            DetectConnection.noInternetConnection(getActivity());
        }
    }

    private void init() {
        MainPage.title.setText("Order History");
        try {
            empId = getSavedUserData(getActivity(), "userId");

            selectedEmp = getArguments().getString("EMPLOYEE_ID");
            if(selectedEmp!=null){
                System.out.println("Selected Emp:"+selectedEmp);
                empId= selectedEmp;
            }
            System.out.println("EMP Get OrderHis:"+selectedEmp);

        } catch (Exception e) {
            e.printStackTrace();
        }

        getOrderList(empId);
        fabAddOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainPage)getActivity()).loadFragment(new SalesOrders(),true);
            }
        });
    }

    private void getOrderList(String empId) {


        pbOrderHis.setVisibility(View.VISIBLE);

        recyclerView.clearOnScrollListeners();
        userOrderResponseList.clear();

        Call<List<UserOrderResponse>> call = Api.getClient().getOrderList(empId);
        call.enqueue(new Callback<List<UserOrderResponse>>() {
            @Override
            public void onResponse(Call<List<UserOrderResponse>> call, Response<List<UserOrderResponse>> response) {

                if (response.isSuccessful()) {

                    userOrderResponseList = response.body();
                    if (userOrderResponseList.size() > 0) {

                        OrderAdapter adapter = new OrderAdapter(getActivity(), userOrderResponseList);
                        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                        recyclerView.setAdapter(adapter);
                        recyclerView.setHasFixedSize(true);
                        adapter.notifyDataSetChanged();

                        pbOrderHis.setVisibility(View.GONE);
                        tvNoOrderFound.setVisibility(View.GONE);

                    } else {
                        tvNoOrderFound.setVisibility(View.VISIBLE);
                        pbOrderHis.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "No Order Found", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    tvNoOrderFound.setVisibility(View.VISIBLE);
                    pbOrderHis.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), "No Order Found", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<List<UserOrderResponse>> call, Throwable t) {
                Log.e("orderError", "" + t.getMessage());
                pbOrderHis.setVisibility(View.GONE);
            }
        });

    }
}