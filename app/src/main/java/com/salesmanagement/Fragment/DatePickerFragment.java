package com.salesmanagement.Fragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.widget.DatePicker;

import androidx.fragment.app.DialogFragment;

public class DatePickerFragment extends DialogFragment {


    DatePickerDialog.OnDateSetListener ondateSet;
    DatePicker.OnDateChangedListener onDateC;
    private int year, month, day;

    public DatePickerFragment() {
    }

    public void setCallBack(DatePickerDialog.OnDateSetListener ondate) {
        ondateSet = ondate;
    }


    @SuppressLint("NewApi")
    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        year = args.getInt("year");
        month = args.getInt("month");
        day = args.getInt("day");
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        DatePickerDialog picker = new DatePickerDialog(getActivity(),
                ondateSet, year, month, day);
        // picker.getDatePicker().setMinDate((System.currentTimeMillis() - 10000));
        picker.getDatePicker().setMaxDate(System.currentTimeMillis());
        return picker;

        // return new DatePickerDialog(getActivity(), ondateSet, year, month, day);
    }
}
