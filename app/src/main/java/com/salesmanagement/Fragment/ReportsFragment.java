package com.salesmanagement.Fragment;

import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Extra.DetectConnection;
import com.salesmanagement.R;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ReportsFragment extends Fragment {

    @BindViews({R.id.cv_visit_report,R.id.cv_sale_report})
    List<CardView> cardViewList;
    public ReportsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_reports, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        ((MainPage) getActivity()).lockUnlockDrawer(1);
        init();

    }

    private void init() {
        MainPage.title.setText("Reports");

    }
    @OnClick({R.id.cv_visit_report,R.id.cv_sale_report,R.id.cv_Collection_report})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.cv_sale_report:
                ((MainPage)getActivity()).loadFragment(new VisitorFragment("Sales"),true);
                break;
            case R.id.cv_visit_report:
                ((MainPage)getActivity()).loadFragment(new VisitorFragment("Visit"),true);
                break;
            case R.id.cv_Collection_report:
                ((MainPage)getActivity()).loadFragment(new VisitorFragment("Collections"),true);
                break;
        }
    }
}