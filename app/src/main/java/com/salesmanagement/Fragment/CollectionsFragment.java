package com.salesmanagement.Fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputEditText;
import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Extra.DetectConnection;
import com.salesmanagement.Model.CollectionRequest;
import com.salesmanagement.Model.CompanyResponse;
import com.salesmanagement.Model.DealerResponse;
import com.salesmanagement.R;
import com.salesmanagement.Retrofit.Api;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import fr.ganfra.materialspinner.MaterialSpinner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.salesmanagement.Extra.Common.getSavedUserData;


public class CollectionsFragment extends Fragment {


    public String dealerName, dealerAddress, companyName, strTransactionType;
    public List<DealerResponse> dealerResponseList = new ArrayList<>();
    public String[] dealerIdList, dealerNameList, dealerAddressList, companyIdList, companyNameList;
    public List<CompanyResponse> companyResponseList = new ArrayList<>();
    @BindViews({R.id.dealerSpinnerCollection, R.id.dealerSpinnerCompany, R.id.transactionTypeCollection
    })
    List<MaterialSpinner> materialSpinnerList;
    @BindViews({R.id.collectionAmount, R.id.dateCollection, R.id.edtChequeNumberCollection, R.id.bankNameCollection, R.id.accNumberCollection, R.id.ifscCodeCollection,
            R.id.endDateChequeCollection})
    List<TextInputEditText> textInputEditTextList;
    @BindView(R.id.cv_submit_collection)
    CardView cv_submit_collection;
    Integer companyId, dealerId;
    @BindView(R.id.tieFirmNameCollection)
            TextInputEditText tieFirmNameCollection;
    Dialog dialogDealer;
    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog
            .OnDateSetListener() {


        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {


            textInputEditTextList.get(1).setText(String.valueOf(year) + "-" + String.valueOf(monthOfYear + 1)
                    + "-" + String.valueOf(dayOfMonth));

            System.out.println("Date: " + textInputEditTextList.get(1).getText().toString());
            // boatBookedDate=String.valueOf(year)+"-"+String.valueOf(monthOfYear+1)+"-"+String.valueOf(dayOfMonth);


        }
    };


    public CollectionsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_collections, container, false);
        ButterKnife.bind(this, view);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy =
                    new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        callToGetCompanyList();
        callToGetDealer();

        return view;
    }

    private void init() {


        materialSpinnerList.get(0).setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                try {

                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));

                    dealerId = Integer.valueOf(dealerIdList[position]);
                    dealerName = dealerNameList[position];
                    dealerAddress = dealerAddressList[position];
                    //getCompanyList();


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        materialSpinnerList.get(1).setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                try {

                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));

                    companyId = Integer.valueOf(companyIdList[position]);
                    System.out.println("" + companyId);
                    companyName = companyNameList[position];


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        List<String> transactionType = new ArrayList<>();
        transactionType.add("Online");
        transactionType.add("Cash");
        transactionType.add("Google Pay");
        transactionType.add("Phone Pay");

        final ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, transactionType);
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
        materialSpinnerList.get(2).setAdapter(adapter);
        materialSpinnerList.get(2).setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                try {

                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));

                    strTransactionType = transactionType.get(position);
                    if (strTransactionType.equals("Cheque")) {
                        textInputEditTextList.get(2).setVisibility(View.VISIBLE);
                        textInputEditTextList.get(3).setVisibility(View.VISIBLE);
                        textInputEditTextList.get(4).setVisibility(View.VISIBLE);
                        textInputEditTextList.get(5).setVisibility(View.VISIBLE);
                        textInputEditTextList.get(6).setVisibility(View.VISIBLE);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        textInputEditTextList.get(1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });

        tieFirmNameCollection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogDealer=new Dialog(getActivity());

                dialogDealer.setContentView(R.layout.drawable_searchable_spinner);

                // dialogFirm.getWindow().setLayout(650,800);
                Window window = dialogDealer.getWindow();
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                dialogDealer.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialogDealer.show();

                EditText editText=dialogDealer.findViewById(R.id.etFirm);
                TextView tvSelect=  dialogDealer.findViewById(R.id.tvSelect);
                tvSelect.setText("Select Firm");
                ListView lview=dialogDealer.findViewById(R.id.listViewFirm);

                ArrayAdapter<String> adapter=new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1,dealerNameList);
                lview.setAdapter(adapter);

                editText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        adapter.getFilter().filter(charSequence);
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });
                lview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        tieFirmNameCollection.setText(adapter.getItem(i));
                        dialogDealer.dismiss();

                        dealerId = Integer.valueOf(dealerIdList[i]);
                        dealerName = dealerNameList[i];
                        System.out.println("DealerName:"+dealerName);
                        System.out.println("DealerId:"+dealerId);



                    }
                });
            }
        });
    }

    private void showDatePicker() {
        Log.d("Date", "Show Date Picker");
        DatePickerFragment date = new DatePickerFragment();
        /**
         * Set Up Current Date Into dialog
         */
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);
        /**
         * Set Call back to capture selected date
         */
        date.setCallBack(ondate);
        date.show(getFragmentManager(), "Date Picker");
    }

    @Override
    public void onStart() {
        super.onStart();
        MainPage.title.setText("Add Collections");
        ((MainPage) getActivity()).lockUnlockDrawer(1);
        if (DetectConnection.checkInternetConnection(getActivity())) {

            init();

        } else {
            DetectConnection.noInternetConnection(getActivity());
        }


    }

    public void callToGetDealer() {
        //Call<List<DealerResponse>> call = Api.getClient().getDealerByEmp(Integer.valueOf(MainPage.userId));
        Call<List<DealerResponse>> call = Api.getClient().getDealerList();
        call.enqueue(new Callback<List<DealerResponse>>() {
            @Override
            public void onResponse(Call<List<DealerResponse>> call, Response<List<DealerResponse>> response) {
                if (response.isSuccessful()) {
                    try {
                        dealerResponseList = response.body();
                        if (dealerResponseList != null) {

                            dealerIdList = new String[dealerResponseList.size()];
                            dealerNameList = new String[dealerResponseList.size()];
                            dealerAddressList = new String[dealerResponseList.size()];
                            for (int i = 0; i < dealerResponseList.size(); i++) {
                                dealerIdList[i] = dealerResponseList.get(i).getDealershipId();
                                dealerNameList[i] = dealerResponseList.get(i).getFirmName();
                                dealerAddressList[i] = dealerResponseList.get(i).getFirmAddress();
                            }
                            try {
                                final ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, dealerNameList);
                                adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
                                materialSpinnerList.get(0).setAdapter(adapter);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        } else {

                            Log.d("Coman:", "No Dealer Found");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {

                }


            }

            @Override
            public void onFailure(Call<List<DealerResponse>> call, Throwable t) {


            }
        });

    }

    public void callToGetCompanyList() {

        companyResponseList.clear();

        Call<List<CompanyResponse>> call = Api.getClient().getCompanyList();
        call.enqueue(new Callback<List<CompanyResponse>>() {
            @Override
            public void onResponse(Call<List<CompanyResponse>> call, Response<List<CompanyResponse>> response) {

                if (response.isSuccessful()) {
                    try {
                        companyResponseList = response.body();
                        if (companyResponseList != null) {

                            companyIdList = new String[companyResponseList.size()];
                            companyNameList = new String[companyResponseList.size()];

                            for (int i = 0; i < companyResponseList.size(); i++) {
                                companyIdList[i] = companyResponseList.get(i).getCompId();
                                companyNameList[i] = companyResponseList.get(i).getCompName();
                            }

                            try {
                                final ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, companyNameList);
                                adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
                                materialSpinnerList.get(1).setAdapter(adapter);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else {
                            //Toast.makeText(getActivity(), "No Company Found", Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    //Toast.makeText(getActivity(), "No Company Found", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<List<CompanyResponse>> call, Throwable t) {
                Log.e("categoryError", "" + t.getMessage());
                //Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @OnClick({R.id.cv_submit_collection})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cv_submit_collection:
                collectionValidation();
                break;
        }

    }

    private void collectionValidation() {

        String strAmount = textInputEditTextList.get(0).getText().toString();
        String date = textInputEditTextList.get(1).getText().toString();
        String strChequeNumber = textInputEditTextList.get(2).getText().toString();
        String strBankName = textInputEditTextList.get(3).getText().toString();
        String strAccountNumber = textInputEditTextList.get(4).getText().toString();
        String strIfscCode = textInputEditTextList.get(5).getText().toString();
        String strEndDateCheque = textInputEditTextList.get(6).getText().toString();

        if (companyId == null) {
            Toasty.error(getActivity(), "Select company").show();
        } else if (dealerId == null) {
            Toasty.error(getActivity(), "Select dealer").show();
        } else if (strAmount.equals("")) {
            textInputEditTextList.get(0).setError("Enter amount");
        } else if (date.equals("")) {
            Toasty.error(getActivity(), "Select date").show();
        } else if (strTransactionType.equals("")) {
            Toasty.error(getActivity(), "Select transaction type").show();
        } else if (strTransactionType.equals("Cheque")) {
            if (strChequeNumber.equals("")) {
                textInputEditTextList.get(2).setError("Enter Cheque Number");
            } else if (strBankName.equals("")) {
                textInputEditTextList.get(3).setError("Enter Bank Name");
            } else if (strAccountNumber.equals("")) {
                textInputEditTextList.get(4).setError("Enter Account Number");
            } else if (strIfscCode.equals("")) {
                textInputEditTextList.get(5).setError("Enter IFSC Code");
            } else if (strEndDateCheque.equals("")) {
                textInputEditTextList.get(6).setError("Select End Date");
            } else {
                CollectionRequest collectionRequest = new CollectionRequest();
                collectionRequest.setCompId(companyId);
                collectionRequest.setDealershipId(dealerId);
                collectionRequest.setTotalAmount(Float.parseFloat(strAmount));
                collectionRequest.setTotalTransactionSaleDate(date);
                collectionRequest.setTransactionType(strTransactionType);


                collectionRequest.setEmployeeId(Integer.parseInt(getSavedUserData(getActivity(), "userId")));
                callToAddCollection(collectionRequest);
            }


        } else {
            CollectionRequest collectionRequest = new CollectionRequest();
            collectionRequest.setCompId(companyId);
            collectionRequest.setDealershipId(dealerId);
            collectionRequest.setTotalAmount(Float.parseFloat(strAmount));
            collectionRequest.setTotalTransactionSaleDate(date);
            collectionRequest.setTransactionType(strTransactionType);

            collectionRequest.setEmployeeId(Integer.parseInt(getSavedUserData(getActivity(), "userId")));
            callToAddCollection(collectionRequest);

        }


    }

    private void callToAddCollection(CollectionRequest collectionRequest) {


        try {
            ProgressDialog progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Loading...");
            progressDialog.setTitle("Please wait...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
            progressDialog.setCancelable(false);


            Call<Boolean> call = Api.getClient().addCollection(collectionRequest);
            call.enqueue(new Callback<Boolean>() {
                @Override
                public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                    progressDialog.dismiss();
                    if (response.body()) {

                        Toasty.success(getActivity(), "Collection Add Successfully").show();
                        //((MainPage) getActivity()).loadFragment(new DealerListFragment(), true);
                        ((MainPage) getActivity()).removeCurrentFragmentAndMoveBack();
                        //Toast.makeText(getContext(), "" + resImage, Toast.LENGTH_SHORT).show();

                    } else {
                        Toasty.error(getActivity(), "Not  Added").show();
                    }


                    // Toast.makeText(getContext(), "aadharback"+aadharfrontPath, Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onFailure(Call<Boolean> call, Throwable t) {
                    Toast.makeText(getContext(), "fail" + t.getMessage(), Toast.LENGTH_SHORT).show();

                    progressDialog.dismiss();
                }
            });


        } catch (Exception e) {
            e.printStackTrace();

        }


    }
}