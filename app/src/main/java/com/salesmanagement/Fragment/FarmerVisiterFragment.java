package com.salesmanagement.Fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.andreabaccega.widget.FormEditText;
import com.canhub.cropper.CropImage;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Extra.DetectConnection;
import com.salesmanagement.Model.FarmerVisiterRequest;
import com.salesmanagement.Model.UploadImgResponse;
import com.salesmanagement.R;
import com.salesmanagement.Retrofit.Api;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Calendar;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.salesmanagement.Adapter.FarmerListAdapter.farmer_id;


public class FarmerVisiterFragment extends Fragment {

    public String documentType;
    Intent galleryIntent;
    private View view;
    private FormEditText et_area, et_crop, et_demo_place, et_district, et_irrigation, et_medicine, et_other_crop, et_remark, et_water_source, et_work_place;
    private TextView tv_date, tv_image_path, tv_file_choose, tv_submit;
    private LinearLayout linearLayout_landConversation, linearLayout_aggree_to_member;
    private RadioButton rb_land_yes, rb_land_no, rb_agree_member_yes, rb_agree_member_no;
    private String selectedImagePath = "";
    private boolean landValue = false, agree_member_value = false;
    private String responseImage="",imgPath="";
    DatePickerDialog.OnDateSetListener onDate = new DatePickerDialog
            .OnDateSetListener() {


        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {


            tv_date.setText(String.valueOf(year) + "-" + String.valueOf(monthOfYear + 1)
                    + "-" + String.valueOf(dayOfMonth));



        }
    };
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_farmer_visiter, container, false);

        init();

        listener();

        return view;
    }


    private void init() {
        et_area = view.findViewById(R.id.et_area);
        et_crop = view.findViewById(R.id.et_crop);
        tv_date = view.findViewById(R.id.tv_date);
        et_demo_place = view.findViewById(R.id.et_demo_place);
        et_district = view.findViewById(R.id.et_district);
        et_irrigation = view.findViewById(R.id.et_irrigation);
        et_medicine = view.findViewById(R.id.et_medicine);
        et_other_crop = view.findViewById(R.id.et_other_crop);
        et_remark = view.findViewById(R.id.et_remark);
        et_water_source = view.findViewById(R.id.et_water_source);
        et_work_place = view.findViewById(R.id.et_work_place);
        tv_image_path = view.findViewById(R.id.tv_image_path);
        tv_file_choose = view.findViewById(R.id.tv_file_choose);
        linearLayout_landConversation = view.findViewById(R.id.linearLayout_landConversation);
        linearLayout_aggree_to_member = view.findViewById(R.id.linearLayout_aggree_to_member);
        rb_land_yes = view.findViewById(R.id.rb_land_yes);
        rb_land_no = view.findViewById(R.id.rb_land_no);
        rb_agree_member_yes = view.findViewById(R.id.rb_agree_member_yes);
        rb_agree_member_no = view.findViewById(R.id.rb_agree_member_no);
        tv_submit = view.findViewById(R.id.tv_submit);
        System.out.println(""+farmer_id);
    }

    private void listener() {
        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (et_area.testValidity() && et_crop.testValidity() && et_demo_place.testValidity() && et_district.testValidity() && et_irrigation.testValidity() && et_medicine.testValidity() && et_other_crop.testValidity() && et_remark.testValidity() && et_water_source.testValidity() && et_work_place.testValidity()) {

                    if (!selectedImagePath.equalsIgnoreCase("")) {

                        FarmerVisiterRequest farmerVisiterRequest = new FarmerVisiterRequest();

                        farmerVisiterRequest.setArea(et_area.getText().toString());
                        farmerVisiterRequest.setCrop(et_crop.getText().toString());
                        farmerVisiterRequest.setDemoPlace(et_demo_place.getText().toString());
                        farmerVisiterRequest.setDistrict(et_district.getText().toString());
                        farmerVisiterRequest.setIrrigation(et_irrigation.getText().toString());
                        farmerVisiterRequest.setMedicines(et_medicine.getText().toString());
                        farmerVisiterRequest.setOtherCrops(et_other_crop.getText().toString());
                        farmerVisiterRequest.setRemark(et_remark.getText().toString());
                        farmerVisiterRequest.setWaterSource(et_water_source.getText().toString());
                        farmerVisiterRequest.setWorkPlace(et_work_place.getText().toString());
                        farmerVisiterRequest.setLandConservationMember(landValue);
                        farmerVisiterRequest.setAgreeToBeMember(agree_member_value);
                        farmerVisiterRequest.setVisitPhoto(responseImage);
                        farmerVisiterRequest.setEmployeeId(Integer.valueOf(MainPage.userId));
                        farmerVisiterRequest.setFarmerId(farmer_id);
                        Call<Boolean> call = Api.getClient().saveFarmerVisiter(farmerVisiterRequest);
                        call.enqueue(new Callback<Boolean>() {
                            @Override
                            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                                if(response.body()) {
                                    Toast.makeText(getActivity(), "Visit added successfully", Toast.LENGTH_SHORT).show();
                                    ((MainPage)getActivity()).removeCurrentFragmentAndMoveBack();
                                }
                                else {
                                    Toast.makeText(getActivity(), "Visit not added ", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<Boolean> call, Throwable t) {

                            }
                        });

                    } else {
                        Toast.makeText(getActivity(), "Please Select Image", Toast.LENGTH_SHORT).show();
                    }
                }


            }
        });

        tv_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });

        rb_land_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                landValue = true;
            }
        });

        rb_land_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                landValue = false;
            }
        });

        rb_agree_member_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                agree_member_value = true;
            }
        });

        rb_agree_member_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                agree_member_value = false;
            }
        });

        tv_file_choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                documentType = "visitPhoto";

               callToPickupImage();

            }
        });


    }
    private void showDatePicker() {
        Log.d("Date", "Show Date Picker");
        DatePickerFragment date = new DatePickerFragment();
        /**
         * Set Up Current Date Into dialog
         */
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);
        /**
         * Set Call back to capture selected date
         */
        date.setCallBack(onDate);
        date.show(getFragmentManager(), "Date Picker");
    }
    @Override
    public void onStart() {
        super.onStart();
        Log.e("onStart", "called");

        MainPage.title.setText("Farmer Visit");

        ((MainPage) getActivity()).lockUnlockDrawer(1);
        if (DetectConnection.checkInternetConnection(getActivity())) {
            requestPermission();

        } else {
            DetectConnection.noInternetConnection(getActivity());
        }
    }


    private void requestPermission() {

        Dexter.withActivity(getActivity())
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_CONTACTS,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {

                        }
                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            // showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getActivity(), "Error occurred! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();

    }

    private void showSettingsDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }

        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }


    private void sendImage(String imagePath) {

        try {


            File file = new File(imagePath);

            RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("uploadfile", file.getName(), requestBody);


            Call<UploadImgResponse> call = Api.getClient().uploadImg(fileToUpload);
            call.enqueue(new Callback<UploadImgResponse>() {
                @Override
                public void onResponse(Call<UploadImgResponse> call, Response<UploadImgResponse> response) {

                    if (response.body().getFlag()) {
                        selectedImagePath= responseImage = response.body().getPath();

                        //Toast.makeText(getContext(), "" + responseImage, Toast.LENGTH_SHORT).show();
                    } else {
                    }


                    // Toast.makeText(getContext(), "aadharback"+aadharfrontPath, Toast.LENGTH_SHORT).show();


                }

                @Override
                public void onFailure(Call<UploadImgResponse> call, Throwable t) {
                    Toast.makeText(getContext(), "fail" + t.getMessage(), Toast.LENGTH_SHORT).show();


                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }





    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result1 = CropImage.getActivityResult(data);
            if (resultCode == getActivity().RESULT_OK) {
                Bitmap imageBitmap = null;
                try {
                    imageBitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), result1.getOriginalUri());

                    Uri tempUri = getImageUri(getActivity(), imageBitmap);
                    // CALL THIS METHOD TO GET THE ACTUAL PATH
                    File finalFile = new File(getRealPathFromURI(tempUri));


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }


    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {

        if (getActivity().getContentResolver() != null) {
            Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                imgPath = cursor.getString(idx);

                tv_image_path.setText(cursor.getString(idx));

                sendImage((tv_image_path.getText().toString()));
                cursor.close();

                // textViews.get(0).setText(firstPath);

                Log.d("paths", "" + imgPath);
            }
        }
        return imgPath;
    }

    private void callToPickupImage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkCameraPermission()) {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, 111);
            } else if (checkStoragePermission()) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 112);
            } else {
                CropImage.activity().start(getActivity(), FarmerVisiterFragment.this);
            }
        } else {
            CropImage.activity().start(getActivity(), FarmerVisiterFragment.this);

        }
    }

    private boolean checkCameraPermission() {
        return ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkStoragePermission() {
        return ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED;
    }
}