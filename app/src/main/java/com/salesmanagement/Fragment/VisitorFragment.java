package com.salesmanagement.Fragment;

import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.highsoft.highcharts.common.HIColor;
import com.highsoft.highcharts.common.hichartsclasses.HIButtonOptions;
import com.highsoft.highcharts.common.hichartsclasses.HICSSObject;
import com.highsoft.highcharts.common.hichartsclasses.HIChart;

import com.highsoft.highcharts.common.hichartsclasses.HICredits;
import com.highsoft.highcharts.common.hichartsclasses.HIDataLabels;
import com.highsoft.highcharts.common.hichartsclasses.HINavigation;
import com.highsoft.highcharts.common.hichartsclasses.HINetworkgraph;
import com.highsoft.highcharts.common.hichartsclasses.HIOptions;
import com.highsoft.highcharts.common.hichartsclasses.HIPie;
import com.highsoft.highcharts.common.hichartsclasses.HIPlotOptions;
import com.highsoft.highcharts.common.hichartsclasses.HIStyle;
import com.highsoft.highcharts.common.hichartsclasses.HITitle;
import com.highsoft.highcharts.common.hichartsclasses.HITooltip;
import com.highsoft.highcharts.core.HIChartView;
import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Adapter.SaleAndCollectionsAdapter;
import com.salesmanagement.Extra.DetectConnection;
import com.salesmanagement.Model.CollectionAndSaleResponse;
import com.salesmanagement.Model.CollectionsReportResponse;
import com.salesmanagement.Model.SaleTargetRequest;
import com.salesmanagement.Model.SaleTargetResponse;
import com.salesmanagement.Model.SalesReportResponse;
import com.salesmanagement.Model.VisitorReportRequest;
import com.salesmanagement.Model.VisitorReportResponse;
import com.salesmanagement.R;
import com.salesmanagement.Retrofit.Api;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.salesmanagement.Extra.Common.getSavedUserData;
import static com.salesmanagement.Extra.Common.progressDialog;
import static com.salesmanagement.Extra.Common.stateResponseList;

public class VisitorFragment extends Fragment {

   @BindView(R.id.hiChartViewVisitor)
   HIChartView hiChartViewVisitor;
   @BindView(R.id.tvNoRecordFoundVisit)
   TextView tvNoRecordFoundVisit;
    Integer empId;
    String selectedEmp;
    @BindView(R.id.pbVisitorFragment)
    ProgressBar pbVisitorFragment;
    @BindView(R.id.tvTotalVisitorCount)
    TextView tvTotalVisitorCount;
    @BindView(R.id.tvTotalVisitorCountAchieved)
    TextView tvTotalVisitorCountAchieved;
    @BindView(R.id.tvStartDateMonthVisitValue)
    TextView tvStartDateMonthVisitValue;
    @BindView(R.id.tvEndDateMonthVisitValue)
    TextView tvEndDateMonthVisitValue;
    @BindView(R.id.cv_visitor_hichart)
    CardView cv_visitor_hichart;
    @BindView(R.id.cv_details_visitor)
    CardView cv_details_visitor;
    String typeReport="";
    @BindView(R.id.rvSaleAndCollection)
    RecyclerView rvSaleAndCollection;
    SaleAndCollectionsAdapter saleAndCollectionsAdapter;
    List<CollectionsReportResponse> collectionsReportResponseList=new ArrayList<>();
    List<SalesReportResponse> salesReportResponseList=new ArrayList<>();
    public VisitorFragment() {
        // Required empty public constructor
    }

    public VisitorFragment(String typeReport) {
        this.typeReport=typeReport;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_visitor, container, false);
        ButterKnife.bind(this,view);
        //init();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        ((MainPage) getActivity()).lockUnlockDrawer(1);
        if (DetectConnection.checkInternetConnection(getActivity())) {
                init();


        } else {
            DetectConnection.noInternetConnection(getActivity());
        }

    }

    private void init() {

        pbVisitorFragment.setVisibility(View.GONE);
        cv_visitor_hichart.setVisibility(View.GONE);
        cv_details_visitor.setVisibility(View.GONE);

        rvSaleAndCollection.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvSaleAndCollection.setVisibility(View.GONE);
        try {
            empId = Integer.parseInt(getSavedUserData(getActivity(), "userId"));
            selectedEmp = getArguments().getString("EMPLOYEE_ID");
            if(selectedEmp!=null){
                System.out.println("Selected Emp:"+selectedEmp);
                empId= Integer.valueOf(selectedEmp);
            }
            System.out.println("EMP Get:"+selectedEmp);
        }catch (Exception e){
            e.printStackTrace();
        }


        VisitorReportRequest visitorReportRequest=new VisitorReportRequest();
        visitorReportRequest.setEmployeeId(empId);
        if(typeReport.equals("Visit")) {
            MainPage.title.setText("Visitor Reports");
            callToGetVisitorReport(visitorReportRequest);
        }
        else if(typeReport.equals("Sales")){
            MainPage.title.setText("Sales Reports");
            cv_visitor_hichart.setVisibility(View.GONE);
            cv_details_visitor.setVisibility(View.GONE);
            rvSaleAndCollection.setVisibility(View.VISIBLE);
            callToGetCollectionsReport(visitorReportRequest);
        }else if(typeReport.equals("Collections")) {
            MainPage.title.setText("Collections Reports");
            cv_visitor_hichart.setVisibility(View.GONE);
            cv_details_visitor.setVisibility(View.GONE);
            rvSaleAndCollection.setVisibility(View.VISIBLE);
            callToGetCollectionsReport(visitorReportRequest);
        }


    }




    private void callToGetVisitorReport(VisitorReportRequest visitorReportRequest) {
        pbVisitorFragment.setVisibility(View.VISIBLE);
        Call<VisitorReportResponse> call= Api.getClient().getVisitorReport(visitorReportRequest);
        call.enqueue(new Callback<VisitorReportResponse>() {
            @Override
            public void onResponse(Call<VisitorReportResponse> call, Response<VisitorReportResponse> response) {
                try {
                    if(response.body()!=null){

                        tvNoRecordFoundVisit.setVisibility(View.GONE);
                        Integer totalVisitorTarget=response.body().getTotalTargetCnt();
                        Integer totalAchievedVisitorTarget=response.body().getTotalTargetAchievedCnt();
                        if(totalVisitorTarget!=null&&totalAchievedVisitorTarget!=null) {
                            setToHiChart(totalVisitorTarget, totalAchievedVisitorTarget);
                        }

                    }
                    else {
                        hiChartViewVisitor.setVisibility(View.GONE);
                        tvNoRecordFoundVisit.setVisibility(View.VISIBLE);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                finally {
                    pbVisitorFragment.setVisibility(View.GONE);

                }
            }

            @Override
            public void onFailure(Call<VisitorReportResponse> call, Throwable t) {
                        try {

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        finally {
                            hiChartViewVisitor.setVisibility(View.GONE);
                            tvNoRecordFoundVisit.setVisibility(View.GONE);
                            pbVisitorFragment.setVisibility(View.GONE);
                        }
            }
        });



    }
    private void callToGetSalesReport(VisitorReportRequest visitorReportRequest) {

        pbVisitorFragment.setVisibility(View.VISIBLE);
        SaleTargetRequest saleTargetRequest=new SaleTargetRequest();
        saleTargetRequest.setEmployeeId(visitorReportRequest.getEmployeeId());
        Call<CollectionAndSaleResponse> call= Api.getClient().getSaleReport(saleTargetRequest);
        call.enqueue(new Callback<CollectionAndSaleResponse>() {
            @Override
            public void onResponse(Call<CollectionAndSaleResponse> call, Response<CollectionAndSaleResponse> response) {
                try {
                    if(response.body()!=null){

                        tvNoRecordFoundVisit.setVisibility(View.GONE);
                        /*Integer totalSalesTarget=response.body().getSaleTargetMonthWiseAmount();
                        Integer totalAchievedSalesTarget=response.body().getSaleAchivedTargetMonthWiseAmount();
                        if(totalSalesTarget!=null&&totalAchievedSalesTarget!=null) {
                            setToHiChart(totalSalesTarget,totalAchievedSalesTarget);
                        }*/

                    }
                    else {
                        hiChartViewVisitor.setVisibility(View.GONE);
                        tvNoRecordFoundVisit.setVisibility(View.VISIBLE);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                finally {
                    pbVisitorFragment.setVisibility(View.GONE);

                }
            }

            @Override
            public void onFailure(Call<CollectionAndSaleResponse> call, Throwable t) {
                try {

                }catch (Exception e){
                    e.printStackTrace();
                }
                finally {
                    hiChartViewVisitor.setVisibility(View.GONE);
                    tvNoRecordFoundVisit.setVisibility(View.GONE);
                    pbVisitorFragment.setVisibility(View.GONE);
                }
            }
        });

    }
    private void callToGetCollectionsReport(VisitorReportRequest visitorReportRequest) {

        pbVisitorFragment.setVisibility(View.VISIBLE);
        SaleTargetRequest saleTargetRequest=new SaleTargetRequest();
        saleTargetRequest.setEmployeeId(visitorReportRequest.getEmployeeId());
        Call<CollectionAndSaleResponse> call= Api.getClient().getSaleReport(saleTargetRequest);
        call.enqueue(new Callback<CollectionAndSaleResponse>() {
            @Override
            public void onResponse(Call<CollectionAndSaleResponse> call, Response<CollectionAndSaleResponse> response) {
                try {
                    if(response.body()!=null){

                        tvNoRecordFoundVisit.setVisibility(View.GONE);


                        if(typeReport.equals("Collections")){
                            collectionsReportResponseList=response.body().getCollectionsReportResponseList();
                            if(collectionsReportResponseList.size()>0){
                            rvSaleAndCollection.setVisibility(View.VISIBLE);
                            saleAndCollectionsAdapter=new SaleAndCollectionsAdapter(getActivity(),collectionsReportResponseList,typeReport);
                            rvSaleAndCollection.setAdapter(saleAndCollectionsAdapter);
                            }
                            else {
                                Toast.makeText(getActivity(), "No record found", Toast.LENGTH_SHORT).show();
                            }
                        }
                        else if(typeReport.equals("Sales")){
                            salesReportResponseList=response.body().getSalesReportResponseList();
                            if(salesReportResponseList.size()>0) {
                                rvSaleAndCollection.setVisibility(View.VISIBLE);
                                saleAndCollectionsAdapter = new SaleAndCollectionsAdapter(getActivity(), typeReport, salesReportResponseList);
                                rvSaleAndCollection.setAdapter(saleAndCollectionsAdapter);
                            }
                            else {
                                Toast.makeText(getActivity(), "No record found", Toast.LENGTH_SHORT).show();
                            }
                        }

                        /*  Integer totalCollectionsTarget=response.body().getCollectionTargetMonthWiseAmount();
                        Integer totalAchievedCollectionsTarget=response.body().getCollectionAchivedTargetMonthWiseAmount();
                        if(totalCollectionsTarget!=null&&totalAchievedCollectionsTarget!=null) {
                            setToHiChart(totalCollectionsTarget,totalAchievedCollectionsTarget);
                        }*/

                    }
                    else {
                        hiChartViewVisitor.setVisibility(View.GONE);
                        tvNoRecordFoundVisit.setVisibility(View.VISIBLE);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                finally {
                    pbVisitorFragment.setVisibility(View.GONE);

                }
            }

            @Override
            public void onFailure(Call<CollectionAndSaleResponse> call, Throwable t) {
                try {

                }catch (Exception e){
                    e.printStackTrace();
                }
                finally {
                    hiChartViewVisitor.setVisibility(View.GONE);
                    tvNoRecordFoundVisit.setVisibility(View.GONE);
                    pbVisitorFragment.setVisibility(View.GONE);
                }
            }
        });
    }
    private void setToHiChart(int totalVisitorTarget, int totalAchievedVisitorTarget) {
        pbVisitorFragment.setVisibility(View.VISIBLE);
        tvTotalVisitorCount.setText(""+String.valueOf(totalVisitorTarget));
        tvTotalVisitorCountAchieved.setText(""+String.valueOf(totalAchievedVisitorTarget));
        cv_visitor_hichart.setVisibility(View.VISIBLE);
        cv_details_visitor.setVisibility(View.VISIBLE);
        hiChartViewVisitor.setVisibility(View.VISIBLE);
        HIOptions options = new HIOptions();

        HIChart chart = new HIChart();
        chart.setType("pie");
        chart.setPlotShadow(false);
        options.setChart(chart);

        HITitle title = new HITitle();
        if(typeReport.equals("Visit")) {
            title.setText(String.valueOf(totalVisitorTarget) + " Visit Target Per Month");
        }else if(typeReport.equals("Sales")){
            title.setText(String.valueOf(totalVisitorTarget) + " Sales Target Per Month");
        }
        else if(typeReport.equals("Collections")){
            title.setText(String.valueOf(totalVisitorTarget) + " Sales Target Per Month");
        }
        options.setTitle(title);

        HITooltip tooltip = new HITooltip();
        tooltip.setPointFormat("<b>{point.percentage:.1f}%</b>");
        options.setTooltip(tooltip);

        ArrayList<HIDataLabels> dataLabelsList = new ArrayList<>();
        HIDataLabels dataLabels = new HIDataLabels();
        dataLabels.setEnabled(true);
        dataLabels.setStyle(new HIStyle());
        dataLabels.getStyle().setFontSize("12px");
        //networkgraph.setDataLabels(dataLabelsList);
        HIPlotOptions plotOptions = new HIPlotOptions();
        plotOptions.setPie(new HIPie());
        plotOptions.getPie().setAllowPointSelect(true);
        plotOptions.getPie().setCursor("pointer");
        dataLabelsList.add(dataLabels);
        plotOptions.getPie().setDataLabels(dataLabelsList);

        // plotOptions.getPie().getDataLabels().;
       /* plotOptions.getPie().getDataLabels().setFormat("<b>{point.name}</b>: {point.percentage:.1f} %"); ;
        plotOptions.getPie().getDataLabels().setStyle(new HICSSObject());
        plotOptions.getPie().getDataLabels().getStyle().setColor("black");*/
        options.setPlotOptions(plotOptions);

        HIPie pie = new HIPie();
        pie.setName("Brands");
        HashMap<String, Object> map1 = new HashMap<>();
        map1.put("name", "Achieved");
        map1.put("y", totalAchievedVisitorTarget);

        HashMap<String, Object> map2 = new HashMap<>();
        map2.put("name", "Target");
        map2.put("y", totalVisitorTarget);
        map2.put("sliced", true);
        map2.put("selected", true);

        pie.setData(new ArrayList<>(Arrays.asList(map1, map2)));
        //Remove Hicharts.com
        options.setCredits(new HICredits());
        options.getCredits().setEnabled(false);
        //Remove Navigation Button options
        options.setNavigation(new HINavigation());
        options.getNavigation().setButtonOptions(new HIButtonOptions());
        options.getNavigation().getButtonOptions().setEnabled(false);
        ArrayList<String > stringArrayListColors=new ArrayList<>();
        stringArrayListColors.add("#50B432");
        stringArrayListColors.add("#ED561B");
        options.setColors(stringArrayListColors);
        options.setSeries(new ArrayList<>(Collections.singletonList(pie)));
        pbVisitorFragment.setVisibility(View.GONE);
        hiChartViewVisitor.setOptions(options);
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, 0);
        calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        Date monthFirstDay = calendar.getTime();
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date monthLastDay = calendar.getTime();

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String startDateStr = df.format(monthFirstDay);
        String endDateStr = df.format(monthLastDay);

        tvStartDateMonthVisitValue.setText(startDateStr);
        tvEndDateMonthVisitValue.setText(endDateStr);


    }


}