package com.salesmanagement.Fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.salesmanagement.R;

import butterknife.ButterKnife;


public class SalesReportFragment extends Fragment {



    public SalesReportFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_sales_report, container, false);
        ButterKnife.bind(this,view);
        return view;
    }
}