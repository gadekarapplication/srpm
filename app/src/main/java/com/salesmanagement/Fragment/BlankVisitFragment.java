package com.salesmanagement.Fragment;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.textfield.TextInputEditText;
import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Extra.DetectConnection;
import com.salesmanagement.Model.BlankVisitRequest;
import com.salesmanagement.R;
import com.salesmanagement.Retrofit.Api;
import com.salesmanagement.Services.LocationTrack;
import com.salesmanagement.Services.TrackingLocation;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class BlankVisitFragment extends Fragment {


    LocationTrack locationTrack;
    @BindView(R.id.bvGetGPS)
    Button bvGetGPS;
    @BindView(R.id.tvSubmitBlankVisit)
    TextView tvSubmitBlankVisit;
    Double longitude,latitude;
    TrackingLocation trackingLocation;
    private boolean isGPS = false;
    private boolean isContinue = false;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    protected LocationManager
            locationManager;
    private Integer dealerId;
    @BindViews({R.id.blankVisitSubject,R.id.blankVisitDescription,R.id.blankVisitLatLong})
    List<TextInputEditText> textInputEditTextList;
    public BlankVisitFragment() {
        // Required empty public constructor
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_blank_visit, container, false);
        ButterKnife.bind(this,view);
        ((MainPage) getActivity()).lockUnlockDrawer(1);

        if (DetectConnection.checkInternetConnection(getActivity())) {
            init();
        } else {
            DetectConnection.noInternetConnection(getActivity());
        }

        return view;
    }

    private void init() {

        Bundle bundle = getArguments();
        try {
            dealerId = bundle.getInt("DEALER_ID");
            System.out.println("BlankVisitFragment: " + dealerId);

        } catch (Exception e) {
            e.printStackTrace();
        }
        MainPage.title.setText("Blank Visit");
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
         trackingLocation = new TrackingLocation(getActivity(),"Blank");
        trackingLocation.startLocationUpdates(new TrackingLocation.onGpsListener() {
            @Override
            public void gpsStatus(boolean isGPSEnable) {
                if(isGPSEnable){
                    getLocationOver();

                    locationCallback = new LocationCallback() {
                        @Override
                        public void onLocationResult(LocationResult locationResult) {
                            //Log.d("MainActivity","Location CallBack");
                            if (locationResult == null) {
                                //Log.d("MainActivity","Location Result Null");
                                return;
                            }
                            for (Location location : locationResult.getLocations()) {
                                if (location != null) {
                                    longitude = location.getLatitude();
                                    latitude = location.getLongitude();
                                    if (!isContinue) {
                                        //txtLocation.setText(String.format(Locale.US, "%s - %s", wayLatitude, wayLongitude));

                                    } else {

                                    }
                                    if (!isContinue && mFusedLocationClient != null) {
                                        mFusedLocationClient.removeLocationUpdates(locationCallback);
                                    }
                                }
                            }
                        }
                    };
                }
                else {
                    if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        buildAlertMessageNoGps();

                    } else {
                        getLocationOver();
                    }
                }
            }
        });
        bvGetGPS.setOnClickListener(view->{


                //getGpsLocation();
            getLocationOver();
        });

        tvSubmitBlankVisit.setOnClickListener(view->{
            validateForm();
        });
    }



    private void getLocationOver() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions((Activity) getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    1000);

        } else {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());

            locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(3 * 1000); // 3 seconds
            locationRequest.setFastestInterval(3 * 1000); // 3 seconds

            if (isContinue) {
                mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);

            } else {
                try {
                    mFusedLocationClient.getLastLocation().addOnSuccessListener((Activity) getActivity(), location -> {
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();

                            double latti = location.getLatitude();
                            double longi = location.getLongitude();
                            /*latitude = String.valueOf(latti);
                            longitude = String.valueOf(longi);*/

                            Log.d("GetCriminalLocation:", "Lat:" + latitude + "Long:" + longitude);
                            //tvLatitude.setText(String.format(Locale.US, "Latitude: %s", latitude));
                            //tvLongitude.setText(String.format(Locale.US, "Longitude: %s", longitude));
                            textInputEditTextList.get(2).setText(latitude +","+ longitude);
                            //tvLocationUri.setText("Location Uri: https://www.google.com/maps?daddr=" + String.format(Locale.US, "%s - %s", latitude, longitude));

                            Log.d("GetCriminal", "Lat: " + latitude + "Long: " + longitude);

                            //callToGetCriminalRecord(CMISApplication.onGetCityId());
                        } else {
                            mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
                            Log.d("Main Activity", "GetLocation Call back else");
                        }


                    });
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }

    private void getGpsLocation() {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //getActivity().stopService(new Intent(getActivity().getBaseContext(),TrackingLocation.class));
    }

    @Override
    public void onStop() {
        super.onStop();
       // getActivity().stopService(new Intent(getActivity().getBaseContext(),TrackingLocation.class));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //  super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {



            case 1000: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (isContinue) {
                        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);

                    } else {
                        try {
                            mFusedLocationClient.getLastLocation().addOnSuccessListener((Activity) getActivity(), location -> {
                                if (location != null) {
                                    latitude = location.getLatitude();
                                    longitude = location.getLongitude();
                                    Log.d("When get Permission","Lat: "+latitude+"Long: "+longitude);

                                } else {
                                    mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
                                    Log.d("Get Criminal Activity","LOCATION_CALL_BACK else");
                                }
                            });
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }
                } else {
                    Toast.makeText(getActivity(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
                break;
            }




        }

    }

    private void buildAlertMessageNoGps() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Please Turn ON your GPS Connection")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void validateForm() {
                String subject=textInputEditTextList.get(0).getText().toString();
                String description=textInputEditTextList.get(1).getText().toString();
                String latLong=textInputEditTextList.get(2).getText().toString();
             if(subject.equals("")){
                 textInputEditTextList.get(0).setError("Enter Subject");
             }else if(description.equals("")){
                 textInputEditTextList.get(1).setError("Enter Description");
             }
             else if(latLong.equals("")){
                 textInputEditTextList.get(2).setError("Get Lat,Long");
             }
             else {
                 String[] strSplit=latLong.split(",");
                 BlankVisitRequest blankVisitRequest=new BlankVisitRequest();
                 blankVisitRequest.setBlankVisitSubject(subject);
                 blankVisitRequest.setBlankVisitDescription(description);
                 blankVisitRequest.setBlankVisitLatitude(strSplit[0]);
                 blankVisitRequest.setBlankVisitLongitude(strSplit[1]);
                 blankVisitRequest.setDealershipId(dealerId);
                 blankVisitRequest.setEmployeeId(Integer.parseInt(MainPage.userId));
                 callToAddBlankVisit(blankVisitRequest);

             }




    }

    private void callToAddBlankVisit(BlankVisitRequest blankVisitRequest) {


        ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.setTitle("Please wait ");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        progressDialog.setCancelable(false);

        Call<Boolean> call = Api.getClient().addBlankVisit(blankVisitRequest);
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {


                try {
                    progressDialog.dismiss();
                    if (response.body()) {
                        Toasty.success(getActivity(), "Blank Visit added successfully").show();
                        ((MainPage) getActivity()).loadFragment(new BlankVisitListFragment(),true);
                        progressDialog.dismiss();


                    } else {
                        progressDialog.dismiss();

                        Toast.makeText(getContext(), "Record Not Added", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Log.e("Dealer List:", "" + t.getMessage());
                Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();

            }
        });
    }
}