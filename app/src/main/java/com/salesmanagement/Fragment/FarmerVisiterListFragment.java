package com.salesmanagement.Fragment;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.andreabaccega.widget.FormEditText;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Adapter.FarmerVisiterListAdapter;
import com.salesmanagement.Model.FarmerVisiterRequest;
import com.salesmanagement.R;
import com.salesmanagement.Retrofit.Api;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FarmerVisiterListFragment extends Fragment {


    List<FarmerVisiterRequest> farmerVisiterList;
    List<FarmerVisiterRequest> searchFarmerVisitList;
    private View view;
    private RecyclerView rv_farmer_visit_list;
    private FloatingActionButton fab_add;
    private FormEditText et_search;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_farmer_visiter_list, container, false);

        init();
        listener();
        setAdapter();

        return view;
    }

    private void listener() {
        fab_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainPage) getActivity()).loadFragment(new FarmerVisiterFragment(), true);
            }
        });

        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                try {
                    searchList(s.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private void init() {
        rv_farmer_visit_list = view.findViewById(R.id.rv_farmer_visit_list);
        fab_add = view.findViewById(R.id.fab_add);
        et_search = view.findViewById(R.id.et_search);
    }

    private void setAdapter() {

        Call<List<FarmerVisiterRequest>> call = Api.getClient().getFarmerVisiterList(1);
        call.enqueue(new Callback<List<FarmerVisiterRequest>>() {
            @Override
            public void onResponse(Call<List<FarmerVisiterRequest>> call, Response<List<FarmerVisiterRequest>> response) {
                farmerVisiterList = response.body();


                FarmerVisiterListAdapter adapter = new FarmerVisiterListAdapter(getActivity(), response.body());
                rv_farmer_visit_list.setLayoutManager(new LinearLayoutManager(getContext()));
                rv_farmer_visit_list.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                rv_farmer_visit_list.setHasFixedSize(true);
            }

            @Override
            public void onFailure(Call<List<FarmerVisiterRequest>> call, Throwable t) {

                Toast.makeText(getContext(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void searchList(String s) {

        searchFarmerVisitList = new ArrayList<>();
        if (s.length() > 0) {
            for (int i = 0; i < farmerVisiterList.size(); i++)
                if ((farmerVisiterList.get(i).getArea()).toLowerCase().contains(s.toLowerCase().trim())) {
                    searchFarmerVisitList.add(farmerVisiterList.get(i));
                }

            if (searchFarmerVisitList.size() < 1) {

            } else {

            }

        } else {
            searchFarmerVisitList = new ArrayList<>();
            for (int i = 0; i < farmerVisiterList.size(); i++) {
                searchFarmerVisitList.add(farmerVisiterList.get(i));
            }
        }

        try {


            FarmerVisiterListAdapter adapter = new FarmerVisiterListAdapter(getActivity(), farmerVisiterList);
            rv_farmer_visit_list.setLayoutManager(new LinearLayoutManager(getContext()));
            rv_farmer_visit_list.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            rv_farmer_visit_list.setHasFixedSize(true);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onStart() {
        super.onStart();


            MainPage.title.setText("Farmer Visit List");

    }
}