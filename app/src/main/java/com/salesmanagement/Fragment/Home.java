package com.salesmanagement.Fragment;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Database.SRPMDatabase;
import com.salesmanagement.Extra.DetectConnection;
import com.salesmanagement.Model.AttendanceResponse;
import com.salesmanagement.Model.GetPunchInResponse;
import com.salesmanagement.Model.TargetCountResponse;
import com.salesmanagement.R;
import com.salesmanagement.Receiver.TrackingChecker;
import com.salesmanagement.Retrofit.Api;
import com.salesmanagement.Services.LocationTrack;
import com.salesmanagement.Services.TrackingLocation;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Home extends Fragment {

    //a broadcast to know weather the data is synced or not
    public static final String DATA_SAVED_BROADCAST = "com.salesmanagement.datasaved";
    public double longitude, latitude;
    View view;
    //To get Current lat, long
    LocationTrack locationTrack;
    //SQLite database..
    SRPMDatabase srpmDatabase;
    //Broadcast receiver to know the sync status
    private BroadcastReceiver broadcastReceiver;

    @BindView(R.id.checkIn)
    Button btn_check_in;

    private TextView tv_sale_month, tv_sale_achieve, tv_sale_remaing_target, tv_sale_remaing_days, tv_collection_monthly, tv_collection_achieve, tv_collection_remaining_target, tv_collection_remaining_days, tv_visit_monthly, tv_visit_achieve, tv_visit_remaining_target, tv_visit_remaining_days;

    private LinearLayout linearLayout_farmer, linearLayout_shop, linearLayout_attendance, linearLayout_order;

    @BindView(R.id.ivShop)
    ImageView ivShop;
    @BindView(R.id.ivOrder)
    ImageView ivOrder;
    @BindView(R.id.tvOrder)
    TextView tvOrder;
    @BindView(R.id.tvShop)
    TextView tvShop;
    @BindView(R.id.llSalesTarget)
    LinearLayout llSalesTarget;
    @BindView(R.id.llCollectionTarget)
    LinearLayout llCollectionTarget;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        MainPage.title.setText("");

        srpmDatabase = new SRPMDatabase(getActivity());

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy =
                    new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        //the broadcast receiver to update sync status
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

            }
        };

        try {
            //registering the broadcast receiver to update sync status
            getActivity().registerReceiver(new TrackingChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        } catch (Exception e) {
            e.printStackTrace();
        }

        init();

        getData();
        if(MainPage.roleName.equals("FO")||MainPage.roleName.equals("DO")){
            tvShop.setText("Expense");
            tvOrder.setText("Report");
            ivShop.setImageResource(R.drawable.expense);
            ivOrder.setImageResource(R.drawable.report);
            llSalesTarget.setVisibility(View.GONE);
            llCollectionTarget.setVisibility(View.GONE);
        }

        return view;

    }

    private void getData() {
        Call<TargetCountResponse> call = Api.getClient().getTargetResponseCount(1);
        call.enqueue(new Callback<TargetCountResponse>() {
            @Override
            public void onResponse(Call<TargetCountResponse> call, Response<TargetCountResponse> response) {

               // Toast.makeText(getActivity(), "hiiii", Toast.LENGTH_SHORT).show();

                try {


                    tv_sale_month.setText("" + response.body().getSaleTargetMonthWiseAmount());
                    tv_sale_achieve.setText("" + response.body().getSaleAchievedTargetMonthWiseAmount());
                    tv_sale_remaing_days.setText("" + response.body().getRemainingDaysSale());
                    tv_sale_remaing_target.setText("" + response.body().getRemainingTargetSale());

                    tv_collection_monthly.setText("" + response.body().getCollectionTargetMonthWiseAmount());
                    tv_collection_achieve.setText("" + response.body().getCollectionAchievedTargetMonthWiseAmount());
                    tv_collection_remaining_days.setText("" + response.body().getRemainingDaysCollection());
                    tv_collection_remaining_target.setText("" + response.body().getRemainingTargetCollection());

                    tv_visit_monthly.setText("" + response.body().getVisitTargetCntMonthly());
                    tv_visit_achieve.setText("" + response.body().getVisitTargetAchievedCntMonthly());
                    tv_visit_remaining_days.setText("" + response.body().getRemainingDaysVisit());
                    tv_visit_remaining_target.setText("" + response.body().getRemainingTargetVisit());

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<TargetCountResponse> call, Throwable t) {

            }
        });

    }

    private void init() {
        tv_sale_month = view.findViewById(R.id.tv_sale_month);
        tv_sale_achieve = view.findViewById(R.id.tv_sale_achieve);
        tv_sale_remaing_target = view.findViewById(R.id.tv_sale_remaing_target);
        tv_sale_remaing_days = view.findViewById(R.id.tv_sale_remaing_days);

        tv_collection_monthly = view.findViewById(R.id.tv_collection_monthly);
        tv_collection_achieve = view.findViewById(R.id.tv_collection_achieve);
        tv_collection_remaining_target = view.findViewById(R.id.tv_collection_remaining_target);
        tv_collection_remaining_days = view.findViewById(R.id.tv_collection_remaining_days);

        tv_visit_monthly = view.findViewById(R.id.tv_visit_monthly);
        tv_visit_achieve = view.findViewById(R.id.tv_visit_achieve);
        tv_visit_remaining_target = view.findViewById(R.id.tv_visit_remaining_target);
        tv_visit_remaining_days = view.findViewById(R.id.tv_visit_remaining_days);

        linearLayout_farmer = view.findViewById(R.id.linearLayout_farmer);
        linearLayout_shop = view.findViewById(R.id.linearLayout_shop);
        linearLayout_attendance = view.findViewById(R.id.linearLayout_attendance);
        linearLayout_order = view.findViewById(R.id.linearLayout_order);

    }

    @OnClick({R.id.checkIn,R.id.linearLayout_farmer,R.id.linearLayout_shop,R.id.linearLayout_attendance,R.id.linearLayout_order})
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.checkIn:
                if (btn_check_in.getText().toString().equalsIgnoreCase("Check In")) {

                    Toast.makeText(getActivity(), "check in", Toast.LENGTH_SHORT).show();
                    punchIn();

                } else if (btn_check_in.getText().toString().equalsIgnoreCase("Check Out")) {
                    Toast.makeText(getActivity(), "check out", Toast.LENGTH_SHORT).show();
                    punchOut();
                }
                break;

            case R.id.linearLayout_farmer:
                ((MainPage)getActivity()). loadFragment(new FarmerListFragment(), true);
                break;

            case R.id.linearLayout_shop:
                if(tvShop.getText().toString().equals("Expense")){
                    ((MainPage)getActivity()). loadFragment(new ExpenseFragment(), true);
                }else {
                    ((MainPage)getActivity()). loadFragment(new DealerListFragment(), true);
                }

                break;

            case R.id.linearLayout_attendance:
                ((MainPage)getActivity()). loadFragment(new AttendanceViewFragment(), true);
                break;

            case R.id.linearLayout_order:
                 if(tvOrder.getText().toString().equals("Report")){
                     ((MainPage)getActivity()). loadFragment(new ReportsFragment(), true);
                 }else {
                     ((MainPage)getActivity()). loadFragment(new OrderHistory(), true);
                 }
                break;


        }
    }


    public void punchIn() {

        locationTrack = new LocationTrack(getActivity());
        if (locationTrack.canGetLocation()) {
            longitude = locationTrack.getLongitude();
            latitude = locationTrack.getLatitude();
        } else {
            locationTrack.showSettingsAlert();
        }

        TrackingLocation trackingLocation = new TrackingLocation(getActivity(),"Home");
        trackingLocation.startLocationUpdates(new TrackingLocation.onGpsListener() {
            @Override
            public void gpsStatus(boolean isGPSEnable) {

            }
        });


        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

        AttendanceResponse attendanceResponse = new AttendanceResponse();
        attendanceResponse.setEmployeeId("" + MainPage.userId);
        attendanceResponse.setPunchInLatitude("" + Double.toString(longitude));
        attendanceResponse.setPunchInLongitude("" + Double.toString(latitude));
        attendanceResponse.setDate("" + date);


        Call<Boolean> call = Api.getClient().punchIn(attendanceResponse);
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {

                if (response.isSuccessful()) {
                    if (response.body().booleanValue() == true) {
                        Toast.makeText(getActivity(), "Attendance punch-In done", Toast.LENGTH_SHORT).show();
                        getPunchInStatus();
                    } else if (response.body().booleanValue() == false) {
                        Toast.makeText(getActivity(), "Attendance not punch-In", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Attendance not punch-In", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Log.e("punchInError", "" + t.getMessage());
                Toast.makeText(getActivity(), "Attendance Server Error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void punchOut() {

        locationTrack = new LocationTrack(getActivity());
        if (locationTrack.canGetLocation()) {
            longitude = locationTrack.getLongitude();
            latitude = locationTrack.getLatitude();
        } else {
            locationTrack.showSettingsAlert();
        }

        TrackingLocation trackingLocation = new TrackingLocation(getActivity(),"Home");
        trackingLocation.startLocationUpdates(new TrackingLocation.onGpsListener() {
            @Override
            public void gpsStatus(boolean isGPSEnable) {

            }
        });

        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

        AttendanceResponse attendanceResponse = new AttendanceResponse();
        attendanceResponse.setEmployeeId("" + MainPage.userId);
        attendanceResponse.setPunchOutLatitude("" + Double.toString(longitude));
        attendanceResponse.setPunchOutLongitude("" + Double.toString(latitude));
        attendanceResponse.setDate("" + date);


        Call<Boolean> call = Api.getClient().punchOut(attendanceResponse);
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {

                if (response.isSuccessful()) {
                    if (response.body().booleanValue() == true) {
                        Toast.makeText(getActivity(), "Attendance punch-Out done", Toast.LENGTH_SHORT).show();
                        getPunchInStatus();
                    } else if (response.body().booleanValue() == false) {
                        Toast.makeText(getActivity(), "Attendance Already punch-Out", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Attendance not punch-Out", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Log.e("punchInError", "" + t.getMessage());
                Toast.makeText(getActivity(), "Attendance Server Error", Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void onStart() {
        super.onStart();
        Log.e("onStart", "called");
        ((MainPage) getActivity()).lockUnlockDrawer(0);
        if (DetectConnection.checkInternetConnection(getActivity())) {

            getPunchInStatus();

        } else {
            DetectConnection.noInternetConnection(getActivity());
        }
    }

    private void getPunchInStatus() {
        Call<GetPunchInResponse> call = Api.getClient().getPunchInStatus(MainPage.userId);
        call.enqueue(new Callback<GetPunchInResponse>() {
            @Override
            public void onResponse(Call<GetPunchInResponse> call, Response<GetPunchInResponse> response) {
               // Toast.makeText(getContext(), "" + response.body().getPunchIn(), Toast.LENGTH_SHORT).show();

                if (response.body().getPunchIn()) {
                    btn_check_in.setText("Check In");
                } else {
                    btn_check_in.setText("Check Out");
                }
            }

            @Override
            public void onFailure(Call<GetPunchInResponse> call, Throwable t) {

            }
        });
    }


    /*private void requestPermission() {

        Dexter.withActivity(getActivity())
                .withPermissions(
                        android.Manifest.permission.READ_EXTERNAL_STORAGE,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        android.Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            if(btn_check_in.getText().toString().equalsIgnoreCase("Check In")){
                                punchIn();
                            }else {
                                punchOut();
                            }

                        }
                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getActivity(), "Error occurred! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();

    }*/

    private void showSettingsDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }

        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }
}