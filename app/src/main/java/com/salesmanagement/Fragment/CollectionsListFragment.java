package com.salesmanagement.Fragment;

import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Adapter.CollectionsAdapter;
import com.salesmanagement.Extra.DetectConnection;
import com.salesmanagement.Model.CollectionRequest;
import com.salesmanagement.Model.CollectionsResponse;
import com.salesmanagement.Model.CompanyResponse;
import com.salesmanagement.R;
import com.salesmanagement.Retrofit.Api;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.ganfra.materialspinner.MaterialSpinner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.salesmanagement.Extra.Common.getSavedUserData;


public class CollectionsListFragment extends Fragment {

    public List<CompanyResponse> companyResponseList = new ArrayList<>();
    public String[] companyIdList, companyNameList;
    @BindView(R.id.companySpinnerListCollection)
    MaterialSpinner companySpinnerListCollection;
    @BindView(R.id.rvCollections)
    RecyclerView rvCollections;
    CollectionsAdapter collectionsAdapter;
    String companyId;
    Integer empId;
    String companyName;
    @BindView(R.id.pvCollections)
    ProgressBar pvCollections;
    @BindView(R.id.tvNoRecordFoundCollections)
    TextView tvNoRecordFoundCollections;
    List<CollectionsResponse> collectionsResponsesList = new ArrayList<>();
    @BindView(R.id.fabAddCollections)
    FloatingActionButton fabAddCollections;
    int position = 0;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_collections_list, container, false);
        ButterKnife.bind(this, view);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy =
                    new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        pvCollections.setVisibility(View.GONE);
        callToGetCompanyList();
        init();
        return view;
    }

    private void init() {

        empId = Integer.parseInt(getSavedUserData(getActivity(), "userId"));
        rvCollections.setLayoutManager(new LinearLayoutManager(getActivity()));
        companySpinnerListCollection.setSelection(0, false);
        companySpinnerListCollection.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {


                ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                System.out.println("companyListSize() " + companyIdList.length);
                try {

                    companyId = companyIdList[position];
                    companyName = companyNameList[position];
                    rvCollections.setVisibility(View.GONE);
                    getCollectionsList(empId, Integer.valueOf(companyId));


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        fabAddCollections.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainPage) getActivity()).loadFragment(new CollectionsFragment(), true);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        MainPage.title.setText("List Collections");
        ((MainPage) getActivity()).lockUnlockDrawer(1);
        if (DetectConnection.checkInternetConnection(getActivity())) {


            //getCollectionsList(empId,1);

        } else {
            DetectConnection.noInternetConnection(getActivity());
        }
    }


    private void getCollectionsList(Integer empId, Integer companyId) {
        pvCollections.setVisibility(View.VISIBLE);

        CollectionRequest collectionRequest = new CollectionRequest();
        collectionRequest.setEmployeeId(empId);
        collectionRequest.setCompId(companyId);

        Call<List<CollectionsResponse>> call = Api.getClient().getCollections(collectionRequest);
        call.enqueue(new Callback<List<CollectionsResponse>>() {
            @Override
            public void onResponse(Call<List<CollectionsResponse>> call, Response<List<CollectionsResponse>> response) {

                if (response.isSuccessful()) {
                    pvCollections.setVisibility(View.GONE);
                    try {
                        collectionsResponsesList.clear();
                        collectionsResponsesList = response.body();
                        if (collectionsResponsesList.size() > 0) {
                            tvNoRecordFoundCollections.setVisibility(View.GONE);
                            rvCollections.setVisibility(View.VISIBLE);
                            collectionsAdapter = new CollectionsAdapter(collectionsResponsesList, getContext());
                            rvCollections.setAdapter(collectionsAdapter);
                        } else {
                            rvCollections.setVisibility(View.GONE);
                            tvNoRecordFoundCollections.setVisibility(View.VISIBLE);
                        }


                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                } else {
                    //Toast.makeText(getActivity(), "No Company Found", Toast.LENGTH_SHORT).show();
                    pvCollections.setVisibility(View.GONE);
                    tvNoRecordFoundCollections.setVisibility(View.VISIBLE);
                    rvCollections.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(Call<List<CollectionsResponse>> call, Throwable t) {
                Log.e("Collection Error", "" + t.getMessage());
                //Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
                pvCollections.setVisibility(View.GONE);
            }
        });


    }

    public void callToGetCompanyList() {

        companyResponseList.clear();

        Call<List<CompanyResponse>> call = Api.getClient().getCompanyList();
        call.enqueue(new Callback<List<CompanyResponse>>() {
            @Override
            public void onResponse(Call<List<CompanyResponse>> call, Response<List<CompanyResponse>> response) {

                if (response.isSuccessful()) {
                    try {
                        companyResponseList = response.body();
                        if (companyResponseList != null) {

                            companyIdList = new String[companyResponseList.size()];
                            companyNameList = new String[companyResponseList.size()];

                            for (int i = 0; i < companyResponseList.size(); i++) {
                                companyIdList[i] = companyResponseList.get(i).getCompId();
                                companyNameList[i] = companyResponseList.get(i).getCompName();
                            }

                            try {
                                final ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, companyNameList);
                                adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);

                                companySpinnerListCollection.setAdapter(adapter);
                                companySpinnerListCollection.setSelection(0, false);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else {
                            //Toast.makeText(getActivity(), "No Company Found", Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    //Toast.makeText(getActivity(), "No Company Found", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<List<CompanyResponse>> call, Throwable t) {
                Log.e("categoryError", "" + t.getMessage());
                //Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
            }
        });

    }
}