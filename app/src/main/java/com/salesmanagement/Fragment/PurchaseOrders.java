package com.salesmanagement.Fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Adapter.TabViewAdapter;
import com.salesmanagement.Extra.DetectConnection;
import com.salesmanagement.R;

import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;


public class PurchaseOrders extends Fragment {

    private View view;
    //private ViewPager viewPager;
    private FrameLayout viewPager;
    private TabLayout tabLayout;
    //private TabViewAdapter adapter;


    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_purchase_orders, container, false);
        ButterKnife.bind(this, view);
       // setRetainInstance(true);


        //initComponent();
        ((MainPage) getActivity()).lockUnlockDrawer(1);
        Log.d("OpenTabInOnCrete","onCreate");
        MainPage.title.setText("Order");

        if (DetectConnection.checkInternetConnection(getActivity())) {
            initComponent();
        } else {
            Toasty.warning(getActivity(), "No Internet Connection", Toasty.LENGTH_SHORT, true).show();
        }

        return view;

    }

    private void initComponent() {

        viewPager = view.findViewById(R.id.view_pager);
        tabLayout = view.findViewById(R.id.tab_layout);
        //loadFragment(new SalesOrders(),true);
        tabLayout.addTab(tabLayout.newTab().setText("Purchase Goods"),true);
        tabLayout.addTab(tabLayout.newTab().setText("Order History"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabTextColors(ContextCompat.getColor(getActivity(), R.color.white), ContextCompat.getColor(getActivity(), R.color.white));
        setCurrentTabFragment(0);

       /* Fragment[] tabs = new Fragment[2];

        tabs[0] = new SalesOrders();
        tabs[1] = new OrderHistory();*/


       // adapter = new TabViewAdapter(getActivity().getSupportFragmentManager(), tabLayout.getTabCount(), tabs, 1);
        //viewPager.setAdapter(adapter);
        //viewPager.setAdapter(new PagerAdapter(getFragmentManager(), tabLayout.getTabCount()));
        //viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                //viewPager.setCurrentItem(tab.getPosition());
                setCurrentTabFragment(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //adapter.forwardActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onStart() {
        super.onStart();

    }


    public class PagerAdapter extends FragmentStatePagerAdapter{
        int mNumOfTabs;

        public PagerAdapter(@NonNull FragmentManager fm, int mNumOfTabs) {
            super(fm, mNumOfTabs);
        }


        @NonNull
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new SalesOrders();

                case 1:
                    new OrderHistory();

                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }
    }


    public void removeCurrentFragmentAndMoveBack() {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.popBackStack();
    }

    public void loadFragment(Fragment fragment, Boolean bool) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.view_pager, fragment);
        if (bool) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    private void setCurrentTabFragment(int tabPosition)
    {
        switch (tabPosition)
        {
            case 0 :
                loadFragment(new SalesOrders(),true);
                break;
            case 1 :
                loadFragment(new OrderHistory(),true);
                break;
        }
    }

}