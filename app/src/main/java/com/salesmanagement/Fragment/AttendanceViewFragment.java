package com.salesmanagement.Fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.applandeo.materialcalendarview.EventDay;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;
import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Extra.DetectConnection;
import com.salesmanagement.Extra.EventDecorator;
import com.salesmanagement.Model.AttendanceReqModel;
import com.salesmanagement.Model.EmpAttendanceModel;
import com.salesmanagement.Model.MonthlyAttendanceResponseModel;
import com.salesmanagement.R;
import com.salesmanagement.Retrofit.Api;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.salesmanagement.Extra.Common.getSavedUserData;

public class AttendanceViewFragment extends Fragment implements OnMonthChangedListener {

    @BindView(R.id.calendarView)
    MaterialCalendarView calendarView;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    List<MonthlyAttendanceResponseModel> attendanceResponseModelList = new ArrayList<>();
    Integer empId;
       String selectedEmp;
    @BindView(R.id.pbAttendance)
    ProgressBar pbAttendance;
    @BindViews({R.id.tvPresentDays,R.id.tvAbsentDays,R.id.tvWeekOffDays})
    List<TextView> textViewList;
    public AttendanceViewFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_attendance_view, container, false);
        ButterKnife.bind(this, view);
        // init();
        return view;
    }

    private void init() {
        MainPage.title.setText("Attendance");
        pbAttendance.setVisibility(View.GONE);
        calendarView.setOnMonthChangedListener(this);
        Bundle bundle=getArguments();
        try {
            empId = Integer.parseInt(getSavedUserData(getActivity(), "userId"));

            selectedEmp = getArguments().getString("EMPLOYEE_ID");
            if(selectedEmp!=null){
                System.out.println("Selected Emp:"+selectedEmp);
                empId= Integer.valueOf(selectedEmp);
            }
            System.out.println("EMP Get:"+selectedEmp);

        } catch (Exception e) {
            e.printStackTrace();
        }
        String todayDate = simpleDateFormat.format(CalendarDay.today().getDate());
        callToGetAttendance(todayDate);
        calendarView.addDecorator(new DayViewDecorator() {
            @Override
            public boolean shouldDecorate(CalendarDay day) {
                return day.getCalendar().get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY;
            }

            @Override
            public void decorate(DayViewFacade view) {
                // add red foreground span
                view.addSpan(new ForegroundColorSpan(
                        ContextCompat.getColor(getContext(),R.color.error_color )));
            }
        });
        textViewList.get(0).setVisibility(View.GONE);
        textViewList.get(1).setVisibility(View.GONE);
        textViewList.get(2).setVisibility(View.GONE);

    }

    private void callToGetAttendance(String strDate) {
        pbAttendance.setVisibility(View.VISIBLE);

        AttendanceReqModel attendanceReqModel = new AttendanceReqModel();

        attendanceReqModel.setEmployeeId(empId);
        attendanceReqModel.setViewDate(strDate);

        Call<EmpAttendanceModel> call = Api.getClient().getMonthlyAttendanceList(attendanceReqModel);
        call.enqueue(new Callback<EmpAttendanceModel>() {
            @Override
            public void onResponse(Call<EmpAttendanceModel> call, Response<EmpAttendanceModel> response) {
                try {
                    pbAttendance.setVisibility(View.GONE);
                    if (response.code() == 200) {
                        textViewList.get(0).setVisibility(View.VISIBLE);
                        textViewList.get(1).setVisibility(View.VISIBLE);
                        textViewList.get(2).setVisibility(View.VISIBLE);
                        attendanceResponseModelList = response.body().getAttendanceResponseModelList();
                        getAttendanceList(attendanceResponseModelList);
                       String strPresentDay= String.valueOf(response.body().getPresentcnt());
                       String strAbsentDay   = String.valueOf(response.body().getAbsetcnt());
                        String strWeekOff= String.valueOf(response.body().getWeekoffcnt());

                        textViewList.get(0).setText("Present Days Count: "+strPresentDay);
                        textViewList.get(1).setText("Absent Days Count: "+strAbsentDay);
                        textViewList.get(2).setText("WeekOff Days Count: "+strWeekOff);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<EmpAttendanceModel> call, Throwable t) {
                pbAttendance.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void getAttendanceList(List<MonthlyAttendanceResponseModel> monthlyAttendanceResponseModelList) {


        List<EventDay> events = new ArrayList<>();
        //SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        for (int i = 0; i < monthlyAttendanceResponseModelList.size(); i++) {
            if (monthlyAttendanceResponseModelList.get(i).getStart().contains("-")) {

                try {
                    Calendar calendar = Calendar.getInstance();

                    calendar.setTime(simpleDateFormat.parse("" + monthlyAttendanceResponseModelList.get(i).getStart()));
                   if(!monthlyAttendanceResponseModelList.get(i).getTitle().equals("Week Off")) {
                       EventDecorator eventDecorator = new EventDecorator(calendarView, calendar, R.color.errorColor, monthlyAttendanceResponseModelList.get(i).getTitle());
                       calendarView.addDecorator(eventDecorator);
                   }


                } catch (Exception e) {
                    e.printStackTrace();

                }
            }

        }


    }

    @Override
    public void onMonthChanged(MaterialCalendarView widget, CalendarDay date) {
        // System.out.println("Get Date:"+simpleDateFormat.format(date.getDate()));
        callToGetAttendance(simpleDateFormat.format(date.getDate()));
    }

    @Override
    public void onStart() {
        super.onStart();
        ((MainPage) getActivity()).lockUnlockDrawer(1);
        if (DetectConnection.checkInternetConnection(getActivity())) {
            init();
        } else {
            DetectConnection.noInternetConnection(getActivity());
        }
    }
}