package com.salesmanagement.Fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.andreabaccega.widget.FormEditText;


import com.canhub.cropper.CropImage;
import com.canhub.cropper.CropImageView;
import com.google.android.material.textfield.TextInputEditText;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Extra.DetectConnection;
import com.salesmanagement.Model.ChequeRequestModel;
import com.salesmanagement.Model.ChequeResponseModel;
import com.salesmanagement.Model.CompanyResponse;
import com.salesmanagement.Model.DealerResponse;
import com.salesmanagement.Model.UploadImgResponse;
import com.salesmanagement.R;
import com.salesmanagement.Retrofit.Api;



import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.ganfra.materialspinner.MaterialSpinner;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ChequeMasterFragment extends Fragment {
    public String documentType;
    public List<CompanyResponse> companyResponseList = new ArrayList<>();
    public List<DealerResponse> dealerResponseList = new ArrayList<>();
    public String[] dealerIdList, dealerNameList, dealerAddressList, companyIdList, companyNameList, categoryIdList, categoryNameList, productIdList, productNameList, sizeIdList, sizeNameList, amountList;
    Intent galleryIntent;
    private View view;
    private FormEditText et_company_name, et_dealer_name, et_employee, et_cheque_number, et_bank_name, et_account_number, et_ifsc_code, et_amount;
    private TextView tv_start_date, tv_expiry_date, tv_image_path, tv_file_choose, tv_submit;
    private DatePickerDialog picker;
    private String responseImage="",chequeStatus="";
    private Spinner spinner_company, spinner_dealer;

    private String companyId, companyName, dealerId, dealerName;

    private String formStatus = "", imgPath = "";

    private int chequeId;

    ChequeResponseModel chequeResponseModel;
    @BindView(R.id.tieDealerName)
    TextInputEditText tieDealerName;
    Dialog dialogDealer;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_cheque_master, container, false);
        ButterKnife.bind(this,view);
        init();

        Bundle bundle = getArguments();

        if (bundle != null) {
            formStatus = bundle.getString("formStatus");
            chequeId = bundle.getInt("chequeId");


            setUpdateData();

            tv_submit.setText("Update");


        } else {

        }


        listener();
        getCompanyList();
        getDealerList();
        return view;
    }

    private void setUpdateData() {

        Call<ChequeResponseModel> call = Api.getClient().getChequeDetail(chequeId);
        call.enqueue(new Callback<ChequeResponseModel>() {
            @Override
            public void onResponse(Call<ChequeResponseModel> call, Response<ChequeResponseModel> response) {

                if(response.body()!=null) {
                    chequeResponseModel = response.body();
                    tv_start_date.setText(response.body().getDate());
                    et_cheque_number.setText(response.body().getChequeNo());
                    et_bank_name.setText(response.body().getBankName());
                    et_account_number.setText(response.body().getAccountNo());
                    et_ifsc_code.setText(response.body().getIfsccode());
                    et_amount.setText("" + response.body().getAmount());
                    tv_expiry_date.setText("" + response.body().getExpiaryDate());
                    tv_image_path.setText("" + response.body().getCheckDetailImage());
                    chequeStatus = response.body().getStatus();
                    responseImage = response.body().getCheckDetailImage();
                }
            }

            @Override
            public void onFailure(Call<ChequeResponseModel> call, Throwable t) {


                Log.e("errror", t.getMessage());
            }
        });

    }


    private void init() {
        et_company_name = view.findViewById(R.id.et_company_name);
        et_dealer_name = view.findViewById(R.id.et_dealer_name);
        et_employee = view.findViewById(R.id.et_employee);
        tv_start_date = view.findViewById(R.id.tv_start_date);
        et_cheque_number = view.findViewById(R.id.et_cheque_number);
        et_bank_name = view.findViewById(R.id.et_bank_name);
        et_account_number = view.findViewById(R.id.et_account_number);
        et_ifsc_code = view.findViewById(R.id.et_ifsc_code);
        et_amount = view.findViewById(R.id.et_amount);
        tv_expiry_date = view.findViewById(R.id.tv_expiry_date);
        tv_image_path = view.findViewById(R.id.tv_image_path);
        tv_file_choose = view.findViewById(R.id.tv_file_choose);
        tv_submit = view.findViewById(R.id.tv_submit);
        spinner_company = view.findViewById(R.id.spinner_company);
        spinner_dealer = view.findViewById(R.id.spinner_dealer);
        tieDealerName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogDealer=new Dialog(getActivity());

                dialogDealer.setContentView(R.layout.drawable_searchable_spinner);

                // dialogFirm.getWindow().setLayout(650,800);
                Window window = dialogDealer.getWindow();
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                dialogDealer.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialogDealer.show();

                EditText editText=dialogDealer.findViewById(R.id.etFirm);
                TextView tvSelect=  dialogDealer.findViewById(R.id.tvSelect);
                tvSelect.setText("Select Firm");
                ListView lview=dialogDealer.findViewById(R.id.listViewFirm);

                ArrayAdapter<String> adapter=new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1,dealerNameList);
                lview.setAdapter(adapter);

                editText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        adapter.getFilter().filter(charSequence);
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });
                lview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        tieDealerName.setText(adapter.getItem(i));
                        dialogDealer.dismiss();

                        dealerId = dealerIdList[i];
                        dealerName = dealerNameList[i];
                        System.out.println("DealerName:"+dealerName);
                        System.out.println("DealerId:"+dealerId);



                    }
                });
            }
        });

    }

    private void getCompanyList() {

        companyResponseList.clear();

        Call<List<CompanyResponse>> call = Api.getClient().getCompanyList();
        call.enqueue(new Callback<List<CompanyResponse>>() {
            @Override
            public void onResponse(Call<List<CompanyResponse>> call, Response<List<CompanyResponse>> response) {

                if (response.isSuccessful()) {
                    try {
                        companyResponseList = response.body();
                        if (companyResponseList != null) {

                            companyIdList = new String[companyResponseList.size()];
                            companyNameList = new String[companyResponseList.size()];

                            for (int i = 0; i < companyResponseList.size(); i++) {
                                companyIdList[i] = companyResponseList.get(i).getCompId();
                                companyNameList[i] = companyResponseList.get(i).getCompName();
                            }

                            try {
                                final ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, companyNameList);
                                adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
                                spinner_company.setAdapter(adapter);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else {
                            Toast.makeText(getActivity(), "No Company Found", Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getActivity(), "No Company Found", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<List<CompanyResponse>> call, Throwable t) {
                Log.e("categoryError", "" + t.getMessage());
                Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getDealerList() {

        dealerResponseList.clear();

        //Call<List<DealerResponse>> call = Api.getClient().getDealerByEmp(Integer.valueOf(MainPage.userId));
        Call<List<DealerResponse>> call = Api.getClient().getDealerList();
        call.enqueue(new Callback<List<DealerResponse>>() {
            @Override
            public void onResponse(Call<List<DealerResponse>> call, Response<List<DealerResponse>> response) {
                if (response.isSuccessful()) {
                    try {
                        dealerResponseList = response.body();
                        if (dealerResponseList != null) {

                            dealerIdList = new String[dealerResponseList.size()];
                            dealerNameList = new String[dealerResponseList.size()];
                            dealerAddressList = new String[dealerResponseList.size()];
                            for (int i = 0; i < dealerResponseList.size(); i++) {
                                dealerIdList[i] = dealerResponseList.get(i).getDealershipId();
                                dealerNameList[i] = dealerResponseList.get(i).getFirmName();
                                dealerAddressList[i] = dealerResponseList.get(i).getFirmAddress();
                            }

                            try {
                                final ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, dealerNameList);
                                adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
                                spinner_dealer.setAdapter(adapter);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else {
                            Toast.makeText(getActivity(), "No Dealer Found", Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getActivity(), "No Dealer Found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<DealerResponse>> call, Throwable t) {
                Log.e("categoryError", "" + t.getMessage());
                Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e("onStart", "called");
        if (formStatus.equalsIgnoreCase("update")) {
            MainPage.title.setText("Cheque Update");
        } else {
            MainPage.title.setText("Add Cheque");
        }

        ((MainPage) getActivity()).lockUnlockDrawer(1);
        if (DetectConnection.checkInternetConnection(getActivity())) {
            requestPermission();

        } else {
            DetectConnection.noInternetConnection(getActivity());
        }
    }

    private void listener() {
        tv_file_choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                documentType = "chequePhoto";


                callToPickupImage();

            }
        });


        tv_start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startDate();
            }
        });

        tv_expiry_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expiryDate();
            }
        });

        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!tv_start_date.getText().toString().equalsIgnoreCase("")) {
                    if (et_cheque_number.testValidity() && et_bank_name.testValidity() && et_account_number.testValidity() && et_ifsc_code.testValidity() && et_amount.testValidity()) {



                            ChequeRequestModel chequeRequestModel = new ChequeRequestModel();
                            chequeRequestModel.setCompId(Integer.valueOf(companyId));
                            chequeRequestModel.setDealershipId(Integer.valueOf(dealerId));
                            chequeRequestModel.setEmployeeId(Integer.valueOf(MainPage.userId));
                            chequeRequestModel.setEmployeeName(et_employee.getText().toString());
                            chequeRequestModel.setDate(tv_start_date.getText().toString());
                            chequeRequestModel.setChequeNo(et_cheque_number.getText().toString());
                            chequeRequestModel.setBankName(et_bank_name.getText().toString());
                            chequeRequestModel.setAccountNo(et_account_number.getText().toString());
                            chequeRequestModel.setIfsccode(et_ifsc_code.getText().toString());
                            chequeRequestModel.setAmount(Integer.valueOf(et_amount.getText().toString()));
                            //chequeRequestModel.setExpiaryDate(tv_expiry_date.getText().toString());
                            chequeRequestModel.setCheckDetailImage(responseImage);
                            chequeRequestModel.setChequeDetailId(chequeId);

                            if (formStatus.equalsIgnoreCase("update")) {
                                chequeRequestModel.setExpiaryDate(chequeResponseModel.getExpiaryDate());
                                ProgressDialog progressDialog = new ProgressDialog(getActivity());
                                progressDialog.setMessage("Loading...");
                                progressDialog.setTitle("Please wait ");
                                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                progressDialog.show();
                                progressDialog.setCancelable(false);
                                chequeRequestModel.setStatus(chequeStatus);
                                Call<Boolean> call = Api.getClient().updateChequeDetail(chequeRequestModel);
                                call.enqueue(new Callback<Boolean>() {
                                    @Override
                                    public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                                        progressDialog.dismiss();
                                         ((MainPage) getActivity()).removeCurrentFragmentAndMoveBack();

                                    }

                                    @Override
                                    public void onFailure(Call<Boolean> call, Throwable t) {
                                        progressDialog.dismiss();
                                    }
                                });

                            } else {
                                 ProgressDialog progressDialog = new ProgressDialog(getActivity());
                                    progressDialog.setMessage("Loading...");
                                     progressDialog.setTitle("Please wait ");
                                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                    progressDialog.show();
                                    progressDialog.setCancelable(false);

                                Call<Boolean> call = Api.getClient().saveChequeDetail(chequeRequestModel);
                                call.enqueue(new Callback<Boolean>() {
                                    @Override
                                    public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                                        progressDialog.dismiss();
                                        if(response.body()) {

                                            ((MainPage) getActivity()).removeCurrentFragmentAndMoveBack();
                                        }else {
                                            Toast.makeText(getActivity(), "Not Submit", Toast.LENGTH_SHORT).show();
                                        }

                                    }

                                    @Override
                                    public void onFailure(Call<Boolean> call, Throwable t) {
                                        progressDialog.dismiss();
                                    }
                                });
                            }



                    }

                } else {
                    Toast.makeText(getActivity(), "Please Select Start Date!!", Toast.LENGTH_SHORT).show();
                }


            }
        });

        spinner_company.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                try {

                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));

                    companyId = companyIdList[position];
                    companyName = companyNameList[position];




                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_dealer.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                try {

                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));

                    dealerId = dealerIdList[position];
                    dealerName = dealerNameList[position];




                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    private void callToPickupImage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkCameraPermission()) {
                System.out.println("Check camera permission");
                requestPermissions(new String[]{Manifest.permission.CAMERA}, 111);
            } else if (checkStoragePermission()) {
                System.out.println("Check read permission");
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 112);
            } else {
                System.out.println("Else");
                Intent intentLicenseBack = com.canhub.cropper.CropImage.activity(null)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .getIntent(getActivity());
                startActivityForResult(intentLicenseBack, 200);
               /* CropImage.activity().setGuidelines(CropImageView.Guidelines.ON)
                        .start(getActivity(), ChequeMasterFragment.this);*/
                /*ImagePicker.with(getActivity())
                        .crop()	    			//Crop image(Optional), Check Customization for more option
                        .compress(1024)			//Final image size will be less than 1 MB(Optional)
                        .start();*/

            }
        } else {
            System.out.println("Outer Else");
            CropImage.activity().start(getActivity(), ChequeMasterFragment.this);

        }
    }

    private boolean checkCameraPermission() {
        return ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkStoragePermission() {
        return ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED;
    }

    private void startDate() {
        final Calendar cldr = Calendar.getInstance();
        int day = cldr.get(Calendar.DAY_OF_MONTH);
        int month = cldr.get(Calendar.MONTH);

        int year = cldr.get(Calendar.YEAR);

        // date picker dialog
        picker = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        String sday, smonth;
                        monthOfYear = monthOfYear + 1;
                        if (dayOfMonth < 10) {
                            sday = "0" + String.valueOf(dayOfMonth);
                        } else {
                            sday = String.valueOf(dayOfMonth);
                        }
                        if (monthOfYear < 10) {
                            smonth = "0" + String.valueOf(monthOfYear);
                        } else {
                            smonth = String.valueOf(monthOfYear);
                        }
                        //sbirthdate = (String.valueOf(year) + "-" + smonth + "-" + sday);
                        tv_start_date.setText(year + "-" + smonth + "-" + sday);
                        int cyear = Calendar.getInstance().get(Calendar.YEAR);

                        // age.setText(String.valueOf(iage));

                    }
                }, year, month, day);
        picker.getDatePicker().setMaxDate(System.currentTimeMillis());
        picker.getDatePicker().setMaxDate(System.currentTimeMillis() + (1000 * 60 * 60));
        picker.show();
    }

    private void expiryDate() {
        final Calendar cldr = Calendar.getInstance();
        int day = cldr.get(Calendar.DAY_OF_MONTH);
        int month = cldr.get(Calendar.MONTH);

        int year = cldr.get(Calendar.YEAR);

        // date picker dialog
        picker = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        String sday, smonth;
                        monthOfYear = monthOfYear + 1;
                        if (dayOfMonth < 10) {
                            sday = "0" + String.valueOf(dayOfMonth);
                        } else {
                            sday = String.valueOf(dayOfMonth);
                        }
                        if (monthOfYear < 10) {
                            smonth = "0" + String.valueOf(monthOfYear);
                        } else {
                            smonth = String.valueOf(monthOfYear);
                        }
                        //sbirthdate = (String.valueOf(year) + "-" + smonth + "-" + sday);
                        tv_expiry_date.setText(year + "-" + smonth + "-" + sday);
                        int cyear = Calendar.getInstance().get(Calendar.YEAR);

                        // age.setText(String.valueOf(iage));

                    }
                }, year, month, day);
        picker.getDatePicker().setMaxDate(System.currentTimeMillis());
        picker.getDatePicker().setMaxDate(System.currentTimeMillis() + (1000 * 60 * 60));
        picker.show();
    }


    private void requestPermission() {

        Dexter.withActivity(getActivity())
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {

                        }
                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            // showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getActivity(), "Error occurred! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();

    }

    private void showSettingsDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }

        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }


    private void sendImage(String imagePath) {

        try {


            File file = new File(imagePath);

            RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("uploadfile", file.getName(), requestBody);


            Call<UploadImgResponse> call = Api.getClient().uploadImg(fileToUpload);
            call.enqueue(new Callback<UploadImgResponse>() {
                @Override
                public void onResponse(Call<UploadImgResponse> call, Response<UploadImgResponse> response) {
                     try {
                         if (response.body().getFlag()) {
                             responseImage = response.body().getPath();

                             // Toast.makeText(getContext(), "" + responseImage, Toast.LENGTH_SHORT).show();
                         } else {
                         }
                     }catch (Exception e){
                         e.printStackTrace();
                     }



                    // Toast.makeText(getContext(), "aadharback"+aadharfrontPath, Toast.LENGTH_SHORT).show();


                }

                @Override
                public void onFailure(Call<UploadImgResponse> call, Throwable t) {
                    Toast.makeText(getContext(), "fail" + t.getMessage(), Toast.LENGTH_SHORT).show();


                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


   /* @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {

            // When an Image is picked
            if (documentType.equalsIgnoreCase("chequePhoto")) {

                if (requestCode == 0 && resultCode == RESULT_OK && null != data) {
                    // Get the Image from data
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    assert cursor != null;
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    // firstPath = cursor.getString(columnIndex);
                    tv_image_path.setText(cursor.getString(columnIndex));


                    sendImage((tv_image_path.getText().toString()));

                    cursor.close();

                } else {
                    Toast.makeText(getActivity(), "You haven't picked Image", Toast.LENGTH_LONG).show();
                }


            }

        } catch (Exception e) {
            Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_LONG).show();
        }

    }*/


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        /*CropImage.ActivityResult result1 = CropImage.getActivityResult(data);
        if(result1.isSuccessful()){
            Uri tempUri=result1.getOriginalUri();
            File finalFile = new File(getRealPathFromURI(tempUri));
        }*/
        /*if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            System.out.println("RequesttCode"+requestCode);
            CropImage.ActivityResult result1 = CropImage.getActivityResult(data);

            if (resultCode == getActivity().RESULT_OK) {
                System.out.println("Result is ok");
                Bitmap imageBitmap = null;
                try {
                    imageBitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), result1.getOriginalUri());

                    Uri tempUri = getImageUri(getActivity(), imageBitmap);
                    // CALL THIS METHOD TO GET THE ACTUAL PATH
                    File finalFile = new File(getRealPathFromURI(tempUri));


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        else*/ if(resultCode==getActivity().RESULT_OK){
            if(requestCode==200) {

                com.canhub.cropper.CropImage.ActivityResult result = com.canhub.cropper.CropImage.getActivityResult(data);
                String filePath = result.getUriFilePath(getActivity(), true);
                System.out.println("File Path:" + filePath);
                File fileDisplayPicture = new File(filePath);
                System.out.println("Display Picture:" + filePath);
                Uri.fromFile(fileDisplayPicture);
                tv_image_path.setText(filePath);

                sendImage(filePath);
            }
        }



    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {

        if (getActivity().getContentResolver() != null) {
            Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                imgPath = cursor.getString(idx);

                tv_image_path.setText(cursor.getString(idx));

                sendImage((tv_image_path.getText().toString()));
                cursor.close();

                // textViews.get(0).setText(firstPath);

                Log.d("paths", "" + imgPath);
            }
        }
        return imgPath;
    }


}