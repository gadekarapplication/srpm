package com.salesmanagement.Fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.canhub.cropper.CropImage;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Extra.DetectConnection;
import com.salesmanagement.Model.DealerListResponse;
import com.salesmanagement.Model.DealerRegistrationRequest;
import com.salesmanagement.Model.UploadImgResponse;
import com.salesmanagement.R;
import com.salesmanagement.Retrofit.Api;



import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;


public class DealerRegistration extends Fragment {

    public String documentType;
    View view;
    LinearLayout productContainer;
    @BindView(R.id.addMoreProductsButton)
    TextView addMoreProductsButton;
    TextInputEditText cropName;
    TextInputLayout cropNameLayout;
    @BindViews({R.id.firmName, R.id.nameOfDirector, R.id.mobileNumber,
            R.id.adharNumber, R.id.emailId, R.id.gstNumber,
            R.id.panNumber, R.id.creditLimit, R.id.birthDateDealer,
            R.id.etBankName, R.id.edBranchName, R.id.edAccountNumber, R.id.edIfscCode, R.id.selectDocumentType})
    List<TextInputEditText> textInputEditTextList;
    @BindViews({R.id.tvDealerPhoto, R.id.tvIdProof, R.id.tvUploadGSTPhoto,
            R.id.tvUploadChequePhoto,
            R.id.tvChooseDPhoto, R.id.tvChooseIDPhoto, R.id.tvChooseGSTPhoto,
            R.id.tvChooseCqPhoto})
    List<TextView> textViewList;
    Intent galleryIntent;
    String dealerPhoto = "";
    String dealerIdProof = "";
    String dealerGSTPhoto = "";
    String dealerCheque = "";
    String selectedIdProofDocument = "",imgPath;
    DealerListResponse dealerResponse;
    @BindView(R.id.tvSubmitDealer)
    TextView tvSubmitDealer;
    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog
            .OnDateSetListener() {


        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {


            textInputEditTextList.get(8).setText(String.valueOf(year) + "-" + String.valueOf(monthOfYear + 1)
                    + "-" + String.valueOf(dayOfMonth));

            System.out.println("DOB: " + textInputEditTextList.get(8).getText().toString());
            // boatBookedDate=String.valueOf(year)+"-"+String.valueOf(monthOfYear+1)+"-"+String.valueOf(dayOfMonth);


        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_dealer_registration, container, false);
        ButterKnife.bind(this, view);
        init();

        return view;
    }

    private void init() {
        MainPage.title.setText("Dealer Registration");
        Bundle bundle = getArguments();
        if (bundle != null) {
            try {
                dealerResponse = bundle.getParcelable("DEALER_DETAILS");

                setDealerUpdateUI();

            } catch (Exception e) {
                e.printStackTrace();
            }
            tvSubmitDealer.setText("Update");
            MainPage.title.setText("Dealer Update");
        }
        initViews();

        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View addView = layoutInflater.inflate(R.layout.create_vegitable_dialog, null);
        addView.setVisibility(View.GONE);
        ImageView buttonRemove = (ImageView) addView.findViewById(R.id.deleteButton);
        cropName = (TextInputEditText) addView.findViewById(R.id.cropName);
        cropNameLayout = (TextInputLayout) addView.findViewById(R.id.cropNameLayout);

        buttonRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((LinearLayout) addView.getParent()).removeView(addView);
                listAllAddView();

            }
        });
        textInputEditTextList.get(8).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Date", "Click on Date");
                showDatePicker();
            }
        });
        addMoreProductsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View addView = layoutInflater.inflate(R.layout.create_vegitable_dialog, null);
                ImageView buttonRemove = (ImageView) addView.findViewById(R.id.deleteButton);
                cropName = (TextInputEditText) addView.findViewById(R.id.cropName);
                cropNameLayout = (TextInputLayout) addView.findViewById(R.id.cropNameLayout);

                buttonRemove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        ((LinearLayout) addView.getParent()).removeView(addView);
                        listAllAddView();

                    }
                });

                productContainer.addView(addView);
                listAllAddView();

            }
        });
        productContainer.addView(addView);
        listAllAddView();
        textViewList.get(4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                documentType = "dealerPhoto";

                /*galleryIntent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, 0);*/

                callToPickupImage();


            }
        });
        textViewList.get(5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                documentType = "IDPhoto";
                callToPickupImage();
               /* galleryIntent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, 0);*/

            }
        });
        textViewList.get(6).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                documentType = "GSTPhoto";
                callToPickupImage();
                /*galleryIntent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, 0);*/

            }
        });
        textViewList.get(7).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                documentType = "ChqPhoto";
                callToPickupImage();
                /*galleryIntent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, 0);*/

            }
        });
        final int[] checkedItem = {-1};
        textInputEditTextList.get(13).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // AlertDialog builder instance to build the alert dialog
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());

                // set the custom icon to the alert dialog
                // alertDialog.setIcon(R.drawable.image_logo);

                // title of the alert dialog
                alertDialog.setTitle("Select Document Type");

                // list of the items to be displayed to
                // the user in the form of list
                // so that user can select the item from
                final String[] listItems = new String[]{"Aadhar Card", "PAN Card", "Drivnig Licence", "Passport"};

                // the function setSingleChoiceItems is the function which builds
                // the alert dialog with the single item selection
                alertDialog.setSingleChoiceItems(listItems, checkedItem[0], new DialogInterface.OnClickListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        // update the selected item which is selected by the user
                        // so that it should be selected when user opens the dialog next time
                        // and pass the instance to setSingleChoiceItems method
                        checkedItem[0] = which;

                        // now also update the TextView which previews the selected item
                        // tvSelectedItemPreview.setText("Selected Item is : " + listItems[which]);
                        textInputEditTextList.get(13).setText(listItems[which]);

                        // when selected an item the dialog should be closed with the dismiss method
                        dialog.dismiss();
                    }
                });

                // set the negative button if the user
                // is not interested to select or change
                // already selected item
                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                // create and build the AlertDialog instance
                // with the AlertDialog builder instance
                AlertDialog customAlertDialog = alertDialog.create();

                // show the alert dialog when the button is clicked
                customAlertDialog.show();
            }
        });
    }

    private void callToPickupImage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkCameraPermission()) {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, 111);
            } else if (checkStoragePermission()) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 112);
            } else {
                CropImage.activity().start(getActivity(), DealerRegistration.this);
            }
        } else {
            CropImage.activity().start(getActivity(), DealerRegistration.this);

        }
    }

    private boolean checkCameraPermission() {
        return ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkStoragePermission() {
        return ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED;
    }
    private void showDatePicker() {
        Log.d("Date", "Show Date Picker");
        DatePickerFragment date = new DatePickerFragment();
        /**
         * Set Up Current Date Into dialog
         */
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);
        /**
         * Set Call back to capture selected date
         */
        date.setCallBack(ondate);
        date.show(getFragmentManager(), "Date Picker");
    }

    private void initViews() {
        productContainer = (LinearLayout) view.findViewById(R.id.container);
    }

    private void listAllAddView() {

        int childCount = productContainer.getChildCount();

        for (int i = 0; i < childCount; i++) {
            View thisChild = productContainer.getChildAt(i);
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e("onStart", "called");
        ((MainPage) getActivity()).lockUnlockDrawer(1);
        if (DetectConnection.checkInternetConnection(getActivity())) {
            //requestPermission(getActivity());
            init();
        } else {
            DetectConnection.noInternetConnection(getActivity());
        }
    }

    @OnClick({R.id.tvSubmitDealer})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvSubmitDealer:

                validateDealerRegistration();
                break;

        }

    }

    private void validateDealerRegistration() {

        String firmName = textInputEditTextList.get(0).getText().toString();
        String nameOfDirector = textInputEditTextList.get(1).getText().toString();
        String mobileNumber = textInputEditTextList.get(2).getText().toString();
        String adharNumber = textInputEditTextList.get(3).getText().toString();
        String emailId = textInputEditTextList.get(4).getText().toString();
        String gstNumber = textInputEditTextList.get(5).getText().toString();
        String panNumber = textInputEditTextList.get(6).getText().toString();
        String creditLimit = textInputEditTextList.get(7).getText().toString();
        String dobDealer = textInputEditTextList.get(8).getText().toString();

        String bankName = textInputEditTextList.get(9).getText().toString();
        String branchName = textInputEditTextList.get(10).getText().toString();
        String accountNumber = textInputEditTextList.get(11).getText().toString();
        String ifscCode = textInputEditTextList.get(12).getText().toString();

        String selectedDocType = textInputEditTextList.get(13).getText().toString();

        if (firmName.equals("")) {
            textInputEditTextList.get(0).setError("This field is required");
        } else if (nameOfDirector.equals("")) {
            textInputEditTextList.get(1).setError("This field is required");
        } else if (mobileNumber.equals("")) {
            textInputEditTextList.get(2).setError("This field is required");
        } else if (adharNumber.equals("")) {
            textInputEditTextList.get(3).setError("This field is required");
        } else if (emailId.equals("")) {
            textInputEditTextList.get(4).setError("This field is required");
        } else if (gstNumber.equals("")) {
            textInputEditTextList.get(5).setError("This field is required");
        } else if (panNumber.equals("")) {
            textInputEditTextList.get(6).setError("This field is required");
        } else if (creditLimit.equals("")) {
            textInputEditTextList.get(7).setError("This field is required");
        } else if (dobDealer.equals("")) {
            textInputEditTextList.get(8).setError("This field is required");
        } else if (bankName.equals("")) {
            textInputEditTextList.get(9).setError("This field is required");
        } else if (branchName.equals("")) {
            textInputEditTextList.get(10).setError("This field is required");
        } else if (accountNumber.equals("")) {
            textInputEditTextList.get(11).setError("This field is required");
        } else if (ifscCode.equals("")) {
            textInputEditTextList.get(12).setError("This field is required");
        } else if (dealerPhoto.equals("")) {
            //  textViewList.get(0).setError("This field is required");
            Toast.makeText(getActivity(), "Select  photo", Toast.LENGTH_SHORT).show();
        } else if (selectedDocType.equals("")) {
            textInputEditTextList.get(13).setError("Select document type");
        } else if (dealerIdProof.equals("")) {
            Toast.makeText(getActivity(), "Select ID Proof", Toast.LENGTH_SHORT).show();
            //textViewList.get(1).setError("This field is required");
        } else if (dealerGSTPhoto.equals("")) {
            //textViewList.get(2).setError("This field is required");
            Toast.makeText(getActivity(), "Select GST photo", Toast.LENGTH_SHORT).show();
        } else if (dealerCheque.equals("")) {
            //textViewList.get(3).setError("This field is required");
            Toast.makeText(getActivity(), "Select cheque photo", Toast.LENGTH_SHORT).show();
        } else if (selectedDocType.equals("")) {
            Toast.makeText(getActivity(), "Select ID Proof Document", Toast.LENGTH_SHORT).show();
        } else {
            DealerRegistrationRequest dealerRegistrationRequest = new DealerRegistrationRequest();
            dealerRegistrationRequest.setFirmName(firmName);
            dealerRegistrationRequest.setDealerFullName(nameOfDirector);
            dealerRegistrationRequest.setMobileNo(mobileNumber);
            dealerRegistrationRequest.setDealerEmail(emailId);
            dealerRegistrationRequest.setDealerAadharNo(adharNumber);
            dealerRegistrationRequest.setDealerPanNo(panNumber);
            dealerRegistrationRequest.setDealerGSTNo(gstNumber);
            dealerRegistrationRequest.setCreditLimit(Integer.valueOf(creditLimit));
            dealerRegistrationRequest.setDealerDOB(dobDealer);

            dealerRegistrationRequest.setDealerBankName(bankName);
            dealerRegistrationRequest.setDealerBranch(branchName);
            dealerRegistrationRequest.setDealerAccountNo(accountNumber);
            dealerRegistrationRequest.setDealerIFSC(ifscCode);

            dealerRegistrationRequest.setDocumentName(selectedDocType);
            dealerRegistrationRequest.setDealerPhoto(dealerPhoto);
            dealerRegistrationRequest.setDealerGSTPhoto(dealerGSTPhoto);
            dealerRegistrationRequest.setDealerIDProof(dealerIdProof);
            dealerRegistrationRequest.setDealerChequePhoto(dealerCheque);
            dealerRegistrationRequest.setEmployeeId(Integer.valueOf(MainPage.userId));
            if (tvSubmitDealer.getText().toString().equals("Update")) {
                dealerRegistrationRequest.setStatus(dealerResponse.getStatus());
                dealerRegistrationRequest.setDealershipId(dealerResponse.getDealershipId());
                callToUpdateDealer(dealerRegistrationRequest);
            } else {
                callToRegisterDealer(dealerRegistrationRequest);
            }
        }


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == 203) {
                CropImage.ActivityResult result1 = CropImage.getActivityResult(data);
                if (resultCode == getActivity().RESULT_OK) {
                    Bitmap imageBitmap = null;
                    try {
                        imageBitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), result1.getOriginalUri());

                        Uri tempUri = getImageUri(getActivity(), imageBitmap);
                        // CALL THIS METHOD TO GET THE ACTUAL PATH
                        //File finalFile = new File(getRealPathFromURI(tempUri));
                        imgPath = getRealPathFromURI(tempUri);
                        if (documentType.equals("dealerPhoto")) {
                            textViewList.get(0).setText(imgPath);
                            imgPath = textViewList.get(0).getText().toString();

                        }
                        else if (documentType.equals("IDPhoto")) {
                            textViewList.get(1).setText(imgPath);
                            imgPath = textViewList.get(1).getText().toString();


                        } else if (documentType.equals("GSTPhoto")) {
                            textViewList.get(2).setText(imgPath);
                            imgPath = textViewList.get(2).getText().toString();

                        } else if (documentType.equals("ChqPhoto")) {
                            textViewList.get(3).setText(imgPath);
                            imgPath = textViewList.get(3).getText().toString();

                        }
                        if (!imgPath.equals(""))
                            sendImageToServer(imgPath, documentType);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                // Get the Image from data
                /*Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                assert cursor != null;
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                // firstPath = cursor.getString(columnIndex);
                String path = "";
                if (documentType.equals("dealerPhoto")) {
                    textViewList.get(0).setText(cursor.getString(columnIndex));
                    path = textViewList.get(0).getText().toString();

                } else if (documentType.equals("IDPhoto")) {
                    textViewList.get(1).setText(cursor.getString(columnIndex));
                    path = textViewList.get(1).getText().toString();


                } else if (documentType.equals("GSTPhoto")) {
                    textViewList.get(2).setText(cursor.getString(columnIndex));
                    path = textViewList.get(2).getText().toString();

                } else if (documentType.equals("ChqPhoto")) {
                    textViewList.get(3).setText(cursor.getString(columnIndex));
                    path = textViewList.get(3).getText().toString();

                }

                if (!path.equals(""))
                    sendImageToServer(path, documentType);

                cursor.close();*/


            } else {
                Toast.makeText(getActivity(), "You haven't picked Image", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {

        if (getActivity().getContentResolver() != null) {
            Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                imgPath = cursor.getString(idx);




                cursor.close();

                // textViews.get(0).setText(firstPath);

                Log.d("paths", "" + imgPath);
            }
        }
        return imgPath;
    }

    private void sendImageToServer(String path, String docType) {

        try {


            final File file = new File(path);

            RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("uploadfile", file.getName(), requestBody);


            Call<UploadImgResponse> call = Api.getClient().uploadImg(fileToUpload);
            call.enqueue(new Callback<UploadImgResponse>() {
                @Override
                public void onResponse(Call<UploadImgResponse> call, Response<UploadImgResponse> response) {
                    String resImage = "";
                    if (response.body().getFlag()) {

                        if (docType.equals("dealerPhoto")) {
                            dealerPhoto = response.body().getPath();
                        } else if (docType.equals("IDPhoto")) {
                            dealerIdProof = response.body().getPath();
                        } else if (docType.equals("GSTPhoto")) {
                            dealerGSTPhoto = response.body().getPath();
                        } else if (docType.equals("ChqPhoto")) {
                            dealerCheque = response.body().getPath();
                        }


                        //Toast.makeText(getContext(), "" + resImage, Toast.LENGTH_SHORT).show();

                    } else {

                    }


                    // Toast.makeText(getContext(), "aadharback"+aadharfrontPath, Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onFailure(Call<UploadImgResponse> call, Throwable t) {
                    Toast.makeText(getContext(), "fail" + t.getMessage(), Toast.LENGTH_SHORT).show();


                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void callToRegisterDealer(DealerRegistrationRequest dealerRegistrationRequest) {

        try {
            ProgressDialog progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Loading...");
            progressDialog.setTitle("Please wait");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
            progressDialog.setCancelable(false);


            Call<Boolean> call = Api.getClient().addDealer(dealerRegistrationRequest);
            call.enqueue(new Callback<Boolean>() {
                @Override
                public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                    progressDialog.dismiss();
                    if (response.body()) {

                        Toasty.success(getActivity(), "Successfully Add").show();
                        //((MainPage) getActivity()).loadFragment(new DealerListFragment(), true);
                        ((MainPage) getActivity()).removeCurrentFragmentAndMoveBack();
                        //Toast.makeText(getContext(), "" + resImage, Toast.LENGTH_SHORT).show();

                    } else {
                        Toasty.error(getActivity(), "Not  Added").show();
                    }


                    // Toast.makeText(getContext(), "aadharback"+aadharfrontPath, Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onFailure(Call<Boolean> call, Throwable t) {
                    Toast.makeText(getContext(), "fail" + t.getMessage(), Toast.LENGTH_SHORT).show();

                    progressDialog.dismiss();
                }
            });


        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    private void callToUpdateDealer(DealerRegistrationRequest dealerRegistrationRequest) {

        try {
            ProgressDialog progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Loading...");
            progressDialog.setTitle("Please wait...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
            progressDialog.setCancelable(false);


            Call<Boolean> call = Api.getClient().updateDealer(dealerRegistrationRequest);
            call.enqueue(new Callback<Boolean>() {
                @Override
                public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                    progressDialog.dismiss();
                    if (response.body()) {

                        Toasty.success(getActivity(), "Successfully Update").show();
                        //((MainPage) getActivity()).loadFragment(new DealerListFragment(), true);
                        ((MainPage) getActivity()).removeCurrentFragmentAndMoveBack();
                        //Toast.makeText(getContext(), "" + resImage, Toast.LENGTH_SHORT).show();

                    } else {
                        Toasty.error(getActivity(), "Not  Added").show();
                    }


                    // Toast.makeText(getContext(), "aadharback"+aadharfrontPath, Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onFailure(Call<Boolean> call, Throwable t) {
                    Toast.makeText(getContext(), "fail" + t.getMessage(), Toast.LENGTH_SHORT).show();

                    progressDialog.dismiss();
                }
            });


        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    private void setDealerUpdateUI() {
        String firmName = dealerResponse.getFirmName();
        textInputEditTextList.get(0).setText(firmName);
        String nameOfDirector = dealerResponse.getDealerFullName();
        textInputEditTextList.get(1).setText(nameOfDirector);
        String mobileNumber = dealerResponse.getMobileNo();
        textInputEditTextList.get(2).setText(mobileNumber);
        String adharNumber = dealerResponse.getDealerAadharNo();
        textInputEditTextList.get(3).setText(adharNumber);
        String emailId = dealerResponse.getDealerEmail();
        textInputEditTextList.get(4).setText(emailId);
        String gstNumber = dealerResponse.getDealerGSTNo();
        textInputEditTextList.get(5).setText(gstNumber);
        String panNumber = dealerResponse.getDealerPanNo();
        textInputEditTextList.get(6).setText(panNumber);
        String creditLimit = String.valueOf(dealerResponse.getCreditLimit());
        textInputEditTextList.get(7).setText(creditLimit);
        String dobDealer = dealerResponse.getDealerDOB();
        textInputEditTextList.get(8).setText(dobDealer);

        String bankName = dealerResponse.getDealerBankName();
        textInputEditTextList.get(9).setText(bankName);
        String branchName = dealerResponse.getDealerBranch();
        textInputEditTextList.get(10).setText(branchName);
        String accountNumber = dealerResponse.getDealerAccountNo();
        textInputEditTextList.get(11).setText(accountNumber);
        String ifscCode = dealerResponse.getDealerIFSC();
        textInputEditTextList.get(12).setText(ifscCode);

        String selectedDocType = dealerResponse.getDocumentName();
        textInputEditTextList.get(13).setText(selectedDocType);

        dealerPhoto = dealerResponse.getDealerPhoto();
        dealerGSTPhoto = dealerResponse.getDealerGSTPhoto();
        dealerIdProof = dealerResponse.getDealerIDProof();
        dealerCheque = dealerResponse.getDealerChequePhoto();


    }
}