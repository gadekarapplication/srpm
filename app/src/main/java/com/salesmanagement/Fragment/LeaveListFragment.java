package com.salesmanagement.Fragment;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Adapter.AddressListAdapter;
import com.salesmanagement.Extra.DetectConnection;
import com.salesmanagement.Model.DealerAddressResponseModel;
import com.salesmanagement.R;
import com.salesmanagement.Retrofit.Api;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LeaveListFragment extends Fragment {
    public static String employeeId;
    @BindView(R.id.rvLeaveList)
    RecyclerView rvLeaveList;
    private List<LeaveResponseModel>LeaveResponseModelList= new ArrayList<>();
    LeaveListAdapter adapter;
    @BindView(R.id.fabAddLeaveList)
    FloatingActionButton fabAddLeaveList;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_leave_list, container, false);
        ButterKnife.bind(this,view);


        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e("onStart", "called");
        ((MainPage) getActivity()).lockUnlockDrawer(1);
        if (DetectConnection.checkInternetConnection(getActivity())) {

            init();
        } else {
            DetectConnection.noInternetConnection(getActivity());
        }

    }

    private void init() {
        MainPage.title.setText("Leave Application List");
        rvLeaveList.setLayoutManager(new LinearLayoutManager(getActivity()));
        getLeaveList(Integer.valueOf(MainPage.userId));
        fabAddLeaveList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainPage)getActivity()).loadFragment(new LeaveFragment(),true);
            }
        });

    }

    private void getLeaveList(Integer empId) {

        ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.setTitle("Please wait ");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        progressDialog.setCancelable(false);
        Call<List<LeaveResponseModel>> call = Api.getClient().getLeaveList(MainPage.userId);
        call.enqueue(new Callback<List<LeaveResponseModel>>() {
            @Override
            public void onResponse(Call<List<LeaveResponseModel>> call, Response<List<LeaveResponseModel>> response) {
                if (response.isSuccessful()) {
                    try {
                       LeaveResponseModelList.clear();
                        LeaveResponseModelList = response.body();
                        if (LeaveResponseModelList.size() > 0) {

                            adapter = new LeaveListAdapter(getActivity(), LeaveResponseModelList);
                            rvLeaveList.setAdapter(adapter);
                            rvLeaveList.setHasFixedSize(true);
                            progressDialog.dismiss();
                            rvLeaveList.setVisibility(View.VISIBLE);

                        } else {
                            progressDialog.dismiss();
                            rvLeaveList.setVisibility(View.GONE);
                            Toast.makeText(getContext(), "No leave applications found ", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }
                } else {
                    progressDialog.dismiss();
                    rvLeaveList.setVisibility(View.GONE);
                    Toast.makeText(getContext(), "No leave applications found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<LeaveResponseModel>> call, Throwable t) {
                Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                rvLeaveList.setVisibility(View.GONE);
            }
        });
    }
}