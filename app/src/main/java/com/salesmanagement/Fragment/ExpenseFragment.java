package com.salesmanagement.Fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Adapter.DealerListAdapter;
import com.salesmanagement.Adapter.ExpenseAdapter;
import com.salesmanagement.Extra.DetectConnection;
import com.salesmanagement.Model.DealerListResponse;
import com.salesmanagement.Model.ExpenseResponse;
import com.salesmanagement.R;
import com.salesmanagement.Retrofit.Api;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.salesmanagement.Extra.Common.getSavedUserData;


public class ExpenseFragment extends Fragment {


    @BindView(R.id.rvExpense)
    RecyclerView rvExpense;
    @BindView(R.id.fabAddExpense)
    FloatingActionButton fabAddExpense;
    ExpenseAdapter expenseAdapter;
    List<ExpenseResponse> expenseResponseList=new ArrayList<>();
    @BindView(R.id.pbExpense)
    ProgressBar pbExpense;
    Integer empId;
    @BindView(R.id.swapToRefreshExpense)
    SwipeRefreshLayout swapToRefreshExpense;
    public ExpenseFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_expense, container, false);
        ButterKnife.bind(this,view);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        ((MainPage) getActivity()).lockUnlockDrawer(1);
        if (DetectConnection.checkInternetConnection(getActivity())) {
            init();
        } else {
            DetectConnection.noInternetConnection(getActivity());
        }

    }

    private void init() {

        MainPage.title.setText("Expense List");
        rvExpense.setLayoutManager(new LinearLayoutManager(getActivity()));
        fabAddExpense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainPage) getActivity()).loadFragment(new AddExpenseFragment(), true);
            }
        });
        try {
            empId = Integer.parseInt(getSavedUserData(getActivity(), "userId"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        swapToRefreshExpense.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        init();
                    }
                }
        );

        callToGetExpenseList(empId);
    }

    private void callToGetExpenseList(Integer empId) {
        swapToRefreshExpense.setRefreshing(false);
        pbExpense.setVisibility(View.VISIBLE);
        Call<List<ExpenseResponse>> call = Api.getClient().getExpense(empId);
        call.enqueue(new Callback<List<ExpenseResponse>>() {
            @Override
            public void onResponse(Call<List<ExpenseResponse>> call, Response<List<ExpenseResponse>> response) {

                if (response.isSuccessful()) {
                    pbExpense.setVisibility(View.GONE);
                    try {
                        // progressDialog.dismiss();
                        expenseResponseList.clear();
                        expenseResponseList = response.body();
                        if (expenseResponseList.size() > 0) {
                            //SubCategoryAdapter adapter = new SubCategoryAdapter(getActivity(), subCategoryResponseList);
                            expenseAdapter = new ExpenseAdapter( getActivity(),expenseResponseList);
                            //rvDealerList.setLayoutManager(new GridLayoutManager(getActivity(), 3));
                            rvExpense.setAdapter(expenseAdapter);
                            expenseAdapter.notifyDataSetChanged();
                            rvExpense.setHasFixedSize(true);


                            rvExpense.setVisibility(View.VISIBLE);

                        } else {
                            //progressDialog.dismiss();
                            rvExpense.setVisibility(View.GONE);
                            Toast.makeText(getContext(), "No expense list found", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    // progressDialog.dismiss();
                    pbExpense.setVisibility(View.GONE);
                    rvExpense.setVisibility(View.GONE);
                    Toast.makeText(getContext(), "No expense list found", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<List<ExpenseResponse>> call, Throwable t) {
                Log.e("Expense List:", "" + t.getMessage());
                Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
                // progressDialog.dismiss();
                pbExpense.setVisibility(View.GONE);
                rvExpense.setVisibility(View.GONE);
            }
        });

    }
}