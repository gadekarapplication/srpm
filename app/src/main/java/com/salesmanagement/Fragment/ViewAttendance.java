package com.salesmanagement.Fragment;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.listeners.OnCalendarPageChangeListener;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;
import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Adapter.AttendanceAdapter;
import com.salesmanagement.Extra.DetectConnection;
import com.salesmanagement.Extra.DrawableUtils;
import com.salesmanagement.Model.AttendanceReqModel;
import com.salesmanagement.Model.EmpAttendanceModel;
import com.salesmanagement.Model.MonthlyAttendanceResponseModel;
import com.salesmanagement.R;
import com.salesmanagement.Retrofit.Api;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ViewAttendance extends Fragment {

    View view;
    @BindView(R.id.calendarView)
    CalendarView calendarView;

    @BindView(R.id.rv_calender)
    RecyclerView rv_calender;

    private AttendanceAdapter attendanceAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_view_attendance, container, false);
        ButterKnife.bind(this, view);
        MainPage.title.setText("View AttendanceResponse");

        setAdapter();

        calendarView.setOnDayClickListener(new OnDayClickListener() {
            @Override
            public void onDayClick(EventDay eventDay) {

                try {

                    DateFormat inputFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");
                    DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date date = inputFormat.parse(eventDay.getCalendar().getTime().toString());
                    String outputDate = outputFormat.format(date);

                    getAttendance(outputDate);


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        Calendar mCalendar = Calendar.getInstance();
        calendarView.setOnForwardPageChangeListener(new OnCalendarPageChangeListener() {
            @Override
            public void onChange() {
                mCalendar.set(mCalendar.MONTH, mCalendar.get( mCalendar.MONTH ) + 1);
                String dateOfMonth=   getFirstDate(mCalendar.DAY_OF_MONTH, mCalendar);
                System.out.println("DateOfMonth:"+dateOfMonth);
            }
        });
        return view;
    }

    private void setAdapter() {



    }

    private void getAttendance(String outputDate) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.order_details_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        TextView txtyes = dialog.findViewById(R.id.yes);
        TextView txtattendance = dialog.findViewById(R.id.attendance);

        txtattendance.setText("It's demo attendance");

        txtyes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

            }
        });

        dialog.show();

    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e("onStart", "called");
        ((MainPage) getActivity()).lockUnlockDrawer(1);
        if (DetectConnection.checkInternetConnection(getActivity())) {
            getAttendanceList();
        } else {
            DetectConnection.noInternetConnection(getActivity());
        }
    }

    private void getAttendanceList() {

        List<EventDay> events = new ArrayList<>();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(simpleDateFormat.parse("2021-06-23"));
            events.add(new EventDay(calendar, DrawableUtils.getCircleDrawableWithText(getActivity(), "")));
            calendarView.setEvents(events);

        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    private String getFirstDate(int dayOfMonth, Calendar cal) {
        cal.set(dayOfMonth, cal.getActualMinimum(Calendar.DAY_OF_MONTH)); //(Month, 1)
        return getFormatFromStringDate("yyyy-MM-dd", cal.getTime());
    }

    private String getLastDate(int dayOfMonth, Calendar cal) {
        cal.set(dayOfMonth,  cal.getActualMaximum(Calendar.DAY_OF_MONTH)); //(Month, 31)
        return getFormatFromStringDate("yyyy-MM-dd", cal.getTime());
    }

    public String getFormatFromStringDate(String pattern, Date date) {
        return new java.text.SimpleDateFormat(pattern).format(date); //java.text.DateFormat.getDateTimeInstance().format(ts.getTime())
    }

}