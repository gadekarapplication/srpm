package com.salesmanagement.Fragment;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Extra.DetectConnection;
import com.salesmanagement.Fragment.DatePickerFragment;
import com.salesmanagement.Model.LeaveformRequest;
import com.salesmanagement.R;
import com.salesmanagement.Retrofit.Api;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LeaveFragment extends Fragment {
   
    @BindViews({R.id.Title,R.id.Description,R.id.leaveDate})
    List<TextInputEditText> textInputEditTextList;
    @BindView(R.id.tvSubmitLeave)
    TextView tvSubmitLeave;
    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog
            .OnDateSetListener() {


        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {


            textInputEditTextList.get(2).setText(String.valueOf(year) + "-" + String.valueOf(monthOfYear + 1)
                    + "-" + String.valueOf(dayOfMonth));


        }
    };


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_leave, container, false);
        ButterKnife.bind(this, view);
        ((MainPage) getActivity()).lockUnlockDrawer(1);
        if (DetectConnection.checkInternetConnection(getActivity())) {
            init();
        } else {
            DetectConnection.noInternetConnection(getActivity());
        }

        return view;
    }

    private void init() {

         tvSubmitLeave.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 validation();
             }
         });
        textInputEditTextList.get(2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Date", "Click on Date");
                showDatePicker();
            }
        });

    }

    private void validation() {
        String title=textInputEditTextList.get(0).getText().toString();
        String description=textInputEditTextList.get(1).getText().toString();
        String date=textInputEditTextList.get(2).getText().toString();

        if (title.equals("")) {
            textInputEditTextList.get(0).setError("Enter title");
        } else if (description.equals(""))
        {
            textInputEditTextList.get(1).setError("Enter description");
        }else if(date.equals("")){
            textInputEditTextList.get(2).setError("Select date");
        }
        else{
            LeaveformRequest leaveformRequest =new LeaveformRequest();
            leaveformRequest.setLeaveType(title);
            leaveformRequest.setLeaveDescription(description);
            leaveformRequest.setLeaveDate(date);
            leaveformRequest.setEmpolyeeId(Integer.valueOf(MainPage.userId));
            leaveForm(leaveformRequest);
        }
    }

    private void leaveForm(LeaveformRequest leaveformRequest) {
        ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.setTitle("Please wait ");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        progressDialog.setCancelable(false);

        Call<Boolean> call = Api.getClient().leaveform(leaveformRequest);
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {


                try {
                    progressDialog.dismiss();
                    if (response.body()) {
                        Toasty.success(getActivity(), "Leave submitted successfully").show();
                        ((MainPage) getActivity()).removeCurrentFragmentAndMoveBack();
                        progressDialog.dismiss();


                    } else {
                        progressDialog.dismiss();

                        Toast.makeText(getContext(), "Leave Not Submitted", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {

                Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();

            }
        });

    }
    private void showDatePicker() {
        Log.d("Date", "Show Date Picker");
        ForwardDatePicker date = new ForwardDatePicker();
        /**
         * Set Up Current Date Into dialog
         */
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);
        /**
         * Set Call back to capture selected date
         */
        date.setCallBack(ondate);
        date.show(getFragmentManager(), "Date Picker");
    }


}