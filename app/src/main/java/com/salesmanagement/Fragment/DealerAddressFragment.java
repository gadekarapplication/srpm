package com.salesmanagement.Fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputEditText;
import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Extra.DetectConnection;
import com.salesmanagement.Model.AreaResponse;
import com.salesmanagement.Model.CityResponse;
import com.salesmanagement.Model.CountryResponse;
import com.salesmanagement.Model.DealerAddressModel;
import com.salesmanagement.Model.DealerAddressResponseModel;
import com.salesmanagement.Model.DistrictResponse;
import com.salesmanagement.Model.PinCodeResponse;
import com.salesmanagement.Model.StateResponse;
import com.salesmanagement.Model.TalukaResponse;
import com.salesmanagement.R;
import com.salesmanagement.Retrofit.Api;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.salesmanagement.Extra.Common.areaResponseList;
import static com.salesmanagement.Extra.Common.callToGetArea;
import static com.salesmanagement.Extra.Common.callToGetCity;
import static com.salesmanagement.Extra.Common.callToGetCountry;
import static com.salesmanagement.Extra.Common.callToGetDistrict;
import static com.salesmanagement.Extra.Common.callToGetPinCode;
import static com.salesmanagement.Extra.Common.callToGetState;
import static com.salesmanagement.Extra.Common.callToGetTaluka;
import static com.salesmanagement.Extra.Common.cityResponseList;
import static com.salesmanagement.Extra.Common.countryResponseList;
import static com.salesmanagement.Extra.Common.districtResponseList;
import static com.salesmanagement.Extra.Common.pinCodeResponseList;
import static com.salesmanagement.Extra.Common.stateResponseList;
import static com.salesmanagement.Extra.Common.talukaResponseList;


public class DealerAddressFragment extends Fragment {

    @BindView(R.id.tvSubmitDealerAddress)
    TextView tvSubmitDealerAddress;
    @BindViews({R.id.addressType, R.id.fullAddress, R.id.edtContactNumber,
            R.id.edtFlatNumber, R.id.edtcountry, R.id.edtState,
            R.id.edtDistrict, R.id.edtTaluka, R.id.edtCity,
            R.id.edtPincode, R.id.edtArea, R.id.edtLandMark})
    List<TextInputEditText> inputEditTextList;
    //List<CountryResponse> countryResponseList=new ArrayList<>();
    Integer selectedPosCountry = -1;
    //List<StateResponse> stateResponseList=new ArrayList<>();
    Integer selectedPosState = -1;
    //List<DistrictResponse> districtResponseList=new ArrayList<>();
    Integer selectedPosDistrict = -1;
    //List<TalukaResponse> talukaResponseList=new ArrayList<>();
    Integer selectedPosTaluka = -1;
    //List<CityResponse> cityResponseList=new ArrayList<>();
    Integer selectedPosCity = -1;
    Integer selectedPosPinCode = -1;
    Integer selectedPosArea = -1;
    DealerAddressResponseModel dealerAddressResponseModel;
    AreaResponse areaResponse;
    private Integer dealerId, areaId;

    public DealerAddressFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dealer_address, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    private void init() {
        MainPage.title.setText("Add address");
        Bundle bundle = getArguments();

        try {

            if (getArguments().get("DEALER_ID") != null) {

                dealerId = bundle.getInt("DEALER_ID");
                System.out.println("DealerAddressFragment: " + dealerId);
            } else if (getArguments().get("DEALER_ADDRESS") != null) {
                MainPage.title.setText("Update address");
                dealerAddressResponseModel = bundle.getParcelable("DEALER_ADDRESS");

                System.out.println("Area Id:" + dealerAddressResponseModel.getAreaId());
                areaId = dealerAddressResponseModel.getAreaId();
                dealerId = dealerAddressResponseModel.getDealershipId();
                System.out.println("Landmark:" + dealerAddressResponseModel.getLandmark());

                callToGetAddressByAreaId(areaId);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        //callToGetCountry();
        callToGetCountry();
        final int[] checkedItem = {-1};
        inputEditTextList.get(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // AlertDialog builder instance to build the alert dialog
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());

                alertDialog.setTitle("Select address type");
                final String[] listItems = new String[]{"Shop Address", "Godown Address", "Other Address"};

                alertDialog.setSingleChoiceItems(listItems, checkedItem[0], new DialogInterface.OnClickListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        checkedItem[0] = which;


                        inputEditTextList.get(0).setText(listItems[which]);

                        dialog.dismiss();
                    }
                });

                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                    }
                });

                AlertDialog customAlertDialog = alertDialog.create();

                customAlertDialog.show();
            }
        });
        inputEditTextList.get(4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());

                alertDialog.setTitle("Select Country");
                // final String[] listItems = new String[]{"Shop Address","Godown Address", "Other Address"};
                final ArrayAdapter<CountryResponse> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.select_dialog_singlechoice, countryResponseList);
                alertDialog.setSingleChoiceItems(arrayAdapter, selectedPosCountry, new DialogInterface.OnClickListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        selectedPosCountry = which;
                        countryResponseList.get(selectedPosCountry).getCountryId();
                        countryResponseList.get(selectedPosCountry).getCountryName();
                        inputEditTextList.get(4).setText(countryResponseList.get(selectedPosCountry).getCountryName());
                        callToGetState(countryResponseList.get(selectedPosCountry).getCountryId());
                        dialog.dismiss();
                    }
                });

                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                    }
                });

                AlertDialog customAlertDialog = alertDialog.create();

                customAlertDialog.show();

            }
        });


        inputEditTextList.get(5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());

                alertDialog.setTitle("Select State");
                // final String[] listItems = new String[]{"Shop Address","Godown Address", "Other Address"};
                final ArrayAdapter<StateResponse> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.select_dialog_singlechoice, stateResponseList);
                alertDialog.setSingleChoiceItems(arrayAdapter, selectedPosState, new DialogInterface.OnClickListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        selectedPosState = which;
                        stateResponseList.get(selectedPosState).getStateId();
                        stateResponseList.get(selectedPosState).getStateName();
                        inputEditTextList.get(5).setText(stateResponseList.get(selectedPosState).getStateName());
                        callToGetDistrict(stateResponseList.get(selectedPosState).getStateId());
                        dialog.dismiss();
                    }
                });

                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                    }
                });

                AlertDialog customAlertDialog = alertDialog.create();

                customAlertDialog.show();

            }
        });


        inputEditTextList.get(6).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());

                alertDialog.setTitle("Select District");
                // final String[] listItems = new String[]{"Shop Address","Godown Address", "Other Address"};
                final ArrayAdapter<DistrictResponse> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.select_dialog_singlechoice, districtResponseList);
                alertDialog.setSingleChoiceItems(arrayAdapter, selectedPosDistrict, new DialogInterface.OnClickListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        selectedPosDistrict = which;
                        districtResponseList.get(selectedPosDistrict).getStateId();
                        districtResponseList.get(selectedPosDistrict).getStateName();
                        inputEditTextList.get(6).setText(districtResponseList.get(selectedPosDistrict).getDistrictName());
                        callToGetTaluka(districtResponseList.get(selectedPosDistrict).getDistrictId());
                        dialog.dismiss();
                    }
                });

                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                    }
                });

                AlertDialog customAlertDialog = alertDialog.create();

                customAlertDialog.show();

            }
        });
        inputEditTextList.get(7).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());

                alertDialog.setTitle("Select Taluka");
                // final String[] listItems = new String[]{"Shop Address","Godown Address", "Other Address"};
                final ArrayAdapter<TalukaResponse> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.select_dialog_singlechoice, talukaResponseList);
                alertDialog.setSingleChoiceItems(arrayAdapter, selectedPosTaluka, new DialogInterface.OnClickListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        selectedPosTaluka = which;

                        inputEditTextList.get(7).setText(talukaResponseList.get(selectedPosTaluka).getTalukaName());
                        callToGetCity(talukaResponseList.get(selectedPosTaluka).getTalukaId());
                        dialog.dismiss();
                    }
                });

                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                    }
                });

                AlertDialog customAlertDialog = alertDialog.create();

                customAlertDialog.show();

            }
        });

        inputEditTextList.get(8).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());

                alertDialog.setTitle("Select city");
                // final String[] listItems = new String[]{"Shop Address","Godown Address", "Other Address"};
                final ArrayAdapter<CityResponse> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.select_dialog_singlechoice, cityResponseList);
                alertDialog.setSingleChoiceItems(arrayAdapter, selectedPosCity, new DialogInterface.OnClickListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        selectedPosCity = which;

                        inputEditTextList.get(8).setText(cityResponseList.get(selectedPosCity).getCityName());
                        callToGetPinCode(cityResponseList.get(selectedPosCity).getCityId());
                        dialog.dismiss();
                    }
                });

                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                    }
                });

                AlertDialog customAlertDialog = alertDialog.create();

                customAlertDialog.show();

            }
        });

        inputEditTextList.get(9).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());

                alertDialog.setTitle("Select pincode");
                // final String[] listItems = new String[]{"Shop Address","Godown Address", "Other Address"};
                final ArrayAdapter<PinCodeResponse> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.select_dialog_singlechoice, pinCodeResponseList);
                alertDialog.setSingleChoiceItems(arrayAdapter, selectedPosPinCode, new DialogInterface.OnClickListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        selectedPosPinCode = which;

                        inputEditTextList.get(9).setText(pinCodeResponseList.get(selectedPosPinCode).getPincodeName());
                        callToGetArea(pinCodeResponseList.get(selectedPosCity).getPincodeId());
                        dialog.dismiss();
                    }
                });

                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                    }
                });

                AlertDialog customAlertDialog = alertDialog.create();

                customAlertDialog.show();

            }
        });


        inputEditTextList.get(10).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());

                alertDialog.setTitle("Select Area");
                // final String[] listItems = new String[]{"Shop Address","Godown Address", "Other Address"};
                final ArrayAdapter<AreaResponse> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.select_dialog_singlechoice, areaResponseList);
                alertDialog.setSingleChoiceItems(arrayAdapter, selectedPosArea, new DialogInterface.OnClickListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        selectedPosArea = which;
                        areaId = areaResponseList.get(selectedPosArea).getAreaId();
                        inputEditTextList.get(10).setText(areaResponseList.get(selectedPosArea).getAreaName());

                        dialog.dismiss();
                    }
                });

                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                    }
                });

                AlertDialog customAlertDialog = alertDialog.create();

                customAlertDialog.show();

            }
        });
    }


    @OnClick({R.id.tvSubmitDealerAddress})
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.tvSubmitDealerAddress:
                validateAddress();
                break;


        }


    }

    private void validateAddress() {
        String addressType = inputEditTextList.get(0).getText().toString();
        String fullAddress = inputEditTextList.get(1).getText().toString();
        String contactNumber = inputEditTextList.get(2).getText().toString();
        String flatNumber = inputEditTextList.get(3).getText().toString();
        String selectedCountry = inputEditTextList.get(4).getText().toString();
        String selectedState = inputEditTextList.get(5).getText().toString();
        String selectedDistrict = inputEditTextList.get(6).getText().toString();
        String selectedTaluka = inputEditTextList.get(7).getText().toString();
        String selectedCity = inputEditTextList.get(8).getText().toString();
        String selectedPinCode = inputEditTextList.get(9).getText().toString();
        String selectedArea = inputEditTextList.get(10).getText().toString();
        String landMark = inputEditTextList.get(11).getText().toString();
        if (addressType.equals("")) {
            Toasty.error(getActivity(), "Select address type").show();
        } else if (fullAddress.equals("")) {
            inputEditTextList.get(1).setError("Enter address");
        } else if (contactNumber.equals("")) {
            inputEditTextList.get(2).setError("Enter contact number");
        } else if (flatNumber.equals("")) {
            inputEditTextList.get(3).setError("Enter flat number");
        } else if (selectedCountry.equals("")) {
            Toasty.error(getActivity(), "Select country").show();
        } else if (selectedState.equals("")) {
            Toasty.error(getActivity(), "Select state").show();
        } else if (selectedDistrict.equals("")) {
            Toasty.error(getActivity(), "Select district").show();
        } else if (selectedTaluka.equals("")) {
            Toasty.error(getActivity(), "Select taluka").show();
        } else if (selectedCity.equals("")) {
            Toasty.error(getActivity(), "Select city").show();
        } else if (selectedPinCode.equals("")) {
            Toasty.error(getActivity(), "Select pincode").show();
        } else if (selectedArea.equals("")) {
            Toasty.error(getActivity(), "Select area").show();
        } else if (landMark.equals("")) {
            inputEditTextList.get(11).setError("Enter landmark");
        } else {
            DealerAddressModel dealerAddressModel = new DealerAddressModel();
            dealerAddressModel.setAddressType(addressType);
            dealerAddressModel.setFullAddress(fullAddress);
            dealerAddressModel.setLandmark(landMark);
            dealerAddressModel.setArea(selectedArea);
            dealerAddressModel.setMobileNo(contactNumber);
            dealerAddressModel.setFlatNo(flatNumber);
            dealerAddressModel.setAreaId(areaId);
            dealerAddressModel.setDealershipId(dealerId);
            dealerAddressModel.setStatus("Active");
            if (tvSubmitDealerAddress.getText().equals("Update")) {
                dealerAddressModel.setDealerAddressId(dealerAddressResponseModel.getDealerAddressId());
                callToUpdateAddress(dealerAddressModel);
            } else {
                callToAddDealerAddress(dealerAddressModel);
            }


        }

    }


    private void callToAddDealerAddress(DealerAddressModel dealerAddressModel) {

        ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.setTitle("Please wait ");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        progressDialog.setCancelable(false);
        Call<Boolean> call = Api.getClient().addDealerAddress(dealerAddressModel);
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {


                try {

                    if (response.body()) {
                        Toasty.success(getActivity(), "Address added successfully").show();
                        ((MainPage) getActivity()).removeCurrentFragmentAndMoveBack();
                        progressDialog.dismiss();


                    } else {
                        progressDialog.dismiss();

                        Toast.makeText(getContext(), "Record Not Add", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Log.e("Dealer List:", "" + t.getMessage());
                Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();

            }
        });
    }

    private void callToUpdateAddress(DealerAddressModel dealerAddressModel) {

        ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.setTitle("Please wait ");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        progressDialog.setCancelable(false);
        Call<Boolean> call = Api.getClient().updateDealerAddress(dealerAddressModel);
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {


                try {

                    if (response.body()) {
                        Toasty.success(getActivity(), "Address updated successfully").show();

                        ((MainPage) getActivity()).removeCurrentFragmentAndMoveBack();
                        progressDialog.dismiss();


                    } else {
                        progressDialog.dismiss();

                        Toast.makeText(getContext(), "Record Not Update", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Log.e("Dealer List:", "" + t.getMessage());
                Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();

            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e("onStart", "called");
        ((MainPage) getActivity()).lockUnlockDrawer(1);
        if (DetectConnection.checkInternetConnection(getActivity())) {
            init();
        } else {
            DetectConnection.noInternetConnection(getActivity());
        }

    }


    private void callToGetAddressByAreaId(Integer areaId) {

        ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.setTitle("Please wait ");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        progressDialog.setCancelable(false);
        Call<AreaResponse> call = Api.getClient().getAddressByAreaId(areaId);
        call.enqueue(new Callback<AreaResponse>() {
            @Override
            public void onResponse(Call<AreaResponse> call, Response<AreaResponse> response) {


                try {

                    if (response.body() != null) {
                        areaResponse = response.body();
                        updateAddressUI();
                        progressDialog.dismiss();


                    } else {
                        progressDialog.dismiss();

                        Toast.makeText(getContext(), "Record Not Add", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<AreaResponse> call, Throwable t) {
                Log.e("Dealer List:", "" + t.getMessage());
                Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();

            }
        });

    }

    private void updateAddressUI() {
        tvSubmitDealerAddress.setText("Update");
        String addressType = dealerAddressResponseModel.getAddressType();
        inputEditTextList.get(0).setText(addressType);
        String fullAddress = dealerAddressResponseModel.getFullAddress();
        inputEditTextList.get(1).setText(fullAddress);
        String contactNumber = dealerAddressResponseModel.getMobileNo();
        inputEditTextList.get(2).setText(contactNumber);
        String flatNumber = dealerAddressResponseModel.getFlatNo();
        inputEditTextList.get(3).setText(flatNumber);
        String selectedCountry = areaResponse.getCountryName();
        inputEditTextList.get(4).setText(selectedCountry);

        String selectedState = areaResponse.getStateName();
        inputEditTextList.get(5).setText(selectedState);

        String selectedDistrict = areaResponse.getDistrictName();
        inputEditTextList.get(6).setText(selectedDistrict);
        String selectedTaluka = areaResponse.getTalukaName();
        inputEditTextList.get(7).setText(selectedTaluka);
        String selectedCity = areaResponse.getCityName();
        inputEditTextList.get(8).setText(selectedCity);
        String selectedPinCode = areaResponse.getPincodeName();
        inputEditTextList.get(9).setText(selectedPinCode);
        String selectedArea = areaResponse.getAreaName();
        inputEditTextList.get(10).setText(selectedArea);
        String landMark = dealerAddressResponseModel.getLandmark();
        inputEditTextList.get(11).setText(landMark);


    }


}