package com.salesmanagement.Fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.andreabaccega.widget.FormEditText;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Adapter.FarmerListAdapter;
import com.salesmanagement.Extra.DetectConnection;
import com.salesmanagement.Model.FarmerRequestModel;
import com.salesmanagement.R;
import com.salesmanagement.Retrofit.Api;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FarmerRegistarationFragment extends Fragment {

    List<FarmerRequestModel> farmerList;
    List<FarmerRequestModel> searchFarmerList;
    private View view;
    private FormEditText et_name, et_mobile, et_address, et_crop_name, et_area, et_search;
    private TextView tv_submit;
    private RecyclerView rv_farmer_list;
    private FarmerListAdapter farmerListAdapter;
    private FloatingActionButton fab_add;

    private LinearLayout linearLayout_list, linearLayout_add;

    private String formStatus = "";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_farmer_registaration, container, false);

        init();
        listener();


        Bundle bundle = getArguments();
        if (bundle != null) {
            formStatus = bundle.getString("formStatus");

            if (formStatus.equalsIgnoreCase("update")) {
                Toast.makeText(getActivity(), "update", Toast.LENGTH_SHORT).show();
                tv_submit.setText("Update");

                et_name.setText(FarmerListAdapter.farmerName);
                et_mobile.setText(FarmerListAdapter.mobNo);
                et_address.setText(FarmerListAdapter.address);
                et_crop_name.setText(FarmerListAdapter.crop);
                et_area.setText(FarmerListAdapter.areaCrop);
            } else {
                Toast.makeText(getActivity(), "new", Toast.LENGTH_SHORT).show();
            }
        }








      /*  if (FarmerListAdapter.formStatus.equalsIgnoreCase("Update")) {
            linearLayout_add.setVisibility(View.VISIBLE);
            linearLayout_list.setVisibility(View.GONE);
            fab_add.setVisibility(View.GONE);

            tv_submit.setText("Update");

            et_name.setText(FarmerListAdapter.farmerName);
            et_mobile.setText(FarmerListAdapter.mobNo);
            et_address.setText(FarmerListAdapter.address);
            et_crop_name.setText(FarmerListAdapter.crop);
            et_area.setText(FarmerListAdapter.areaCrop);
        }*/



      /*  et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                try {
                    searchRecharge(s.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });*/

        return view;

    }


   /* private void searchRecharge(String s) {

        searchFarmerList = new ArrayList<>();
        if (s.length() > 0) {
            for (int i = 0; i < farmerList.size(); i++)
                if ((farmerList.get(i).getName() + "" + farmerList.get(i).getMobileNo()).toLowerCase().contains(s.toLowerCase().trim())) {
                    searchFarmerList.add(farmerList.get(i));
                }

            if (searchFarmerList.size() < 1) {

            } else {

            }

        } else {
            searchFarmerList = new ArrayList<>();
            for (int i = 0; i < farmerList.size(); i++) {
                searchFarmerList.add(farmerList.get(i));
            }
        }

        try {


            FarmerListAdapter adapter = new FarmerListAdapter(getActivity(), searchFarmerList);
            rv_farmer_list.setLayoutManager(new LinearLayoutManager(getContext()));
            rv_farmer_list.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            rv_farmer_list.setHasFixedSize(true);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void setAdapter() {

        Call<List<FarmerRequestModel>> call = Api.getClient().getFarmerList(1);
        call.enqueue(new Callback<List<FarmerRequestModel>>() {
            @Override
            public void onResponse(Call<List<FarmerRequestModel>> call, Response<List<FarmerRequestModel>> response) {
                farmerList = response.body();

                FarmerListAdapter adapter = new FarmerListAdapter(getActivity(), response.body());
                rv_farmer_list.setLayoutManager(new LinearLayoutManager(getContext()));
                rv_farmer_list.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                rv_farmer_list.setHasFixedSize(true);
            }

            @Override
            public void onFailure(Call<List<FarmerRequestModel>> call, Throwable t) {

            }
        });
    }*/


    private void init() {
        et_name = view.findViewById(R.id.et_name);
        et_mobile = view.findViewById(R.id.et_mobile);
        et_address = view.findViewById(R.id.et_address);
        et_crop_name = view.findViewById(R.id.et_crop_name);
        et_area = view.findViewById(R.id.et_area);
        et_search = view.findViewById(R.id.et_search);
        tv_submit = view.findViewById(R.id.tv_submit);
        rv_farmer_list = view.findViewById(R.id.rv_farmer_list);
        fab_add = view.findViewById(R.id.fab_add);
        linearLayout_list = view.findViewById(R.id.linearLayout_list);
        linearLayout_add = view.findViewById(R.id.linearLayout_add);
    }

    private void listener() {

        fab_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                linearLayout_add.setVisibility(View.VISIBLE);
                linearLayout_list.setVisibility(View.GONE);
                fab_add.setVisibility(View.GONE);

            }
        });

        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_name.testValidity() && et_mobile.testValidity() && et_address.testValidity() && et_crop_name.testValidity() && et_area.testValidity()) {
                    FarmerRequestModel farmerRequestModel = new FarmerRequestModel();
                    farmerRequestModel.setName(et_name.getText().toString());
                    farmerRequestModel.setMobileNo(et_mobile.getText().toString());
                    farmerRequestModel.setAddress(et_address.getText().toString());
                    farmerRequestModel.setCrops(et_crop_name.getText().toString());
                    farmerRequestModel.setAreaAcre(et_area.getText().toString());
                    farmerRequestModel.setFarmerId(FarmerListAdapter.farmer_id);
                    farmerRequestModel.setEmployeeId(Integer.valueOf(MainPage.userId));

                    if (tv_submit.getText().toString().equalsIgnoreCase("Update")) {

                        Call<Boolean> call = Api.getClient().updateFarmer(farmerRequestModel);
                        call.enqueue(new Callback<Boolean>() {
                            @Override
                            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                                //Toast.makeText(getActivity(), "update" + response.body().booleanValue(), Toast.LENGTH_SHORT).show();
                                ((MainPage) getActivity()).removeCurrentFragmentAndMoveBack();
                            }

                            @Override
                            public void onFailure(Call<Boolean> call, Throwable t) {

                            }
                        });

                    } else {
                        Call<Boolean> call = Api.getClient().saveFarmer(farmerRequestModel);
                        call.enqueue(new Callback<Boolean>() {
                            @Override
                            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                                //Toast.makeText(getActivity(), "" + response.body().booleanValue(), Toast.LENGTH_SHORT).show();


                                ((MainPage) getActivity()).removeCurrentFragmentAndMoveBack();

                          /*  linearLayout_list.setVisibility(View.VISIBLE);
                            linearLayout_add.setVisibility(View.GONE);
                            fab_add.setVisibility(View.VISIBLE);*/
                            }

                            @Override
                            public void onFailure(Call<Boolean> call, Throwable t) {

                            }
                        });
                    }
                }


            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e("onStart", "called");
        if (formStatus.equalsIgnoreCase("update")) {
            MainPage.title.setText("Farmer Update");
        } else {
            MainPage.title.setText("Add Farmer");
        }
        ((MainPage) getActivity()).lockUnlockDrawer(1);
        if (DetectConnection.checkInternetConnection(getActivity())) {

        } else {
            DetectConnection.noInternetConnection(getActivity());
        }
    }
}