package com.salesmanagement.Receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.salesmanagement.Database.SRPMDatabase;
import com.salesmanagement.Model.TrackingResponse;
import com.salesmanagement.Retrofit.Api;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TrackingChecker extends BroadcastReceiver {

    //1 means data is synced and 0 means data is not synced
    public static final int NAME_SYNCED_WITH_SERVER = 1;
    public static final int NAME_NOT_SYNCED_WITH_SERVER = 0;
    //a broadcast to know weather the data is synced or not
    public static final String DATA_SAVED_BROADCAST = "com.salesmanagement.davasaved";
    //context and database helper object
    private Context context;
    private SRPMDatabase db;


    @Override
    public void onReceive(Context context, Intent intent) {

        this.context = context;
        db = new SRPMDatabase(context);

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        //if there is a network
        if (activeNetwork != null) {
            //if connected to wifi or mobile data plan
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI || activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {

                //getting all the unSynced names
                Cursor cursor = db.getUnSyncedLocation(NAME_NOT_SYNCED_WITH_SERVER);
                Log.e("CursorData", "" + cursor.getCount());
                if (cursor.moveToFirst()) {
                    do {
                        //calling the method to save the un-synced name to MySQL
                        saveLocation(cursor.getString(cursor.getColumnIndex("tracking_id")),
                                cursor.getString(cursor.getColumnIndex("latitude")),
                                cursor.getString(cursor.getColumnIndex("longitude")),
                                cursor.getString(cursor.getColumnIndex("userId")),
                                cursor.getString(cursor.getColumnIndex("tracking_date")),
                                cursor.getString(cursor.getColumnIndex("tracking_status"))
                        );
                    } while (cursor.moveToNext());
                }
            }
        }

    }

    private void saveLocation(String tracking_id, String latitude, String longitude, String userId, String tracking_date, String tracking_status) {

        TrackingResponse trackingResponse = new TrackingResponse();
        trackingResponse.setLatitude("" + latitude);
        trackingResponse.setLongitude("" + longitude);
        trackingResponse.setEmployeeId("" + userId);
        trackingResponse.setTrackingDate("" + tracking_date);

        Call<Boolean> call = Api.getClient().saveLocation(trackingResponse);
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {

                if (response.isSuccessful()) {
                    if (response.body().booleanValue() == true) {
                        Log.e("locationSaved", "" + response.body());
                        //sending the broadcast to refresh the list
                        db.updateLocationStatus(tracking_id, NAME_SYNCED_WITH_SERVER);
                        //sending the broadcast to refresh the list
                        context.sendBroadcast(new Intent(DATA_SAVED_BROADCAST));
                    } else if (response.body().booleanValue() == false) {
                        Log.e("locationFailedToSave", "" + response.body());
                    }
                } else {
                    Log.e("locationFailedToSave", "" + response.body());
                }

            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Log.e("locationServerError", "" + t.getMessage());
            }
        });


    }
}
