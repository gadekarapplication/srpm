package com.salesmanagement.popup;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.salesmanagement.Model.ProductResponse;
import com.salesmanagement.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PopupProductListAdapter extends RecyclerView.Adapter<PopupProductListAdapter.PopupProductListViewHolder>  {

    Context mContext;
    List<ProductResponse> productResponseList;
    private  IOnClickProduct onItemClickListener;
    public  PopupProductListAdapter(){

    }

    public PopupProductListAdapter(Context mContext, List<ProductResponse> productResponseList) {
        this.mContext = mContext;
        this.productResponseList = productResponseList;
    }

    @NonNull
    @Override
    public PopupProductListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(mContext).inflate(R.layout.recycler_view_pop_up_product_items,parent,false);

        return new PopupProductListViewHolder(view,onItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull PopupProductListViewHolder holder, int position) {
                holder.textViewProductName.setText(productResponseList.get(position).getProductName());
    }

    @Override
    public int getItemCount() {
        return productResponseList.size();
    }

    public void setProductName(List<ProductResponse> result) {
        this.productResponseList=result;
        notifyDataSetChanged();

    }

    class PopupProductListViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewProductName)
        TextView textViewProductName;
        private IOnClickProduct mListener;
        public PopupProductListViewHolder(@NonNull View itemView,final IOnClickProduct mListener) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener != null) {
                        mListener.onClickProduct(getAdapterPosition(),productResponseList.get(getAdapterPosition()).getProductName(),productResponseList.get(getAdapterPosition()).getProductId());
                    }
                }
            });
        }

    }

    public interface IOnClickProduct{
        void onClickProduct(int position,String productName,String productId);
    }
    public void setOnProductClickListener(IOnClickProduct listener) {
        this.onItemClickListener = listener;
    }

}
