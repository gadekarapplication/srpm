package com.salesmanagement.popup;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;

import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.salesmanagement.Model.ProductResponse;
import com.salesmanagement.R;

import java.util.ArrayList;
import java.util.List;

public class PopupProductList extends PopupWindow implements PopupWindow.OnDismissListener,PopupProductListAdapter.IOnClickProduct{

     PopupWindow popupWindow;
     RecyclerView rvSearchableProduct;
     ImageView imageViewClosePL;
     private SearchView searchViewProduct;
     PopupProductListAdapter popupProductListAdapter;
     List<ProductResponse> productResponseList;
     private boolean isClickItem;
     private CallBack mCallBack;
     private int position;
     private int prePosition;
     public PopupProductList(List<ProductResponse> productResponseList){
          this.productResponseList=productResponseList;
     }

     public  void showPopup(Context mContext){
          View contentView = LayoutInflater.from(mContext).inflate(R.layout.custom_dailog_product, null);
          popupWindow=new PopupWindow(mContext.getApplicationContext());
          popupWindow.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
          popupWindow.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
          popupWindow.setContentView(contentView);
          popupWindow = new PopupWindow(contentView, popupWindow.getWidth(), popupWindow.getHeight(),true);
          popupWindow.showAtLocation(contentView, Gravity.CENTER, 0, 0);
          popupWindow.setBackgroundDrawable(new ColorDrawable(0x00000000));
          popupWindow.setOutsideTouchable(true);

          imageViewClosePL=contentView.findViewById(R.id.imageViewClosePL);
          imageViewClosePL.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                    popupWindow.dismiss();
               }
          });

          rvSearchableProduct=contentView.findViewById(R.id.rvSearchableProduct);
          searchViewProduct=contentView.findViewById(R.id.searchVProduct);

          searchViewProduct.onActionViewExpanded();
          //searchViewHospital.setIconified(true);
          searchViewProduct.setIconifiedByDefault(true);
          searchViewProduct.setQueryHint("Search by name product");
          searchViewProduct.clearFocus();
          EditText searchEditText = (EditText) searchViewProduct.findViewById(R.id.search_src_text);
          searchEditText.getText();
          //searchEditText.setTextSize(TypedValue.COMPLEX_UNIT_DIP, context.getResources().getDimensionPixelSize(R.dimen.text_search_hint));


          searchEditText.setTextColor(mContext.getResources().getColor(R.color.black));
          searchEditText.setHintTextColor(mContext.getResources().getColor(R.color.colorGreyOne));


          ImageView searchMagIcon = (ImageView)searchViewProduct.findViewById(R.id.search_close_btn);
          searchMagIcon.setImageResource(R.drawable.close_icon);

          //search_close_btn
          searchViewProduct.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
               @Override
               public boolean onQueryTextSubmit(String query) {
                    return false;
               }

               @Override
               public boolean onQueryTextChange(String query) {
                    processQuery(query);
                    return true;
               }
          });

          rvSearchableProduct.setLayoutManager(new LinearLayoutManager(mContext));
          popupProductListAdapter=new PopupProductListAdapter(mContext,productResponseList);
          popupProductListAdapter.setOnProductClickListener(this);
          rvSearchableProduct.setAdapter(popupProductListAdapter);

     }


     @Override
     public void onDismiss() {
          if (!isClickItem) {
               mCallBack.callback("-1","-1");
          }
     }

     @Override
     public void onClickProduct(int position, String productName, String productId) {
          this.position=position;
          changePos(true,productName,productId);
     }
     private void changePos(boolean isCloseWindow, final String dist_name, final String dist_id) {

          if (position != prePosition && prePosition != 0) {
               // items.get(prePosition).setActive(false);
               popupProductListAdapter.notifyItemChanged(prePosition);
          }

          if (position > 0) {
               //items.get(position).setActive(true);
               popupProductListAdapter.notifyItemChanged(position);
          }
          if (isCloseWindow) {

               (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {

                         mCallBack.callback(dist_name,dist_id);
                         destroyPopWindow();
                    }
               }, 400);
          }
     }

     public interface CallBack {
          void callback(String value, String Id);
     }
     public void setCallBack(PopupProductList.CallBack callBack) {
          mCallBack = callBack;
     }
     private void destroyPopWindow() {
          if (popupWindow != null) {
               popupWindow.dismiss();
               popupWindow = null;
          }
     }
     private void processQuery(String query) {
          List<ProductResponse> result=new ArrayList<>();
          for (ProductResponse productResponse:productResponseList){
               if(productResponse.getProductName().toLowerCase().contains(query.toLowerCase())){
                    result.add(productResponse);
               }
          }
          popupProductListAdapter.setProductName(result);

     }
}
