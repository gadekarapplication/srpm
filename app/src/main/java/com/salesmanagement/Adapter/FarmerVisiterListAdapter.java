package com.salesmanagement.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Fragment.FarmerVisitDetailFragment;
import com.salesmanagement.Model.FarmerVisiterRequest;
import com.salesmanagement.R;

import java.util.List;

import butterknife.ButterKnife;

public class FarmerVisiterListAdapter extends RecyclerView.Adapter<FarmerVisiterListAdapter.MyViewHolder> {


    public static int farmer_id;
    Context context;
    List<FarmerVisiterRequest> farmerVisiterList;

    public FarmerVisiterListAdapter(Context context, List<FarmerVisiterRequest> farmerVisiterList) {
        this.context = context;
        this.farmerVisiterList = farmerVisiterList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.farmer_visiter_list, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {


        holder.tv_area_name.setText(farmerVisiterList.get(position).getArea());
        holder.tv_crop_name.setText(farmerVisiterList.get(position).getCrop());
        holder.tv_date.setText(farmerVisiterList.get(position).getDate());
        holder.tv_place.setText(farmerVisiterList.get(position).getDemoPlace());
        holder.tv_district.setText(farmerVisiterList.get(position).getDistrict());
        holder.tv_remark.setText(farmerVisiterList.get(position).getRemark());
        holder.tv_irrigation.setText(farmerVisiterList.get(position).getIrrigation());
        holder.tv_medicine.setText(farmerVisiterList.get(position).getMedicines());


        holder.tv_view_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                FarmerVisitDetailFragment farmerVisitDetailFragment = new FarmerVisitDetailFragment();
                Bundle bundle = new Bundle();

                bundle.putInt("farmer_visit_id", farmerVisiterList.get(position).getFarmerVisitId());

                farmerVisitDetailFragment.setArguments(bundle);
                ((MainPage) context).loadFragment(farmerVisitDetailFragment, true);


            }
        });


    }

    @Override
    public int getItemCount() {
        return farmerVisiterList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tv_area_name, tv_crop_name, tv_date, tv_place, tv_district, tv_remark, tv_irrigation, tv_medicine, tv_view_detail;

        private ImageView iv_update;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            tv_area_name = itemView.findViewById(R.id.tv_area_name);
            tv_crop_name = itemView.findViewById(R.id.tv_crop_name);
            tv_date = itemView.findViewById(R.id.tv_date);
            tv_place = itemView.findViewById(R.id.tv_place);
            tv_district = itemView.findViewById(R.id.tv_district);
            tv_remark = itemView.findViewById(R.id.tv_remark);
            tv_irrigation = itemView.findViewById(R.id.tv_irrigation);
            tv_medicine = itemView.findViewById(R.id.tv_medicine);
            tv_view_detail = itemView.findViewById(R.id.tv_view_detail);
            iv_update = itemView.findViewById(R.id.iv_update);
        }
    }
}
