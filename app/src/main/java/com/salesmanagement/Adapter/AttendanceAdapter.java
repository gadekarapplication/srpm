package com.salesmanagement.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.salesmanagement.Model.MonthlyAttendanceResponseModel;
import com.salesmanagement.R;

import java.util.List;

public class AttendanceAdapter extends RecyclerView.Adapter<AttendanceAdapter.MyViewHolder> {

    private Context context;
    private List<MonthlyAttendanceResponseModel> monthlyAttendanceList;


    public AttendanceAdapter(Context context, List<MonthlyAttendanceResponseModel> monthlyAttendanceList) {
        this.context = context;
        this.monthlyAttendanceList = monthlyAttendanceList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.attendance_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {


        if (monthlyAttendanceList.get(position).getTitle().equalsIgnoreCase("null")) {
            holder.tv_date.setText("" + monthlyAttendanceList.get(position).getDay());
            holder.tv_date.setBackground(context.getDrawable(R.drawable.disable_back_color));
        } else if (monthlyAttendanceList.get(position).getTitle().equalsIgnoreCase("Absent")) {
            holder.tv_date.setText("" + monthlyAttendanceList.get(position).getDay());
            holder.tv_date.setBackground(context.getDrawable(R.drawable.absent_color));
        } else {
            holder.tv_date.setText("" + monthlyAttendanceList.get(position).getDay());
            holder.tv_date.setBackground(context.getDrawable(R.drawable.present_back_color));

        }


    }

    @Override
    public int getItemCount() {
        return monthlyAttendanceList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_date;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_date = itemView.findViewById(R.id.tv_date);


        }
    }
}
