package com.salesmanagement.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Fragment.FarmerRegistarationFragment;
import com.salesmanagement.Fragment.FarmerVisiterListFragment;
import com.salesmanagement.Model.FarmerRequestModel;
import com.salesmanagement.R;

import java.util.List;

import butterknife.ButterKnife;

public class FarmerListAdapter extends RecyclerView.Adapter<FarmerListAdapter.MyViewHolder> {

    public static String formStatus = "New", farmerName = "", mobNo, address = "", areaCrop = "", crop = "";

    public static int farmer_id;
    Context context;
    List<FarmerRequestModel> farmerRequestModelList;

    public FarmerListAdapter(Context context, List<FarmerRequestModel> farmerRequestModels) {
        this.context = context;
        this.farmerRequestModelList = farmerRequestModels;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.farmer_list_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.tv_farmer_name.setText(farmerRequestModelList.get(position).getName());
        holder.tv_mobile_no.setText(farmerRequestModelList.get(position).getMobileNo());
        holder.tv_address.setText(farmerRequestModelList.get(position).getAddress());
        holder.tv_area_crop.setText(farmerRequestModelList.get(position).getAreaAcre());
        holder.tv_crop.setText(farmerRequestModelList.get(position).getCrops());

        holder.iv_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                formStatus = "Update";



                farmerName = farmerRequestModelList.get(position).getName();
                mobNo = farmerRequestModelList.get(position).getMobileNo();
                address = farmerRequestModelList.get(position).getAddress();
                areaCrop = farmerRequestModelList.get(position).getAreaAcre();
                crop = farmerRequestModelList.get(position).getCrops();

                FarmerRegistarationFragment farmerRegistarationFragment = new FarmerRegistarationFragment();
                Bundle bundle = new Bundle();

                bundle.putString("formStatus", "update");

                farmerRegistarationFragment.setArguments(bundle);
                ((MainPage) context).loadFragment(farmerRegistarationFragment, true);


            }
        });

        holder.iv_visit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                farmer_id = farmerRequestModelList.get(position).getFarmerId();
                ((MainPage) context).loadFragment(new FarmerVisiterListFragment(), true);
            }
        });


    }

    @Override
    public int getItemCount() {
        return farmerRequestModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tv_farmer_name, tv_mobile_no, tv_address, tv_area_crop, tv_crop;

        private ImageView iv_update, iv_visit;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            tv_farmer_name = itemView.findViewById(R.id.tv_farmer_name);
            tv_mobile_no = itemView.findViewById(R.id.tv_mobile_no);
            tv_address = itemView.findViewById(R.id.tv_address);
            tv_area_crop = itemView.findViewById(R.id.tv_area_crop);
            tv_crop = itemView.findViewById(R.id.tv_crop);
            iv_update = itemView.findViewById(R.id.iv_update);
            iv_visit = itemView.findViewById(R.id.iv_visit);
        }
    }
}
