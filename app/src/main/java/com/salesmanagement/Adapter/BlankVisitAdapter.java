package com.salesmanagement.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.salesmanagement.Model.BlankVisitResponse;
import com.salesmanagement.R;

import java.util.Date;
import java.util.List;

import butterknife.BindViews;
import butterknife.ButterKnife;

public class BlankVisitAdapter extends RecyclerView.Adapter<BlankVisitAdapter.BlankVisitViewHolder>{
    Context mContext;
    List<BlankVisitResponse> blankVisitResponseList;

    public BlankVisitAdapter(Context mContext, List<BlankVisitResponse> blankVisitResponseList) {
        this.mContext = mContext;
        this.blankVisitResponseList = blankVisitResponseList;
    }

    @NonNull
    @Override
    public BlankVisitViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(mContext).inflate(R.layout.recycler_view_blank_visit_item,parent,false);
        return new BlankVisitViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BlankVisitViewHolder holder, int position) {
            try {
                holder.textViewList.get(4).setText(blankVisitResponseList.get(position).getDealerFullName());
                holder.textViewList.get(0).setText(blankVisitResponseList.get(position).getBlankVisitSubject());
                holder.textViewList.get(1).setText(blankVisitResponseList.get(position).getBlankVisitDescription());
                holder.textViewList.get(2).setText(blankVisitResponseList.get(position).getBlankVisitLatitude()+","+blankVisitResponseList.get(position).getBlankVisitLongitude());
                //holder.textViewList.get(0).setText(blankVisitResponseList.get(position).getBlankVisitSubject());
                String longDate= String.valueOf(blankVisitResponseList.get(position).getBlankVisitDate());
                long millisecond = Long.parseLong(longDate);

                String dateString = android.text.format.DateFormat.format("dd-MM-yyyy hh:mm:ss a", new Date(millisecond)).toString();


                holder.textViewList.get(3).setText(dateString);
            }catch (Exception e){
                e.printStackTrace();
            }
    }

    @Override
    public int getItemCount() {
        return blankVisitResponseList.size();
    }

    public class BlankVisitViewHolder extends RecyclerView.ViewHolder {
        @BindViews({R.id.tvBVSubjectValue,R.id.tvBVDescriptionValue,
                R.id.tvLatLongValue,R.id.tvDateBlankVisitValue,R.id.tvBVDealerValue})
        List<TextView> textViewList;
        public BlankVisitViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
