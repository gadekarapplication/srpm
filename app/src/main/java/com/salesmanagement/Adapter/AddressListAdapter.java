package com.salesmanagement.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Fragment.AllDealerAddress;
import com.salesmanagement.Fragment.DealerAddressFragment;
import com.salesmanagement.Fragment.PlaceOrderDetails;
import com.salesmanagement.Model.DealerAddressResponseModel;
import com.salesmanagement.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

public class AddressListAdapter extends RecyclerView.Adapter<AddressListAdapter.MyViewHolder> {

    Context context;
    private List<DealerAddressResponseModel> addressResponseModelList = new ArrayList<>();

    public AddressListAdapter(List<DealerAddressResponseModel> addressResponseModelList, Context context) {

        this.context = context;
        this.addressResponseModelList = addressResponseModelList;

    }


    @NonNull
    @Override
    public AddressListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_view_dealer_addres, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AddressListAdapter.MyViewHolder holder, int position) {

        try {
            holder.textViewList.get(0).setText(addressResponseModelList.get(position).getAddressType());
            holder.textViewList.get(1).setText(addressResponseModelList.get(position).getFullAddress());
            holder.textViewList.get(2).setText(addressResponseModelList.get(position).getFlatNo());
            holder.textViewList.get(3).setText(addressResponseModelList.get(position).getArea());
            holder.textViewList.get(4).setText(addressResponseModelList.get(position).getLandmark());
            holder.ivViewEditDealerAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    PlaceOrderDetails placeOrderDetails = new PlaceOrderDetails();
                    Bundle bundle = new Bundle();
                    bundle.putString("dealerId", ""+ AllDealerAddress.dealerId);
                    bundle.putString("companyId", ""+ AllDealerAddress.companyId);
                    bundle.putString("companyAddress", ""+ addressResponseModelList.get(position).getFullAddress());
                    placeOrderDetails.setArguments(bundle);
                    ((MainPage) context).loadFragment(placeOrderDetails, true);

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return addressResponseModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindViews({R.id.tvDealerAddressTypeValue, R.id.tvFullAddressValue, R.id.tvFlatNumberValue, R.id.tvAreaValue, R.id.tvLandMarkValue})
        List<TextView> textViewList;
        @BindView(R.id.ivViewEditDealerAddress)
        ImageView ivViewEditDealerAddress;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
