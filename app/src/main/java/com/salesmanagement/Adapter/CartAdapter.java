package com.salesmanagement.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputEditText;
import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Extra.DetectConnection;
import com.salesmanagement.Fragment.MyCart;
import com.salesmanagement.Model.CartRequest;
import com.salesmanagement.Model.CartResponse;
import com.salesmanagement.R;
import com.salesmanagement.Retrofit.Api;

import java.util.List;

import butterknife.BindViews;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MyViewHolder> {

    Context context;
    List<CartResponse> cartResponseList;
    CartResponse cartResponse;
    String[] discountList = {"% (Percentage)", "₹ (Amount)"};
    String discountType,strFragment;
    IDeleteCartItem iDeleteCartItem;

    public CartAdapter(Context context, List<CartResponse> cartResponseList,IDeleteCartItem iDeleteCartItem,String strFragment) {
        this.context = context;
        this.cartResponseList = cartResponseList;
        this.iDeleteCartItem= iDeleteCartItem;
        this.strFragment=strFragment;
    }

    public CartAdapter(Context context, List<CartResponse> cartResponseList) {
        this.context = context;
        this.cartResponseList = cartResponseList;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_product_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CartAdapter.MyViewHolder holder, int position) {

        cartResponse = cartResponseList.get(position);

        try {

            holder.textViews.get(0).setText("Product Name: " + cartResponseList.get(position).getProductName());
            holder.textViews.get(1).setText("Product Size: " + cartResponseList.get(position).getSize() + "/" + cartResponseList.get(position).getUnit());
            holder.textViews.get(2).setText("Product Quantity: " + cartResponseList.get(position).getQty());
            holder.textViews.get(3).setText("Discount Amount: " + cartResponseList.get(position).getDiscountAmount());
            holder.textViews.get(4).setText("Final Amount: " + cartResponseList.get(position).getFinalAmount());


            holder.textViews.get(5).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
                    dialog.setContentView(R.layout.update_cart_details);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    dialog.setCancelable(false);
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    lp.copyFrom(dialog.getWindow().getAttributes());
                    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    lp.gravity = Gravity.CENTER;

                    dialog.getWindow().setAttributes(lp);
                    //Spinner discountTypeSpinner = (Spinner) dialog.findViewById(R.id.discountTypeSpinner);
                    TextInputEditText productName = (TextInputEditText) dialog.findViewById(R.id.productName);
                    TextInputEditText productSize = (TextInputEditText) dialog.findViewById(R.id.productSize);
                    TextInputEditText productPriceUpdate=   dialog.findViewById(R.id.productPriceUpdate);
                    TextInputEditText productQuantity = (TextInputEditText) dialog.findViewById(R.id.productQuantity);
                    TextInputEditText productGSTUpdate = (TextInputEditText) dialog.findViewById(R.id.productGSTUpdate);
                    TextInputEditText productAmount = (TextInputEditText) dialog.findViewById(R.id.productAmount);
                    TextInputEditText productDiscountInPercent= dialog.findViewById(R.id.productDiscountInPercent);
                    TextInputEditText productDiscountInRS = dialog.findViewById(R.id.productDiscountInRS);

                    try {

                        productName.setText("" + cartResponseList.get(position).getProductName());
                        productSize.setText("" + cartResponseList.get(position).getSize() + "/" + cartResponseList.get(position).getUnit());
                        productPriceUpdate.setText(""+cartResponseList.get(position).getProductAmount());
                        productQuantity.setText("" + cartResponseList.get(position).getQty());
                        productDiscountInPercent.setText(""+cartResponseList.get(position).getDiscountPercentage());
                        productGSTUpdate.setText("" + cartResponseList.get(position).getGstPercentage());
                        productDiscountInRS.setText("" + cartResponseList.get(position).getDiscountAmount());
                        productAmount.setText("" + cartResponseList.get(position).getFinalAmount());

                        productDiscountInPercent.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                            }

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {

                            }

                            @Override
                            public void afterTextChanged(Editable s) {
                                try {
                                    productDiscountInPercent.setSelection(productDiscountInPercent.getText().toString().length());
                                    if (productDiscountInPercent.getText().toString().length() > 0) {
                                        // if (discountType.equalsIgnoreCase("% (Percentage)")) {
                                        double subAmount = Double.parseDouble(cartResponseList.get(position).getProductAmount()) * Double.parseDouble(productQuantity.getText().toString());
                                        double discountAmount = Double.parseDouble(productDiscountInPercent.getText().toString().trim());
                                        double gstAmount = Double.parseDouble(cartResponseList.get(position).getGstPercentage());
                                        double discount = subAmount * (discountAmount / 100f);

                                        productDiscountInRS.setText(" "+discount);
                                        discount = subAmount - discount;
                                        discount = discount + (discount * (gstAmount / 100f));
                                        String discountTotal = String.format("%.2f", (discount));
                                        productAmount.setText("" + discountTotal);





                                    } else {
                                        try {
                                            double subAmount = Double.parseDouble(cartResponseList.get(position).getProductAmount()) * Double.parseDouble(productQuantity.getText().toString());
                                            String discountTotal = String.format("%.2f", (subAmount));
                                            productAmount.setText("" + discountTotal);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        productQuantity.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                            }

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {

                            }

                            @Override
                            public void afterTextChanged(Editable s) {
                                try {
                                    if(productQuantity.getText().toString().length()>0){
                                        int localQuantity= Integer.parseInt(productQuantity.getText().toString());
                                        if(!productQuantity.getText().toString().equals("")){
                                            if(productDiscountInPercent.getText().toString().length()>0){
                                                double subAmount = Double.parseDouble(cartResponseList.get(position).getProductAmount()) * Double.parseDouble(productQuantity.getText().toString());
                                                double discountAmount = Double.parseDouble(productDiscountInPercent.getText().toString().trim());
                                                double gstAmount = Double.parseDouble(cartResponseList.get(position).getGstPercentage());
                                                double discount = subAmount * (discountAmount / 100f);

                                                productDiscountInRS.setText(" "+discount);
                                                discount = subAmount - discount;
                                                discount = discount + (discount * (gstAmount / 100f));
                                                String discountTotal = String.format("%.2f", (discount));
                                                productAmount.setText("" + discountTotal);
                                            }
                                            else {
                                                try {
                                                    double subAmount = Double.parseDouble(cartResponseList.get(position).getProductAmount()) * Double.parseDouble(productQuantity.getText().toString());
                                                    String discountTotal = String.format("%.2f", (subAmount));
                                                    productAmount.setText("" + discountTotal);
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }else {
                                            productQuantity.setError("Quantity Should be at least one");
                                        }

                                    }
                                    else {
                                        productQuantity.setError("Quantity Should be at least one");
                                    }
                                }catch (Exception e){
                                    e.printStackTrace();
                                }


                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    /*try {
                        final ArrayAdapter adapter = new ArrayAdapter(context, android.R.layout.simple_spinner_item, discountList);
                        adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
                        discountTypeSpinner.setAdapter(adapter);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }*/

                   /* discountTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                            discountType = discountList[position];
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });*/

                    dialog.findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    dialog.findViewById(R.id.updateCart).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if (DetectConnection.checkInternetConnection(context)) {
                                CartRequest cartRequest=new CartRequest();
                                System.out.println("CartId:"+cartResponseList.get(position).getCartId());
                                cartRequest.setCartId(cartResponseList.get(position).getCartId());
                                cartRequest.setDealershipId("" + cartResponseList.get(position).getDealershipId());
                                cartRequest.setCompId(cartResponseList.get(position).getCompId());
                                cartRequest.setEmployeeId(MainPage.userId);
                                cartRequest.setProductId(cartResponseList.get(position).getProductId());
                                cartRequest.setSizeWiseProductId(cartResponseList.get(position).getSizeWiseProductId());
                                cartRequest.setGstPercentage(cartResponseList.get(position).getGstPercentage());
                                /*double subAmount = Double.parseDouble(textInputEditTexts.get(0).getText().toString());
                                double discountAmount = Double.parseDouble(textInputEditTexts.get(6).getText().toString().trim());
                                double gstAmountPercentage = Double.parseDouble(textInputEditTexts.get(5).getText().toString().trim());
                                double discount = subAmount * (discountAmount / 100f);
                                //System.out.println("Discount:"+discount);
                                double totalDisAmount=(discount)*(Integer.parseInt(productQuantity));
                                //System.out.println("Total Dis Amount:"+totalDisAmount);
                                discount = subAmount - discount;
                                //discount = discount + (discount * (gstAmountPercentage / 100f));
                                //String discountTotal = String.format("%.2f", (discount));
                                double gstAmount = (discount) * (gstAmountPercentage / 100f);
                                double totalGSTAmount=(gstAmount)*(Integer.parseInt(productQuantity));
                                //System.out.println("Total GST Amount: "+totalGSTAmount);*/
                                cartRequest.setGstAmount(cartResponseList.get(position).getGstAmount());
                                cartRequest.setDiscountAmount( ""+productDiscountInRS.getText().toString());
                                double discountAmount = Double.parseDouble(productDiscountInPercent.getText().toString().trim());
                                cartRequest.setDiscountPercentage(String.format("%.2f",discountAmount));

                                cartRequest.setQty("" + productQuantity.getText().toString());
                                cartRequest.setFinalAmount("" + productAmount.getText().toString());


                                Call<Boolean> call = Api.getClient().updateCart(cartRequest);
                                call.enqueue(new Callback<Boolean>() {
                                    @Override
                                    public void onResponse(Call<Boolean> call, Response<Boolean> response) {

                                        if (response.isSuccessful()) {
                                            if (response.body().booleanValue() == true) {
                                                Toast.makeText(context, "Update Done", Toast.LENGTH_SHORT).show();
                                                dialog.dismiss();
                                                 //((MainPage) context).removeCurrentFragmentAndMoveBack();
                                                MyCart myCart = new MyCart();
                                                Bundle bundle = new Bundle();
                                                bundle.putString("dealerId", "" + cartResponseList.get(position).getDealershipId());
                                                bundle.putString("companyId", "" + cartResponseList.get(position).getCompId());
                                                myCart.setArguments(bundle);
                                                ((MainPage) context).loadFragment(myCart, true);
                                            } else if (response.body().booleanValue() == false) {
                                                dialog.dismiss();
                                                Toast.makeText(context, "Update Failed", Toast.LENGTH_SHORT).show();
                                            }
                                        } else {
                                            dialog.dismiss();
                                            Toast.makeText(context, "Update Failed", Toast.LENGTH_SHORT).show();
                                        }

                                    }

                                    @Override
                                    public void onFailure(Call<Boolean> call, Throwable t) {
                                        Log.e("updateCartError", "" + t.getMessage());
                                        dialog.dismiss();
                                        Toast.makeText(context, "Update Failed", Toast.LENGTH_SHORT).show();
                                    }
                                });

                            } else {
                                DetectConnection.noInternetConnection(context);
                            }

                        }
                    });


                    dialog.show();


                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        holder.textViews.get(6).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    cartResponseList.get(position).getCartId();
                System.out.println("Delete Cart Id:"+cartResponseList.get(position).getCartId());

                iDeleteCartItem.deleteCartItem(position,Integer.parseInt(cartResponseList.get(position).getCartId()));
                removeAt(position);
            }
        });
        if(strFragment.equals("PlaceOrder")){
            holder.textViews.get(6).setVisibility(View.GONE);
            holder.textViews.get(5).setVisibility(View.GONE);
        }
        else {
            holder.textViews.get(6).setVisibility(View.VISIBLE);
            holder.textViews.get(5).setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return cartResponseList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindViews({R.id.productName, R.id.productSize, R.id.productQuantity,
                R.id.productDiscountAmount, R.id.productAmount, R.id.update, R.id.delete})
        List<TextView> textViews;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public interface IDeleteCartItem{
               void  deleteCartItem(int position, int cartId);
    }
    public void removeAt(int position) {
        cartResponseList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, cartResponseList.size());
    }
}
