package com.salesmanagement.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.salesmanagement.Model.CollectionsResponse;
import com.salesmanagement.R;

import java.util.List;

import butterknife.BindViews;
import butterknife.ButterKnife;

public class CollectionsAdapter extends RecyclerView.Adapter<CollectionsAdapter.CollectionsViewHolder> {
    private List<CollectionsResponse> collectionsResponseList;
    private Context mContext;

    public CollectionsAdapter(List<CollectionsResponse> collectionsResponseList, Context mContext) {
        this.collectionsResponseList = collectionsResponseList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public CollectionsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.recycler_view_collections_item, parent, false);
        return new CollectionsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CollectionsViewHolder holder, int position) {
        try {
            holder.textViewList.get(0).setText(collectionsResponseList.get(position).getDealerFullName());
            holder.textViewList.get(1).setText(String.valueOf(collectionsResponseList.get(position).getTotalAmount()));
            holder.textViewList.get(2).setText(collectionsResponseList.get(position).getTransactionType());
            holder.textViewList.get(3).setText(collectionsResponseList.get(position).getTotalTransactionSaleDate());
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return collectionsResponseList.size();
    }

    public class CollectionsViewHolder extends RecyclerView.ViewHolder {
        @BindViews({R.id.tvDealerNameValue, R.id.tvAmountValue, R.id.tvTransactionTypeValue, R.id.tvDateValue})
        List<TextView> textViewList;

        public CollectionsViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
