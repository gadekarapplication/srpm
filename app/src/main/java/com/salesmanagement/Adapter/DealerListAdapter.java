package com.salesmanagement.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Fragment.BlankVisitFragment;
import com.salesmanagement.Fragment.DealerAddressListFragment;
import com.salesmanagement.Fragment.DealerRegistration;
import com.salesmanagement.Model.BlankVisitRequest;
import com.salesmanagement.Model.DealerListResponse;
import com.salesmanagement.Model.DealerResponse;
import com.salesmanagement.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

public class DealerListAdapter extends RecyclerView.Adapter<DealerListAdapter.DealerListViewHolder> {
    List<DealerResponse> dealerListResponseList;
    Context mContext;
    Bundle bundle;

    public DealerListAdapter(List<DealerResponse> dealerListResponseList, Context mContext) {
        this.dealerListResponseList = dealerListResponseList;
        this.mContext = mContext;
    }

    public DealerListAdapter() {
    }

    @NonNull
    @Override
    public DealerListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.recycler_view_active_dealer_item, parent, false);
        return new DealerListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DealerListViewHolder holder, int position) {
        try {
            holder.textViewList.get(0).setText(dealerListResponseList.get(position).getDealerFullName());
            holder.textViewList.get(1).setText(dealerListResponseList.get(position).getFirmName());
            holder.textViewList.get(2).setText(dealerListResponseList.get(position).getMobileNo());
            holder.textViewList.get(3).setText(dealerListResponseList.get(position).getDealerEmail());
            holder.ivViewAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DealerAddressListFragment dealerAddressListFragment = new DealerAddressListFragment();
                    bundle = new Bundle();

                    bundle.putInt("DEALER_ID", +Integer.parseInt(dealerListResponseList.get(position).getDealershipId()));
                    dealerAddressListFragment.setArguments(bundle);


                    ((MainPage) mContext).loadFragment(dealerAddressListFragment, true);
                }
            });
            holder.ivViewEditDealer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DealerRegistration dealerRegistration = new DealerRegistration();
                    bundle = new Bundle();
                    bundle.putParcelable("DEALER_DETAILS", dealerListResponseList.get(holder.getAdapterPosition()));
                    dealerRegistration.setArguments(bundle);
                    ((MainPage) mContext).loadFragment(dealerRegistration, true);
                }
            });

            holder.ivBlankVisit.setOnClickListener(view->{
                BlankVisitFragment blankVisitFragment = new BlankVisitFragment();
                bundle = new Bundle();

                bundle.putInt("DEALER_ID", +Integer.parseInt(dealerListResponseList.get(position).getDealershipId()));
                blankVisitFragment.setArguments(bundle);
                ((MainPage) mContext).loadFragment(blankVisitFragment, true);
            });

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return dealerListResponseList.size();
    }

    public void setCriminalName(ArrayList<DealerResponse> result) {
        this.dealerListResponseList = result;
        notifyDataSetChanged();
    }

    public class DealerListViewHolder extends RecyclerView.ViewHolder {
        @BindViews({R.id.tvDealerNameValue, R.id.tvFirmNameValue, R.id.tvMobileNumberValue, R.id.tvEmailValue})
        List<TextView> textViewList;
        @BindView(R.id.ivViewAddress)
        ImageView ivViewAddress;
        @BindView(R.id.ivViewEditDealer)
        ImageView ivViewEditDealer;
        @BindView(R.id.ivBlankVisit)
        ImageView ivBlankVisit;

        public DealerListViewHolder(@NonNull View itemView) {

            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
