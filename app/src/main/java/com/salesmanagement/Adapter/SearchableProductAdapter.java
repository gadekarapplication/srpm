package com.salesmanagement.Adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.salesmanagement.Model.ProductResponse;
import com.salesmanagement.R;

import java.util.ArrayList;
import java.util.List;

import gr.escsoft.michaelprimez.searchablespinner.interfaces.ISpinnerSelectedView;

public class SearchableProductAdapter  extends ArrayAdapter<ProductResponse> implements Filterable, ISpinnerSelectedView {
    private Context mContext;
    private List<ProductResponse> mBackupStrings;
    private List<ProductResponse> mStrings;
    private StringFilter mStringFilter = new StringFilter();

    public SearchableProductAdapter(@NonNull Context context, List<ProductResponse> productResponseList) {
        super(context, R.layout.product_list_view);
        mContext = context;
        mStrings = productResponseList;
        mBackupStrings = productResponseList;

    }
    @Override
    public int getCount() {
        return mStrings == null ? 0 : mStrings.size() + 1;
    }
    @Override
    public ProductResponse getItem(int position) {
        if (mStrings != null && position > 0) {
            ProductResponse productResponse = new ProductResponse();
            productResponse.setProductName(mStrings.get(position - 1).getProductName());

            return productResponse;
        }
        else
            return null;
    }
    @Override
    public long getItemId(int position) {
        if (mStrings == null && position > 0)
            return mStrings.get(position).hashCode();
        else
            return -1;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = null;
        if (position == 0) {
            view = getNoSelectionView();
        } else {
            view = View.inflate(mContext, R.layout.product_list_view, null);

            TextView dispalyName = (TextView) view.findViewById(R.id.TxtVw_DisplayName);

            dispalyName.setText(mStrings.get(position-1).getProductName());
        }
        return view;
    }

    @Override
    public View getNoSelectionView() {
        View view = View.inflate(mContext, R.layout.no_item_selection, null);
        return view;
    }

    @Override
    public View getSelectedView(int position) {
        View view = null;
        if (position == 0) {
            view = getNoSelectionView();
        } else {
            view = View.inflate(mContext, R.layout.product_list_view, null);

            TextView dispalyName = (TextView) view.findViewById(R.id.TxtVw_DisplayName);

            dispalyName.setText(mStrings.get(position-1).getProductName());
        }
        return view;
    }


    public class StringFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            final FilterResults filterResults = new FilterResults();
            if (TextUtils.isEmpty(constraint)) {
                filterResults.count = mBackupStrings.size();
                filterResults.values = mBackupStrings;
                return filterResults;
            }
            final List<ProductResponse> filterStrings = new ArrayList<>();
            for (ProductResponse productResponse : mBackupStrings) {
                if (productResponse.getProductName().toLowerCase().contains(constraint)) {
                    filterStrings.add(productResponse);
                }
            }
            filterResults.count = filterStrings.size();
            filterResults.values = filterStrings;
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mStrings = (ArrayList) results.values;
            notifyDataSetChanged();
        }
    }
    private class ItemView {

        public TextView mTextView;
    }

    public enum ItemViewType {
        ITEM, NO_SELECTION_ITEM;
    }
}
