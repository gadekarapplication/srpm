package com.salesmanagement.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Fragment.DealerAddressFragment;
import com.salesmanagement.Model.DealerAddressResponseModel;
import com.salesmanagement.R;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

public class DealerAddressListAdapter extends RecyclerView.Adapter<DealerAddressListAdapter.DealerAddressViewHolder> {
    List<DealerAddressResponseModel> addressResponseModelList;
    Context mContext;

    public DealerAddressListAdapter(List<DealerAddressResponseModel> addressResponseModelList, Context mContext) {
        this.addressResponseModelList = addressResponseModelList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public DealerAddressViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.recycler_view_dealer_addres, parent, false);
        return new DealerAddressViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DealerAddressViewHolder holder, int position) {
        try {
            holder.textViewList.get(0).setText(addressResponseModelList.get(position).getAddressType());
            holder.textViewList.get(1).setText(addressResponseModelList.get(position).getFullAddress());
            holder.textViewList.get(2).setText(addressResponseModelList.get(position).getFlatNo());
            holder.textViewList.get(3).setText(addressResponseModelList.get(position).getArea());
            holder.textViewList.get(4).setText(addressResponseModelList.get(position).getLandmark());
            holder.ivViewEditDealerAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DealerAddressFragment dealerAddressFragment = new DealerAddressFragment();
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("DEALER_ADDRESS", addressResponseModelList.get(holder.getAdapterPosition()));
                    dealerAddressFragment.setArguments(bundle);
                    ((MainPage) mContext).loadFragment(dealerAddressFragment, true);

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return addressResponseModelList.size();
    }

    public class DealerAddressViewHolder extends RecyclerView.ViewHolder {
        @BindViews({R.id.tvDealerAddressTypeValue, R.id.tvFullAddressValue, R.id.tvFlatNumberValue,
                R.id.tvAreaValue, R.id.tvLandMarkValue})
        List<TextView> textViewList;
        @BindView(R.id.ivViewEditDealerAddress)
        ImageView ivViewEditDealerAddress;

        public DealerAddressViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

}
