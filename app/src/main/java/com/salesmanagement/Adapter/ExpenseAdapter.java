package com.salesmanagement.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Fragment.AddExpenseFragment;
import com.salesmanagement.Fragment.DealerAddressFragment;
import com.salesmanagement.Model.ExpenseResponse;
import com.salesmanagement.R;

import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ExpenseAdapter  extends RecyclerView.Adapter<ExpenseAdapter.ExpenseViewHolder>{
    Context mContext;
    List<ExpenseResponse> expenseResponseList;

    public ExpenseAdapter(Context mContext, List<ExpenseResponse> expenseResponseList) {
        this.mContext = mContext;
        this.expenseResponseList = expenseResponseList;
    }

    @NonNull
    @Override
    public ExpenseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(mContext).inflate(R.layout.recycler_view_expense_item,parent,false);

        return new ExpenseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ExpenseViewHolder holder, int position) {

        //holder.tvExpenseDateValue.setText(""+expenseResponseList.get(position).getDatetime());
        try {
            holder.tvExpenseTypeValue.setText(""+expenseResponseList.get(position).getExpenseType());
            holder.tvExpenseDescValue.setText(""+expenseResponseList.get(position).getDescription());
            holder.tvExpenseAmountValue.setText(""+expenseResponseList.get(position).getExpenseAmt());

            String endTime= String.valueOf(expenseResponseList.get(position).getDatetime());

            long endTimeMillSecond=Long.parseLong(endTime);
            //String strStartTime = android.text.format.DateFormat.format("hh:mm a", new Date(millisecond)).toString();
            String strEndTime = android.text.format.DateFormat.format("yyyy-MM-dd", new Date(endTimeMillSecond)).toString();
            System.out.println(strEndTime);
            holder.tvExpenseDateValue.setText(""+strEndTime);
            //String strEndTime24H = android.text.format.DateFormat.format("HH:mm:ss", new Date(endTimeMillSecond)).toString();

        }catch (Exception e){
            e.printStackTrace();
        }
        holder.ivViewEditExpense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddExpenseFragment addExpenseFragment = new AddExpenseFragment();
                Bundle bundle = new Bundle();
                bundle.putParcelable("EXPENSE", expenseResponseList.get(holder.getAdapterPosition()));
                addExpenseFragment.setArguments(bundle);
                ((MainPage) mContext).loadFragment(addExpenseFragment, true);


            }
        });
        try {

            String url=expenseResponseList.get(position).getExpenseReceipt();
            if(url!=null){
                Glide
                        .with(mContext)
                        .load(url)
                        .centerCrop()
                        .into(holder.ivExpenseRV);
            }
            else {
                holder.ivExpenseRV.setImageResource(R.drawable.no_image_found);
            }



        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return expenseResponseList.size();
    }

    public class   ExpenseViewHolder extends RecyclerView.ViewHolder {
       @BindView(R.id.tvExpenseTypeValue)
       TextView tvExpenseTypeValue;
       @BindView(R.id.tvExpenseDescValue)
       TextView tvExpenseDescValue;
       @BindView(R.id.tvExpenseAmountValue)
       TextView tvExpenseAmountValue;
       @BindView(R.id.tvExpenseDateValue)
       TextView tvExpenseDateValue;
       @BindView(R.id.ivViewEditExpense)
        ImageView ivViewEditExpense;
       @BindView(R.id.ivExpenseRV)
       ImageView ivExpenseRV;
       public ExpenseViewHolder(@NonNull View itemView)
       {
           super(itemView);
           ButterKnife.bind(this,itemView);
       }
   }
}
