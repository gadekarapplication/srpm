package com.salesmanagement.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.salesmanagement.Model.UserOrderResponse;
import com.salesmanagement.R;

import java.util.List;

import butterknife.BindViews;
import butterknife.ButterKnife;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.MyViewHolder> {

    Context context;
    List<UserOrderResponse> userOrderResponseList;

    public OrderAdapter(Context context, List<UserOrderResponse> userOrderResponseList) {

        this.context = context;
        this.userOrderResponseList = userOrderResponseList;

    }

    @NonNull
    @Override
    public OrderAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.orer_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderAdapter.MyViewHolder holder, int position) {

        UserOrderResponse userOrderResponse = userOrderResponseList.get(position);

        try {

            holder.textViews.get(0).setText("" + userOrderResponseList.get(position).getOrderNo());
            holder.textViews.get(1).setText("Rs. " + userOrderResponseList.get(position).getOrderFinalAmount());
            holder.textViews.get(2).setText("" + userOrderResponseList.get(position).getOrderDate());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return userOrderResponseList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindViews({R.id.orderNumber, R.id.orderAmount, R.id.orderDate})
        List<TextView> textViews;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }
}
