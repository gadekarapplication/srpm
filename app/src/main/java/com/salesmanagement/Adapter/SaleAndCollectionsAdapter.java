package com.salesmanagement.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.highsoft.highcharts.common.hichartsclasses.HIButtonOptions;
import com.highsoft.highcharts.common.hichartsclasses.HIChart;
import com.highsoft.highcharts.common.hichartsclasses.HICredits;
import com.highsoft.highcharts.common.hichartsclasses.HIDataLabels;
import com.highsoft.highcharts.common.hichartsclasses.HINavigation;
import com.highsoft.highcharts.common.hichartsclasses.HIOptions;
import com.highsoft.highcharts.common.hichartsclasses.HIPie;
import com.highsoft.highcharts.common.hichartsclasses.HIPlotOptions;
import com.highsoft.highcharts.common.hichartsclasses.HIStyle;
import com.highsoft.highcharts.common.hichartsclasses.HITitle;
import com.highsoft.highcharts.common.hichartsclasses.HITooltip;
import com.highsoft.highcharts.core.HIChartView;
import com.salesmanagement.Database.SRPMDatabase;
import com.salesmanagement.Model.CollectionAndSaleResponse;
import com.salesmanagement.Model.CollectionsReportResponse;
import com.salesmanagement.Model.SalesReportResponse;
import com.salesmanagement.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

public class SaleAndCollectionsAdapter  extends  RecyclerView.Adapter<SaleAndCollectionsAdapter.SaleAndCollectionsViewHolder>{
    Context mContext;
    List<CollectionsReportResponse> collectionsReportResponseList;
    List<SalesReportResponse> salesReportResponseList;
    String typeReport="";
    public SaleAndCollectionsAdapter(Context mContext, List<CollectionsReportResponse> collectionsReportResponseList,String typeReport) {
        this.mContext = mContext;
        this.collectionsReportResponseList = collectionsReportResponseList;
        this.typeReport=typeReport;
    }
    public SaleAndCollectionsAdapter(Context mContext,String typeReport, List<SalesReportResponse> salesReportResponseList) {
        this.mContext = mContext;
        this.salesReportResponseList = salesReportResponseList;
        this.typeReport=typeReport;
    }

    @NonNull
    @Override
    public SaleAndCollectionsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(mContext).inflate(R.layout.recycler_view_collections_report_item,parent,false);
        return new SaleAndCollectionsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SaleAndCollectionsViewHolder holder, int position) {
        if(typeReport.equals("Collections")){
            String totalCollectionsTarget=collectionsReportResponseList.get(position).getCollectionTargetMonthWiseAmount();
            String totalAchievedCollectionsTarget=collectionsReportResponseList.get(position).getCollectionAchivedTargetMonthWiseAmount();
            Integer timePeriodInMonth=collectionsReportResponseList.get(position).getTimePeriod();

            String remainingDays=collectionsReportResponseList.get(position).getRemainingDays();
            String remainingTarget=collectionsReportResponseList.get(position).getRemainingTarget();
            String startDate=  collectionsReportResponseList.get(position).getStartDate();
            if(totalCollectionsTarget!=null&&totalAchievedCollectionsTarget!=null) {
                holder.setToHiChart(totalCollectionsTarget,totalAchievedCollectionsTarget,timePeriodInMonth,remainingDays,remainingTarget,startDate);
            }
        }
        else if(typeReport.equals("Sales")){
            String totalSalesTarget=salesReportResponseList.get(position).getSaleTargetMonthWiseAmount();
            String totalAchievedSalesTarget=salesReportResponseList.get(position).getSaleAchivedTargetMonthWiseAmount();
            Integer timePeriodInMonth=salesReportResponseList.get(position).getTimePeriod();
            String startDate=  salesReportResponseList.get(position).getStartDate();
            String remainingDays=salesReportResponseList.get(position).getRemainingDays();
            String remainingTarget=salesReportResponseList.get(position).getRemainingTarget();
            if(totalSalesTarget!=null&&totalAchievedSalesTarget!=null) {
                holder.setToHiChart(totalSalesTarget,totalAchievedSalesTarget,timePeriodInMonth,remainingDays,remainingTarget,startDate);
            }
        }

    }

    @Override
    public int getItemCount() {
        int listSize = 0;
        if(typeReport.equals("Sales")){
            listSize=   salesReportResponseList.size();
        }
        else if (typeReport.equals("Collections")){
            listSize= collectionsReportResponseList.size();
        }
        return listSize;
    }

    public class SaleAndCollectionsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cv_collections_hichart)
        public CardView cv_collections_hichart;
        @BindView(R.id.hiChartViewcollections)
        public HIChartView hiChartViewcollections;
        @BindViews({R.id.tvTotalcollectionsCount,R.id.tvTotalcollectionsCountAchieved,R.id.tvStartDateMonthcollectionsValue,
                R.id.tvRemainingDaysCollectionsValue,R.id.tvRemainingTargetCollectionsValue})
        List<TextView> textViewList;
        public SaleAndCollectionsViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        public void setToHiChart(String totalVisitorTarget, String totalAchievedVisitorTarget,Integer timePeriodInMonth,String remainingDays,String remainingTarget,String startDate) {

            textViewList.get(0).setText(""+String.valueOf(totalVisitorTarget));
           textViewList.get(1).setText(""+String.valueOf(totalAchievedVisitorTarget));
            cv_collections_hichart.setVisibility(View.VISIBLE);
            //cv_details_visitor.setVisibility(View.VISIBLE);
            hiChartViewcollections.setVisibility(View.VISIBLE);
            HIOptions options = new HIOptions();

            HIChart chart = new HIChart();
            chart.setType("pie");
            chart.setPlotShadow(false);
            options.setChart(chart);

            HITitle title = new HITitle();
           if(typeReport.equals("Sales")){
                title.setText(String.valueOf(totalVisitorTarget) + " Sales Target Within "+timePeriodInMonth +" Month");
            }
            else if(typeReport.equals("Collections")){
                title.setText(String.valueOf(totalVisitorTarget) + " Collections Target Within "+timePeriodInMonth +" Month");
            }
            //title.setText(String.valueOf(totalVisitorTarget) + " Collections Target Period Month");
            options.setTitle(title);

            HITooltip tooltip = new HITooltip();
            tooltip.setPointFormat("<b>{point.percentage:.1f}%</b>");
            options.setTooltip(tooltip);

            ArrayList<HIDataLabels> dataLabelsList = new ArrayList<>();
            HIDataLabels dataLabels = new HIDataLabels();
            dataLabels.setEnabled(true);
            dataLabels.setStyle(new HIStyle());
            dataLabels.getStyle().setFontSize("12px");
            //networkgraph.setDataLabels(dataLabelsList);
            HIPlotOptions plotOptions = new HIPlotOptions();
            plotOptions.setPie(new HIPie());
            plotOptions.getPie().setAllowPointSelect(true);
            plotOptions.getPie().setCursor("pointer");
            dataLabelsList.add(dataLabels);
            plotOptions.getPie().setDataLabels(dataLabelsList);

            // plotOptions.getPie().getDataLabels().;
       /* plotOptions.getPie().getDataLabels().setFormat("<b>{point.name}</b>: {point.percentage:.1f} %"); ;
        plotOptions.getPie().getDataLabels().setStyle(new HICSSObject());
        plotOptions.getPie().getDataLabels().getStyle().setColor("black");*/
            options.setPlotOptions(plotOptions);

            HIPie pie = new HIPie();
            pie.setName("Brands");
            HashMap<String, Object> map1 = new HashMap<>();
            map1.put("name", "Achieved");
            map1.put("y", Float.valueOf(totalAchievedVisitorTarget));

            HashMap<String, Object> map2 = new HashMap<>();
            map2.put("name", "Target");
            map2.put("y", Float.valueOf(totalVisitorTarget));
            map2.put("sliced", true);
            map2.put("selected", true);

            pie.setData(new ArrayList<>(Arrays.asList(map1, map2)));
            //Remove Hicharts.com
            options.setCredits(new HICredits());
            options.getCredits().setEnabled(false);
            //Remove Navigation Button options
            options.setNavigation(new HINavigation());
            options.getNavigation().setButtonOptions(new HIButtonOptions());
            options.getNavigation().getButtonOptions().setEnabled(false);
            ArrayList<String > stringArrayListColors=new ArrayList<>();
            stringArrayListColors.add("#50B432");
            stringArrayListColors.add("#ED561B");
            options.setColors(stringArrayListColors);
            options.setSeries(new ArrayList<>(Collections.singletonList(pie)));
            //pbVisitorFragment.setVisibility(View.GONE);
            hiChartViewcollections.setOptions(options);
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MONTH, 0);
            calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
            Date monthFirstDay = calendar.getTime();
            calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
            Date monthLastDay = calendar.getTime();

            SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
            String startDateStr = df.format(monthFirstDay);
            String endDateStr = df.format(monthLastDay);

            textViewList.get(2).setText(startDate);
            textViewList.get(3).setText(String.valueOf(remainingDays));
            textViewList.get(4).setText(String.valueOf(remainingTarget));


        }

    }


}
