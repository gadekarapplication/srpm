package com.salesmanagement.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Fragment.ChequeMasterFragment;
import com.salesmanagement.Fragment.FarmerVisiterListFragment;
import com.salesmanagement.Model.ChequeResponseModel;
import com.salesmanagement.R;

import java.util.List;

import butterknife.ButterKnife;

public class ChequeListAdapter extends RecyclerView.Adapter<ChequeListAdapter.MyViewHolder> {

    public static String formStatus = "New", farmerName = "", mobNo, address = "", areaCrop = "", crop = "";

    public static int farmer_id;
    Context context;
    List<ChequeResponseModel> chequeList;

    public ChequeListAdapter(Context context, List<ChequeResponseModel> chequeList) {
        this.context = context;
        this.chequeList = chequeList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cheque_list_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.tv_dealer_name.setText(chequeList.get(position).getDealerFullName());

        holder.tv_emp_no.setText(chequeList.get(position).getEmployeeName());

        holder.tv_cheque_no.setText(chequeList.get(position).getChequeNo());

        holder.tv_bank_name.setText(chequeList.get(position).getBankName());

        holder.tv_account_no.setText(chequeList.get(position).getAccountNo());

        holder.tv_amount.setText("" + chequeList.get(position).getAmount());

        holder.tv_status.setText(chequeList.get(position).getStatus());


        holder.iv_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                ChequeMasterFragment chequeMasterFragment = new ChequeMasterFragment();
                Bundle bundle = new Bundle();

                bundle.putString("formStatus", "update");
                bundle.putInt("chequeId", chequeList.get(position).getChequeDetailId());

                chequeMasterFragment.setArguments(bundle);
                ((MainPage) context).loadFragment(chequeMasterFragment, true);


            }
        });

        holder.iv_visit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((MainPage) context).loadFragment(new FarmerVisiterListFragment(), true);
            }
        });


    }

    @Override
    public int getItemCount() {
        return chequeList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tv_dealer_name, tv_emp_no, tv_cheque_no, tv_account_no, tv_bank_name, tv_amount, tv_status;

        private ImageView iv_update, iv_visit;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            tv_dealer_name = itemView.findViewById(R.id.tv_dealer_name);
            tv_emp_no = itemView.findViewById(R.id.tv_emp_no);
            tv_cheque_no = itemView.findViewById(R.id.tv_cheque_no);
            tv_bank_name = itemView.findViewById(R.id.tv_bank_name);
            tv_account_no = itemView.findViewById(R.id.tv_account_no);
            tv_amount = itemView.findViewById(R.id.tv_amount);
            tv_status = itemView.findViewById(R.id.tv_status);
            iv_update = itemView.findViewById(R.id.iv_update);
            iv_visit = itemView.findViewById(R.id.iv_visit);
        }
    }
}
