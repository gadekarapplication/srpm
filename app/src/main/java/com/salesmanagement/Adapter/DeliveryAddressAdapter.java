package com.salesmanagement.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.salesmanagement.Activity.MainPage;
import com.salesmanagement.Fragment.DealerAddressFragment;
import com.salesmanagement.Fragment.MyCart;
import com.salesmanagement.Fragment.PlaceOrderDetails;
import com.salesmanagement.Model.DealerAddressResponseModel;
import com.salesmanagement.R;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

public class DeliveryAddressAdapter extends RecyclerView.Adapter<DeliveryAddressAdapter.DeliveryAddressViewHolder> {
    public int mSelectedItem = -1;
    Context mContext;
    List<DealerAddressResponseModel> addressResponseModelList;
    String companyId,dealerId;
    public DeliveryAddressAdapter(Context mContext, List<DealerAddressResponseModel> addressResponseModelList,String companyId,String dealerId) {
        this.mContext = mContext;
        this.addressResponseModelList = addressResponseModelList;
        this.companyId=companyId;
        this.dealerId=dealerId;
    }

    @NonNull
    @Override
    public DeliveryAddressViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(mContext).inflate(R.layout.recycler_view_delivery_address_selection,parent,false);
        return new DeliveryAddressViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DeliveryAddressViewHolder holder, int position) {
        try {
            if (mSelectedItem == -1) {
                holder.textViewList.get(5).setVisibility(View.GONE);
            } else {
                if (mSelectedItem == position) {
                    holder.textViewList.get(5).setVisibility(View.VISIBLE);
                } else {
                    holder.textViewList.get(5).setVisibility(View.GONE);
                }
            }
            holder.textViewList.get(0).setText(addressResponseModelList.get(position).getAddressType());
            holder.textViewList.get(1).setText(addressResponseModelList.get(position).getFullAddress());
            holder.textViewList.get(2).setText(addressResponseModelList.get(position).getFlatNo());
            holder.textViewList.get(3).setText(addressResponseModelList.get(position).getArea());
            holder.textViewList.get(4).setText(addressResponseModelList.get(position).getLandmark());
            holder.ivViewEditDealerAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DealerAddressFragment dealerAddressFragment = new DealerAddressFragment();
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("DEALER_ADDRESS", addressResponseModelList.get(holder.getAdapterPosition()));
                    dealerAddressFragment.setArguments(bundle);
                    ((MainPage) mContext).loadFragment(dealerAddressFragment, true);

                }
            });
            holder.rbAddress.setChecked(position==mSelectedItem);

            holder.rbAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   // mSelectedItem = position;

                    if (position == mSelectedItem) {
                        mSelectedItem = -1;
                        holder.rbAddress.setChecked(false);
                        notifyDataSetChanged();
                        holder.textViewList.get(5).setVisibility(View.VISIBLE);
                    } else {
                        mSelectedItem = position;
                        holder.rbAddress.setChecked(true);
                        holder.textViewList.get(5).setVisibility(View.GONE);
                        notifyDataSetChanged();
                    }


                }
            });
            holder.textViewList.get(5).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PlaceOrderDetails orderDetails = new PlaceOrderDetails();
                    Bundle bundle = new Bundle();
                    bundle.putString("dealerId", "" + dealerId);
                    bundle.putString("companyId", "" + companyId);
                    bundle.putString("addressId",""+String.valueOf(addressResponseModelList.get(position).getDealerAddressId()));
                    orderDetails.setArguments(bundle);
                    ((MainPage) mContext).loadFragment(orderDetails, true);
                    Log.d("AddressId:","get -->" +addressResponseModelList.get(position).getDealerAddressId());
                }
            });
            holder.ivViewEditDealerAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DealerAddressFragment dealerAddressFragment = new DealerAddressFragment();
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("DEALER_ADDRESS", addressResponseModelList.get(holder.getAdapterPosition()));
                    dealerAddressFragment.setArguments(bundle);
                    ((MainPage) mContext).loadFragment(dealerAddressFragment, true);

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return addressResponseModelList.size();
    }

    public class DeliveryAddressViewHolder extends RecyclerView.ViewHolder {
        @BindViews({R.id.tvDealerAddressTypeValue, R.id.tvFullAddressValue, R.id.tvFlatNumberValue,
                R.id.tvAreaValue, R.id.tvLandMarkValue,R.id.tvDeliveryToAddress})
        List<TextView> textViewList;
        @BindView(R.id.ivViewEditDealerAddress)
        ImageView ivViewEditDealerAddress;
        @BindView(R.id.rbAddress)
        RadioButton rbAddress;
        public DeliveryAddressViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
